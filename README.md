# INSTALLATION #

## OSX ##

First, you will need to install Apple's developer tools (Xcode) and NumPy, a core package for scientific computing with Python. On recent OSX installations it can be done with two simple shell commands

~~~
xcode-select --install
sudo easy_install numpy
~~~

next simply run brickworx with

~~~
./brickworx.sh
~~~

If run for the first time the script will download and compile cctbx sources from the [official web page](http://cctbx.sourceforge.net/) (100MG). Otherwise it will simply start the program.

## LINUX ##

First, you will need to install

 1. python >= 2.6 (NOT Python3)

 2. python-dev (header files and a static library for Python)

 3. g++ >=4.4

 3. numpy >= 1.5 (core package for scientific computing with Python)

For example on recent Debian or Ubuntu you can simply type:
~~~
sudo aptitude install g++ python python-dev python-numpy
~~~
or on Centos 
~~~
sudo yum install gcc-c++ python-devel numpy
~~~
next run a command 

~~~
./brickworx.sh
~~~
If run for the first time the script will download and compile cctbx sources from the [official web page](http://cctbx.sourceforge.net/)  (100MG). Otherwise it will simply start the program. If you face any problems with the installation contact me at gchojnowski@gmail.com


# QUICK START #

 1. get test data from [PDB_REDO](http://www.cmbi.ru.nl/pdb_redo/). Each entry page has a link "Conservatively optimised structure" at the bottom.
Click MTZ to download a map. e.g. [1ehz](http://www.cmbi.ru.nl/pdb_redo/eh/1ehz/1ehz_besttls.mtz)

 2. list MTZ column labels

~~~
./brickworx.sh --mtzin=1ehz_besttls.mtz 
 ==> Reading input map
 ==> Available MAP labels:
         FC,PHIC
         FC_ALL,PHIC_ALL
         FWT,PHWT
         DELFWT,PHDELWT
         FC_ALL_LS,PHIC_ALL_LS
~~~

 3. run program with a command

~~~
./brickworx.sh --mtzin=1ehz_besttls.mtz --labin=FWT,PHWT --rna
~~~

