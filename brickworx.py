#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================

import os, sys, re

from brickworx_config import *
sys.path.append(os.path.join(ROOT, "./lib/"))

import string
import urllib2, urllib
from optparse import OptionParser
import random

# ----------------- LOCAL DEPS ------------------------------------------------

from fittest import Fittest
from maps import dividenconquer, get_mask

# ------------------- CCTBX ---------------------------------------------------

import iotbx
from iotbx import reflection_file_reader
from iotbx import reflection_file_utils
from mmtbx.maps import utils
from iotbx import file_reader
import iotbx.map_tools

# -----------------------------------------------------------------------------

def init_maps(mtz_fname, labin, debug=False):

    print " ==> Reading input map"

    reflection_file = reflection_file_reader.any_reflection_file( file_name = mtz_fname )
    symm = reflection_file.file_content().crystals()[0].crystal_symmetry()

    xray_data_server =  reflection_file_utils.reflection_file_server(
                                            crystal_symmetry = symm,
                                            force_symmetry = True,
                                            reflection_files=[reflection_file])


    all_labels = []

    for label in utils.get_map_coeff_labels(xray_data_server,
                                            exclude_anomalous=False,
                                            exclude_fmodel=False,
                                            keep_array_labels=True):
        if isinstance(label, list):
            if len(label)>3: continue
            all_labels.append( ",".join(ss.split(',')[0] for ss in label) )
        else:
            all_labels.append( label )


    if len(all_labels)<2:
        labin=all_labels[-1]
        print " ==> It seems that there is only one map available. Using %s" % labin

    try:
        labin = labin.split(',')

        f = xray_data_server.get_xray_data(file_name = None, \
                                           labels = labin[0], \
                                           ignore_all_zeros = False, \
                                           parameter_scope="").as_non_anomalous_array().resolution_filter(d_min=2.0)

        phi = xray_data_server.get_xray_data(file_name = None, \
                                           labels = labin[1], \
                                           ignore_all_zeros = False, \
                                           parameter_scope="")

        if len(labin)>2:
            fom = xray_data_server.get_xray_data(file_name = None, \
                                           labels = labin[2], \
                                           ignore_all_zeros = False, \
                                           parameter_scope="")
        else:
            fom = None


        map_coeffs = iotbx.map_tools.combine_f_phi_and_fom(f=f, phi=phi, fom=fom)

    except:
        print " ==> Available MAP labels:"
        for label in all_labels:
            if not isinstance(label, list): print "         %s" % label
        exit(0)

    map_coeffs.set_info(str(map_coeffs.info()).split('/')[-1])
    map_coeffs.show_summary(prefix="     --> ")

    target_map = map_coeffs.fft_map(resolution_factor = 1/3.0)
    target_map.apply_sigma_scaling()

    if 0: target_map.as_ccp4_map("xyz.map")

    target_miller_set = map_coeffs.miller_set(anomalous_flag = False,\
                                                        indices = map_coeffs.indices())

    assert reflection_file.file_content().n_symmetry_matrices() > 0

    return target_map, target_miller_set, symm


# -----------------------------------------------------------------------------

def parse_args():
    """setup program options parsing"""
    parser = OptionParser()


    parser.add_option("-o", "--pdbout_model", action="store", \
                            dest="model_ofname", type="string", metavar="FILENAME", \
                  help="Final model output filename", default=None)

    parser.add_option("--pdbout_phosphates", action="store", \
                            dest="po4_ofname", type="string", metavar="FILENAME", \
                  help="write detected phosphates to a file", default=None)

    parser.add_option("--pdbin", action="store", dest="pdb_ifname", type="string", metavar="FILENAME", \
                  help="input PDB file with phosphorus atoms", default=None)

    parser.add_option("--labin", action="store", dest="labin", type="string", metavar="F,PHI,[FOM]", \
                  help="MTZ file column names", default=None)

    parser.add_option("--mtzin", action="store", dest="mtz_ifname", type="string", metavar="FILENAME", \
                  help="input map in MTZ format", default=None)

    parser.add_option("--stems", action="store_true", dest="stems", default=False, \
                  help="build dsRNA stems only (fast mode)")

    parser.add_option("--dna", action="store_true", dest="dna", default=False, \
                  help="build dsDNA-only model")

    parser.add_option("--rna", action="store_true", dest="rna", default=False, \
                  help="build RNA stems and loops")

    parser.add_option("--debug", action="store_true", dest="debug", default=False, \
                  help="debugging output...")

    (options, _args)  = parser.parse_args()
    return (parser, options)


def main():

    (parser, options) = parse_args()
    if not options.mtz_ifname:
        parser.print_help()
        exit(1)

    target_map, target_miller_set, symm = init_maps(os.path.expanduser(options.mtz_ifname), \
                                                            options.labin)


    if not [options.rna or options.stems, options.dna].count(True) == 1:
        print "Please specify target molecule type RNA/DNA"
        exit(1)

    fittest = Fittest(target_map, target_miller_set, symm)

    target_po4_patterns = []

    if options.pdb_ifname:
        if not fittest.get_phosphates_from_file(pdb_ifname=os.path.expanduser(options.pdb_ifname), \
                                             verbose=False):
            print " ==> Input PDB file broken, exitting..."
            exit(1)
        target_po4_patterns.append( fittest.knuspr_structure )
    else:
        # detect phosphate groups
        for mask_verts in dividenconquer( target_map, symm ):
            if not mask_verts:
                buffer_thickness = 20.0
                mask = None
            else:
                buffer_thickness = 0.0
                mask = get_mask( target_map, symm, mask_verts )

            fittest.run_svm_knuspr(debug = options.debug, \
                                    mask = mask, \
                                    max_buffer_thickness = buffer_thickness, \
                                    verbose = False )

            target_po4_patterns.append( fittest.knuspr_structure )

        fittest.merge_knuspr_results(target_po4_patterns)


    if options.po4_ofname:
        print " ==> Writing detected phosphates to: %s" % options.po4_ofname
        fittest.peaks_as_pdb.write_pdb_file(file_name=os.path.expanduser(options.po4_ofname), \
                                                crystal_symmetry=symm)

    results_hierarchy = iotbx.pdb.hierarchy.root()

    if options.stems or options.rna:
        stem_models = ["./data/model_stems/RNA_4bps.pdb", \
                       "./data/model_stems/RNA_3bps.pdb"]
    else:
        stem_models = ["./data/model_stems/DNA_4bps.pdb", \
                       "./data/model_stems/DNA_3bps.pdb"]

    stem_models = [os.path.join(ROOT, x) for x in stem_models]


    #print " *** (1/4)"
    for po4_pattern in target_po4_patterns:
        for idx, model_ofname in enumerate(stem_models):
            fittest.iterate_matches(input_knuspr_structure = po4_pattern, \
                                    calc_cc = True, \
                                    pattern_fname = model_ofname, \
                                    data_path = None, \
                                    verbose = False, \
                                    np = 1, \
                                    check_sequence = False, \
                                    debug = False, \
                                    ss_restrains = True, \
                                    results_hierarchy = results_hierarchy, \
                                    stats = False, \
                                    build_dna=options.dna)
        #print " *** (%i/4)" % (idx+2)

    stems_no = stemsnumber = len(results_hierarchy.models())

    if not (options.stems or options.dna) and stems_no:
        results_hierarchy = fittest.add_loops_simple(results_hierarchy)

    results_hierarchy = fittest.postprocess_matches(results_hierarchy, stems_no)

    print " ==> Found %i residues and %i chain segments" % (results_hierarchy.overall_counts().n_residues, \
                                                           results_hierarchy.overall_counts().n_chains)

    if not options.model_ofname:
        model_ofname = options.mtz_ifname+"_brickworx_model.pdb"
    else:
        model_ofname = options.model_ofname

    if model_ofname:
        print " ==> Writing output model to: %s" % model_ofname

        # remove BREAK keywords, Coot doesn't like them
        for rg in results_hierarchy.residue_groups():
            rg.link_to_previous = True


        results_hierarchy.write_pdb_file(os.path.expanduser(model_ofname), \
                                            crystal_symmetry = fittest.symm)

    print " ==> Normal termination of the program"


if __name__ == "__main__":
    random.seed(1)
    main()

