#!/bin/bash

ABSPATH=$(cd "$(dirname "$0")"; pwd)
export CLIBD_MON=$ABSPATH/data/mon_lib/
export MMTBX_CCP4_MONOMER_LIB=$ABSPATH/data/mon_lib/

if [ -f $ABSPATH/virtualenv/cctbx_build/setpaths_all.sh ] ; then

    # use local installation
    . $ABSPATH/virtualenv/cctbx_build/setpaths_all.sh

else

    # first, get optional params
    SCONS_N=4
    while [[ $# > 1 ]]
    do
        key="$1"
        shift

        if [ $key == "--scons_j" ]; then
            SCONS_N="$1"
            shift
        fi
    done

    echo " ==> Requested number of CPUs: "$SCONS_N


    mkdir $ABSPATH/virtualenv

    # try to compile cctbx
    ./_install_cctbx -j $SCONS_N

    if [ $? -gt 0 ]; then
        echo "CCTBX installation failed..."
        rm -r $ABSPATH/virtualenv
        exit 1;
    fi

    . $ABSPATH/virtualenv/cctbx_build/setpaths_all.sh

    echo "Installation complete"
fi


if [ ! -f $ABSPATH/lib/smatch_cpp/smatch.so ] ; then

    # try to compile the python modeule smatch.so
    cd $ABSPATH
    libtbx.scons
     if [ $? -gt 0 ]; then
        echo "Failed to compile brickworx modules..."
        libtbx.scons -c
        exit 1;
    fi

    # an ugly patch for paths in cctbx libs
    if hash install_name_tool 2>/dev/null; then
        find . -name '*.so' | xargs  -IXX install_name_tool -change lib/libboost_python.dylib `pwd`/cctbx_build/lib/libboost_python.dylib XX
    fi
    cd $ABSPATH
fi


libtbx.python $ABSPATH/brickworx.py $@

