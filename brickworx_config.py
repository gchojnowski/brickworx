# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================


import os

ROOT = os.path.dirname(os.path.realpath(__file__))

RNADB_URL = "http://iimcb.genesilico.pl/rnabricks/"
LOCAL_REDO_URL = "http://rnabricks/site_media/data/custom/dataset/"

CLUSTERS_URL = "%s/api/list_clusters/" % RNADB_URL
FRAGS_URL = "%s/site_media/data/fragments/" % RNADB_URL

