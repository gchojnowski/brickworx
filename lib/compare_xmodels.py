#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================

import sys, os
import re
import numpy as np
import gzip
import urllib2

TMP_DIR = './tmp/'

# ------------------- CCTBX --------------------------------------------------

import iotbx
from iotbx import pdb
from cctbx import crystal
from mmtbx.maps import utils

from cctbx.array_family import flex
from iotbx.pdb import input as pdb_input
from iotbx.pdb import common_residue_names_get_class
from scitbx import matrix

# --------------------CCTBX --------------------------------------------------

PYRIMIDINE_ATOMS = ["C1'", 'C2', 'C4', 'C5', 'C6', 'N1', 'N3',]
PURINE_ATOMS = ["C1'", 'C2', 'C4', 'C5', 'C6', 'C8', 'N1', 'N3', 'N7', 'N9']

class CompareXModels:

    def __get_model(self, fname, prune_waters=False):
        input_file = open(fname)
        input_file_content = input_file.read()
        input_pdb = pdb_input(source_info = None, lines = input_file_content)
        input_hierarchy = input_pdb.construct_hierarchy()
        symm = input_pdb.crystal_symmetry()
        input_file.close()


        if prune_waters:
            sel_cache = input_hierarchy.atom_selection_cache()
            isel = sel_cache.iselection
            input_hierarchy = input_hierarchy.select(isel("not water"))

        return input_hierarchy, symm


    # -------------------------------------------------------------------------

    # fpair_sub.c: residue_ident
    # identify uncommon residues in a way similar to RNAVIEW
    # identifying a residue as follows:
    #  R-base  Y-base  amino-acid, others [default]
    # (puRine)
    #   +1        0        -1        -2   [default]
    #

    def _get_atom_by_name(self, resi, name):
        for atom in resi.atoms():
            if atom.name.strip()==name:
                return atom
        return None



    def _get_base_type(self, resi, dcrt=2.0, dcrt2=3.0):

        base_id = -2

        N9 = self._get_atom_by_name(resi, 'N9')
        N1 = self._get_atom_by_name(resi, 'N1')
        C2 = self._get_atom_by_name(resi, 'C2')
        C6 = self._get_atom_by_name(resi, 'C6')

        if N1 and C2 and C6:
            d1 = np.linalg.norm(np.array(N1.xyz, float) - np.array(C2.xyz, float))
            d2 = np.linalg.norm(np.array(N1.xyz, float) - np.array(C6.xyz, float))
            d3 = np.linalg.norm(np.array(C2.xyz, float) - np.array(C6.xyz, float))

            if d1<=dcrt and d2<=dcrt and d3<=dcrt2:
                base_id = 0
                if N9:
                    d3 = np.linalg.norm(np.array(N1.xyz, float) - np.array(N9.xyz, float))
                    if d3 >= 3.5 and d3 <= 4.5:
                        base_id = 1
            return base_id

        # try to detect an amino-acid
        CA = self._get_atom_by_name(resi, 'CA')
        C  = self._get_atom_by_name(resi, 'C')
        if not C:
            C  = self._get_atom_by_name(resi, 'N')
        if C and CA:
            d = np.linalg.norm(np.array(CA.xyz, float) - np.array(C.xyz, float))
            if d <= dcrt:
                base_id = -1
            return base_id
        return base_id


    # ------------------------------------------------------------------------

    def __init__(self, target_fname=None, target=None, verbose=False):
        """
            allows for an empty init to enable the use of some
            utility functions 
        """

        if [target_fname, target].count(None)<2:

            assert [target_fname, target].count(None)==1

            if target_fname:
                self.target_basename = os.path.splitext(os.path.basename(target_fname))[0]
                self.target_hierarchy, self.target_symm = self.__get_model(target_fname)
                if verbose: self.target_hierarchy.write_pdb_file(file_name=self.target_basename+"_asu.pdb",\
                                                        crystal_symmetry=self.target_symm)
            else:
                self.target_hierarchy, self.target_symm = target
                self.target_basename = 'target'

            # count nuc bases
            self.nucs_count = 0
            for resi in self.target_hierarchy.atom_groups():
                if self._get_base_type(resi)<0: continue
                self.nucs_count += 1



    # ------------------------------------------------------------------------
    # ------------------------------------------------------------------------


    def _sel_coords(self, resi, inp_names=None):
        # TODO this will fail if aa are present
        if inp_names:
            names=inp_names
        elif self._get_base_type(resi)>0:
            names = PURINE_ATOMS
        else: names = PYRIMIDINE_ATOMS

        sites = flex.vec3_double()
        sites.resize(len(names))
        for atom in resi.atoms():
            try:
                try:
                    idx = names.index(atom.name.strip())
                except:
                    idx = names.index(atom.name.replace("*", "'").strip())

                sites[idx] = atom.xyz
            except:
                pass

        return sites

    # ------------------------------------------------------------------------

    def _match_residues(self, resi_i, \
                                resi_j, \
                                rt_mx_ji, \
                                uc, \
                                P_dist=1.5, \
                                C1p_dist=1.0, \
                                P_only=False, \
                                extended=False):

        # check P-distances
        try:
            xyz_i = np.array(self._get_atom_by_name(resi_i, 'P').xyz)
            xyz_j = np.array(uc.orthogonalize(rt_mx_ji * uc.fractionalize(self._get_atom_by_name(resi_j, 'P').xyz)))
        except:
            return False

        #assert uc.distance(uc.fractionalize(xyz_i), rt_mx_ji*uc.fractionalize(self._get_atom_by_name(resi_j, 'P').xyz))<1.5


        if np.sqrt(np.add.reduce((xyz_i-xyz_j)*(xyz_i-xyz_j)))>P_dist: return False

        if not P_only:
            # check C1'-distances
            xyz_i = self._get_atom_by_name(resi_i, "C1'")
            if not xyz_i: xyz_i = self._get_atom_by_name(resi_i, "C1*")

            xyz_j = self._get_atom_by_name(resi_j, "C1'")
            if not xyz_j: xyz_j = self._get_atom_by_name(resi_j, "C1*")

            xyz_i = np.array(xyz_i.xyz)
            xyz_j = np.array(uc.orthogonalize(rt_mx_ji * uc.fractionalize(xyz_j.xyz)))


            if np.sqrt(np.add.reduce((xyz_i-xyz_j)*(xyz_i-xyz_j)))>C1p_dist: return False


            if extended:
                orth_mx = uc.orthogonalization_matrix()
                rtmx_cart = rt_mx_ji.as_rational().as_float() * uc.fractionalization_matrix()

                # check if the two are of the same type
                if (self._get_base_type(resi_i) != self._get_base_type(resi_j)) or self._get_base_type(resi_i)<0: return False

                x = self._sel_coords(resi_i)
                y = orth_mx * (rtmx_cart * self._sel_coords(resi_j))
                #print list(x), list(y)#(x-y).dot())#flex.mean((x-y).dot())
                if flex.mean((x-y).dot())>1.0: return False

        return True

    # ------------------------------------------------------------------------

    def _check_resi_pair(self, pair, \
                                complete_asu_mappings, \
                                P_dist=1.5, \
                                C1p_dist=1.0, \
                                P_only=False, \
                                extended=False, \
                                verbose=False):

        # there can be multiple pairs (with different rtmx)
        # for a selected atom within a selected distance - j_sym is an index

        uc = complete_asu_mappings.unit_cell()

        resi_i = self.complete_pdb_hierarchy.atoms()[pair.i_seq].parent().parent()
        resi_j = self.complete_pdb_hierarchy.atoms()[pair.j_seq].parent().parent()
        # check models:
        if resi_i.parent().parent().id == resi_j.parent().parent().id: return None

        #  check if resi_i belongs to the first model (target structure)
        if resi_i.parent().parent().id > resi_j.parent().parent().id: resi_i, resi_j = resi_j, resi_i

        if resi_i in self.mapped_residues: return None


        rt_mx_i = complete_asu_mappings.get_rt_mx_i(pair)
        rt_mx_j = complete_asu_mappings.get_rt_mx_j(pair)
        rt_mx_ji = rt_mx_i.inverse().multiply(rt_mx_j)


        if not self._match_residues(resi_i, resi_j, rt_mx_ji, uc, P_dist, C1p_dist, P_only, extended):
            return None


        if verbose:
            orth_mx = uc.orthogonalization_matrix()
            rtmx_cart = rt_mx_ji.as_rational().as_float() * uc.fractionalization_matrix()

            resi = resi_j.detached_copy()
            resi.atoms().set_xyz(new_xyz=orth_mx * (rtmx_cart * resi_j.atoms().extract_xyz()))

            try:
                chain = self.output_hierarchy.models()[0].chains()[0]
            except:
                self.output_hierarchy.models()[0].append_chain(iotbx.pdb.hierarchy.chain(id="A"))
                chain = self.output_hierarchy.models()[0].chains()[0]

            chain.append_residue_group(resi)

        self.mapped_residues.append(resi_i)

        # target resseq and chain_id
        return  (resi_i.resseq, resi_i.parent().id)

    # ---------------------------------------------------------------------


    def compare_nuc_positions(self, pattern_fname=None, \
                                    pattern=None, \
                                    P_dist=1.5, \
                                    C1p_dist=1.0, \
                                    extended=True, \
                                    verbose=True, \
                                    P_only=False):

        assert [pattern_fname, pattern].count(None)==1
        if pattern_fname:
            pattern_basename = os.path.splitext(os.path.basename(pattern_fname))[0]
            pattern_hierarchy, pattern_symm = self.__get_model(pattern_fname)
        else:
            pattern_hierarchy, pattern_symm = pattern
            pattern_basename = 'pattern'

        if pattern_symm == None: pattern_symm = self.target_symm
        assert self.target_symm.is_similar_symmetry( pattern_symm )

        self.output_hierarchy = iotbx.pdb.hierarchy.root()
        self.output_hierarchy.append_model(iotbx.pdb.hierarchy.model(id="0"))

        self.resi_pairs = set([])
        self.mapped_residues = []

        for midx, model in enumerate(pattern_hierarchy.models()):

            self.complete_pdb_hierarchy = iotbx.pdb.hierarchy.root()
            self.complete_pdb_hierarchy.append_model(iotbx.pdb.hierarchy.model(id="0"))
            self.complete_pdb_hierarchy.append_model(iotbx.pdb.hierarchy.model(id="1"))

            for chain in self.target_hierarchy.chains():
                self.complete_pdb_hierarchy.models()[0].append_chain(chain.detached_copy())

            for chain in model.chains():
                self.complete_pdb_hierarchy.models()[1].append_chain(chain.detached_copy())

            sel_cache = self.complete_pdb_hierarchy.atom_selection_cache()
            assert sel_cache.n_seq == self.complete_pdb_hierarchy.atoms_size()
            isel = sel_cache.iselection

            complete_xray_structure = self.complete_pdb_hierarchy.extract_xray_structure(crystal_symmetry=pattern_symm)

            assert len(list(self.complete_pdb_hierarchy.atoms())) == len(complete_xray_structure.scatterers())

            complete_asu_mappings = complete_xray_structure.asu_mappings(buffer_thickness=0.0)

            pair_generator = crystal.neighbors_simple_pair_generator(complete_asu_mappings, distance_cutoff=1.5)

            self.mapped_residues = []

            for pair in pair_generator:
                res = self._check_resi_pair(pair, complete_asu_mappings, \
                                            P_dist, C1p_dist, P_only=P_only, \
                                            extended=extended, verbose=verbose)

                if res:
                    self.resi_pairs.add( res )



        if verbose:
            for idx, pair in enumerate(self.resi_pairs):
                print idx, pair
            #self.output_hierarchy.write_pdb_file(TMP_DIR+"%s.mapped.pdb"%pattern_basename, crystal_symmetry=self.target_symm)

        return pattern_hierarchy.overall_counts().n_residues


def test():
    if len(sys.argv)<3: exit(1)
    xcmp = CompareXModels(sys.argv[1])
    n_res_pattern = xcmp.compare_nuc_positions(sys.argv[2], P_only=True, extended=False, verbose=False)

    n_res_target = xcmp.nucs_count
    n_res_overlap = len(xcmp.resi_pairs)
    print "#n_target(coverage) n_res_overlap(coverage) n_res_pattern(coverage)"
    print "%d (%.2f%%) %d (100%%) %d (%.2f%%)" % (n_res_target, \
                                    100*float(n_res_overlap)/float(n_res_target),  \
                                    n_res_overlap, \
                                    n_res_pattern, \
                                    100*float(n_res_overlap)/float(n_res_pattern))
if __name__=="__main__":
    test()








