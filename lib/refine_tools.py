#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================

import re
from mmtbx.refinement.real_space import individual_sites
import mmtbx.command_line.real_space_refine as rs
import mmtbx.monomer_library.pdb_interpretation
from mmtbx import monomer_library
import time
from cctbx.array_family import flex
import cctbx, scitbx

from BasePairsConst import base_pairs_families

# -----------------------------------------------------------------------------


class refinery:

    # -------------------------------------------------------------------------

    @staticmethod
    def create_hbond_proxies(pdb_hierarchy, basepairs, uc=None):
        import numpy as np
        from mmtbx.geometry_restraints import hbond
        from mmtbx.secondary_structure import utils as ss_utils


        build_proxies = hbond.build_simple_hbond_proxies()
        #build_proxies = hbond.build_lennard_jones_proxies
        # Fabiola et al. (2002) Protein Sci. 11:1415-23
        # http://www.ncbi.nlm.nih.gov/pubmed/12021440
        #build_proxies = hbond.build_implicit_hbond_proxies


        selection_cache = pdb_hierarchy.atom_selection_cache()
        atoms = pdb_hierarchy.atoms()

        for bp in basepairs:

            resname1 = ss_utils.get_residue_name_from_selection(
                resi_sele="resseq %i"%bp[0],
                selection_cache=selection_cache,
                atoms=atoms).upper()

            resname2 = ss_utils.get_residue_name_from_selection(
                resi_sele="resseq %i"%bp[1],
                selection_cache=selection_cache,
                atoms=atoms).upper()

            bpname = "%s%s%s"%(resname1.strip(), resname2.strip(), bp[2])
            inv_bpname = bpname[1]+bpname[0]+bpname[3]+bpname[2]+bpname[4:]

            atom_pairs = None
            distance_values = None
            for item in base_pairs_families:
                if item[0] == bpname or item[0] == inv_bpname:
                    atom_pairs = item[1]
                    break

            if not atom_pairs: continue

            distance_values = []
            for item in atom_pairs:
                distance_values.append((3.0 if item[0][0]==item[1][0]=='N' else 2.8, None, None))

            assert (len(atom_pairs) == len(distance_values))
            n_proxies = 0
            hbond_counts = [1]*atoms.size()
            use_db_values=True
            sigma = 0.05
            slack = 0.0
            remove_outliers = False

            for i, (name1, name2) in enumerate(atom_pairs):
                # atom name order in BasePairConst is broken...
                try:
                    sele1 = "name %s and resseq %i" % (name1, bp[0])
                    sele2 = "name %s and resseq %i" % (name2, bp[1])
                    (i_seq,j_seq) = ss_utils.hydrogen_bond_from_selection_pair(sele1, sele2, selection_cache)
                except:
                    sele1 = """name %s and resseq %i""" % (name2, bp[0])
                    sele2 = """name %s and resseq %i""" % (name1, bp[1])
                    try:
                        (i_seq,j_seq) = ss_utils.hydrogen_bond_from_selection_pair(sele1, sele2, selection_cache)
                    except:
                        continue

                if (hbond_counts[i_seq] > 2) or (hbond_counts[j_seq] > 2):
                    #print >> log, "One or more atoms already bonded:"
                    #print >> log, "  %s" % atoms[i_seq].fetch_labels().id_str()
                    #print >> log, "  %s" % atoms[j_seq].fetch_labels().id_str()
                    continue
                hbond_counts[i_seq] += 1
                hbond_counts[j_seq] += 1
                if (use_db_values) :
                    bp_distance_cut = -1
                    if (distance_values[i][0] is not None) :
                        bp_distance = float(distance_values[i][0])
                    else:
                        bp_distance = distance_ideal
                    if (distance_values[i][1] is not None) :
                        bp_sigma = float(distance_values[i][1])
                    else:
                        bp_sigma = sigma
                    if (distance_values[i][2] is not None) :
                        bp_slack = float(distance_values[i][2])
                    else:
                        bp_slack = slack
                else:
                    bp_distance = distance_ideal
                    bp_distance_cut = distance_cut
                    bp_sigma = sigma
                    bp_slack = slack

                if (remove_outliers) and (distance_cut > 0) :
                    dist = atoms[i_seq].distance(atoms[j_seq])
                    if (dist > distance_cut) :
                        continue

                build_proxies.add_proxy(
                    i_seqs=[i_seq,j_seq],
                    distance_ideal=bp_distance,
                    distance_cut=bp_distance_cut,
                    weight=1/(bp_sigma ** 2),
                    slack=bp_slack)
                build_proxies.add_nonbonded_exclusion(i_seq, j_seq)
                n_proxies += 1
        return build_proxies.proxies

    # -------------------------------------------------------------------------


    def __init__(self, pdb_hierarchy, \
                    inp_map, \
                    symm, \
                    basepairs=None, \
                    d_min=3.0, \
                    max_iteration=150, \
                    optimize_weights=False):

        self.optimize_weights = optimize_weights

        xrs_poor = pdb_hierarchy.extract_xray_structure()
        selection = flex.bool(xrs_poor.scatterers().size(), True)


        processed_pdb_file = monomer_library.pdb_interpretation.process(
                        mon_lib_srv = monomer_library.server.server(),
                        ener_lib    = monomer_library.server.ener_lib(),
                        raw_records = pdb_hierarchy.as_pdb_string(crystal_symmetry=symm),
                        strict_conflict_handling = False,
                        force_symmetry           = True,
                        log                      = None)

        geometry_restraints_manager = rs.get_geometry_restraints_manager(
                                        processed_pdb_file = processed_pdb_file,
                                        xray_structure     = xrs_poor)

        if basepairs:

            hbond_params = self.create_hbond_proxies(pdb_hierarchy, basepairs, uc=symm.unit_cell())

            sctr_keys = xrs_poor.scattering_type_registry().type_count_dict().keys()
            has_hd = "H" in sctr_keys or "D" in sctr_keys

            geometry = processed_pdb_file.geometry_restraints_manager(
                    show_energies                = False,
                    plain_pairs_radius           = 5,
                    hydrogen_bond_proxies        = hbond_params,
                    assume_hydrogens_all_missing = not has_hd)
            geometry_restraints_manager = mmtbx.restraints.manager(
                    geometry      = geometry,
                    normalization = True)

            geometry_restraints_manager.crystal_symmetry = xrs_poor.crystal_symmetry()

        else:
            geometry_restraints_manager = rs.get_geometry_restraints_manager(
                                            processed_pdb_file = processed_pdb_file,
                                            xray_structure     = xrs_poor)



        self.rsr_simple_refiner = individual_sites.simple(
                                target_map                  = inp_map,
                                selection                   = selection,
                                real_space_gradients_delta  = d_min/4,
                                max_iterations              = max_iteration,
                                geometry_restraints_manager = geometry_restraints_manager.geometry)


    # -------------------------------------------------------------------------

    def process(self, xrs_poor, verbose=False):

        t0 = time.time()

        if self.optimize_weights:
            p = (0.08, 8.0)
            for start_value in [1.0,]:

                refined = individual_sites.refinery(
                                            refiner                  = self.rsr_simple_refiner,
                                            xray_structure           = xrs_poor,
                                            start_trial_weight_value = start_value,
                                            rms_bonds_limit          = p[0],
                                            rms_angles_limit         = p[1],
                                            optimize_weight=False)


                xrs_refined = xrs_poor.replace_sites_cart(refined.sites_cart_result)

        else:
            self.rsr_simple_refiner.refine( xray_structure = xrs_poor, weight = 0.8)
            xrs_refined = xrs_poor.replace_sites_cart(self.rsr_simple_refiner.sites_cart())

        if verbose: dist = flex.mean(flex.sqrt((xrs_poor.sites_cart() - xrs_refined.sites_cart()).dot()))


        t1 = time.time()-t0
        if verbose: print "     Done (time = %.2f, dist = %.2f)" % (t1, dist)

        return xrs_refined

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------



def refine_resi_by_resi(pdb_hierarchy_poor, \
                        inp_map=None, \
                        symm=None, \
                        verbose=False, \
                        processed_pdb_file=None):

    d_min = 2.0
    xrs_poor = pdb_hierarchy_poor.extract_xray_structure()
    selection = flex.bool(xrs_poor.scatterers().size(), True)

    if not processed_pdb_file:
        processed_pdb_file = monomer_library.pdb_interpretation.process(
                            mon_lib_srv = monomer_library.server.server(),
                            ener_lib    = monomer_library.server.ener_lib(),
                            raw_records = pdb_hierarchy_poor.as_pdb_string(crystal_symmetry=symm),
                            strict_conflict_handling = False,
                            force_symmetry           = True,
                            log                      = None)

    geometry_restraints_manager = rs.get_geometry_restraints_manager(
                                        processed_pdb_file = processed_pdb_file,
                                        xray_structure     = xrs_poor)

    xrs_refined = xrs_poor.deep_copy_scatterers()

    for rg_poor in pdb_hierarchy_poor.residue_groups():
        reference_selection = flex.size_t()
        reference_sites = flex.vec3_double()
        for a in rg_poor.atoms():
            reference_selection.append(a.i_seq)
            reference_sites.append(a.xyz)


        rsr_simple_refiner = individual_sites.simple(
                            target_map                  = inp_map,
                            selection                   = selection,
                            real_space_gradients_delta  = d_min/4,
                            max_iterations              = 100,
                            geometry_restraints_manager = geometry_restraints_manager.geometry)

        refined = individual_sites.refinery(
                            refiner                  = rsr_simple_refiner,
                            xray_structure           = xrs_poor,
                            start_trial_weight_value = 666,
                            rms_bonds_limit          = 0.06,
                            rms_angles_limit         = 6)
        xrs_refined = xrs_refined.replace_sites_cart(refined.sites_cart_result, selection=selection)

    pdb_hierarchy_poor.adopt_xray_structure(xrs_refined, assert_identical_id_str=False)


# -----------------------------------------------------------------------------


def refine_complete_structure(pdb_hierarchy_poor, \
                                inp_map=None, \
                                symm=None, \
                                max_iteration=150, \
                                optimize_weights=True, \
                                verbose=False, \
                                hbond_params=None):

    t0 = time.time()

    d_min = 3.0
    xrs_poor = pdb_hierarchy_poor.extract_xray_structure()
    selection = flex.bool(xrs_poor.scatterers().size(), True)

    processed_pdb_file = monomer_library.pdb_interpretation.process(
                        mon_lib_srv = monomer_library.server.server(),
                        ener_lib    = monomer_library.server.ener_lib(),
                        raw_records = pdb_hierarchy_poor.as_pdb_string(crystal_symmetry=symm),
                        strict_conflict_handling = False,
                        force_symmetry           = True,
                        log                      = None)


    # define secondary structure restrains
    sctr_keys = xrs_poor.scattering_type_registry().type_count_dict().keys()
    has_hd = "H" in sctr_keys or "D" in sctr_keys

    geometry = processed_pdb_file.geometry_restraints_manager(
            show_energies                = False,
            plain_pairs_radius           = 5,
            hydrogen_bond_proxies        = hbond_params,
            assume_hydrogens_all_missing = not has_hd)

    geometry_restraints_manager = mmtbx.restraints.manager(
            geometry      = geometry,
            normalization = True)

    geometry_restraints_manager.crystal_symmetry = xrs_poor.crystal_symmetry()

    # -----------------------

    rsr_simple_refiner = individual_sites.simple(
                                target_map                  = inp_map,
                                selection                   = selection,
                                real_space_gradients_delta  = d_min/4,
                                max_iterations              = max_iteration,
                                geometry_restraints_manager = geometry_restraints_manager.geometry)

    if optimize_weights:
        p = (0.08, 8.0)
        start_value = 1
        for start_value in [1.0,]:

            refined = individual_sites.refinery(
                                        refiner                  = rsr_simple_refiner,
                                        xray_structure           = xrs_poor,
                                        start_trial_weight_value = start_value,
                                        rms_bonds_limit          = p[0],
                                        rms_angles_limit         = p[1],
                                        optimize_weight=False)


            xrs_refined = xrs_poor.replace_sites_cart(refined.sites_cart_result)

            if verbose: dist = flex.mean(flex.sqrt((xrs_poor.sites_cart() - xrs_refined.sites_cart()).dot()))
    else:
        rsr_simple_refiner.refine( xray_structure = xrs_poor, weight = 1.0)
        xrs_refined = xrs_poor.replace_sites_cart(rsr_simple_refiner.sites_cart())
        if verbose: dist = flex.mean(flex.sqrt((xrs_poor.sites_cart() - xrs_refined.sites_cart()).dot()))

    t1 = time.time()-t0
    if verbose: print "     Done (time = %.2f, dist = %.2f)" % (t1, dist)

    pdb_hierarchy_poor.adopt_xray_structure(xrs_refined, assert_identical_id_str=False)


    return 1


# -----------------------------------------------------------------------------


def rigid_body_refinement_of_bases(pdb_hierarchy_poor, inp_map=None):
    import mmtbx.refinement.real_space.rigid_body



    assert inp_map

    rg_poor = pdb_hierarchy_poor.residue_groups().next().detached_copy()


    flags = cctbx.geometry_restraints.flags.flags(generic_restraints=True)

    generic_restraints_manager = mmtbx.geometry_restraints.manager()

    reference_selection = flex.size_t()
    reference_sites = flex.vec3_double()
    for rg in pdb_hierarchy_poor.residue_groups():
        for a in rg.atoms():
            # TODO: fix atoms selection
            if re.match(".*['*]$", a.name):
                reference_selection.append(a.i_seq)
                reference_sites.append(a.xyz)

    restraints_manager = cctbx.geometry_restraints.manager.manager(
                                generic_restraints_manager = generic_restraints_manager)

    restraints_manager.generic_restraints_manager.reference_manager.\
                        add_coordinate_restraints(
                                sites_cart = reference_sites,
                                selection  = reference_selection,
                                sigma      = 1.0)

    resolution_factor = 0.1
    d_min=3.0
    lbfgs_termination_params=scitbx.lbfgs.termination_parameters(max_iterations = 250)
    for rg_poor in pdb_hierarchy_poor.residue_groups():
        minimized = mmtbx.refinement.real_space.rigid_body.refine(
              residue                     = rg_poor,
              density_map                 = self.target_map.real_map_unpadded(),
              geometry_restraints_manager = restraints_manager,
              real_space_target_weight    = 1,
              real_space_gradients_delta  = d_min*resolution_factor,
              lbfgs_termination_params    = lbfgs_termination_params,
              unit_cell                   = self.symm.unit_cell(),
              cctbx_geometry_restraints_flags = flags)
              #states_collector            = states_collector)

        atoms = rg_poor.atoms()
        atoms.set_xyz(minimized.sites_cart_residue)

    return 1




def rigid_body_refinement(pdb_hierarchy_poor, \
                            inp_map = None, \
                            symm = None, \
                            max_iterations = 50, \
                            states_collector = None):

    import mmtbx.refinement.real_space.rigid_body

    #states_collector = mmtbx.utils.states(pdb_hierarchy  = pdb_hierarchy_poor, \
    #                                        xray_structure = pdb_hierarchy_poor.extract_xray_structure())


    rg_poor = pdb_hierarchy_poor.residue_groups().next().detached_copy()


    flags = cctbx.geometry_restraints.flags.flags(generic_restraints=True)

    generic_restraints_manager = mmtbx.geometry_restraints.manager()

    reference_selection = flex.size_t()
    reference_sites = flex.vec3_double()
    for rg in pdb_hierarchy_poor.residue_groups():
        for a in rg.atoms():
            # TODO: fix atoms selection
            if re.match(".*['*]$", a.name):
                reference_selection.append(a.i_seq)
                reference_sites.append(a.xyz)

    restraints_manager = cctbx.geometry_restraints.manager.manager(
                                generic_restraints_manager = generic_restraints_manager)

    resolution_factor = 0.1
    d_min=3.0
    lbfgs_termination_params=scitbx.lbfgs.termination_parameters(max_iterations = max_iterations)

    minimized = mmtbx.refinement.real_space.rigid_body.refine(
          residue                     = pdb_hierarchy_poor,
          density_map                 = inp_map,
          geometry_restraints_manager = restraints_manager,
          real_space_target_weight    = 1,
          real_space_gradients_delta  = d_min*resolution_factor,
          lbfgs_termination_params    = lbfgs_termination_params,
          unit_cell                   = symm.unit_cell(),
          cctbx_geometry_restraints_flags = flags,
          states_collector            = states_collector)

    atoms = pdb_hierarchy_poor.atoms()
    atoms.set_xyz(minimized.sites_cart_residue)

    return 1



