#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================

import sys, os

try:
    from brickworx_config import CLUSTERS_URL, FRAGS_URL
except:
    sys.path.append("../")
    from brickworx_config import CLUSTERS_URL, FRAGS_URL
sys.path.append("../lib/ClaRNA/")

import string
import re
import iotbx
from scitbx import matrix
from cctbx.array_family import flex
from scitbx.math import superpose
try:
    from networkx.readwrite import json_graph
except:
    pass

import pdb as pdb_debugger
import gzip
from iotbx.pdb import input as pdb_input


import json

from StringIO import StringIO

import urllib, urllib2


# between purines and pyrimidines, the normals are reversed, because
# the rings are reversed with respect to the helix axis.
NORMAL_SUPPORT = {
        'C':['N1','C2','N3','C4','C5','C6'],
        'U':['N1','C2','N3','C4','C5','C6'],
        'T':['N1','C2','N3','C4','C5','C6'],
        'G':['N1','C2','C4','N3','C5','C6'],
        'A':['N1','C2','C4','N3','C5','C6'],
        'TRP':['CD2','CE2','CZ2','CH2','CZ3','CE3'],
        'HIS':['CG','ND1','CE1','NE2','CD2'],
        'PHE':['CG','CD2','CE2','CZ','CE1','CD1'],
        'TYR':['CG','CD2','CE2','CZ','CE1','CD1'],
        }


# -----------------------------------------------------------------------------

class looper:


    def __init__(self):
        pass


    # -------------------------------------------------------------------------

    # -------------------------------------------------------------------------


    def get_list_of_clusters(self, fragment_type = ['stem', 'loop', 'strand'][1], \
                                        max_distance = [1.0, 2.0, 5.0][1], \
                                        skip_target_related=False):
        '''
        output:
        cluster_id number_of_members cluster_name medoid_filename
        '''
        output_str = ""
        # get number of pages first
        data = urllib.urlencode({'max_distance'  : max_distance, \
                                'fragment_type' : fragment_type, })

        n_pages = string.atoi( urllib2.urlopen(url=CLUSTERS_URL, data=data).read() )
        print "n_pages = %i" % n_pages
        for idx in range(n_pages):
            output_str += urllib2.urlopen(url=CLUSTERS_URL+"/page=%i"%idx, data=data).read()
        print " ==> Got %i frags" % len(output_str.split('\n'))
        return output_str.split('\n')

    # -------------------------------------------------------------------------

    def sync_motifs_dir(self, min_score=0.5, motifs_dir = "./motifs_tmp/"):
        cutils = Clarna_utils()

        complete_data_dict = {}

        for idx,item in enumerate(self.get_list_of_clusters(skip_target_related=True)[:]):
            try:
                medoid_fname = item.split()[-1]
            except:
                continue
            if medoid_fname:
                medoid_fname = medoid_fname.rstrip('.pdb.gz')
                url = urllib2.urlopen(url = FRAGS_URL+"/%s.pdb.gz" % \
                                                    medoid_fname)
                url_f = StringIO(url.read())
                gzipfile = gzip.GzipFile(fileobj = url_f)


                gzipfile.seek(0)
                raw_records = flex.std_string()
                raw_records.extend(flex.split_lines(gzipfile.read()))
                frag = pdb_input(source_info=None, lines=raw_records).construct_hierarchy()

                if frag.overall_counts().n_residues > 30: continue


                closing_pairs = [sorted(x) for x in self.find_closing_basepairs(frag)]



                data = {}
                # get list of basepairs and extract WC ones
                data['basepairs'] = self.get_basepairs_list(cutils, frag, min_clarna_score=min_score)
                wc_pairs = [sorted(x[0], x[1]) for x in data['basepairs'] if x[2]=="WW_cis"]


                # extract P's
                sel_cache = frag.atom_selection_cache()
                assert sel_cache.n_seq == frag.atoms_size()
                isel = sel_cache.iselection
                pattern = frag.atoms().select(isel(r"name P")).extract_xyz()
                data['pattern'] = list(pattern)
                data['xyz'] = list(frag.atoms().select(isel(r"not element H")).extract_xyz())
                closing_pairs_ref = []

                # get CONFIRMED closing WC basepairs and coresponding reference atoms
                frag_ends = self.find_closing_basepairs(frag)
                for bp in closing_pairs:
                    if not bp in wc_pairs: continue

                    ref_points = self.get_basepair_ref(frag, (bp[1], bp[0]))
                    closing_pairs_ref.append(list(ref_points.as_1d()))

                if not closing_pairs_ref:
                    print " ===> Skipping oddity: ", medoid_fname
                    continue

                data['closing_pairs_ref'] = closing_pairs_ref
                complete_data_dict[os.path.basename(medoid_fname)] = data

                print idx, medoid_fname, [x in wc_pairs for x in closing_pairs].count(False), frag.overall_counts().n_residues

                # save pdb file and a graph
                gzipfile.seek(0)
                gzip.open(motifs_dir+'%s.pdb.gz'%medoid_fname, 'w').write(gzipfile.read())

                f = gzip.open(motifs_dir+'%s.json.gz'%medoid_fname, "w")
                json_graph.dump(res_graph, f, indent=2)


        gzip.open(motifs_dir+"data.json.gz", "w").write(json.dumps(complete_data_dict))



    # -------------------------------------------------------------------------

    def read_fragment(self, fname, remote=False):
        raw_records = flex.std_string()

        if remote:
            try:
                url = urllib2.urlopen(url = FRAGS_URL+"/%s" % fname)
                url_f = StringIO(url.read())
                gzipfile = gzip.GzipFile(fileobj = url_f)
                raw_records.extend(flex.split_lines(gzipfile.read()))
            except:
                print fname

        else:

            if re.match(r".*\.gz$",fname):
                f = gzip.open(fname,"r")
            else:
                f = open(fname,"r")

            raw_records.extend(flex.split_lines(f.read()))

        return pdb_input(source_info=None, lines=raw_records).construct_hierarchy()



    # -------------------------------------------------------------------------

    def _get_atom_by_name(self, resi, name):
        for atom in resi.atoms():
            if atom.name.strip()==name:
                return atom
        return None

    # -------------------------------------------------------------------------

    def _get_base_normal_vector(self, resi):

        resname = resi.resname.strip().upper()
        normal_set = NORMAL_SUPPORT[resname]

        vec_1 = matrix.col( self._get_atom_by_name(resi, normal_set[1]).xyz ) - \
                matrix.col( self._get_atom_by_name(resi, normal_set[0]).xyz )


        vec_2 = matrix.col( self._get_atom_by_name(resi, normal_set[3]).xyz ) - \
                matrix.col( self._get_atom_by_name(resi, normal_set[2]).xyz )


        normal_vec = vec_1.cross(vec_2)
        return normal_vec.normalize()

    # -------------------------------------------------------------------------

    def _get_base_ref(self, ag):

        ag_normal = self._get_base_normal_vector(ag)
        resname = ag.resname.strip().upper()
        ag_ref = [
                            self._get_atom_by_name(ag, "C1'").xyz, \
                            self._get_atom_by_name(ag, "N1" if resname in ["U", "C"] else "N9").xyz, \
                            matrix.col(self._get_atom_by_name(ag, "N1" if resname in ["U", "C"] else "N9").xyz) + \
                                ag_normal ]

        return ag_ref

    # -------------------------------------------------------------------------

    def get_basepair_ref(self, frag, basepair):

        isel_cache = frag.atom_selection_cache()
        isel = isel_cache.iselection
        assert isel_cache.n_seq == frag.atoms_size()

        refs =[]

        for resseq in basepair:
            refs.extend( self._get_base_ref(frag.select(isel(r"resseq %i" % resseq)).only_atom_group()) )

        return flex.vec3_double(refs)


    # -------------------------------------------------------------------------

    def find_closing_basepairs(self, frag):
        """
            finds pairs of resi that are (should be?) paired.
            i.e. link_to_previous=False for 'next'
        """
        basepairs = []# get closing basepairs and coresponding reference atoms

        residue_groups = [(rg, string.atoi(rg.resseq)) for rg in frag.residue_groups()]

        residue_groups = sorted(residue_groups, key = lambda x: x[1])

        for i in xrange(len(residue_groups)):
            if residue_groups[i][0].link_to_previous == False or \
                abs(residue_groups[i][1]-residue_groups[i-1][1])>1:
                basepairs.append( (residue_groups[i][1], residue_groups[i-1][1]) )

        return basepairs

    # -------------------------------------------------------------------------

    def match_fragments_simple(self, ref_points, move_points):

        superposition = superpose.least_squares_fit(ref_points, move_points, method=["kearsley", "kabsch"][0])
        rtmx = matrix.rt((superposition.r, superposition.t))

        return rtmx


    # -------------------------------------------------------------------------


    def get_basepairs_list(self, cutils, frag, min_clarna_score=0.5):
        # preprocess basepairing info
        frag_fileobj = StringIO(frag.as_pdb_string())
        res_graph = cutils.start(frag_fileobj)
        basepairs_dict = {}
        #basepairs_list = []

        for (u,v,d) in res_graph.edges(data=True):
            if d['type']=='contact' and d['weight']>min_clarna_score:
                desc = d['desc'].split('_')[1][0].lower()+d['desc'].split('_')[0]
                bp_id = (string.atoi(u[1:]), string.atoi(v[1:]))

                if not tuple(bp_id[::-1]) in basepairs_dict.keys():
                    #basepairs_dict[bp_id] = (d['n_type'], desc)
                    #basepairs_list.append( (bp_id[0], bp_id[1], \
                    #                        desc[1:]+("_cis" if desc[0]=="c" else "_tran"), \
                    #                        d['n_type']) )

                    basepairs_dict[bp_id] =  (bp_id[0], bp_id[1], \
                                              desc[1:]+("_cis" if desc[0]=="c" else "_tran"), \
                                              d['n_type'])

        #return basepairs_list
        return [y for (x,y) in basepairs_dict.items()]



    # -------------------------------------------------------------------------


    def preprocess_files(self, data_path="../data/motifs/"):


        cutils = Clarna_utils()



        if not data_path: return 1
        complete_data_dict = {}
        path_fnames = [data_path+xxx for xxx in os.listdir(data_path) if re.match(".*\.pdb(.gz)?$", xxx)]
        for idx,fname in enumerate(path_fnames):
            print idx, fname
            frag = self.read_fragment(fname)

            data = {}

            data['basepairs'] = self.get_basepairs_list(cutils, frag)


            # extract P's
            sel_cache = frag.atom_selection_cache()
            assert sel_cache.n_seq == frag.atoms_size()
            isel = sel_cache.iselection
            pattern = frag.atoms().select(isel(r"name P")).extract_xyz()
            data['pattern'] = list(pattern)
            data['xyz'] = list(frag.atoms().select(isel(r"not element H")).extract_xyz())
            closing_pairs = []
            # get closing basepairs and coresponding reference atoms
            frag_ends = self.find_closing_basepairs(frag)
            for bp in frag_ends:
                try:
                    ref_points = self.get_basepair_ref(frag, (bp[1], bp[0]))
                except:
                    continue
                closing_pairs.append(list(ref_points.as_1d()))

            data['closing_pairs_ref'] = closing_pairs
            complete_data_dict[os.path.basename(fname)] = data


        gzip.open(data_path+"/data.json.gz", "w").write(json.dumps(complete_data_dict))










if __name__=="__main__":

    from clarna_utils import Clarna_utils
    looper_obj = looper()
    #looper_obj.sync_motifs_dir()
    looper_obj.preprocess_files(sys.argv[1])
