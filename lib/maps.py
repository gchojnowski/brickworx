#! /usr/bin/env libtbx.python
# code below was adapted from CCTBX

import sys, os
import urllib2, urllib
from cStringIO import StringIO
import numpy as np
try:
    from brickworx_config import *
except:
    sys.path.append("../")
    from brickworx_config import *

from optparse import OptionParser

# ----------- CCTBX
from cctbx import miller
import mmtbx.bulk_solvent.bulk_solvent_and_scaling as bss
import iotbx
import mmtbx.utils
import mmtbx
import scitbx, math
from iotbx import reflection_file_reader
from iotbx import reflection_file_utils
from cctbx import crystal
from scitbx import matrix
from cctbx.array_family import flex
from cctbx import maptbx
from iotbx.pdb import input as pdb_input
from libtbx.math_utils import ifloor, iceil
# -----------------


# ------------------------------------------------------------------------------
# -----------------------------------------------------------------------------

def get_mask( target_map, symm, verts ):

    n_real = target_map.n_real()
    n = n_real[0]*n_real[1]*n_real[2]
    mask_1 = flex.double([1 for i in xrange(n)])
    mask_1.resize(flex.grid(n_real))
    mask_0 = flex.double([0 for i in xrange(n)])
    mask_0.resize(flex.grid(n_real))


    maptbx.set_box( map_data_from = mask_1,
                    map_data_to   = mask_0,
                    start         = verts[0],
                    end           = verts[1] )

    return mask_0

# -----------------------------------------------------------------------------


def dividenconquer( target_map, symm, box=1 ):
    """
        generate masks in very large ASU
    """

    asu = symm.direct_space_asu().as_float_asu()
    uc = symm.unit_cell()

    asu_vol = uc.volume() / symm.space_group().order_z()

    axis_cut = [1]*3
    box_max_orth = uc.orthogonalize(asu.box_max())
    shortes_axis = box_max_orth.index(min(box_max_orth))


    for i in range(3):
        while ( box_max_orth[i]/(axis_cut[i]+1.) ) > 120:
            axis_cut[i] += 1

    if sum(axis_cut) == 3: return [None]


    asu_grid_pts = [x*y for (x,y) in zip(asu.box_max(), target_map.n_real())]
    steps = [ifloor(x/y) for (x,y) in zip(asu_grid_pts, axis_cut)]


    n_real = target_map.n_real()
    n = n_real[0]*n_real[1]*n_real[2]
    mask1 = flex.double([1 for i in xrange(n)])
    mask1.resize(flex.grid(n_real))

    masks = []

    for ix in range(axis_cut[0]):
        for iy in range(axis_cut[1]):
            for iz in range(axis_cut[2]):
                first = (ix*steps[0], iy*steps[1], iz*steps[2])
                last = (ix+1)*steps[0], (iy+1)*steps[1], (iz+1)*steps[2]

                #masks.append( flex.double([0 for i in xrange(n)]) )
                #masks[-1].resize(flex.grid(n_real))

                #maptbx.set_box( map_data_from = mask1,
                #                map_data_to   = masks[-1],
                #                start         = first,
                #                end           = last )
                masks.append( (first, last) )
    print " ==> Processing %d boxes" % len(masks)
    return masks

# -----------------------------------------------------------------------------
# cctbx/maptbx/__init__.py:region_density_correlation


def local_region_density_correlation(large_unit_cell, \
                                    large_d_min, \
                                    large_density_map, \
                                    frag, \
                                    atom_radius=2.0, \
                                    residue_resolution=True):

    atoms = frag.atoms()
    atoms.set_b(new_b=flex.double(atoms.size(), 20.0))
    atoms.set_occ(new_occ=flex.double(atoms.size(), 1.0))




    sites_cart = frag.atoms().extract_xyz()

    sites_frac_large = large_unit_cell.fractionalize(sites_cart)

    large_frac_min = sites_frac_large.min()
    large_frac_max = sites_frac_large.max()
    large_n_real = large_density_map.focus()
    from scitbx import fftpack
    large_ucp = large_unit_cell.parameters()
    small_n_real = [0,0,0]
    small_origin_in_large_grid = [0,0,0]
    small_abc = [0,0,0]
    sites_frac_shift = [0,0,0]
    for i in xrange(3):
        grid_step = large_ucp[i] / large_n_real[i]
        # XXX to be on the safe-side (VdW radius)
        buffer = 5.0#large_d_min / grid_step
        grid_min = ifloor(large_frac_min[i] * large_n_real[i] - buffer)
        grid_max = iceil(large_frac_max[i] * large_n_real[i] + buffer)
        min_grid = grid_max - grid_min + 1
        small_n_real[i] = fftpack.adjust_gridding(min_grid=min_grid, max_prime=5)
        if (small_n_real[i] < large_n_real[i]):
            shift_min = (small_n_real[i] - min_grid) // 2
            small_origin_in_large_grid[i] = grid_min - shift_min
            small_abc[i] = small_n_real[i] * grid_step
            sites_frac_shift[i] = small_origin_in_large_grid[i] / large_n_real[i]
        else:
            small_n_real[i] = large_n_real[i]
            small_origin_in_large_grid[i] = 0
            small_abc[i] = large_ucp[i]
            sites_frac_shift[i] = 0

    sites_cart_shift = large_unit_cell.orthogonalize(sites_frac_shift)
    sites_cart_small = sites_cart - sites_cart_shift
    from cctbx import xray


    small_xray_structure = xray.structure(
      crystal_symmetry=crystal.symmetry(
        unit_cell=tuple(small_abc)+large_ucp[3:],
        space_group_symbol="P1"),
      scatterers=frag.extract_xray_structure().scatterers())#work_scatterers)

    small_xray_structure.set_sites_cart(sites_cart=sites_cart_small)
    small_f_calc = small_xray_structure.structure_factors(
      d_min=large_d_min).f_calc()
    small_gridding = maptbx.crystal_gridding(unit_cell=small_f_calc.unit_cell(),
                                      space_group_info=small_f_calc.space_group_info(),
                                      pre_determined_n_real=small_n_real)
    from cctbx import miller

    small_fft_map = miller.fft_map(crystal_gridding=small_gridding,
                                    fourier_coefficients=small_f_calc)
    small_fft_map.apply_sigma_scaling()
    small_map = small_fft_map.real_map_unpadded()

    small_copy_from_large_map = maptbx.copy(
                    map_unit_cell=large_density_map,
                    first=small_origin_in_large_grid,
                    last=matrix.col(small_origin_in_large_grid) + matrix.col(small_n_real) - matrix.col((1,1,1)))

    assert small_copy_from_large_map.all() == small_map.all()



    if not residue_resolution:

        grid_indices = maptbx.grid_indices_around_sites(
            unit_cell=small_xray_structure.unit_cell(),
            fft_n_real=small_n_real,
            fft_m_real=small_n_real,
            sites_cart=sites_cart_small,
            site_radii=site_radii)

        assert small_copy_from_large_map.all() == small_map.all()
        corr = flex.linear_correlation(
            x=small_map.select(grid_indices),
            y=small_copy_from_large_map.select(grid_indices))
        if (not corr.is_well_defined()):
            return None
        return corr.coefficient()




    from cctbx.eltbx import van_der_waals_radii
    from libtbx import group_args

    result = []
    for rg in frag.residue_groups():
        atom_radii = []
        for atom in rg.atoms():
            try:
                atom_radii.append( van_der_waals_radii.vdw.table[atom.element.strip()])
            except:
                atom_radii.append( van_der_waals_radii.vdw.table['O'])

        grid_indices = maptbx.grid_indices_around_sites(
                    unit_cell=small_xray_structure.unit_cell(),
                    fft_n_real=small_n_real,
                    fft_m_real=small_n_real,
                    sites_cart=flex.vec3_double(rg.atoms().extract_xyz()) - sites_cart_shift,
                    site_radii=flex.double(rg.atoms().size(), atom_radius))



        corr = flex.linear_correlation( x=small_map.select(grid_indices), \
                                        y=small_copy_from_large_map.select(grid_indices))

        assert corr.is_well_defined()

        residue = group_args(rg=rg, resid = rg.resid(), chain_id  = rg.parent().id)
        result.append( group_args(residue = residue, cc = corr.coefficient()) )

    return result

# -----------------------------------------------------------------------------

class RealSpaceCorrelations:

    """
    Calculates correlation coefs of calculated and 2Fo-Fc ('experimental') maps.
    args: a list of pdb files (FULL content of the assymetric unit MUST be given) 
    and mtz file with experimental amplitudes (labelled FP)

    output: list of residues with low correlation coef or weak density
    """
    def __init__(self, tmp_dir='./tmp/'):

        self.tmp_dir = tmp_dir

    # ---------------------------------------------------------------------------------------------


    def init_xdata(self):


        # a xtallographic version of a hierchical structure (a must for structure factor calcs)
        self.xray_structure = self.complete_pdb_hierarchy.extract_xray_structure(self.symm)

        # the two should be of equal size
        assert self.xray_structure.sites_cart().size() == self.complete_pdb_hierarchy.atoms().size()


        # extracts crystallographic data from a mtz file (miller indices and structure factors)
        self.xdata = self.extract_data_and_flags(self.mtz_filename, self.symm)

        # parameters and some heuristics, do not touch!
        self.atom_detail    = False
        self.residue_detail = True
        self.atom_radius = 1.5
        self.number_of_grid_points = 50
        self.resolution_factor = 1.0/3.0
        self.grid_step = min(self.atom_radius*(4*np.pi/(3*self.number_of_grid_points))**(1./3), self.xdata.f_obs.d_min()*self.resolution_factor)


        # defines electron density maps used for calcs
        self.map_str_type = "Fc"
        self.map_xray_type = "2mFo-DFc"

        self.map_name_obj = mmtbx.map_names(map_name_string = self.map_str_type)

    # ---------------------------------------------------------------------------------------------

    def init_from_pdbredo(self, pdbids_list, verbose=False, malibu=True):

        print " --> Initializing form PDB_REDO %s" % ('(local copy)' if malibu else '')

        self.preprocessed_RNAs = []

        self.mtz_filename = self.tmp_dir+"%s.mtz"%pdbids_list[0]

        if malibu:
            try:
                mtz_data = urllib2.urlopen("%s/%s_final.mtz" % (LOCAL_REDO_URL, pdbids_list[0]))
            except:
                print "LOCAL_REDO_URL not available, exiting..."
                exit(0)
        else:
            mtz_data = urllib2.urlopen("http://www.cmbi.ru.nl/pdb_redo/%s/%s/%s_final.mtz" % (pdbids_list[0][1:3], \
                                                                                        pdbids_list[0], \
                                                                                        pdbids_list[0]) )

        f = open(self.mtz_filename, 'w')
        f.write(mtz_data.read())
        f.close()
        print " --> Got MTZ"

        self.pdb_filename = self.tmp_dir+"%s.pdb"%pdbids_list[0]


        pdb_hierarchies = []
        for pdb_id in pdbids_list:
            raw_records = flex.std_string()
            if malibu:
                try:
                    remote_str = urllib2.urlopen("%s/%s_final.pdb" % (LOCAL_REDO_URL,pdb_id)).read()
                except:
                    print "LOCAL_REDO_URL not available, exiting..."
                    exit(0)
            else:
                remote_str = urllib2.urlopen("http://www.cmbi.ru.nl/pdb_redo/%s/%s/%s_final.pdb" % \
                                                (pdb_id[1:3], pdb_id, pdb_id)).read()

            raw_records.extend(flex.split_lines(remote_str))
            if verbose: open(self.tmp_dir+"%s.pdb"%pdb_id, "w").write(remote_str)
            pdb_inp = pdb_input(source_info=None, lines=raw_records)
            pdb_hierarchies.append(pdb_inp.construct_hierarchy())
            self.symm = pdb_inp.crystal_symmetry()

        self.complete_pdb_hierarchy = iotbx.pdb.hierarchy.join_roots(roots=pdb_hierarchies)
        if verbose: self.complete_pdb_hierarchy.write_pdb_file(file_name="%s.asu.pdb"%self.pdb_filename,\
                                                                                 crystal_symmetry=self.symm)


        self.init_xdata()


    # ---------------------------------------------------------------------------------------------

    def init_from_rnadb(self, pdbids_list, verbose=False):

        print " --> Initializing form RNA Bricks"

        self.preprocessed_RNAs = []

        self.mtz_filename = self.tmp_dir+"%s.mtz"%pdbids_list[0]

        # all mtz files from the ASU set must be identical
        mtz_data = urllib2.urlopen(RNADB_URL+"/updater/get_mtz/%s.mtz"%pdbids_list[0])
        open(self.mtz_filename, 'w').write(mtz_data.read())

        self.pdb_filename = self.tmp_dir+"%s.pdb"%pdbids_list[0]


        pdb_hierarchies = []
        for pdb_id in pdbids_list:
            raw_records = flex.std_string()
            remote_str = urllib2.urlopen(RNADB_URL+"/updater/get_pdb/%s.pdb"%pdb_id).read()
            raw_records.extend(flex.split_lines(remote_str))
            if verbose: open(self.tmp_dir+"%s.pdb"%pdb_id, "w").write(remote_str)
            pdb_inp = pdb_input(source_info=None, lines=raw_records)
            pdb_hierarchies.append(pdb_inp.construct_hierarchy())
            self.symm = pdb_inp.crystal_symmetry()


        self.complete_pdb_hierarchy = iotbx.pdb.hierarchy.join_roots(roots=pdb_hierarchies)
        if verbose: self.complete_pdb_hierarchy.write_pdb_file(file_name="%s.asu.pdb"%self.pdb_filename,\
                                                                                 crystal_symmetry=self.symm)


        self.init_xdata()

    # ---------------------------------------------------------------------------------------------


    def init_from_files(self, pdb_filenames, mtz_filename, verbose=False):
        print " --> Initializing form local files"

        self.mtz_filename = mtz_filename
        self.pdb_filename = pdb_filenames[0]

        # creates xray structure from coordinates (multiple files allowed)
        pdb_hierarchies = []
        for filename in pdb_filenames:
            pdb_inp = iotbx.pdb.input(file_name=filename)
            self.symm = pdb_inp.crystal_symmetry()
            pdb_hierarchy_tmp = pdb_inp.construct_hierarchy()
            pdb_hierarchy_tmp.atoms().reset_i_seq()
            pdb_hierarchies.append(pdb_hierarchy_tmp)


        # procedure adds suffixes to the merged chain ids
        # chain_suffixes="123456789" + "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvwxyz"
        self.complete_pdb_hierarchy = iotbx.pdb.hierarchy.join_roots(roots=pdb_hierarchies)

        if verbose: self.complete_pdb_hierarchy.write_pdb_file(file_name="%s._asu.pdb"%self.pdb_filename,\
                                                                                 crystal_symmetry=self.symm)


        self.init_xdata()

    # ---------------------------------------------------------------------------------------------

    def extract_data_and_flags(self, reflection_file_name, crystal_symmetry):
        data_and_flags = None
        reflection_file = reflection_file_reader.any_reflection_file(
                        file_name = reflection_file_name)
        reflection_file_server = reflection_file_utils.reflection_file_server(
                        crystal_symmetry = crystal_symmetry,
                        force_symmetry   = True,
                        reflection_files = [reflection_file])
        try:
            fobs = reflection_file_server.get_xray_data(file_name=None,\
                                                        labels="FP,SIGFP",
                                                        ignore_all_zeros=True,
                                                        parameter_scope="xray_data")
        except:
            fobs = reflection_file_server.get_xray_data(file_name=None,\
                                                        labels="I,SIGI",
                                                        ignore_all_zeros=True,
                                                        parameter_scope="xray_data")


        parameters = mmtbx.utils.data_and_flags_master_params().extract()
        parameters.force_anomalous_flag_to_be_equal_to = False

        # XXX    check the label
        parameters.labels = "I" if fobs._observation_type.__str__().strip()=="xray.intensity" else "FP"

        data_and_flags = mmtbx.utils.determine_data_and_flags(
                    reflection_file_server = reflection_file_server,
                    parameters             = parameters,
                    data_description       = "X-ray data",
                    extract_r_free_flags   = False,
                    log                    = StringIO())

        return data_and_flags

    # ---------------------------------------------------------------------------------------------

    def calc_map(self, d_min=0.0, verbose=False, phasemix=False, fudge_factor=0.0):

        def biased_phases(f_calc, fudge_factor):
            phases_bias = flex.arg(f_calc.data(), True)
            centric_flags = f_calc.centric_flags().data()
            acentric_flags = ~centric_flags
            centric_phases = phases_bias.select(centric_flags)
            acentric_phases = phases_bias.select(acentric_flags)
            sel = flex.random_double(size=centric_phases.size()) < (0.5 * fudge_factor)
            centric_phases.set_selected(sel, centric_phases.select(sel) + 180)
            acentric_phases += (flex.random_double(size=acentric_phases.size())
                          * 360 - 180) * fudge_factor
            phases_bias.set_selected(centric_flags, centric_phases)
            phases_bias.set_selected(acentric_flags, acentric_phases)

            return phases_bias


        print " --> Preparing model map"

        if d_min<0.01: d_min = self.xdata.f_obs.d_min()

        f_calc=miller.array(miller_set = self.xdata.f_obs).\
            structure_factors_from_scatterers(xray_structure=self.xray_structure,algorithm="direct").f_calc()


        print " --> Mixing phases with f_obs"

        if phasemix:

            # -----------------
            phases = flex.arg(f_calc.phases(deg=True).data(), True)
            centric_flags = f_calc.centric_flags().data()
            acentric_flags = ~centric_flags
            centric_phases = phases.select(centric_flags)
            acentric_phases = phases.select(acentric_flags)
            sel = flex.random_double(size=centric_phases.size()) < (0.5 * fudge_factor)
            centric_phases.set_selected(sel, centric_phases.select(sel) + 180)
            acentric_phases += (flex.random_double(size=acentric_phases.size())
                          * 360 - 180) * fudge_factor
            phases.set_selected(centric_flags, centric_phases)
            phases.set_selected(acentric_flags, acentric_phases)
            # ------------------
            hires_select = f_calc.d_spacings().data() < d_min
            print "Fraction of random phases: %.2f"%(list(hires_select).count(True)/hires_select.size())

            phases.set_selected(~hires_select,  f_calc.phases(deg=True).data())
            f_obs_phasemix = self.xdata.f_obs.phase_transfer(phases, deg=True)
            ma_phasemix = miller.array(miller_set=f_obs_phasemix, data=f_obs_phasemix.data())
            self.miller_set = f_obs_phasemix
            output_mtz = ma_phasemix.as_mtz_dataset('FP')
            output_mtz.add_miller_array(
                                miller_array  = miller.array(miller_set = f_obs_phasemix, \
                                                data = abs(f_obs_phasemix).data() * 0.1), \
                                column_root_label = "SIGFP", column_types='Q')



            fft_map_xray = f_obs_phasemix.fft_map(symmetry_flags=maptbx.use_space_group_symmetry)

        else:
            self.miller_set = miller.build_set(
                crystal_symmetry=self.xdata.f_obs.crystal_symmetry(),
                anomalous_flag=False,
                d_min=d_min)

            new_phases = biased_phases(f_calc, fudge_factor=fudge_factor)
            print " --> Mean phase error = %.2f deg" % \
                            np.fabs((new_phases-f_calc.phases(deg=True).data()).as_numpy_array()).mean()
            print " --> FOM              = %.4f" % \
                            np.cos((new_phases-f_calc.phases(deg=True).data()).as_numpy_array()*math.pi/180.0).mean()



            f_obs_phasemix = self.xdata.f_obs.phase_transfer(new_phases, deg=True)
            f_obs_dmin = f_obs_phasemix.common_set(self.miller_set)
            ma_dmin = miller.array(miller_set=f_obs_dmin, data=f_obs_dmin.data())

            self.output_map_mtz = ma_dmin.as_mtz_dataset(column_root_label="FP")
            self.output_map_mtz.add_miller_array(
                                miller_array  = miller.array(miller_set = f_obs_dmin, data = abs(f_obs_dmin).data() * 0.1),
                                column_root_label = "SIGFP", column_types='Q')

            self.fft_map_xray = f_obs_dmin.fft_map(symmetry_flags=maptbx.use_space_group_symmetry)
            self.fft_map_xray.apply_sigma_scaling()

        if verbose:
            print " --> Writing output map (%s)" % (self.pdb_filename+'.dmin_%.2f.mtz'%d_min)
            self.output_map_mtz.mtz_object().write(self.pdb_filename+'.dmin_%.2f.mtz'%d_min)

            self.fft_map_xray.as_ccp4_map(file_name=self.pdb_filename+'.dmin_%.2f.ccp4'%d_min)

        return 1


    def write_map(self, fname, ccp4=False):
        self.output_map_mtz.mtz_object().write(fname)
        if ccp4: self.fft_map_xray.as_ccp4_map(file_name='%s.ccp4'%fname)


    # ---------------------------------------------------------------------------------------------
    # ---------------------------------------------------------------------------------------------

