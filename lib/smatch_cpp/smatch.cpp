// =============================================================================
// Created at:
//  International Institute of Molecular and Cell Biology in Warsaw, Poland
// Author:
//  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
// =============================================================================

#include "smatch.h"
#include <boost/python/extract.hpp>
#include <boost/python/numeric.hpp>
#include <boost/python/module.hpp>
#include <boost/python/args.hpp>
#include <scitbx/mat3.h>
#include <boost/python/tuple.hpp>


#include <boost/python/init.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>


namespace bp = boost::python;

void test_array(boost::python::numeric::array array){
    scitbx::vec3<double> result;
    bp::tuple array_shape = bp::extract<bp::tuple>(array.attr("shape"));
    int array_dim = bp::len(array_shape);
    printf("array dim: %i\n", array_dim);
    if(array_dim>1)
        printf("array shape: %ix%i\n", \
                (int)bp::extract<int>(array_shape[0]), \
                (int)bp::extract<int>(array_shape[1]));
    printf("array[1,1] = %f\n", (float)bp::extract<float>(array[bp::make_tuple(1,1)]));
    boost::ref(array);
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

class supermatch
{

    private:
        smatch<double>* sm_;
        int reference_size;

        bp::list
          vec2list_( const std::vector<int> & vec){

            bp::list l;
            for(__typeof(vec.begin()) dd=vec.begin(); dd!=vec.end(); ++dd)
                l.append(*dd);

            return l;

          };


    public:
        // Constructor
        //supermatch(scitbx::af::const_ref<scitbx::vec3<double> > reference)
        //{
        //    sm_ = new smatch<double>(reference);
        //    reference_size=reference.size();
        //};


        supermatch(scitbx::af::const_ref<scitbx::vec3<double> > reference, \
                    bp::tuple & unit_cell_tpl, \
                    const std::string spg_symbol_str ){

            reference_size=reference.size();

            if( bp::len(unit_cell_tpl) == 6 ){
                scitbx::af::double6 d3_cell;
                for(unsigned i=0; i<6; ++i)
                    d3_cell[i] = (double)bp::extract<double>(unit_cell_tpl[i]);
                cctbx::uctbx::unit_cell cell(d3_cell);
                cctbx::sgtbx::space_group_type sg_type( spg_symbol_str );
                cctbx::sgtbx::space_group space_group = sg_type.group();

                sm_ = new smatch<double>( reference, cell, space_group );
            }else{
              sm_ = new smatch<double>( reference );
            }

        };



        // Destructor
        ~supermatch() { delete sm_; };

        // helper
        int get_reference_size() { return reference_size; };

        // find all matches of a pattern to the refernce set
        bp::list find_matches(
            scitbx::af::const_ref<scitbx::vec3<double> > const& moving, \
            double max_rmsd, \
            bool strict){

                bp::list res_list;
                int n_match = sm_->find_match(moving, max_rmsd, strict);
                for(int i=0; i<n_match; ++i)
                    res_list.append( bp::make_tuple(vec2list_(sm_->get_match(i)), \
                                                    sm_->get_rotation(i), \
                                                    sm_->get_translation(i), \
                                                    sm_->get_rmsd(i) ) );
                return res_list;
        };

        // finetune a match i.e. find pattern points counterparts
        // and recalculate rot/trans. by default this function is
        // called internally in the smatch::find_match
        bp::tuple finetune_match(scitbx::af::const_ref<scitbx::vec3<double> > const& moving, \
                            bp::tuple & inp_rot, \
                            bp::tuple & inp_trans, \
                            bool strict){

           assert( bp::len(inp_rot) == 9 );
           assert( bp::len(inp_trans) == 3 );
           std::vector<int> finetuned_match;

           mat3<double> rotation = bp::extract< mat3<double> >(inp_rot);
           vec3<double> translation = bp::extract< vec3<double> >(inp_trans);

           double rms;
           rms = sm_->finetune_transformation(moving, \
                                          rotation, \
                                          translation, \
                                          finetuned_match, \
                                          strict);

            return bp::make_tuple(vec2list_(finetuned_match), \
                                            rotation, \
                                            translation, \
                                            rms);

        };


        // --------------------------------------------------------------------

        bp::tuple finetune_match_symm(scitbx::af::const_ref<scitbx::vec3<double> > const& moving, \
                            bp::tuple & inp_rot, \
                            bp::tuple & inp_trans, \
                            bp::tuple & unit_cell_tpl, \
                            const std::string spg_symbol_str, \
                            bool strict){

            assert( bp::len(inp_rot) == 9 );
            assert( bp::len(inp_trans) == 3 );
            assert( bp::len(unit_cell_tpl) == 6 );

            std::vector<int> finetuned_match;

            mat3<double> rotation = bp::extract< mat3<double> >(inp_rot);
            vec3<double> translation = bp::extract< vec3<double> >(inp_trans);


            scitbx::af::double6 d3_cell;
            for(unsigned i=0; i<6; ++i)
                d3_cell[i] = (double)bp::extract<double>(unit_cell_tpl[i]);
            cctbx::uctbx::unit_cell cell(d3_cell);
            cctbx::sgtbx::space_group_type sg_type( spg_symbol_str );
            cctbx::sgtbx::space_group space_group = sg_type.group();

            double rms;
            rms = sm_->finetune_transformation_symm(moving, \
                                                    rotation, \
                                                    translation, \
                                                    finetuned_match, \
                                                    cell, \
                                                    space_group, \
                                                    strict);

            //rms = sm_->finetune_transformation_wrapper(moving, \
                                                       rotation, \
                                                       translation, \
                                                       finetuned_match, \
                                                       strict);



            return bp::make_tuple(vec2list_(finetuned_match), \
                                            rotation, \
                                            translation, \
                                            rms);

        };


        // ----------------------------------------------------------------------------------------

        // symm defs check implemented
        bp::tuple finetune_match_wrapper(scitbx::af::const_ref<scitbx::vec3<double> > const& moving, \
                                            bp::tuple & inp_rot, \
                                            bp::tuple & inp_trans, \
                                            bool strict){

            assert( bp::len(inp_rot) == 9 );
            assert( bp::len(inp_trans) == 3 );

            std::vector<int> finetuned_match;

            mat3<double> rotation = bp::extract< mat3<double> >(inp_rot);
            vec3<double> translation = bp::extract< vec3<double> >(inp_trans);



            double rms;

            rms = sm_->finetune_transformation_wrapper(moving, \
                                                       rotation, \
                                                       translation, \
                                                       finetuned_match, \
                                                       strict);



            return bp::make_tuple(vec2list_(finetuned_match), \
                                            rotation, \
                                            translation, \
                                            rms);

        };



};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

scitbx::mat3<double>
  superpose(
    scitbx::af::const_ref<scitbx::vec3<double> > const& reference,
    scitbx::af::const_ref<scitbx::vec3<double> > const& moving)
  {
    smatch<double>* sm;
    sm = new smatch<double>(reference);
    sm->set_ref(reference);
    sm->find_match(moving, 1.0, true);
    return sm->superpose(reference, moving);
  };



// MUST match the module name
BOOST_PYTHON_MODULE(smatch){

    bp::numeric::array::set_module_and_type("numpy", "ndarray");

    // supermatch
    bp::class_<supermatch> sm( "supermatch", bp::init<scitbx::af::const_ref<scitbx::vec3<double> >, bp::tuple &, const std::string >());

    sm.def( "find_matches", &supermatch::find_matches);

    sm.def( "finetune_match", &supermatch::finetune_match );
    sm.def( "finetune_match_symm", &supermatch::finetune_match_symm );
    sm.def( "finetune_match_wrapper", &supermatch::finetune_match_wrapper );

    sm.def( "get_reference_size", &supermatch::get_reference_size);

    bp::def( "superpose", superpose, ( bp::arg("reference"), bp::arg("moving")) );

    bp::def( "test_array", &test_array );
}



