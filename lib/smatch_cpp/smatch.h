// =============================================================================
// Created at:
//  International Institute of Molecular and Cell Biology in Warsaw, Poland
// Author:
//  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
// =============================================================================


#ifndef SMATCH_H
#define SMATCH_H

#define FOR(v,p,k) for(unsigned int v=p;v<=k;++v)
#define FORD(v,p,k) for(int v=p;v>=k;--v)
#define FORL(v,p,k) for(unsigned int v=p;v<k;++v)
#define REP(i,n) for(int i=0;i<(n);++i)
#define VAR(v,i) __typeof(i) v=(i)
#define FOREACH(i,c) for(VAR(i,(c).begin());i!=(c).end();++i)
#define PB push_back
#define MP std::make_pair
#define ST first
#define ND second
#define SIZE(x) (int)x.size()
#define ALL(c) c.begin(),c.end()


#include <typeinfo>


#include <stdlib.h>

#include <scitbx/vec3.h>
#include <scitbx/mat3.h>
#include <scitbx/math/superpose.h>
#include <scitbx/math/r3_rotation.h>

#include <scitbx/array_family/shared_reductions.h>
#include <scitbx/array_family/shared_algebra.h>
#include <scitbx/array_family/ref_algebra.h>

#include <cctbx/crystal/pair_tables.h>
#include <cctbx/crystal/incremental_pairs.h>

#include <cctbx/sgtbx/direct_space_asu/proto/direct_space_asu.h>
#include <boost/shared_ptr.hpp>

#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>

#include <ANN/ANN.h>


#include <boost/graph/adjacency_list.hpp>

#include <boost/graph/max_cardinality_matching.hpp>


#include <lemon/list_graph.h>
#include <lemon/matching.h>

using namespace scitbx;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS> boost_graph;

//defined in annlib/include/ANN/ANN.h
//const ANNbool   ANN_ALLOW_SELF_MATCH    =   ANNtrue;

template <typename FloatT=double>
class smatch
{

    private:

        static const int max_kdQuery_size = 100;            //KD-tree number of nearest neighbors per query
        static const FloatT kdQuery_eps;// = 0.0;           //KD-tree error bound (default = 0.0)
        static const int dimm = 3;



        std::vector< std::vector<int> > res_match_;
        std::vector< mat3< FloatT > > res_rot_;
        std::vector< vec3< FloatT > > res_trans_;
        std::vector< FloatT > res_rmsd_;


        ANNkd_tree*       reference_kdTree_;                // KD-tree structure

        ANNpointArray       dataPts_;                       // data points


        scitbx::af::const_ref< vec3<FloatT> > reference_;

        void create_kdtree(scitbx::af::const_ref< vec3<FloatT> > points, bool asu=false);

        void query_kdTree(const vec3<FloatT> query_point, \
                            std::vector<int> & nnIdx_vec, \
                            std::vector<FloatT> & dists_vec, \
                            const FloatT max_dist, \
                            bool asu=false);

        const std::vector< vec3<int> >
            find_pivot_patterns(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving, const bool strict);

        void find_pivot_matches(const FloatT rmsd_tollerance, \
                                vec3<int> & pivot,  \
                                std::vector< vec3<int> > & pivot_matches, \
                                const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving);

        //const std::pair< mat3< FloatT >, vec3< FloatT > >
        const int
            compute_pivot_transformation(const vec3<int> & pivot, \
                                         const vec3<int> & pivot_match, \
                                         const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving, \
                                         scitbx::mat3< FloatT > &rotation, \
                                         scitbx::vec3< FloatT > &translation);


        const af::shared< vec3< FloatT > >
            coords_transform(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving, \
                             const mat3< FloatT > & rotation, \
                             const vec3< FloatT > & translation);



        inline void _progressbar(unsigned int i, unsigned int n, int width=50);



        // symmetry and pairs
        //cctbx::crystal::incremental_pairs<> incremental_pairs_;
        cctbx::uctbx::unit_cell unit_cell_;
        cctbx::sgtbx::space_group space_group_;
        cctbx::crystal::direct_space_asu::float_asu<> float_asu_;
        boost::shared_ptr< cctbx::crystal::direct_space_asu::asu_mappings<> > asu_mappings_;

        bool xtal_symm_initialized_;
    public:

        smatch(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & reference);

        smatch(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & reference, \
               const cctbx::uctbx::unit_cell & unit_cell, \
               const cctbx::sgtbx::space_group & space_group);


        ~smatch();


        void kdTest();

        const mat3< FloatT > get_rotation(int i) { return res_rot_[i]; };
        const vec3< FloatT > get_translation(int i) { return res_trans_[i]; };
        const std::vector<int> get_match(int i) { return res_match_[i]; };
        const FloatT get_rmsd(int i) { return res_rmsd_[i]; };




        // -------------------------------------------------------------------------------
        // returns fine-tuned RMS, updated rotrans_pair, and new match (of arbitrary length)
        FloatT finetune_transformation(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving, \
                                             mat3< FloatT > & rotation, \
                                             vec3< FloatT > & translation, \
                                             std::vector<int> & finetuned_match, \
                                       const bool strict);

        // symmetry-aware version of the above: finds close neghbors of pattrern moints in a xtal
        FloatT finetune_transformation_symm(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving, \
                                                  mat3< FloatT > & rotation, \
                                                  vec3< FloatT > & translation, \
                                                  std::vector<int> & finetuned_match, \
                                            const cctbx::uctbx::unit_cell &unit_cell,
                                            const cctbx::sgtbx::space_group &space_group,
                                            const bool strict);


         FloatT finetune_transformation_symm_fast(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving, \
                                                        mat3< FloatT > & rotation, \
                                                        vec3< FloatT > & translation, \
                                                        std::vector<int> & finetuned_match, \
                                                  const bool strict);


         // use symm_incr version if xtal symm was declared on init, standard finetune_transformation if not
         FloatT finetune_transformation_wrapper(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving, \
                                                        mat3< FloatT > & rotation, \
                                                        vec3< FloatT > & translation, \
                                                        std::vector<int> & finetuned_match, \
                                                  const bool strict);


        // redundancy helper - common code for the two above
        FloatT finetune_matching_main(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving, \
                                      const scitbx::af::const_ref< vec3<FloatT> > reference_cpy, \
                                            mat3< FloatT > & rotation, \
                                            vec3< FloatT > & translation, \
                                            std::vector<int> & finetuned_match, const bool strict);
                                      //const boost_graph & graph, \
                                      const bool strict);


        // -------------------------------------------------------------------------------


        const scitbx::mat3<FloatT>
            superpose(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & reference, \
                      const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving);

        //add all the init stuff to this finc (incl. kd-tree)
        void set_ref(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & reference_site);

        int find_match(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving, \
                        const FloatT rmsd_tollerance, \
                        const bool strict);

};

template <typename FloatT>
const FloatT smatch<FloatT>::kdQuery_eps = 0.0;

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// a simple function that draws a progressbar

template <typename FloatT>
inline void
smatch<FloatT>::_progressbar(unsigned int ni, unsigned int ntot, int width){


    if ( (ni != ntot) && (ni % (ntot/width+1) != 0) ) return;

    float ratio  =  (float)ni/(float)ntot;
    int   c      =  ratio * width;

    std::printf(" --> Matching patterns: %3i%% [", (int)(ratio*100));
    FORL(i, 0, c) std::cout << "=";
    FORL(i, c, width) std::cout << " ";
    std::cout << "]";
    if ( ni < ntot )
        std::cout <<"\r"<< std::flush;
    else
        std::cout<<"\n";


};


// ----------------------------------------------------------------------------
//default constructor, no symmetry

template <typename FloatT>
smatch<FloatT>::smatch(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & reference)
{
    reference_ = reference;
    create_kdtree(reference_);
    //no xtal symmetry info given on input
    xtal_symm_initialized_ = false;
};

// ----------------------------------------------------------------------------
//a contructor that defines symmetry-related objects

template <typename FloatT>
smatch<FloatT>::smatch(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & reference, \
                       const cctbx::uctbx::unit_cell & unit_cell, \
                       const cctbx::sgtbx::space_group & space_group)
{

    xtal_symm_initialized_ = true;



    unit_cell_ = unit_cell;
    space_group_ = space_group;

    reference_ = reference;
    create_kdtree(reference_);

    FloatT distance_cutoff = 2.0;
    FloatT buffer_thickness = 10.0;

    const FloatT min_distance_sym_equiv = 0.5;
    af::shared< vec3< FloatT > > sites_frac;



    cctbx::sgtbx::asu::direct_space_asu metric_free_asu( space_group.type() );
    float_asu_ = metric_free_asu.as_float_asu(unit_cell, 1.0E-6);


    asu_mappings_.reset(new cctbx::crystal::direct_space_asu::asu_mappings<> \
                                        (space_group_, float_asu_, buffer_thickness));



    asu_mappings_->process_sites_cart(reference_, min_distance_sym_equiv);


};

// ----------------------------------------------------------------------------
// default destructor

template <typename FloatT>
smatch<FloatT>::~smatch(){

    res_match_.clear();
    res_rot_.clear();
    res_trans_.clear();
    res_rmsd_.clear();
    delete reference_kdTree_;
    delete dataPts_;
};

// ----------------------------------------------------------------------------
//helper function that re-assigns reference points set, no symmetry!

template <typename FloatT>
void
smatch<FloatT>::set_ref(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & reference_site)
{
    reference_ = reference_site;
    create_kdtree(reference_);
    xtal_symm_initialized_ = false;

};


// ----------------------------------------------------------------------------
//a simple KD-tree object test

template <typename FloatT>
void
smatch<FloatT>::kdTest()
{

    std::vector<int> nnIdx_vec(0);
    std::vector<FloatT> dists_vec(0);

    query_kdTree(reference_[0], nnIdx_vec, dists_vec, 20.);


    std::cout << "\tNN:\tIndex\tDistance\n";
    for (unsigned int i = 0; i < nnIdx_vec.size(); ++i) {
        std::cout << "\t" << i << "\t" << nnIdx_vec[i] << "\t" << dists_vec[i] << "\n";
    }


};

// ----------------------------------------------------------------------------
//a function with self-explanatory name

template <typename FloatT>
void
smatch<FloatT>::query_kdTree(const vec3<FloatT> query_point, \
                                std::vector<int>  &nnIdx_vec, \
                                std::vector<FloatT> &dists_vec, \
                                const FloatT max_dist, \
                                bool asu)
{

    ANNidxArray  nnIdx;
    ANNdistArray dists;

    nnIdx = new ANNidx[max_kdQuery_size];          // allocate near neigh indices
    dists = new ANNdist[max_kdQuery_size];         // allocate near neighbor dists

    ANNdist max_radius_sq = max_dist*max_dist;
    ANNpoint    queryPt;
    queryPt = (ANNcoord*)query_point.begin();

    reference_kdTree_->annkFRSearch(
                            queryPt,
                            max_radius_sq,
                            max_kdQuery_size,
                            nnIdx,
                            dists,
                            kdQuery_eps);

    nnIdx_vec.clear();
    dists_vec.clear();
    int i = 0;
    while (nnIdx[i]>=0 && i<max_kdQuery_size) {
        nnIdx_vec.push_back( nnIdx[i] );
        dists_vec.push_back( sqrt(dists[i]) );
        i++;
    }

    delete nnIdx;
    delete dists;

};

// ----------------------------------------------------------------------------
// creator of all theKD-tree related objects

template <typename FloatT>
void
smatch<FloatT>::create_kdtree(scitbx::af::const_ref< vec3<FloatT> > points, bool asu)
{

    int n = points.size();

    dataPts_ = annAllocPts(n, dimm);             // allocate data points

    for(int i=0; i<n; ++i) dataPts_[i] = (ANNcoord*)points[i].begin();

    reference_kdTree_ = new ANNkd_tree(          // build search structure
                            dataPts_,            // the data points
                            n,                   // number of points
                            dimm);               // dimension of space


};

// ----------------------------------------------------------------------------
// a simple helper function that transorms a set of cartesian coords given
// rotation martic and translation vector

template <typename FloatT>
const af::shared< vec3< FloatT > >
smatch<FloatT>::coords_transform(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving, \
                                 const mat3< FloatT > & rotation, \
                                 const vec3< FloatT > & translation)
{

    af::shared< vec3< FloatT > > moving_moved;
    moving_moved.reserve( moving.size() );

    std::transform(
      moving.begin(),
      moving.end(),
      std::back_inserter( moving_moved ),
      boost::lambda::ret< vec3< FloatT > >(
          boost::lambda::ret< vec3< FloatT > >(
              rotation * boost::lambda::_1 )
              + translation ) );

    return moving_moved;

};


// ----------------------------------------------------------------------------
//scitbx/math/boost_python/math_ext.cpp
//scitbx/math/superpose.h

template <typename FloatT>
const scitbx::mat3<FloatT>
smatch<FloatT>::superpose(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & reference, \
                          const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving)
{
    return scitbx::math::superpose::superposition<>::kearsley_rotation( reference, moving );
};


// ----------------------------------------------------------------------------
//http://stackoverflow.com/questions/12991758/creating-all-possible-k-combinations-of-n-items-in-c

template <typename FloatT>
const std::vector< vec3<int> >
smatch<FloatT>::find_pivot_patterns(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving, const bool strict)
{
    int size = moving.size();
    std::vector< vec3<int> > pivot_patterns;

    pivot_patterns.clear();

    std::vector<bool> v(size);
    std::fill(v.begin() + dimm, v.end(), true);

    do {
        std::vector<int> tmp_vec(0);
        for (int i = 0; i < size; ++i) {
            if (!v[i]) {
                //std::cout << (i) << " ";
                tmp_vec.push_back(i);
            }
        }
        assert(tmp_vec.size() == 3);
        pivot_patterns.push_back(vec3<int>(tmp_vec[0], tmp_vec[1], tmp_vec[2]));
        //std::cout << "\n";
        // in strict mode 3 should be enough
        // however, if missing couterparts are allowed
        // (strict = true) one should test ALL pivots
        if(pivot_patterns.size()>3 && strict) break;
    } while (std::next_permutation(v.begin(), v.end()));

    return pivot_patterns;

};

//-----------------------------------------------------------------------------
// find all triplets (pivots) in a reference set that match a given triplet from pattern

template <typename FloatT>
void
smatch<FloatT>::find_pivot_matches(const FloatT rmsd_tolerance, \
                                   vec3<int> &pivot, \
                                   std::vector< vec3<int> > &pivot_matches, \
                                   const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving)
{
    assert ( pivot.size() == 3 );
    //std::cout<<"pivot: "<<pivot[0]<<", "<<pivot[1]<<", "<<pivot[2]<<"\n";

    int n = reference_.size();
    pivot_matches.clear();

    //std::vector< vec3<int> > res(0);
    std::vector< std::vector<int> > neigh1(n);
    std::vector< std::vector<int> > neigh2(n);


    FloatT d;
    FloatT d1 = (moving[pivot[0]] - moving[pivot[1]]).length();
    FloatT d2 = (moving[pivot[0]] - moving[pivot[2]]).length();
    FloatT d3 = (moving[pivot[1]] - moving[pivot[2]]).length();

    FloatT d_max = 1.5*rmsd_tolerance + std::max(d1,d2);

    // old-style approach, for testing only
    //for(int i=0; i<n-1; i++){
    //    for(int j=i+1; j<n; j++){
    //        d = (reference_[i] - reference_[j]).length();
    //        if( fabs(d-d1) <= (1.5*rmsd_tolerance) ){
    //            neigh1[i].push_back(j);
    //            neigh1[j].push_back(i);
    //        }
    //        if( fabs(d-d2) <= (1.5*rmsd_tolerance) ){
    //            neigh2[i].push_back(j);
    //            neigh2[j].push_back(i);
    //        }
    //    }
    //}
    std::vector<int> nnIdx_vec(0);
    std::vector<FloatT> dists_vec(0);

    for(int i=0; i<n-1; ++i){
        query_kdTree(reference_[i], nnIdx_vec, dists_vec, d_max);
        assert (nnIdx_vec.size() == dists_vec.size() );

        for(unsigned int j=0; j<nnIdx_vec.size(); ++j){
            if(nnIdx_vec[j]<=i) continue;
            if( fabs(dists_vec[j]-d1) <= (1.5*rmsd_tolerance) ){
                neigh1[i].push_back(nnIdx_vec[j]);
                neigh1[nnIdx_vec[j]].push_back(i);
            }
            if( fabs(dists_vec[j]-d2) <= (1.5*rmsd_tolerance) ){
                neigh2[i].push_back(nnIdx_vec[j]);
                neigh2[nnIdx_vec[j]].push_back(i);
            }
        }
    }

    for(int i=0; i<n; ++i)
        FOREACH(n1, neigh1[i])
            FOREACH(n2, neigh2[i])
                if(*n1 != *n2){
                    d = (reference_[*n1] - reference_[*n2]).length();
                    if( fabs(d-d3) <= (1.5*rmsd_tolerance) ){
                        vec3<int> v(i, *n1, *n2);
                        pivot_matches.push_back(v);
                    }
                }


};

//-----------------------------------------------------------------------------
// calc rotation matrix and translation vector for a given pair of pivots
// the two are of 3 elements by definition, and variable type :)

template <typename FloatT>
const int
smatch<FloatT>::compute_pivot_transformation(const vec3<int> &pivot, \
                                             const vec3<int> &pivot_match, \
                                             const scitbx::af::const_ref< scitbx::vec3<FloatT> > &moving, \
                                             scitbx::mat3< FloatT > &rotation, \
                                             scitbx::vec3< FloatT > &translation)
{


    assert(pivot.size() == pivot_match.size());

    af::shared< vec3< FloatT > > pivot_coords;
    af::shared< vec3< FloatT > > pivot_match_coords;

    for(unsigned int i=0; i<pivot.size(); ++i){
        pivot_coords.push_back( moving[pivot[i]] );
        pivot_match_coords.push_back( reference_[pivot_match[i]] );
    }

    scitbx::math::superpose::least_squares_fit<FloatT> lsq(pivot_match_coords, \
                                                           pivot_coords );
    //std::cout<<"rmsd = "<<lsq.other_sites_rmsd()<<"\n";

    //direct access to the kearsley algprithm, pretty useless here...
    //return scitbx::math::superpose::superposition<FloatT>::kearsley_rotation( pivot_coords.const_ref(), \
    //                                                               pivot_match_coords.const_ref() );

    rotation = lsq.get_rotation();
    translation = lsq.get_translation();
    return 1;//std::pair< mat3< FloatT >, vec3< FloatT > >(lsq.get_rotation(), lsq.get_translation());


};

//-----------------------------------------------------------------------------
// finetune match wrapper, a symm-enhanced function will be selected if symmetry was defined

template <typename FloatT>
FloatT 
smatch<FloatT>::finetune_transformation_wrapper(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving, \
                                                      mat3< FloatT > & rotation, \
                                                      vec3< FloatT > & translation, \
                                                      std::vector<int> & finetuned_match, \
                                                const bool strict){

    if( ! xtal_symm_initialized_ )
        return finetune_transformation(moving, \
                                       rotation, \
                                       translation, \
                                       finetuned_match, \
                                       strict);
    else
        return finetune_transformation_symm_fast(moving, \
                                                 rotation, \
                                                 translation, \
                                                 finetuned_match, \
                                                 /*unit_cell_, \*/
                                                 /*space_group_, \*/
                                                 strict);




};


//-----------------------------------------------------------------------------
// symmetry-enhanced finetiune matching func. only set of asu pairs for a pattern
// is calculated at each run

template <typename FloatT>
FloatT
smatch<FloatT>::finetune_transformation_symm_fast(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving, \
                                        mat3< FloatT > & rotation, \
                                        vec3< FloatT > & translation, \
                                        std::vector<int> & finetuned_match, \
                                        const bool strict){

    int mlen = moving.size();
    int n_vertices = moving.size() + reference_.size();
    finetuned_match.clear();
    finetuned_match.assign(mlen, -3);                                //set all to -3

    lemon::ListGraph lemon_graph;

    lemon::ListGraph::EdgeMap<float> lemon_weights(lemon_graph);
    std::vector< lemon::ListGraph::Node > lemon_nodes;
    std::map< lemon::ListGraph::Node,int > lemon_nodes_map;

    lemon::ListGraph::Node node;
    lemon::ListGraph::Edge edge;

    REP(i, n_vertices){
        node = lemon_graph.addNode();
        lemon_nodes.push_back(node);
        lemon_nodes_map[node] = i;
    }

    af::shared< vec3< FloatT > > sites_frac;
    af::shared< vec3< FloatT > > moved_moving;
    moved_moving = coords_transform(moving, rotation, translation);



    FloatT distance_cutoff = 2.0;
    FloatT buffer_thickness = 10.0;


    //make a local, shared copy of the reference_
    //if a match contains symmate, it will be added at the end (at the graph-matching step)
    scitbx::af::shared< vec3<FloatT> > reference_shared_copy;
    FOREACH(item, reference_){
        //if( float_asu_.is_inside( unit_cell_.fractionalize(vec3<FloatT>(*item)) ) )
        reference_shared_copy.push_back(*item);
        sites_frac.push_back( unit_cell_.fractionalize(vec3<FloatT>(*item)) );
    }


    FOREACH(xyz, moved_moving){
        sites_frac.push_back( unit_cell_.fractionalize(vec3<FloatT>(*xyz)) );
        asu_mappings_->process( unit_cell_.fractionalize(vec3<FloatT>(*xyz)) );

    }

    cctbx::crystal::pair_asu_table<> pair_asu_table(asu_mappings_);
    pair_asu_table.add_all_pairs(distance_cutoff);

    cctbx::crystal::pair_sym_table pair_sym_table = pair_asu_table.extract_pair_sym_table(false, true);


    for(unsigned i=0; i<mlen; ++i){

        //std::printf("i: %d\n", i);
        //frac_i - pattern 'moving' set of points
        const cctbx::fractional<> frac_i = sites_frac[i+reference_.size()];
        const cctbx::crystal::pair_sym_dict pair_sym_dict = pair_sym_table[i+reference_.size()];
        //neighbors iteration
        for(cctbx::crystal::pair_sym_dict::const_iterator pair=pair_sym_dict.begin(); \
                                                            pair!=pair_sym_dict.end(); \
                                                            ++pair ) {
            const int j = pair->first;
            if(j>reference_.size()) continue; //skip matches with pattern 'moving' points (last mlen entries)


            //std::printf("  j: %d\n", j);
            //frac_j - target 'reference' set
            const cctbx::fractional<> frac_j = sites_frac[j];
            const cctbx::crystal::pair_sym_ops sym_ops = pair->second;

            // symops iteration
            for(cctbx::crystal::pair_sym_ops::const_iterator sym_op = sym_ops.begin();
                                                                sym_op!=sym_ops.end(); \
                                                                ++sym_op) {

                const cctbx::fractional<> frac_ji = (*sym_op) * frac_j;
                FloatT distance = unit_cell_.distance(frac_i, frac_ji);
                if(sym_op->is_unit_mx()){

                    if( lemon::findEdge(lemon_graph, lemon_nodes[i], lemon_nodes[j+mlen]) != lemon::INVALID ) continue;

                    edge = lemon_graph.addEdge( lemon_nodes[i], lemon_nodes[j+mlen] );
                    lemon_weights[ edge ] = 1.0/distance;
                }else{
                    reference_shared_copy.push_back( unit_cell_.orthogonalize(vec3<FloatT>(frac_ji)) );

                    node = lemon_graph.addNode();
                    lemon_nodes.push_back(node);
                    lemon_nodes_map[node] = mlen+reference_shared_copy.size()-1;

                    edge = lemon_graph.addEdge( lemon_nodes[i], node );
                    lemon_weights[ edge ] = 1.0/distance;


                }
                //std::printf("%i %i    %-20s %8.3f\n", lemon_nodes_map[lemon_graph.u(edge)], \
                                                      lemon_nodes_map[lemon_graph.v(edge)]-mlen, \
                                                      sym_op->as_xyz().c_str(), \
                                                      distance);
            }
        }
    }

    lemon::MaxWeightedMatching< lemon::ListGraph, lemon::ListGraph::EdgeMap<float> > \
                                                            mwm(lemon_graph, lemon_weights);
    mwm.run();

    for(unsigned int in=0; in<mlen; ++in){
        if(mwm.matching(lemon_nodes[in]) == lemon::INVALID) continue;
        /*
        std::cout << " + {" << lemon_nodes_map[lemon_nodes[in]] << ", "\
                     << lemon_nodes_map[mwm.mate(lemon_nodes[in])]-mlen << "}" <<" ";
        std::cout<<(moved_moving[in] - reference_shared_copy[lemon_nodes_map[mwm.mate(lemon_nodes[in])]-mlen]).length()<<"\n";
        */
        finetuned_match[in] = lemon_nodes_map[mwm.mate(lemon_nodes[in])]-mlen;
    }

    //remove ALL mappings added in that function
    REP(i, mlen) asu_mappings_->discard_last();

    return finetune_matching_main(moving, reference_shared_copy.const_ref(), rotation, translation, finetuned_match, strict);

};


//-----------------------------------------------------------------------------
// symmetry-enhanced finetiune matching func. a complete set of asu pairs is defined
// at each run


template <typename FloatT>
FloatT
smatch<FloatT>::finetune_transformation_symm(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving, \
                                        mat3< FloatT > & rotation, \
                                        vec3< FloatT > & translation, \
                                        std::vector<int> & finetuned_match, \
                                        const cctbx::uctbx::unit_cell &unit_cell,
                                        const cctbx::sgtbx::space_group &space_group,
                                        const bool strict){

    int mlen = moving.size();
    int n_vertices = moving.size() + reference_.size();
    finetuned_match.clear();
    finetuned_match.assign(mlen, -3);                                //set all to -3

    // graph init, booset version
    boost_graph graph( n_vertices );

    // and lemon one
    lemon::ListGraph lemon_graph;

    lemon::ListGraph::EdgeMap<float> lemon_weights(lemon_graph);
    std::vector< lemon::ListGraph::Node > lemon_nodes;
    std::map< lemon::ListGraph::Node,int > lemon_nodes_map;

    lemon::ListGraph::Node node;
    lemon::ListGraph::Edge edge;

    REP(i, n_vertices){
        node = lemon_graph.addNode();
        lemon_nodes.push_back(node);
        lemon_nodes_map[node] = i;
    }
    // -----

    FloatT distance_cutoff = 2.0;
    FloatT buffer_thickness = 10.0;

    const FloatT min_distance_sym_equiv = 0.5;
    af::shared< vec3< FloatT > > sites_frac;

    cctbx::sgtbx::asu::direct_space_asu metric_free_asu( space_group.type() );
    cctbx::crystal::direct_space_asu::float_asu<> float_asu = metric_free_asu.as_float_asu(unit_cell, 1.0E-6);

    boost::shared_ptr< cctbx::crystal::direct_space_asu::asu_mappings<> > \
                    asu_mappings( new cctbx::crystal::direct_space_asu::asu_mappings<> \
                                        (space_group, float_asu, buffer_thickness) );

    af::shared< vec3< FloatT > > moved_moving;
    moved_moving = coords_transform(moving, rotation, translation);


    //make a local, shared copy of the reference_
    //if a match contains symmate, it will be added at the end (at the graph-matching step)
    scitbx::af::shared< vec3<FloatT> > reference_shared_copy;



    FOREACH(item, reference_)
        //if( float_asu_.is_inside( unit_cell.fractionalize(vec3<FloatT>(*item)) ) )
        reference_shared_copy.push_back(*item);


    FOREACH(xyz, moved_moving)
        sites_frac.push_back( unit_cell.fractionalize(vec3<FloatT>(*xyz)) );
    FOREACH(xyz, reference_)
        sites_frac.push_back( unit_cell.fractionalize(vec3<FloatT>(*xyz)) );


    asu_mappings->process_sites_frac(sites_frac.const_ref(), min_distance_sym_equiv);
    //FOREACH(xyz, moved_moving)
    //    asu_mappings_->process(*xyz);

    //asu_mappings_->process_sites_cart(moved_moving.const_ref(), min_distance_sym_equiv);

    cctbx::crystal::pair_asu_table<> pair_asu_table(asu_mappings);
    pair_asu_table.add_all_pairs(distance_cutoff);

    cctbx::crystal::pair_sym_table pair_sym_table = pair_asu_table.extract_pair_sym_table(false, true);

    for(unsigned i=0; i<mlen; ++i){

        //std::printf("i: %d\n", i);
        const cctbx::fractional<> frac_i = sites_frac[i];
        const cctbx::crystal::pair_sym_dict pair_sym_dict = pair_sym_table[i];
        //neighbors iteration
        for(cctbx::crystal::pair_sym_dict::const_iterator pair=pair_sym_dict.begin(); \
                                                            pair!=pair_sym_dict.end(); \
                                                            ++pair ) {
            const int j = pair->first;
            if(j<mlen) continue; //match with patern point (first m entries)


            //std::printf("  j: %d\n", j);
            const cctbx::fractional<> frac_j = sites_frac[j];
            const cctbx::crystal::pair_sym_ops sym_ops = pair->second;

            // symops iteration
            for(cctbx::crystal::pair_sym_ops::const_iterator sym_op = sym_ops.begin();
                                                                sym_op!=sym_ops.end(); \
                                                                ++sym_op) {

                const cctbx::fractional<> frac_ji = (*sym_op) * frac_j;
                FloatT distance = unit_cell.distance(frac_i, frac_ji);
                if(sym_op->is_unit_mx()){
                    if( boost::edge(i, j, graph).second ) continue;
                    //boost
                    add_edge(i, j, graph);
                    //lemon
                    edge = lemon_graph.addEdge( lemon_nodes[i], lemon_nodes[j] );
                    lemon_weights[ edge ] = 1.0/distance;
                }else{
                    reference_shared_copy.push_back( unit_cell.orthogonalize(vec3<FloatT>(frac_ji)) );
                    //boost
                    add_edge(i, mlen+reference_shared_copy.size()-1, graph);

                    //lemon
                    node = lemon_graph.addNode();
                    lemon_nodes.push_back(node);
                    lemon_nodes_map[node] = mlen+reference_shared_copy.size()-1;

                    edge = lemon_graph.addEdge( lemon_nodes[i], node );
                    lemon_weights[ edge ] = 1.0/distance;


                }
                //std::printf("    %-20s %8.3f\n", sym_op->as_xyz().c_str(), distance);
            }
        }
    }

    lemon::MaxWeightedMatching< lemon::ListGraph, lemon::ListGraph::EdgeMap<float> > \
                                                            mwm(lemon_graph, lemon_weights);
    mwm.run();

    for(unsigned int in=0; in<mlen; ++in){
        if(mwm.matching(lemon_nodes[in]) == lemon::INVALID) continue;
        /*
        std::cout << " * {" << lemon_nodes_map[lemon_nodes[in]] << ", "\
                     << lemon_nodes_map[mwm.mate(lemon_nodes[in])]-mlen << "}" <<" ";
        std::cout<<(moved_moving[in] - reference_shared_copy[lemon_nodes_map[mwm.mate(lemon_nodes[in])]-mlen]).length()<<"\n";
        */
        finetuned_match[in] = lemon_nodes_map[mwm.mate(lemon_nodes[in])]-mlen;

    }


    //********************

    //// find graph matching and process the results
    //std::vector<boost::graph_traits<boost_graph>::vertex_descriptor> mate(n_vertices);
    //bool success = checked_edmonds_maximum_cardinality_matching(graph, &mate[0]);
    //assert(success);


    //boost::graph_traits<boost_graph>::vertex_iterator vi, vi_end;
    //for(boost::tie(vi,vi_end) = vertices(graph); vi != vi_end; ++vi)
    //    if (mate[*vi] != boost::graph_traits<boost_graph>::null_vertex() && *vi < mate[*vi]){
    //        //std::cout << "{" << *vi << ", " << mate[*vi]-mlen << "}" <<" ";
    //        //std::cout<<(moved_moving[*vi] - reference_[mate[*vi]-mlen]).length()<<"\n";
    //        finetuned_match[*vi] = mate[*vi]-mlen;
    //    }

    //********************

    return finetune_matching_main(moving, reference_shared_copy.const_ref(), rotation, translation, finetuned_match, strict);

};

//-----------------------------------------------------------------------------
// standard finetune_transformation function, no xtal symmetry is used

template <typename FloatT>
FloatT
smatch<FloatT>::finetune_transformation(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving, \
                                        mat3< FloatT > & rotation, \
                                        vec3< FloatT > & translation, \
                                        std::vector<int> & finetuned_match, \
                                        const bool strict)
{

    std::vector<int> nnIdx_vec(0);
    std::vector<FloatT> dists_vec(0);

    //make a local, shared copy of the reference_
    //if a match contains symmate, it will be added at the end
    scitbx::af::shared< vec3<FloatT> > reference_shared_copy;
    FOREACH(item, reference_)
       reference_shared_copy.push_back(*item);


    int mlen = moving.size();
    int n_vertices = moving.size() + reference_.size();
    finetuned_match.clear();
    finetuned_match.assign(mlen, -3);                                //set all to -3

    //boost
    boost_graph graph( n_vertices );

    //init: lemon version
    lemon::ListGraph lemon_graph;

    lemon::ListGraph::EdgeMap<float> lemon_weights(lemon_graph);
    std::vector< lemon::ListGraph::Node > lemon_nodes;
    std::map< lemon::ListGraph::Node,int > lemon_nodes_map;

    lemon::ListGraph::Node node;
    lemon::ListGraph::Edge edge;

    REP(i, n_vertices){
        node = lemon_graph.addNode();
        lemon_nodes.push_back(node);
        lemon_nodes_map[node] = i;
    }


    af::shared< vec3< FloatT > > moved_moving;
    moved_moving = coords_transform(moving, rotation, translation);



    // define a close-neighbors graph

    for(unsigned int imov=0; imov<mlen; ++imov){
        query_kdTree(moved_moving[imov], nnIdx_vec, dists_vec, 2.0);
        //std::cout<<nnIdx_vec.size()<<"\n";
        for (unsigned int inn = 0; inn < nnIdx_vec.size(); ++inn){
            edge = lemon_graph.addEdge( lemon_nodes[imov], lemon_nodes[ mlen+nnIdx_vec[inn] ] );
            lemon_weights[ edge ] = 1.0/dists_vec[inn];

            //boost
            add_edge(imov, mlen+nnIdx_vec[inn], graph);
        }
    }

    lemon::MaxWeightedMatching< lemon::ListGraph, lemon::ListGraph::EdgeMap<float> > \
                                                            mwm(lemon_graph, lemon_weights);
    mwm.run();

    for(unsigned int in=0; in<mlen; ++in){
        if(mwm.matching(lemon_nodes[in]) == lemon::INVALID) continue;
        //std::cout << " * {" << lemon_nodes_map[lemon_nodes[in]] << ", "\
        //             << lemon_nodes_map[mwm.mate(lemon_nodes[in])]-mlen << "}" <<" ";
        //std::cout<<(moved_moving[in] - reference_[lemon_nodes_map[mwm.mate(lemon_nodes[in])]-mlen]).length()<<"\n";
        finetuned_match[in] = lemon_nodes_map[mwm.mate(lemon_nodes[in])]-mlen;

    }


    //********************
    /*
    // find graph matching and process the results
    std::vector<boost::graph_traits<boost_graph>::vertex_descriptor> mate(n_vertices);
    bool success = checked_edmonds_maximum_cardinality_matching(graph, &mate[0]);
    assert(success);


    boost::graph_traits<boost_graph>::vertex_iterator vi, vi_end;
    for(boost::tie(vi,vi_end) = vertices(graph); vi != vi_end; ++vi)
        if (mate[*vi] != boost::graph_traits<boost_graph>::null_vertex() && *vi < mate[*vi]){
            std::cout << "{" << *vi << ", " << mate[*vi]-mlen << "}" <<" ";
            std::cout<<(moved_moving[*vi] - reference_[mate[*vi]-mlen]).length()<<"\n";
            //finetuned_match[*vi] = mate[*vi]-mlen;
        }
    */
    //********************


    return finetune_matching_main(moving, reference_shared_copy.const_ref(), rotation, translation, finetuned_match, strict);

};

//-----------------------------------------------------------------------------
// core part of the finetune_transformation fucntion (shared by all versions)

template <typename FloatT>
FloatT
smatch<FloatT>::finetune_matching_main(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving, \
                                       const scitbx::af::const_ref< vec3<FloatT> > reference_cpy, \
                                             mat3< FloatT > & rotation, \
                                             vec3< FloatT > & translation, \
                                             std::vector<int> & finetuned_match, const bool strict){
                                       //const boost_graph & graph, \
                                       //const bool strict){

    int mlen = moving.size();
    int n_vertices = moving.size() + reference_.size();
    /*
    // find graph matching and process the results
    std::vector<boost::graph_traits<boost_graph>::vertex_descriptor> mate(n_vertices);
    bool success = checked_edmonds_maximum_cardinality_matching(graph, &mate[0]);
    assert(success);


    boost::graph_traits<boost_graph>::vertex_iterator vi, vi_end;
    for(boost::tie(vi,vi_end) = vertices(graph); vi != vi_end; ++vi)
        if (mate[*vi] != boost::graph_traits<boost_graph>::null_vertex() && *vi < mate[*vi]){
            //std::cout << "{" << *vi << ", " << mate[*vi]-mlen << "}" <<" ";
            //std::cout<<(moved_moving[*vi] - reference_[mate[*vi]-mlen]).length()<<"\n";
            finetuned_match[*vi] = mate[*vi]-mlen;
        }
    */
    //std::cout << std::count(finetuned_match.begin(), finetuned_match.end(), -3) << std::endl;

    // three missing or none at all?
    if( std::count(finetuned_match.begin(), finetuned_match.end(), -3)>(strict ? 0 : (mlen-3)) )
        return 999.9;

    // superpose mev and ref points that match roughly
    af::shared< vec3< FloatT > > sel_mov_coords;
    af::shared< vec3< FloatT > > sel_ref_coords;

    for(unsigned int i=0; i<finetuned_match.size(); ++i){
        if( finetuned_match[i]<0 ) continue;
        sel_mov_coords.push_back( moving[i] );
        sel_ref_coords.push_back( reference_cpy[finetuned_match[i]] );
    }

    scitbx::math::superpose::least_squares_fit<FloatT> lsq(sel_ref_coords, \
                                                           sel_mov_coords );
    //std::cout<<"rmsd = "<<lsq.other_sites_rmsd()<<"\n";

    rotation = lsq.get_rotation();
    translation = lsq.get_translation();

    return lsq.other_sites_rmsd();

};

//-----------------------------------------------------------------------------

template <typename FloatT>
int
smatch<FloatT>::find_match(const scitbx::af::const_ref< scitbx::vec3<FloatT> > & moving, \
                           const FloatT rmsd_tollerance, \
                           const bool strict)
{
    // moving pattern size should be at least 3,
    // and no larger than reference_ pattern
    assert( moving.size() > 2 );
    assert( reference_.size() >= moving.size() );


    res_match_.clear();
    res_rot_.clear();
    res_trans_.clear();
    res_rmsd_.clear();


    std::vector< vec3<int> > pivot_patterns;
    std::vector< vec3<int> > pivot_matches;
    FloatT rms;

    std::vector<int> finetuned_match;
    scitbx::mat3< FloatT > rotation;
    scitbx::vec3< FloatT > translation;


    pivot_patterns = find_pivot_patterns(moving, strict);

    unsigned int idx=0;
    unsigned int npivots = pivot_patterns.size();

    FOREACH(pivot, pivot_patterns){
        _progressbar(++idx, npivots, 50);

        find_pivot_matches(rmsd_tollerance, *pivot, pivot_matches, moving);

        FOREACH(pivot_match, pivot_matches){
            compute_pivot_transformation(*pivot, \
                                    *pivot_match, \
                                     moving, \
                                     rotation, \
                                     translation);

            //std::cout<<"pivot : "<<(*pivot)[0]<<", "<<(*pivot)[1]<<", "<<(*pivot)[2]<<"\n";
            //std::cout<<"pivotm: "<<(*pivot_match)[0]<<", "<<(*pivot_match)[1]<<", "<<(*pivot_match)[2]<<"\n";

            rms = finetune_transformation_wrapper(moving, \
                                                  rotation, \
                                                  translation, \
                                                  finetuned_match, \
                                                  strict);
            if( rms > rmsd_tollerance || rms < 0 ) continue;
            // XXX this may cause some memory issues
            // XXX push_back makes a copy (?), and
            // XXX the original object is not cleared
            res_match_.push_back( finetuned_match );
            res_rot_.push_back( rotation );
            res_trans_.push_back( translation );
            res_rmsd_.push_back( rms );


        }
    }

    return res_match_.size();

};


#endif //SMATCH

