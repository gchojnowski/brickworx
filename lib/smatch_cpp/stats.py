#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================
import sys
import string

import random
random.seed(string.atoi(sys.argv[1]))

import iotbx
from iotbx.pdb import input as pdb_input
from cctbx import sgtbx
from cctbx.development import random_structure
import smatch
import timeit


pdb_str_4bps="""\
ATOM      1  P     A A   1       3.063   8.025  -4.135  1.00  0.00           P  
ATOM      2  O1P   A A   1       3.223   8.856  -5.350  1.00  0.00           O  
ATOM      3  O2P   A A   1       1.891   7.121  -4.118  1.00  0.00           O  
ATOM      4  O5'   A A   1       4.396   7.181  -3.881  1.00  0.00           O  
ATOM      5  C5'   A A   1       5.621   7.881  -3.587  1.00  0.00           C  
ATOM      6  C4'   A A   1       6.719   6.889  -3.258  1.00  0.00           C  
ATOM      7  O4'   A A   1       6.486   6.364  -1.919  1.00  0.00           O  
ATOM      8  C3'   A A   1       6.776   5.642  -4.140  1.00  0.00           C  
ATOM      9  O3'   A A   1       7.397   5.908  -5.390  1.00  0.00           O  
ATOM     10  C2'   A A   1       7.567   4.725  -3.208  1.00  0.00           C  
ATOM     11  O2'   A A   1       8.962   4.911  -3.068  1.00  0.00           O  
ATOM     12  C1'   A A   1       6.874   4.999  -1.877  1.00  0.00           C  
ATOM     13  N9    A A   1       5.666   4.158  -1.647  1.00  0.00           N  
ATOM     14  C8    A A   1       4.345   4.449  -1.900  1.00  0.00           C  
ATOM     15  N7    A A   1       3.524   3.496  -1.584  1.00  0.00           N  
ATOM     16  C5    A A   1       4.348   2.496  -1.086  1.00  0.00           C  
ATOM     17  C6    A A   1       4.082   1.215  -0.578  1.00  0.00           C  
ATOM     18  N6    A A   1       2.849   0.697  -0.485  1.00  0.00           N  
ATOM     19  N1    A A   1       5.134   0.481  -0.167  1.00  0.00           N  
ATOM     20  C2    A A   1       6.356   1.001  -0.262  1.00  0.00           C  
ATOM     21  N3    A A   1       6.725   2.179  -0.716  1.00  0.00           N  
ATOM     22  C4    A A   1       5.655   2.893  -1.121  1.00  0.00           C  
ATOM     23  P     U A   2       6.913   5.099  -6.683  1.00  0.00           P  
ATOM     24  O1P   U A   2       7.496   5.711  -7.898  1.00  0.00           O  
ATOM     25  O2P   U A   2       5.439   4.971  -6.666  1.00  0.00           O  
ATOM     26  O5'   U A   2       7.579   3.667  -6.429  1.00  0.00           O  
ATOM     27  C5'   U A   2       8.988   3.595  -6.135  1.00  0.00           C  
ATOM     28  C4'   U A   2       9.376   2.167  -5.806  1.00  0.00           C  
ATOM     29  O4'   U A   2       8.896   1.851  -4.467  1.00  0.00           O  
ATOM     30  C3'   U A   2       8.750   1.087  -6.688  1.00  0.00           C  
ATOM     31  O3'   U A   2       9.417   0.975  -7.938  1.00  0.00           O  
ATOM     32  C2'   U A   2       8.920  -0.112  -5.756  1.00  0.00           C  
ATOM     33  O2'   U A   2      10.194  -0.710  -5.616  1.00  0.00           O  
ATOM     34  C1'   U A   2       8.486   0.493  -4.425  1.00  0.00           C  
ATOM     35  N1    U A   2       7.014   0.438  -4.195  1.00  0.00           N  
ATOM     36  C2    U A   2       6.509  -0.720  -3.655  1.00  0.00           C  
ATOM     37  O2    U A   2       7.207  -1.677  -3.367  1.00  0.00           O  
ATOM     38  N3    U A   2       5.143  -0.735  -3.456  1.00  0.00           N  
ATOM     39  C4    U A   2       4.261   0.286  -3.745  1.00  0.00           C  
ATOM     40  O4    U A   2       3.056   0.157  -3.522  1.00  0.00           O  
ATOM     41  C5    U A   2       4.885   1.461  -4.308  1.00  0.00           C  
ATOM     42  C6    U A   2       6.213   1.503  -4.512  1.00  0.00           C  
ATOM     43  P     A A   3       8.572   0.556  -9.231  1.00  0.00           P  
ATOM     44  O1P   A A   3       9.394   0.756 -10.446  1.00  0.00           O  
ATOM     45  O2P   A A   3       7.262   1.245  -9.214  1.00  0.00           O  
ATOM     46  O5'   A A   3       8.359  -1.008  -8.977  1.00  0.00           O  
ATOM     47  C5'   A A   3       9.505  -1.830  -8.683  1.00  0.00           C  
ATOM     48  C4'   A A   3       9.061  -3.241  -8.354  1.00  0.00           C  
ATOM     49  O4'   A A   3       8.487  -3.248  -7.015  1.00  0.00           O  
ATOM     50  C3'   A A   3       7.950  -3.812  -9.236  1.00  0.00           C  
ATOM     51  O3'   A A   3       8.451  -4.266 -10.486  1.00  0.00           O  
ATOM     52  C2'   A A   3       7.446  -4.913  -8.304  1.00  0.00           C  
ATOM     53  O2'   A A   3       8.196  -6.104  -8.164  1.00  0.00           O  
ATOM     54  C1'   A A   3       7.407  -4.169  -6.973  1.00  0.00           C  
ATOM     55  N9    A A   3       6.139  -3.421  -6.743  1.00  0.00           N  
ATOM     56  C8    A A   3       5.854  -2.099  -6.996  1.00  0.00           C  
ATOM     57  N7    A A   3       4.646  -1.749  -6.680  1.00  0.00           N  
ATOM     58  C5    A A   3       4.079  -2.914  -6.182  1.00  0.00           C  
ATOM     59  C6    A A   3       2.804  -3.206  -5.674  1.00  0.00           C  
ATOM     60  N6    A A   3       1.820  -2.300  -5.581  1.00  0.00           N  
ATOM     61  N1    A A   3       2.574  -4.467  -5.263  1.00  0.00           N  
ATOM     62  C2    A A   3       3.556  -5.362  -5.358  1.00  0.00           C  
ATOM     63  N3    A A   3       4.781  -5.207  -5.812  1.00  0.00           N  
ATOM     64  C4    A A   3       4.984  -3.937  -6.217  1.00  0.00           C  
ATOM     65  P     A A   4       7.514  -4.163 -11.779  1.00  0.00           P  
ATOM     66  O1P   A A   4       8.313  -4.439 -12.994  1.00  0.00           O  
ATOM     67  O2P   A A   4       6.784  -2.876 -11.762  1.00  0.00           O  
ATOM     68  O5'   A A   4       6.490  -5.364 -11.525  1.00  0.00           O  
ATOM     69  C5'   A A   4       7.010  -6.675 -11.231  1.00  0.00           C  
ATOM     70  C4'   A A   4       5.874  -7.623 -10.902  1.00  0.00           C  
ATOM     71  O4'   A A   4       5.387  -7.318  -9.563  1.00  0.00           O  
ATOM     72  C3'   A A   4       4.631  -7.503 -11.784  1.00  0.00           C  
ATOM     73  O3'   A A   4       4.807  -8.156 -13.034  1.00  0.00           O  
ATOM     74  C2'   A A   4       3.612  -8.157 -10.852  1.00  0.00           C  
ATOM     75  O2'   A A   4       3.599  -9.564 -10.712  1.00  0.00           O  
ATOM     76  C1'   A A   4       3.981  -7.510  -9.521  1.00  0.00           C  
ATOM     77  N9    A A   4       3.318  -6.195  -9.291  1.00  0.00           N  
ATOM     78  C8    A A   4       3.793  -4.929  -9.544  1.00  0.00           C  
ATOM     79  N7    A A   4       2.965  -3.982  -9.228  1.00  0.00           N  
ATOM     80  C5    A A   4       1.858  -4.656  -8.730  1.00  0.00           C  
ATOM     81  C6    A A   4       0.628  -4.212  -8.222  1.00  0.00           C  
ATOM     82  N6    A A   4       0.289  -2.919  -8.129  1.00  0.00           N  
ATOM     83  N1    A A   4      -0.247  -5.150  -7.811  1.00  0.00           N  
ATOM     84  C2    A A   4       0.095  -6.433  -7.906  1.00  0.00           C  
ATOM     85  N3    A A   4       1.210  -6.965  -8.360  1.00  0.00           N  
ATOM     86  C4    A A   4       2.067  -6.006  -8.765  1.00  0.00           C  
ATOM     87  P     U B   5      -8.377  -1.902  -3.509  1.00  0.00           P  
ATOM     88  O1P   U B   5      -9.222  -1.943  -2.294  1.00  0.00           O  
ATOM     89  O2P   U B   5      -7.317  -0.869  -3.526  1.00  0.00           O  
ATOM     90  O5'   U B   5      -7.729  -3.341  -3.763  1.00  0.00           O  
ATOM     91  C5'   U B   5      -8.594  -4.454  -4.057  1.00  0.00           C  
ATOM     92  C4'   U B   5      -7.767  -5.681  -4.386  1.00  0.00           C  
ATOM     93  O4'   U B   5      -7.215  -5.525  -5.725  1.00  0.00           O  
ATOM     94  C3'   U B   5      -6.540  -5.913  -3.504  1.00  0.00           C  
ATOM     95  O3'   U B   5      -6.891  -6.491  -2.254  1.00  0.00           O  
ATOM     96  C2'   U B   5      -5.744  -6.826  -4.436  1.00  0.00           C  
ATOM     97  O2'   U B   5      -6.124  -8.181  -4.576  1.00  0.00           O  
ATOM     98  C1'   U B   5      -5.918  -6.101  -5.767  1.00  0.00           C  
ATOM     99  N1    U B   5      -4.915  -5.024  -5.997  1.00  0.00           N  
ATOM    100  C2    U B   5      -3.708  -5.398  -6.537  1.00  0.00           C  
ATOM    101  O2    U B   5      -3.440  -6.552  -6.825  1.00  0.00           O  
ATOM    102  N3    U B   5      -2.804  -4.373  -6.736  1.00  0.00           N  
ATOM    103  C4    U B   5      -3.001  -3.039  -6.447  1.00  0.00           C  
ATOM    104  O4    U B   5      -2.116  -2.211  -6.670  1.00  0.00           O  
ATOM    105  C5    U B   5      -4.298  -2.743  -5.884  1.00  0.00           C  
ATOM    106  C6    U B   5      -5.197  -3.721  -5.680  1.00  0.00           C  
ATOM    107  P     U B   6      -6.022  -6.126  -0.961  1.00  0.00           P  
ATOM    108  O1P   U B   6      -6.710  -6.617   0.254  1.00  0.00           O  
ATOM    109  O2P   U B   6      -5.688  -4.684  -0.978  1.00  0.00           O  
ATOM    110  O5'   U B   6      -4.699  -6.987  -1.215  1.00  0.00           O  
ATOM    111  C5'   U B   6      -4.826  -8.391  -1.509  1.00  0.00           C  
ATOM    112  C4'   U B   6      -3.467  -8.977  -1.838  1.00  0.00           C  
ATOM    113  O4'   U B   6      -3.086  -8.547  -3.177  1.00  0.00           O  
ATOM    114  C3'   U B   6      -2.309  -8.509  -0.956  1.00  0.00           C  
ATOM    115  O3'   U B   6      -2.293  -9.185   0.294  1.00  0.00           O  
ATOM    116  C2'   U B   6      -1.146  -8.847  -1.888  1.00  0.00           C  
ATOM    117  O2'   U B   6      -0.734 -10.193  -2.028  1.00  0.00           O  
ATOM    118  C1'   U B   6      -1.684  -8.332  -3.219  1.00  0.00           C  
ATOM    119  N1    U B   6      -1.422  -6.883  -3.449  1.00  0.00           N  
ATOM    120  C2    U B   6      -0.204  -6.546  -3.989  1.00  0.00           C  
ATOM    121  O2    U B   6       0.645  -7.372  -4.277  1.00  0.00           O  
ATOM    122  N3    U B   6       0.003  -5.195  -4.188  1.00  0.00           N  
ATOM    123  C4    U B   6      -0.884  -4.179  -3.899  1.00  0.00           C  
ATOM    124  O4    U B   6      -0.586  -3.003  -4.122  1.00  0.00           O  
ATOM    125  C5    U B   6      -2.135  -4.631  -3.336  1.00  0.00           C  
ATOM    126  C6    U B   6      -2.363  -5.939  -3.132  1.00  0.00           C  
ATOM    127  P     A B   7      -1.758  -8.408   1.587  1.00  0.00           P  
ATOM    128  O1P   A B   7      -2.072  -9.193   2.802  1.00  0.00           O  
ATOM    129  O2P   A B   7      -2.256  -7.014   1.570  1.00  0.00           O  
ATOM    130  O5'   A B   7      -0.180  -8.418   1.333  1.00  0.00           O  
ATOM    131  C5'   A B   7       0.473  -9.668   1.039  1.00  0.00           C  
ATOM    132  C4'   A B   7       1.932  -9.427   0.710  1.00  0.00           C  
ATOM    133  O4'   A B   7       2.020  -8.860  -0.629  1.00  0.00           O  
ATOM    134  C3'   A B   7       2.654  -8.408   1.592  1.00  0.00           C  
ATOM    135  O3'   A B   7       3.033  -8.968   2.842  1.00  0.00           O  
ATOM    136  C2'   A B   7       3.815  -8.064   0.660  1.00  0.00           C  
ATOM    137  O2'   A B   7       4.888  -8.974   0.520  1.00  0.00           O  
ATOM    138  C1'   A B   7       3.084  -7.921  -0.671  1.00  0.00           C  
ATOM    139  N9    A B   7       2.522  -6.560  -0.901  1.00  0.00           N  
ATOM    140  C8    A B   7       1.253  -6.091  -0.648  1.00  0.00           C  
ATOM    141  N7    A B   7       1.077  -4.846  -0.964  1.00  0.00           N  
ATOM    142  C5    A B   7       2.310  -4.449  -1.462  1.00  0.00           C  
ATOM    143  C6    A B   7       2.779  -3.228  -1.970  1.00  0.00           C  
ATOM    144  N6    A B   7       2.021  -2.126  -2.063  1.00  0.00           N  
ATOM    145  N1    A B   7       4.060  -3.178  -2.381  1.00  0.00           N  
ATOM    146  C2    A B   7       4.808  -4.276  -2.286  1.00  0.00           C  
ATOM    147  N3    A B   7       4.482  -5.467  -1.832  1.00  0.00           N  
ATOM    148  C4    A B   7       3.196  -5.489  -1.427  1.00  0.00           C  
ATOM    149  P     U B   8       3.063  -8.025   4.135  1.00  0.00           P  
ATOM    150  O1P   U B   8       3.223  -8.856   5.350  1.00  0.00           O  
ATOM    151  O2P   U B   8       1.891  -7.121   4.118  1.00  0.00           O  
ATOM    152  O5'   U B   8       4.397  -7.181   3.881  1.00  0.00           O  
ATOM    153  C5'   U B   8       5.621  -7.881   3.587  1.00  0.00           C  
ATOM    154  C4'   U B   8       6.719  -6.889   3.258  1.00  0.00           C  
ATOM    155  O4'   U B   8       6.486  -6.364   1.919  1.00  0.00           O  
ATOM    156  C3'   U B   8       6.776  -5.642   4.140  1.00  0.00           C  
ATOM    157  O3'   U B   8       7.397  -5.908   5.390  1.00  0.00           O  
ATOM    158  C2'   U B   8       7.567  -4.725   3.208  1.00  0.00           C  
ATOM    159  O2'   U B   8       8.962  -4.910   3.068  1.00  0.00           O  
ATOM    160  C1'   U B   8       6.874  -4.999   1.877  1.00  0.00           C  
ATOM    161  N1    U B   8       5.666  -4.158   1.647  1.00  0.00           N  
ATOM    162  C2    U B   8       5.867  -2.911   1.107  1.00  0.00           C  
ATOM    163  O2    U B   8       6.971  -2.482   0.819  1.00  0.00           O  
ATOM    164  N3    U B   8       4.725  -2.160   0.908  1.00  0.00           N  
ATOM    165  C4    U B   8       3.431  -2.543   1.197  1.00  0.00           C  
ATOM    166  O4    U B   8       2.487  -1.783   0.974  1.00  0.00           O  
ATOM    167  C5    U B   8       3.322  -3.869   1.760  1.00  0.00           C  
ATOM    168  C6    U B   8       4.416  -4.621   1.964  1.00  0.00           C  
END
"""

pdb_str_3bps="""\
ATOM      1  P     A A   1       3.063   8.025  -4.135  1.00  0.00           P  
ATOM      2  O1P   A A   1       3.223   8.856  -5.350  1.00  0.00           O  
ATOM      3  O2P   A A   1       1.891   7.121  -4.118  1.00  0.00           O  
ATOM      4  O5'   A A   1       4.396   7.181  -3.881  1.00  0.00           O  
ATOM      5  C5'   A A   1       5.621   7.881  -3.587  1.00  0.00           C  
ATOM      6  C4'   A A   1       6.719   6.889  -3.258  1.00  0.00           C  
ATOM      7  O4'   A A   1       6.486   6.364  -1.919  1.00  0.00           O  
ATOM      8  C3'   A A   1       6.776   5.642  -4.140  1.00  0.00           C  
ATOM      9  O3'   A A   1       7.397   5.908  -5.390  1.00  0.00           O  
ATOM     10  C2'   A A   1       7.567   4.725  -3.208  1.00  0.00           C  
ATOM     11  O2'   A A   1       8.962   4.911  -3.068  1.00  0.00           O  
ATOM     12  C1'   A A   1       6.874   4.999  -1.877  1.00  0.00           C  
ATOM     13  N9    A A   1       5.666   4.158  -1.647  1.00  0.00           N  
ATOM     14  C8    A A   1       4.345   4.449  -1.900  1.00  0.00           C  
ATOM     15  N7    A A   1       3.524   3.496  -1.584  1.00  0.00           N  
ATOM     16  C5    A A   1       4.348   2.496  -1.086  1.00  0.00           C  
ATOM     17  C6    A A   1       4.082   1.215  -0.578  1.00  0.00           C  
ATOM     18  N6    A A   1       2.849   0.697  -0.485  1.00  0.00           N  
ATOM     19  N1    A A   1       5.134   0.481  -0.167  1.00  0.00           N  
ATOM     20  C2    A A   1       6.356   1.001  -0.262  1.00  0.00           C  
ATOM     21  N3    A A   1       6.725   2.179  -0.716  1.00  0.00           N  
ATOM     22  C4    A A   1       5.655   2.893  -1.121  1.00  0.00           C  
ATOM     23  P     U A   2       6.913   5.099  -6.683  1.00  0.00           P  
ATOM     24  O1P   U A   2       7.496   5.711  -7.898  1.00  0.00           O  
ATOM     25  O2P   U A   2       5.439   4.971  -6.666  1.00  0.00           O  
ATOM     26  O5'   U A   2       7.579   3.667  -6.429  1.00  0.00           O  
ATOM     27  C5'   U A   2       8.988   3.595  -6.135  1.00  0.00           C  
ATOM     28  C4'   U A   2       9.376   2.167  -5.806  1.00  0.00           C  
ATOM     29  O4'   U A   2       8.896   1.851  -4.467  1.00  0.00           O  
ATOM     30  C3'   U A   2       8.750   1.087  -6.688  1.00  0.00           C  
ATOM     31  O3'   U A   2       9.417   0.975  -7.938  1.00  0.00           O  
ATOM     32  C2'   U A   2       8.920  -0.112  -5.756  1.00  0.00           C  
ATOM     33  O2'   U A   2      10.194  -0.710  -5.616  1.00  0.00           O  
ATOM     34  C1'   U A   2       8.486   0.493  -4.425  1.00  0.00           C  
ATOM     35  N1    U A   2       7.014   0.438  -4.195  1.00  0.00           N  
ATOM     36  C2    U A   2       6.509  -0.720  -3.655  1.00  0.00           C  
ATOM     37  O2    U A   2       7.207  -1.677  -3.367  1.00  0.00           O  
ATOM     38  N3    U A   2       5.143  -0.735  -3.456  1.00  0.00           N  
ATOM     39  C4    U A   2       4.261   0.286  -3.745  1.00  0.00           C  
ATOM     40  O4    U A   2       3.056   0.157  -3.522  1.00  0.00           O  
ATOM     41  C5    U A   2       4.885   1.461  -4.308  1.00  0.00           C  
ATOM     42  C6    U A   2       6.213   1.503  -4.512  1.00  0.00           C  
ATOM     43  P     A A   3       8.572   0.556  -9.231  1.00  0.00           P  
ATOM     44  O1P   A A   3       9.394   0.756 -10.446  1.00  0.00           O  
ATOM     45  O2P   A A   3       7.262   1.245  -9.214  1.00  0.00           O  
ATOM     46  O5'   A A   3       8.359  -1.008  -8.977  1.00  0.00           O  
ATOM     47  C5'   A A   3       9.505  -1.830  -8.683  1.00  0.00           C  
ATOM     48  C4'   A A   3       9.061  -3.241  -8.354  1.00  0.00           C  
ATOM     49  O4'   A A   3       8.487  -3.248  -7.015  1.00  0.00           O  
ATOM     50  C3'   A A   3       7.950  -3.812  -9.236  1.00  0.00           C  
ATOM     51  O3'   A A   3       8.451  -4.266 -10.486  1.00  0.00           O  
ATOM     52  C2'   A A   3       7.446  -4.913  -8.304  1.00  0.00           C  
ATOM     53  O2'   A A   3       8.196  -6.104  -8.164  1.00  0.00           O  
ATOM     54  C1'   A A   3       7.407  -4.169  -6.973  1.00  0.00           C  
ATOM     55  N9    A A   3       6.139  -3.421  -6.743  1.00  0.00           N  
ATOM     56  C8    A A   3       5.854  -2.099  -6.996  1.00  0.00           C  
ATOM     57  N7    A A   3       4.646  -1.749  -6.680  1.00  0.00           N  
ATOM     58  C5    A A   3       4.079  -2.914  -6.182  1.00  0.00           C  
ATOM     59  C6    A A   3       2.804  -3.206  -5.674  1.00  0.00           C  
ATOM     60  N6    A A   3       1.820  -2.300  -5.581  1.00  0.00           N  
ATOM     61  N1    A A   3       2.574  -4.467  -5.263  1.00  0.00           N  
ATOM     62  C2    A A   3       3.556  -5.362  -5.358  1.00  0.00           C  
ATOM     63  N3    A A   3       4.781  -5.207  -5.812  1.00  0.00           N  
ATOM     64  C4    A A   3       4.984  -3.937  -6.217  1.00  0.00           C  
ATOM     65  P     U B   4      -6.022  -6.126  -0.961  1.00  0.00           P  
ATOM     66  O1P   U B   4      -6.710  -6.617   0.254  1.00  0.00           O  
ATOM     67  O2P   U B   4      -5.688  -4.684  -0.978  1.00  0.00           O  
ATOM     68  O5'   U B   4      -4.699  -6.987  -1.215  1.00  0.00           O  
ATOM     69  C5'   U B   4      -4.826  -8.391  -1.509  1.00  0.00           C  
ATOM     70  C4'   U B   4      -3.467  -8.977  -1.838  1.00  0.00           C  
ATOM     71  O4'   U B   4      -3.086  -8.547  -3.177  1.00  0.00           O  
ATOM     72  C3'   U B   4      -2.309  -8.509  -0.956  1.00  0.00           C  
ATOM     73  O3'   U B   4      -2.293  -9.185   0.294  1.00  0.00           O  
ATOM     74  C2'   U B   4      -1.146  -8.847  -1.888  1.00  0.00           C  
ATOM     75  O2'   U B   4      -0.734 -10.193  -2.028  1.00  0.00           O  
ATOM     76  C1'   U B   4      -1.684  -8.332  -3.219  1.00  0.00           C  
ATOM     77  N1    U B   4      -1.422  -6.883  -3.449  1.00  0.00           N  
ATOM     78  C2    U B   4      -0.204  -6.546  -3.989  1.00  0.00           C  
ATOM     79  O2    U B   4       0.645  -7.372  -4.277  1.00  0.00           O  
ATOM     80  N3    U B   4       0.003  -5.195  -4.188  1.00  0.00           N  
ATOM     81  C4    U B   4      -0.884  -4.179  -3.899  1.00  0.00           C  
ATOM     82  O4    U B   4      -0.586  -3.003  -4.122  1.00  0.00           O  
ATOM     83  C5    U B   4      -2.135  -4.631  -3.336  1.00  0.00           C  
ATOM     84  C6    U B   4      -2.363  -5.939  -3.132  1.00  0.00           C  
ATOM     85  P     A B   5      -1.758  -8.408   1.587  1.00  0.00           P  
ATOM     86  O1P   A B   5      -2.072  -9.193   2.802  1.00  0.00           O  
ATOM     87  O2P   A B   5      -2.256  -7.014   1.570  1.00  0.00           O  
ATOM     88  O5'   A B   5      -0.180  -8.418   1.333  1.00  0.00           O  
ATOM     89  C5'   A B   5       0.473  -9.668   1.039  1.00  0.00           C  
ATOM     90  C4'   A B   5       1.932  -9.427   0.710  1.00  0.00           C  
ATOM     91  O4'   A B   5       2.020  -8.860  -0.629  1.00  0.00           O  
ATOM     92  C3'   A B   5       2.654  -8.408   1.592  1.00  0.00           C  
ATOM     93  O3'   A B   5       3.033  -8.968   2.842  1.00  0.00           O  
ATOM     94  C2'   A B   5       3.815  -8.064   0.660  1.00  0.00           C  
ATOM     95  O2'   A B   5       4.888  -8.974   0.520  1.00  0.00           O  
ATOM     96  C1'   A B   5       3.084  -7.921  -0.671  1.00  0.00           C  
ATOM     97  N9    A B   5       2.522  -6.560  -0.901  1.00  0.00           N  
ATOM     98  C8    A B   5       1.253  -6.091  -0.648  1.00  0.00           C  
ATOM     99  N7    A B   5       1.077  -4.846  -0.964  1.00  0.00           N  
ATOM    100  C5    A B   5       2.310  -4.449  -1.462  1.00  0.00           C  
ATOM    101  C6    A B   5       2.779  -3.228  -1.970  1.00  0.00           C  
ATOM    102  N6    A B   5       2.021  -2.126  -2.063  1.00  0.00           N  
ATOM    103  N1    A B   5       4.060  -3.178  -2.381  1.00  0.00           N  
ATOM    104  C2    A B   5       4.808  -4.276  -2.286  1.00  0.00           C  
ATOM    105  N3    A B   5       4.482  -5.467  -1.832  1.00  0.00           N  
ATOM    106  C4    A B   5       3.196  -5.489  -1.427  1.00  0.00           C  
ATOM    107  P     U B   6       3.063  -8.025   4.135  1.00  0.00           P  
ATOM    108  O1P   U B   6       3.223  -8.856   5.350  1.00  0.00           O  
ATOM    109  O2P   U B   6       1.891  -7.121   4.118  1.00  0.00           O  
ATOM    110  O5'   U B   6       4.397  -7.181   3.881  1.00  0.00           O  
ATOM    111  C5'   U B   6       5.621  -7.881   3.587  1.00  0.00           C  
ATOM    112  C4'   U B   6       6.719  -6.889   3.258  1.00  0.00           C  
ATOM    113  O4'   U B   6       6.486  -6.364   1.919  1.00  0.00           O  
ATOM    114  C3'   U B   6       6.776  -5.642   4.140  1.00  0.00           C  
ATOM    115  O3'   U B   6       7.397  -5.908   5.390  1.00  0.00           O  
ATOM    116  C2'   U B   6       7.567  -4.725   3.208  1.00  0.00           C  
ATOM    117  O2'   U B   6       8.962  -4.910   3.068  1.00  0.00           O  
ATOM    118  C1'   U B   6       6.874  -4.999   1.877  1.00  0.00           C  
ATOM    119  N1    U B   6       5.666  -4.158   1.647  1.00  0.00           N  
ATOM    120  C2    U B   6       5.867  -2.911   1.107  1.00  0.00           C  
ATOM    121  O2    U B   6       6.971  -2.482   0.819  1.00  0.00           O  
ATOM    122  N3    U B   6       4.725  -2.160   0.908  1.00  0.00           N  
ATOM    123  C4    U B   6       3.431  -2.543   1.197  1.00  0.00           C  
ATOM    124  O4    U B   6       2.487  -1.783   0.974  1.00  0.00           O  
ATOM    125  C5    U B   6       3.322  -3.869   1.760  1.00  0.00           C  
ATOM    126  C6    U B   6       4.416  -4.621   1.964  1.00  0.00           C  
END
"""


class match_stats:


    def __init__(self, ref_elements, \
                       pat_elements=None, \
                       element_type='P', \
                       volume_per_atom=150, \
                       pattern_pdb_str=pdb_str_4bps, \
                       random_pattern=False):

        self.ref_sites = random_structure.xray_structure(
                            sgtbx.space_group_info('hall: P 1'),
                            elements=[element_type]*ref_elements,
                            volume_per_atom=volume_per_atom, \
                            min_distance = 4.0, \
                            min_distance_sym_equiv=4.0).sites_cart()
        #min_distance=4.0

        if random_pattern:
            self.pat_sites = random_structure.xray_structure(
                                sgtbx.space_group_info('hall: P 1'),
                                elements=[element_type]*pat_elements,
                                volume_per_atom=volume_per_atom).sites_cart()
        else:
            frag = pdb_input(lines=pattern_pdb_str.split('\n'), source_info=None).construct_hierarchy()

            sel_cache = frag.atom_selection_cache()
            assert sel_cache.n_seq == frag.atoms_size()
            isel = sel_cache.iselection
            self.pat_sites = frag.atoms().select(isel(r"name P")).extract_xyz()



    def dump_sites(self, sites, fname):

        from iotbx import pdb

        output_structure = iotbx.pdb.hierarchy.root()
        output_structure.append_model(iotbx.pdb.hierarchy.model(id="0"))
        chain = iotbx.pdb.hierarchy.chain(id="X")
        output_structure.models()[0].append_chain(chain)

        for index, site in enumerate(sites):
            rg = pdb.hierarchy.residue_group(resseq="%i"%index)
            ag = pdb.hierarchy.atom_group(resname="YYY", altloc=" ")
            atom = iotbx.pdb.hierarchy.atom()

            ag.append_atom(atom)
            rg.append_atom_group(atom_group=ag)
            chain.append_residue_group(rg)

            atom.name = "P"
            atom.set_element('P')
            atom.set_xyz(site)
            atom.set_occ(1.0)
            atom.set_b( 10.0 )

        output_structure.write_pdb_file(fname)



    # -------------------------------------------------------------------------


    def match(self, max_rmsd=1.0):
        start = timeit.default_timer()

        supermatch_obj = smatch.supermatch(self.ref_sites, (), "")
        start = timeit.default_timer()
        res = supermatch_obj.find_matches(self.pat_sites, max_rmsd, True)
        stop = timeit.default_timer()
        return stop-start
        if res:
            return sorted(res, key=lambda x: x[-1])[0][-1]
        else:
            return 999.9
        #self.dump_sites(self.ref_sites, "ref.pdb")
        #self.dump_sites(self.pat_sites, "pat.pdb")


    # -------------------------------------------------------------------------
    # -------------------------------------------------------------------------



def main():


    for nref in range(500, 1000, 50):
        times = []
        for iter in range(3):
            xxx = match_stats(ref_elements=nref, pat_elements=6, random_pattern=False, pattern_pdb_str=pdb_str_3bps)
            t = xxx.match()
            del xxx
            times.append(t)
        print nref, sum(times)/len(times)
        continue


        #try:
        if 1:
            min_ret = 999.9
            for i in range(10):
                xxx = match_stats(ref_elements=100, pat_elements=3)
                ret = xxx.match()
                if ret<min_ret: min_ret=ret
            #print min_ret
            #except:
            #pass


if __name__=="__main__":
    main()
