#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================


EXTENDED_ATOMS = "P,O5?,C5?,O4?,C4?,O3?,C3?,C2?,C1?,O2?"
import numpy as np
from iotbx.pdb import common_residue_names_get_class
from scitbx.math import superpose
import re, os
import gzip

pdb_test = """\
CRYST1   50.000   50.000   50.000  90.00  90.00  90.00 P 1           1
ATOM      1  P   A   B   1      -0.561  24.930   3.240  1.00  0.00           P
ATOM      2  O1P A   B   1      -0.966  26.174   2.548  1.00  0.00           O
ATOM      3  O2P A   B   1      -0.794  23.669   2.498  1.00  0.00           O
ATOM      4  C5' A   B   1       1.374  26.026   4.623  1.00  0.00           C
ATOM      5  O5' A   B   1       0.976  25.026   3.665  1.00  0.00           O
ATOM      6  C4' A   B   1       2.839  25.855   4.972  1.00  0.00           C
ATOM      7  O4' A   B   1       2.976  24.701   5.851  1.00  0.00           O
ATOM      8  C3' A   B   1       3.771  25.542   3.802  1.00  0.00           C
ATOM      9  O3' A   B   1       4.091  26.708   3.055  1.00  0.00           O
ATOM     10  C2' A   B   1       4.938  24.934   4.579  1.00  0.00           C
ATOM     11  O2' A   B   1       5.819  25.782   5.290  1.00  0.00           O
ATOM     12  C1' A   B   1       4.193  24.029   5.557  1.00  0.00           C
ATOM     13  N1  A   B   1       5.956  19.269   4.795  1.00  0.00           N
ATOM     14  C2  A   B   1       6.492  20.351   5.354  1.00  0.00           C
ATOM     15  N3  A   B   1       5.970  21.548   5.514  1.00  0.00           N
ATOM     16  C4  A   B   1       4.720  21.597   5.013  1.00  0.00           C
ATOM     17  C5  A   B   1       4.040  20.575   4.413  1.00  0.00           C
ATOM     18  C6  A   B   1       4.705  19.343   4.302  1.00  0.00           C
ATOM     19  N6  A   B   1       4.160  18.257   3.737  1.00  0.00           N
ATOM     20  N7  A   B   1       2.776  20.995   4.024  1.00  0.00           N
ATOM     21  C8  A   B   1       2.732  22.238   4.394  1.00  0.00           C
ATOM     22  N9  A   B   1       3.882  22.681   5.007  1.00  0.00           N
ATOM     23  P   U   B   2       4.345  26.574   1.481  1.00  0.00           P
ATOM     24  O1P U   B   2       4.399  27.921   0.870  1.00  0.00           O
ATOM     25  O2P U   B   2       3.361  25.636   0.896  1.00  0.00           O
ATOM     26  C5' U   B   2       6.863  26.524   2.208  1.00  0.00           C
ATOM     27  O5' U   B   2       5.801  25.911   1.450  1.00  0.00           O
ATOM     28  C4' U   B   2       8.110  25.666   2.137  1.00  0.00           C
ATOM     29  O4' U   B   2       7.924  24.504   2.996  1.00  0.00           O
ATOM     30  C3' U   B   2       8.431  25.067   0.769  1.00  0.00           C
ATOM     31  O3' U   B   2       9.043  26.018  -0.092  1.00  0.00           O
ATOM     32  C2' U   B   2       9.339  23.922   1.214  1.00  0.00           C
ATOM     33  O2' U   B   2      10.669  24.200   1.605  1.00  0.00           O
ATOM     34  C1' U   B   2       8.556  23.379   2.405  1.00  0.00           C
ATOM     35  N1  U   B   2       7.514  22.381   2.029  1.00  0.00           N
ATOM     36  C2  U   B   2       7.919  21.075   1.899  1.00  0.00           C
ATOM     37  O2  U   B   2       9.071  20.716   2.075  1.00  0.00           O
ATOM     38  N3  U   B   2       6.928  20.178   1.551  1.00  0.00           N
ATOM     39  C4  U   B   2       5.598  20.472   1.327  1.00  0.00           C
ATOM     40  O4  U   B   2       4.802  19.583   1.021  1.00  0.00           O
ATOM     41  C5  U   B   2       5.270  21.869   1.488  1.00  0.00           C
ATOM     42  C6  U   B   2       6.214  22.763   1.828  1.00  0.00           C
ATOM     43  P   A   B   3       8.767  25.926  -1.667  1.00  0.00           P
ATOM     44  O1P A   B   3       9.277  27.147  -2.330  1.00  0.00           O
ATOM     45  O2P A   B   3       7.343  25.605  -1.904  1.00  0.00           O
ATOM     46  C5' A   B   3      11.059  24.656  -1.677  1.00  0.00           C
ATOM     47  O5' A   B   3       9.674  24.675  -2.074  1.00  0.00           O
ATOM     48  C4' A   B   3      11.691  23.332  -2.054  1.00  0.00           C
ATOM     49  O4' A   B   3      11.224  22.313  -1.123  1.00  0.00           O
ATOM     50  C3' A   B   3      11.310  22.780  -3.427  1.00  0.00           C
ATOM     51  O3' A   B   3      12.035  23.416  -4.471  1.00  0.00           O
ATOM     52  C2' A   B   3      11.661  21.309  -3.201  1.00  0.00           C
ATOM     53  O2' A   B   3      13.017  20.908  -3.214  1.00  0.00           O
ATOM     54  C1' A   B   3      11.069  21.082  -1.814  1.00  0.00           C
ATOM     55  N1  A   B   3       7.691  17.241  -2.268  1.00  0.00           N
ATOM     56  C2  A   B   3       9.022  17.282  -2.292  1.00  0.00           C
ATOM     57  N3  A   B   3       9.825  18.317  -2.167  1.00  0.00           N
ATOM     58  C4  A   B   3       9.118  19.452  -1.996  1.00  0.00           C
ATOM     59  C5  A   B   3       7.758  19.569  -1.950  1.00  0.00           C
ATOM     60  C6  A   B   3       7.011  18.389  -2.095  1.00  0.00           C
ATOM     61  N6  A   B   3       5.671  18.356  -2.069  1.00  0.00           N
ATOM     62  N7  A   B   3       7.391  20.893  -1.758  1.00  0.00           N
ATOM     63  C8  A   B   3       8.522  21.525  -1.696  1.00  0.00           C
ATOM     64  N9  A   B   3       9.626  20.714  -1.833  1.00  0.00           N
ATOM     65  P   U   B   4       2.407   9.043  -2.386  1.00  0.00           P
ATOM     66  O1P U   B   4       2.195   7.713  -1.773  1.00  0.00           O
ATOM     67  O2P U   B   4       1.662  10.165  -1.773  1.00  0.00           O
ATOM     68  C5' U   B   4       4.852   8.562  -3.197  1.00  0.00           C
ATOM     69  O5' U   B   4       3.969   9.382  -2.407  1.00  0.00           O
ATOM     70  C4' U   B   4       6.254   9.136  -3.173  1.00  0.00           C
ATOM     71  O4' U   B   4       6.290  10.315  -4.029  1.00  0.00           O
ATOM     72  C3' U   B   4       6.739   9.651  -1.817  1.00  0.00           C
ATOM     73  O3' U   B   4       7.165   8.588  -0.973  1.00  0.00           O
ATOM     74  C2' U   B   4       7.855  10.579  -2.297  1.00  0.00           C
ATOM     75  O2' U   B   4       9.082  10.027  -2.732  1.00  0.00           O
ATOM     76  C1' U   B   4       7.163  11.279  -3.463  1.00  0.00           C
ATOM     77  N1  U   B   4       6.370  12.474  -3.056  1.00  0.00           N
ATOM     78  C2  U   B   4       7.047  13.665  -2.944  1.00  0.00           C
ATOM     79  O2  U   B   4       8.241  13.772  -3.160  1.00  0.00           O
ATOM     80  N3  U   B   4       6.278  14.749  -2.566  1.00  0.00           N
ATOM     81  C4  U   B   4       4.925  14.741  -2.297  1.00  0.00           C
ATOM     82  O4  U   B   4       4.345  15.778  -1.967  1.00  0.00           O
ATOM     83  C5  U   B   4       4.303  13.446  -2.442  1.00  0.00           C
ATOM     84  C6  U   B   4       5.026  12.374  -2.809  1.00  0.00           C
ATOM     85  P   A   B   5       6.969   8.731   0.609  1.00  0.00           P
ATOM     86  O1P A   B   5       7.230   7.428   1.260  1.00  0.00           O
ATOM     87  O2P A   B   5       5.652   9.344   0.894  1.00  0.00           O
ATOM     88  C5' A   B   5       9.477   9.488   0.538  1.00  0.00           C
ATOM     89  O5' A   B   5       8.133   9.761   0.981  1.00  0.00           O
ATOM     90  C4' A   B   5      10.385  10.648   0.889  1.00  0.00           C
ATOM     91  O4' A   B   5      10.114  11.745  -0.030  1.00  0.00           O
ATOM     92  C3' A   B   5      10.176  11.263   2.273  1.00  0.00           C
ATOM     93  O3' A   B   5      10.786  10.483   3.293  1.00  0.00           O
ATOM     94  C2' A   B   5      10.822  12.626   2.029  1.00  0.00           C
ATOM     95  O2' A   B   5      12.231  12.732   1.996  1.00  0.00           O
ATOM     96  C1' A   B   5      10.245  12.978   0.662  1.00  0.00           C
ATOM     97  N1  A   B   5       7.773  17.445   1.214  1.00  0.00           N
ATOM     98  C2  A   B   5       9.065  17.124   1.195  1.00  0.00           C
ATOM     99  N3  A   B   5       9.627  15.944   1.047  1.00  0.00           N
ATOM    100  C4  A   B   5       8.690  14.983   0.903  1.00  0.00           C
ATOM    101  C5  A   B   5       7.334  15.157   0.902  1.00  0.00           C
ATOM    102  C6  A   B   5       6.859  16.467   1.068  1.00  0.00           C
ATOM    103  N6  A   B   5       5.556  16.783   1.088  1.00  0.00           N
ATOM    104  N7  A   B   5       6.691  13.940   0.727  1.00  0.00           N
ATOM    105  C8  A   B   5       7.660  13.084   0.631  1.00  0.00           C
ATOM    106  N9  A   B   5       8.913  13.643   0.727  1.00  0.00           N
ATOM    107  P   U   B   6      10.109  10.442   4.743  1.00  0.00           P
ATOM    108  O1P U   B   6      10.747   9.381   5.556  1.00  0.00           O
ATOM    109  O2P U   B   6       8.638  10.356   4.603  1.00  0.00           O
ATOM    110  C5' U   B   6      11.892  12.285   5.285  1.00  0.00           C
ATOM    111  O5' U   B   6      10.511  11.878   5.321  1.00  0.00           O
ATOM    112  C4' U   B   6      12.024  13.717   5.762  1.00  0.00           C
ATOM    113  O4' U   B   6      11.548  14.604   4.707  1.00  0.00           O
ATOM    114  C3' U   B   6      11.174  14.096   6.975  1.00  0.00           C
ATOM    115  O3' U   B   6      11.759  13.644   8.189  1.00  0.00           O
ATOM    116  C2' U   B   6      11.158  15.613   6.793  1.00  0.00           C
ATOM    117  O2' U   B   6      12.305  16.368   7.132  1.00  0.00           O
ATOM    118  C1' U   B   6      10.896  15.720   5.293  1.00  0.00           C
ATOM    119  N1  U   B   6       9.450  15.682   4.935  1.00  0.00           N
ATOM    120  C2  U   B   6       8.763  16.870   4.982  1.00  0.00           C
ATOM    121  O2  U   B   6       9.283  17.926   5.299  1.00  0.00           O
ATOM    122  N3  U   B   6       7.426  16.796   4.644  1.00  0.00           N
ATOM    123  C4  U   B   6       6.736  15.660   4.271  1.00  0.00           C
ATOM    124  O4  U   B   6       5.538  15.718   3.989  1.00  0.00           O
ATOM    125  C5  U   B   6       7.544  14.462   4.251  1.00  0.00           C
ATOM    126  C6  U   B   6       8.846  14.505   4.575  1.00  0.00           C
END
"""



import smatch
from cctbx import crystal
import cctbx.crystal.direct_space_asu
import numpy
import iotbx.pdb
from scitbx.math import euler_angles_as_matrix
from scitbx.array_family import flex
from scitbx.math import superpose_kearsley_rotation

import urllib2

from iotbx.pdb import input as pdb_input

from scitbx import matrix
import random
if 0:
    random.seed(0)
    flex.set_random_seed(0)

sel_str = r"name C3?"

def random_rotation():
  return euler_angles_as_matrix([random.uniform(0,360) for i in xrange(3)])




def ext_test(pdbid = "1ehz", max_rmsd = 1.0, verbose = False):

    pattern_h = iotbx.pdb.hierarchy.input(pdb_string=pdb_test).hierarchy

    target_str = urllib2.urlopen("http://www.pdb.org/pdb/files/%s.pdb" % pdbid)
    pdbin = pdb_input(source_info=None, lines=target_str.read())
    pdbin = pdb_input(source_info=None, lines=pdb_test.split('\n'))
    target_h = pdbin.construct_hierarchy()
    symm = pdbin.crystal_symmetry()
    uc = symm.unit_cell()

    rot = random_rotation()
    pattern_h.atoms().set_xyz(new_xyz=(rot.elems * pattern_h.atoms().extract_xyz()))



    print symm.space_group_info().type().lookup_symbol()
    print symm.unit_cell().parameters()

    if verbose: target_h.write_pdb_file("reference.pdb")
    if verbose: pattern_h.write_pdb_file("before.pdb")

    sel_cache = target_h.atom_selection_cache()
    isel = sel_cache.iselection
    reference_sites = target_h.atoms().select(isel(sel_str)).extract_xyz()

    sel_cache = pattern_h.atom_selection_cache()
    isel = sel_cache.iselection
    pattern_sites = pattern_h.atoms().select(isel(sel_str)).extract_xyz()


    if 1: # map phosphates to ASU

        xrs = target_h.extract_xray_structure(symm)

        asu_mappings = crystal.direct_space_asu.asu_mappings(
                                        space_group = symm.space_group(),
                                        asu = xrs.direct_space_asu().as_float_asu(),
                                        buffer_thickness = 0.0)

        mapped_sites = []
        for site in reference_sites:
            asu_mappings.process(uc.fractionalize(site))
            mapped_sites.append( asu_mappings.mappings()[-1][0].mapped_site() )


        reference_sites = flex.vec3_double( mapped_sites )

        from iotbx import pdb

        output_structure = iotbx.pdb.hierarchy.root()
        output_structure.append_model(iotbx.pdb.hierarchy.model(id="0"))
        chain = iotbx.pdb.hierarchy.chain(id="X")
        output_structure.models()[0].append_chain(chain)

        for index, site in enumerate(reference_sites):
            rg = pdb.hierarchy.residue_group(resseq="%i"%index)
            ag = pdb.hierarchy.atom_group(resname="YYY", altloc=" ")
            atom = iotbx.pdb.hierarchy.atom()

            ag.append_atom(atom)
            rg.append_atom_group(atom_group=ag)
            chain.append_residue_group(rg)

            atom.name = "P"
            atom.set_element('P')
            atom.set_xyz(site)
            atom.set_occ(1.0)
            atom.set_b( 10.0 )

        #output_structure.write_pdb_file("xyz.pdb")



    if verbose:
        pattern_h.show()
        target_h.show()
        print target_h.overall_counts().n_residues
        print pattern_h.extract_xray_structure().center_of_mass()


    # the calc_rmsd_extended must have a 'strict' match on input
    smatch_obj = smatch.supermatch(reference_sites, uc.parameters(), \
                            symm.space_group_info().type().lookup_symbol())
    res = smatch_obj.find_matches(pattern_sites, max_rmsd, True)

    # here the matches should be already finetuned, test it


    assert len(res) == 2

    for index,res_item in enumerate(res):
        fine = smatch_obj.finetune_match_wrapper(pattern_sites, \
                                                             res_item[1], \
                                                             res_item[2], \
                                                             True)

        assert res_item == fine
        rt = matrix.rt( (fine[1], fine[2]) )

        pcpy = pattern_h.deep_copy()
        pcpy.atoms().set_xyz(new_xyz=(rt * pattern_h.atoms().extract_xyz()))

        pcpy.write_pdb_file("match_%i.pdb" % index)




    exit(1)


    smatch_p1 = smatch.supermatch(reference_sites, reference_sites.max() + (90, 90, 90), "P1")
    smatch_p212121 = smatch.supermatch(reference_sites, (20, 20, 20) + (90, 90, 90), "P 21 21 21")
    crowded_cell_results = []
    for res_item in res:
        xxx = smatch.supermatch(reference_sites, (), "").finetune_match(pattern_sites, \
                                                                res_item[1], \
                                                                res_item[2], \
                                                                True)


        if 0:
            xxx_symm = smatch.supermatch(reference_sites, (), "").finetune_match_symm(pattern_sites, \
                                                                        res_item[1], \
                                                                        res_item[2], \
                                                                        reference_sites.max() + (90, 90, 90), \
                                                                        "P1", \
                                                                        True)

            #thant one should return different results in some of the cases
            yyy_symm = smatch.supermatch(reference_sites, (), "").finetune_match_symm(pattern_sites, \
                                                                        res_item[1], \
                                                                        res_item[2], \
                                                                        (20, 20, 20) + (90, 90, 90), \
                                                                        "P 21 21 21", \
                                                                        True)
        if 1:

            xxx_symm = smatch_p1.finetune_match_wrapper(pattern_sites, \
                                                             res_item[1], \
                                                             res_item[2], \
                                                             True)

            #thant one should return different results in some of the cases
            yyy_symm = smatch_p212121.finetune_match_wrapper(pattern_sites, \
                                                             res_item[1], \
                                                             res_item[2], \
                                                             True)



        assert res_item[-1] == xxx[-1] == xxx_symm[-1]
        crowded_cell_results.append( xxx_symm[-1] == yyy_symm[-1] )

    print "Got %i matches" % len(res)
    print "Got %i diffs in crowded cell" % crowded_cell_results.count(False)
    assert len(res) == 95
    assert crowded_cell_results.count(False) == 28
    unique_res = []
    for res_item in res:
            (match, rot, tran, rms) = res_item
            unique=True
            for unique_item in unique_res:
                if set(unique_item[0]) == set(match):
                    unique=False
                    break
            if unique:
                unique_res.append(res_item)


    for idx,match in enumerate(sorted(unique_res, key=lambda x: x[-1])):


        rt = matrix.rt( (match[1], match[2]) )

        pattern_h.atoms().set_xyz(new_xyz=(rt * pattern_h.atoms().extract_xyz()))
        if verbose: pattern_h.write_pdb_file("after.pdb")


def test(verbose=False):


    ih = iotbx.pdb.hierarchy.input(pdb_string=pdb_test).hierarchy
    if verbose:
        ih.show()
        print  ih.extract_xray_structure().center_of_mass()

    sel_cache = ih.atom_selection_cache()
    isel = sel_cache.iselection
    reference_sites = ih.atoms().select(isel(r"name P")).extract_xyz()

    if verbose: ih.write_pdb_file("before.pdb")
    rot = random_rotation()
    rh = ih.deep_copy()
    rh.atoms().set_xyz(new_xyz=(rot.elems * ih.atoms().extract_xyz()))

    if verbose: ih.write_pdb_file("intermediate.pdb")

    target_sites = rh.atoms().select(isel(r"name P")).extract_xyz()


    print "RMS = %.2f" % reference_sites.rms_difference(target_sites)


    back_rot = smatch.superpose(reference_sites, target_sites)

    rh.atoms().set_xyz(new_xyz=(back_rot * rh.atoms().extract_xyz()))
    if verbose: rh.write_pdb_file("after.pdb")

    if verbose:
        print target_sites.as_double().as_numpy_array()
        print numpy.array(target_sites)

    rms = reference_sites.rms_difference(rh.atoms().select(isel(r"name P")).extract_xyz())

    assert rms < 0.0001
    print "RMS = %.2f" % rms


def main():
    test()
    ext_test()



if __name__=="__main__":
    main()
