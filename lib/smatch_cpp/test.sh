#!/bin/bash

ABSPATH=$(cd "$(dirname "$0")"; pwd)
PROFILE=false

if [ ! -d $ABSPATH/virtualenv ] ; then
    cd $ABSPATH
    . _create_virtualenv

    if [ $? -gt 0 ]; then
        echo "pip failed, check your system settings..."
        exit 1;
    fi

    cd $ABSPATH
    . _install_cctbx

    if [ $? -gt 0 ]; then
        echo "CCTBX installation failed..."
        exit 1;
    fi
    echo "Installation complete"
fi

. $ABSPATH/virtualenv/bin/activate
. $ABSPATH/virtualenv/cctbx_build/setpaths.sh
if [ ! -f smatch.so ] ; then
    cd $ABSPATH
    ./virtualenv/cctbx_build/bin/libtbx.scons
fi
if $PROFILE; then
    libtbx.python -m cProfile -s time $ABSPATH/test.py $@
else
    libtbx.python $ABSPATH/test.py $@
fi

