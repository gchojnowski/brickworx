#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================


EXTENDED_ATOMS = "P,O5?,C5?,O4?,C4?,O3?,C3?,C2?,C1?,O2?"
#INIT_ATOM = r"C3'"
INIT_ATOM = r"P"

import numpy as np
from iotbx.pdb import common_residue_names_get_class
from scitbx.math import superpose
from cctbx.array_family import flex
import re, os
import gzip
from progressbar import ProgressBar, Percentage, Bar, ETA

stem_pdb_string = """\
REMARK    3DNA v2.1 (c) 2012 Dr. Xiang-Jun Lu (http://x3dna.org)
ATOM      1  P     A A   1       3.063   8.025  -4.135  1.00  0.00           P
ATOM      2  O1P   A A   1       3.223   8.856  -5.350  1.00  0.00           O
ATOM      3  O2P   A A   1       1.891   7.121  -4.118  1.00  0.00           O
ATOM      4  O5'   A A   1       4.396   7.181  -3.881  1.00  0.00           O
ATOM      5  C5'   A A   1       5.621   7.881  -3.587  1.00  0.00           C
ATOM      6  C4'   A A   1       6.719   6.889  -3.258  1.00  0.00           C
ATOM      7  O4'   A A   1       6.486   6.364  -1.919  1.00  0.00           O
ATOM      8  C3'   A A   1       6.776   5.642  -4.140  1.00  0.00           C
ATOM      9  O3'   A A   1       7.397   5.908  -5.390  1.00  0.00           O
ATOM     10  C2'   A A   1       7.567   4.725  -3.208  1.00  0.00           C
ATOM     11  O2'   A A   1       8.962   4.911  -3.068  1.00  0.00           O
ATOM     12  C1'   A A   1       6.874   4.999  -1.877  1.00  0.00           C
ATOM     13  N9    A A   1       5.666   4.158  -1.647  1.00  0.00           N
ATOM     14  C8    A A   1       4.345   4.449  -1.900  1.00  0.00           C
ATOM     15  N7    A A   1       3.524   3.496  -1.584  1.00  0.00           N
ATOM     16  C5    A A   1       4.348   2.496  -1.086  1.00  0.00           C
ATOM     17  C6    A A   1       4.082   1.215  -0.578  1.00  0.00           C
ATOM     18  N6    A A   1       2.849   0.697  -0.485  1.00  0.00           N
ATOM     19  N1    A A   1       5.134   0.481  -0.167  1.00  0.00           N
ATOM     20  C2    A A   1       6.356   1.001  -0.262  1.00  0.00           C
ATOM     21  N3    A A   1       6.725   2.179  -0.716  1.00  0.00           N
ATOM     22  C4    A A   1       5.655   2.893  -1.121  1.00  0.00           C
ATOM     23  P     U A   2       6.913   5.099  -6.683  1.00  0.00           P
ATOM     24  O1P   U A   2       7.496   5.711  -7.898  1.00  0.00           O
ATOM     25  O2P   U A   2       5.439   4.971  -6.666  1.00  0.00           O
ATOM     26  O5'   U A   2       7.579   3.667  -6.429  1.00  0.00           O
ATOM     27  C5'   U A   2       8.988   3.595  -6.135  1.00  0.00           C
ATOM     28  C4'   U A   2       9.376   2.167  -5.806  1.00  0.00           C
ATOM     29  O4'   U A   2       8.896   1.851  -4.467  1.00  0.00           O
ATOM     30  C3'   U A   2       8.750   1.087  -6.688  1.00  0.00           C
ATOM     31  O3'   U A   2       9.417   0.975  -7.938  1.00  0.00           O
ATOM     32  C2'   U A   2       8.920  -0.112  -5.756  1.00  0.00           C
ATOM     33  O2'   U A   2      10.194  -0.710  -5.616  1.00  0.00           O
ATOM     34  C1'   U A   2       8.486   0.493  -4.425  1.00  0.00           C
ATOM     35  N1    U A   2       7.014   0.438  -4.195  1.00  0.00           N
ATOM     36  C2    U A   2       6.509  -0.720  -3.655  1.00  0.00           C
ATOM     37  O2    U A   2       7.207  -1.677  -3.367  1.00  0.00           O
ATOM     38  N3    U A   2       5.143  -0.735  -3.456  1.00  0.00           N
ATOM     39  C4    U A   2       4.261   0.286  -3.745  1.00  0.00           C
ATOM     40  O4    U A   2       3.056   0.157  -3.522  1.00  0.00           O
ATOM     41  C5    U A   2       4.885   1.461  -4.308  1.00  0.00           C
ATOM     42  C6    U A   2       6.213   1.503  -4.512  1.00  0.00           C
ATOM     43  P     A A   3       8.572   0.556  -9.231  1.00  0.00           P
ATOM     44  O1P   A A   3       9.394   0.756 -10.446  1.00  0.00           O
ATOM     45  O2P   A A   3       7.262   1.245  -9.214  1.00  0.00           O
ATOM     46  O5'   A A   3       8.359  -1.008  -8.977  1.00  0.00           O
ATOM     47  C5'   A A   3       9.505  -1.830  -8.683  1.00  0.00           C
ATOM     48  C4'   A A   3       9.061  -3.241  -8.354  1.00  0.00           C
ATOM     49  O4'   A A   3       8.487  -3.248  -7.015  1.00  0.00           O
ATOM     50  C3'   A A   3       7.950  -3.812  -9.236  1.00  0.00           C
ATOM     51  O3'   A A   3       8.451  -4.266 -10.486  1.00  0.00           O
ATOM     52  C2'   A A   3       7.446  -4.913  -8.304  1.00  0.00           C
ATOM     53  O2'   A A   3       8.196  -6.104  -8.164  1.00  0.00           O
ATOM     54  C1'   A A   3       7.407  -4.169  -6.973  1.00  0.00           C
ATOM     55  N9    A A   3       6.139  -3.421  -6.743  1.00  0.00           N
ATOM     56  C8    A A   3       5.854  -2.099  -6.996  1.00  0.00           C
ATOM     57  N7    A A   3       4.646  -1.749  -6.680  1.00  0.00           N
ATOM     58  C5    A A   3       4.079  -2.914  -6.182  1.00  0.00           C
ATOM     59  C6    A A   3       2.804  -3.206  -5.674  1.00  0.00           C
ATOM     60  N6    A A   3       1.820  -2.300  -5.581  1.00  0.00           N
ATOM     61  N1    A A   3       2.574  -4.467  -5.263  1.00  0.00           N
ATOM     62  C2    A A   3       3.556  -5.362  -5.358  1.00  0.00           C
ATOM     63  N3    A A   3       4.781  -5.207  -5.812  1.00  0.00           N
ATOM     64  C4    A A   3       4.984  -3.937  -6.217  1.00  0.00           C
ATOM     65  P     U B   4      -6.022  -6.126  -0.961  1.00  0.00           P
ATOM     66  O1P   U B   4      -6.710  -6.617   0.254  1.00  0.00           O
ATOM     67  O2P   U B   4      -5.688  -4.684  -0.978  1.00  0.00           O
ATOM     68  O5'   U B   4      -4.699  -6.987  -1.215  1.00  0.00           O
ATOM     69  C5'   U B   4      -4.826  -8.391  -1.509  1.00  0.00           C
ATOM     70  C4'   U B   4      -3.467  -8.977  -1.838  1.00  0.00           C
ATOM     71  O4'   U B   4      -3.086  -8.547  -3.177  1.00  0.00           O
ATOM     72  C3'   U B   4      -2.309  -8.509  -0.956  1.00  0.00           C
ATOM     73  O3'   U B   4      -2.293  -9.185   0.294  1.00  0.00           O
ATOM     74  C2'   U B   4      -1.146  -8.847  -1.888  1.00  0.00           C
ATOM     75  O2'   U B   4      -0.734 -10.193  -2.028  1.00  0.00           O
ATOM     76  C1'   U B   4      -1.684  -8.332  -3.219  1.00  0.00           C
ATOM     77  N1    U B   4      -1.422  -6.883  -3.449  1.00  0.00           N
ATOM     78  C2    U B   4      -0.204  -6.546  -3.989  1.00  0.00           C
ATOM     79  O2    U B   4       0.645  -7.372  -4.277  1.00  0.00           O
ATOM     80  N3    U B   4       0.003  -5.195  -4.188  1.00  0.00           N
ATOM     81  C4    U B   4      -0.884  -4.179  -3.899  1.00  0.00           C
ATOM     82  O4    U B   4      -0.586  -3.003  -4.122  1.00  0.00           O
ATOM     83  C5    U B   4      -2.135  -4.631  -3.336  1.00  0.00           C
ATOM     84  C6    U B   4      -2.363  -5.939  -3.132  1.00  0.00           C
ATOM     85  P     A B   5      -1.758  -8.408   1.587  1.00  0.00           P
ATOM     86  O1P   A B   5      -2.072  -9.193   2.802  1.00  0.00           O
ATOM     87  O2P   A B   5      -2.256  -7.014   1.570  1.00  0.00           O
ATOM     88  O5'   A B   5      -0.180  -8.418   1.333  1.00  0.00           O
ATOM     89  C5'   A B   5       0.473  -9.668   1.039  1.00  0.00           C
ATOM     90  C4'   A B   5       1.932  -9.427   0.710  1.00  0.00           C
ATOM     91  O4'   A B   5       2.020  -8.860  -0.629  1.00  0.00           O
ATOM     92  C3'   A B   5       2.654  -8.408   1.592  1.00  0.00           C
ATOM     93  O3'   A B   5       3.033  -8.968   2.842  1.00  0.00           O
ATOM     94  C2'   A B   5       3.815  -8.064   0.660  1.00  0.00           C
ATOM     95  O2'   A B   5       4.888  -8.974   0.520  1.00  0.00           O
ATOM     96  C1'   A B   5       3.084  -7.921  -0.671  1.00  0.00           C
ATOM     97  N9    A B   5       2.522  -6.560  -0.901  1.00  0.00           N
ATOM     98  C8    A B   5       1.253  -6.091  -0.648  1.00  0.00           C
ATOM     99  N7    A B   5       1.077  -4.846  -0.964  1.00  0.00           N
ATOM    100  C5    A B   5       2.310  -4.449  -1.462  1.00  0.00           C
ATOM    101  C6    A B   5       2.779  -3.228  -1.970  1.00  0.00           C
ATOM    102  N6    A B   5       2.021  -2.126  -2.063  1.00  0.00           N
ATOM    103  N1    A B   5       4.060  -3.178  -2.381  1.00  0.00           N
ATOM    104  C2    A B   5       4.808  -4.276  -2.286  1.00  0.00           C
ATOM    105  N3    A B   5       4.482  -5.467  -1.832  1.00  0.00           N
ATOM    106  C4    A B   5       3.196  -5.489  -1.427  1.00  0.00           C
ATOM    107  P     U B   6       3.063  -8.025   4.135  1.00  0.00           P
ATOM    108  O1P   U B   6       3.223  -8.856   5.350  1.00  0.00           O
ATOM    109  O2P   U B   6       1.891  -7.121   4.118  1.00  0.00           O
ATOM    110  O5'   U B   6       4.397  -7.181   3.881  1.00  0.00           O
ATOM    111  C5'   U B   6       5.621  -7.881   3.587  1.00  0.00           C
ATOM    112  C4'   U B   6       6.719  -6.889   3.258  1.00  0.00           C
ATOM    113  O4'   U B   6       6.486  -6.364   1.919  1.00  0.00           O
ATOM    114  C3'   U B   6       6.776  -5.642   4.140  1.00  0.00           C
ATOM    115  O3'   U B   6       7.397  -5.908   5.390  1.00  0.00           O
ATOM    116  C2'   U B   6       7.567  -4.725   3.208  1.00  0.00           C
ATOM    117  O2'   U B   6       8.962  -4.910   3.068  1.00  0.00           O
ATOM    118  C1'   U B   6       6.874  -4.999   1.877  1.00  0.00           C
ATOM    119  N1    U B   6       5.666  -4.158   1.647  1.00  0.00           N
ATOM    120  C2    U B   6       5.867  -2.911   1.107  1.00  0.00           C
ATOM    121  O2    U B   6       6.971  -2.482   0.819  1.00  0.00           O
ATOM    122  N3    U B   6       4.725  -2.160   0.908  1.00  0.00           N
ATOM    123  C4    U B   6       3.431  -2.543   1.197  1.00  0.00           C
ATOM    124  O4    U B   6       2.487  -1.783   0.974  1.00  0.00           O
ATOM    125  C5    U B   6       3.322  -3.869   1.760  1.00  0.00           C
ATOM    126  C6    U B   6       4.416  -4.621   1.964  1.00  0.00           C
END
"""

import smatch
import numpy
import iotbx.pdb
from scitbx.math import euler_angles_as_matrix
from scitbx.array_family import flex
from scitbx.math import superpose_kearsley_rotation

import urllib2

from iotbx.pdb import input as pdb_input

from scitbx import matrix


class cover:

    def __init__(self, target_fname=None):
        self.target_fname = target_fname
        pass

    # -------------------------------------------------------------------------


    def read_points_extended(self, \
                             hierarchy, \
                             isel, \
                             accepted_atoms='P', \
                             simrna=False, \
                             remote=False, \
                             host=False):
        """
            read set of points from a PDB file fn.
            the output is a dict of lists of atom 
            positions from consecutive residues
            for requested atom types
            {given_atom_idx:[atom_positions from cons residues]}
        """

        # fpair_sub.c: residue_ident

        # identify uncommon residues in a way similar to RNAVIEW
        # identifying a residue as follows:
        #  R-base  Y-base  amino-acid, others [default]
        # (puRine)
        #   +1        0        -1        -2   [default]
        #

        def _get_atom_by_name(resi, name):
            for atom in resi.atoms():
                if atom.name.strip()==name:
                    return atom
            return None

    # -------------------------------------------------------------------------


        def _get_base_type(resi, dcrt=2.0, dcrt2=3.0):

            base_id = -2

            N9 = _get_atom_by_name(resi, 'N9')
            N1 = _get_atom_by_name(resi, 'N1')
            C2 = _get_atom_by_name(resi, 'C2')
            C6 = _get_atom_by_name(resi, 'C6')

            if N1 and C2 and C6:
                d1 = np.linalg.norm(np.array(N1.xyz, float) - np.array(C2.xyz, float))
                d2 = np.linalg.norm(np.array(N1.xyz, float) - np.array(C6.xyz, float))
                d3 = np.linalg.norm(np.array(C2.xyz, float) - np.array(C6.xyz, float))

                if d1<=dcrt and d2<=dcrt and d3<=dcrt2:
                    base_id = 0
                    if N9:
                        d3 = np.linalg.norm(np.array(N1.xyz, float) - np.array(N9.xyz, float))
                        if d3 >= 3.5 and d3 <= 4.5:
                            base_id = 1
                return base_id

            # try to detect an amino-acid
            CA = _get_atom_by_name(resi, 'CA')
            C  = _get_atom_by_name(resi, 'C')
            if not C:
                C  = _get_atom_by_name(resi, 'N')
            if C and CA:
                d = np.linalg.norm(np.array(CA.xyz, float) - np.array(C.xyz, float))
                if d <= dcrt:
                    base_id = -1
                return base_id
            return base_id


            # ------------------------------------------------------------------------

        atoms = accepted_atoms.split(",")
        points_dict = {}
        points = []
        motif_resids = []

        simrna_sel_string = r"(((resname G or resname A) and (name N9)) or ((resname U or resname C) and (name N1)))"


        #sel_cache = hierarchy.atom_selection_cache()
        #assert sel_cache.n_seq == hierarchy.atoms_size()
        #isel = sel_cache.iselection

        for resi in hierarchy.atom_groups():
            motif_resids.append((resi.resname.strip(), resi.parent().resid().strip()))

            if _get_base_type(resi)<0 and \
                not (common_residue_names_get_class(resi.resname.upper())=="common_rna_dna" or \
                    (common_residue_names_get_class(resi.resname.upper())=="other" and resi.resname.strip()=='N')):
                continue

            for idx,name in enumerate(atoms):
                try:
                    xyz = hierarchy.atoms().select(isel(r"name %s and resid %s and chain %s and model '%s'"%\
                                                        ( name, resi.parent().resid(),\
                                                        resi.parent().parent().id,\
                                                        hierarchy.models()[0].id ))).extract_xyz()
                    assert xyz.size()==1
                    points_dict.setdefault(idx, []).append(list(xyz)[0])
                except:
                    points_dict.setdefault(idx, []).append(None)

            if simrna:
                try:
                    xyz = hierarchy.atoms().select(isel(r"%s and resid %s and model '%s'" % \
                                                            (simrna_sel_string,\
                                                            resi.parent().resid(),\
                                                            hierarchy.models()[0].id))).extract_xyz()
                    assert xyz.size()==1
                    points_dict.setdefault(idx+1, []).append(list(xyz)[0])
                except:
                    points_dict.setdefault(idx+1, []).append(None)
        return points_dict, motif_resids


    # -------------------------------------------------------------------------


    def calc_rmsd_extended(self, \
                           pattern_points, \
                           target_points, \
                           matching, \
                           accepted_atoms, \
                           simrna=False, \
                           input_in_motif=False, \
                           initially_accepted_atom="P"):


        try:
            init_atom_idx = accepted_atoms.split(',').index(initially_accepted_atom)
        except:
            print 'initially_accepted_atom is absent in accepted_atoms: ', fn

        # gchojnowski: prepare a mapping of matching indexes and res ids.
        # if selected atom in missing in a residue, the residue
        # index will be missing in the matching. here an appropriate
        # shift is applied
        idx_mapping = []
        if input_in_motif:
            for idx,item in enumerate(pattern_points[init_atom_idx]):
                if item: idx_mapping.append(idx)
        else:
            for idx,item in enumerate(target_points[init_atom_idx]):
                if item: idx_mapping.append(idx)

        matching = [idx_mapping[idx] for idx in matching]


        T = []
        P = []
        assert len(target_points.keys())==len(pattern_points.keys())
        for key in target_points.keys():
            for atm_idx in xrange(len(matching)):
                if input_in_motif:
                    try:
                        if pattern_points[key][matching[atm_idx]]!=None and \
                           target_points[key][atm_idx]!=None:
                            T.append( target_points[key][atm_idx] )
                            P.append( pattern_points[key][matching[atm_idx]] )
                    except:
                        print 'ERROR: '
                else:
                    try:
                        if target_points[key][matching[atm_idx]]!=None and \
                           pattern_points[key][atm_idx]!=None:
                            T.append( target_points[key][matching[atm_idx]] )
                            P.append( pattern_points[key][atm_idx] )
                    except:
                        print 'ERROR: '

        x = flex.vec3_double(T)
        y = flex.vec3_double(P)

        try:
            superposition = superpose.least_squares_fit(x, y, method=["kearsley", "kabsch"][0])

            rtmx = matrix.rt((superposition.r, superposition.t))
            rms = flex.mean( (x-rtmx*y).dot() )**0.5
            new_rot = superposition.r
            new_tran = superposition.t
            return {"rotation":new_rot, "translation":new_tran,
                    "rmsd":rms, 'matching':matching}
        except:
            return {'rmsd':9999.9}


    # -------------------------------------------------------------------------

    def transform_and_save(self, fname, inpfrag, res, idx):

            rt = matrix.rt( (res['rotation'], res['translation']) )

            frag = inpfrag.deep_copy()

            frag.atoms().set_xyz(new_xyz=(rt * frag.atoms().extract_xyz()))
            frag.write_pdb_file("./tmp/%s_match_%02i.pdb" % ((os.path.basename(fname)).split('.')[0], idx))


    def dump_sites(self, reference_sites, fname):

        from iotbx import pdb

        output_structure = iotbx.pdb.hierarchy.root()
        output_structure.append_model(iotbx.pdb.hierarchy.model(id="0"))
        chain = iotbx.pdb.hierarchy.chain(id="X")
        output_structure.models()[0].append_chain(chain)

        for index, site in enumerate(reference_sites):
            rg = pdb.hierarchy.residue_group(resseq="%i"%index)
            ag = pdb.hierarchy.atom_group(resname="YYY", altloc=" ")
            atom = iotbx.pdb.hierarchy.atom()

            ag.append_atom(atom)
            rg.append_atom_group(atom_group=ag)
            chain.append_residue_group(rg)

            atom.name = "P"
            atom.set_element('P')
            atom.set_xyz(site)
            atom.set_occ(1.0)
            atom.set_b( 10.0 )

        output_structure.write_pdb_file(fname)



    # -------------------------------------------------------------------------


    def cover(self, pdbid = "1ehz", max_rmsd=1.0):

        try:
            target_str = open("reference.pdb", "r")
        except:
            target_str = urllib2.urlopen("http://www.pdb.org/pdb/files/%s.pdb" % pdbid)

        target_h = pdb_input(source_info=None, lines=target_str.read()).construct_hierarchy()


        #target_h.write_pdb_file("./tmp/reference.pdb")

        sel_cache = target_h.atom_selection_cache()
        assert sel_cache.n_seq == target_h.atoms_size()
        target_isel = sel_cache.iselection
        ref_select = target_h.atoms().select(target_isel(r"name %s"%INIT_ATOM))
        reference_sites = ref_select.extract_xyz()

        target_points, target_resids  =  self.read_points_extended(target_h, \
                                                    target_isel, \
                                                    EXTENDED_ATOMS, \
                                                    False)
        print " --> Refefence sites : ", reference_sites.size()

        supermatch_obj = smatch.supermatch(reference_sites, (), "")

        # FIRST match helical frag, and remove matched reference points

        pattern_h = iotbx.pdb.hierarchy.input(pdb_string=stem_pdb_string).hierarchy

        filtres = self.match_pattern(supermatch_obj, target_points, pattern_h, max_rmsd)
        print " --> Stem matches: %i" % len(filtres)

        filtered_refpoints = set(range(reference_sites.size()))
        for ires,res in enumerate(filtres):
            self.transform_and_save("%i.pdb"%ires, pattern_h, res, ires)

            for idx in [1, -2]:
                filtered_refpoints.discard(res['matching'][idx])


        print " --> Removed %i reference sites" % ( reference_sites.size() - len(filtered_refpoints) )

        reference_sites = reference_sites.select(flex.size_t(list(filtered_refpoints)))
        supermatch_obj = smatch.supermatch(reference_sites, (), "")


        # NEXT match fragms from the library to the remaining reference points

        frag_fnames = ["./motifs/"+xxx for xxx in os.listdir("./motifs") \
                            if re.match(".*\.pdb(.gz)?$", xxx)]

        nproc = 1
        widgets = [' --> Matching fragments [%i thread%s]: '%(nproc, 's' if nproc>1 else ''), \
                                Percentage(), ' ', Bar(), ' ', ETA()]

        pbar = ProgressBar(widgets=widgets, maxval=len(frag_fnames)).start()

        for ifn, fn in enumerate(frag_fnames):
            pbar.update(ifn)
            pattern_pdb_string = gzip.open(fn).read()
            pattern_h = iotbx.pdb.hierarchy.input(pdb_string=pattern_pdb_string).hierarchy

            filtres = self.match_pattern(supermatch_obj, target_points, pattern_h, max_rmsd)

            if len(filtres):
                print "[%04i, %40s] Filtered matches: %i" % (ifn, fn, len(filtres))
                for ires, res in enumerate(filtres):
                    print "\t %0i %.2f" % (ires, res['rmsd'])
                    self.transform_and_save(fn, pattern_h, res, ires)

    # -------------------------------------------------------------------------


    def match_pattern(self, supermatch_obj, target_points, pattern_h, max_rmsd):

        sel_cache = pattern_h.atom_selection_cache()
        assert sel_cache.n_seq == pattern_h.atoms_size()
        pattern_isel = sel_cache.iselection
        pat_select =  pattern_h.atoms().select(pattern_isel(r"name %s"%INIT_ATOM))
        pattern_sites = pat_select.extract_xyz()

        if pattern_sites.size() > supermatch_obj.get_reference_size(): return []
        if pattern_sites.size() < 3: return []

        res = supermatch_obj.find_matches(pattern_sites, max_rmsd, True)
        if not res:
            return []

        pattern_points, pattern_resids = self.read_points_extended(pattern_h, \
                                                               pattern_isel, \
                                                               EXTENDED_ATOMS, \
                                                               False)

        unique_res = []
        for res_item in res:
                (match, rot, tran, rms) = res_item
                unique=True
                for unique_item in unique_res:
                    if set(unique_item[0]) == set(match):
                        unique=False
                        break
                if unique:
                    unique_res.append(res_item)

        filtres_list = []
        for idx,match in enumerate(sorted(unique_res, key=lambda x: x[-1])):

            res = self.calc_rmsd_extended(pattern_points, \
                                          target_points, \
                                          matching = [None if x<0 else x for x in match[0]], \
                                          accepted_atoms=EXTENDED_ATOMS, \
                                          simrna=False, \
                                          input_in_motif=False, \
                                          initially_accepted_atom=INIT_ATOM)

            if res['rmsd'] > max_rmsd: continue


            filtres_list.append(res)

        return filtres_list


    # -------------------------------------------------------------------------
    # -------------------------------------------------------------------------



def main():
    cobject = cover()
    cobject.cover(max_rmsd=1.0)



if __name__=="__main__":
    main()
