#!/bin/sh

#export MMTBX_CCP4_MONOMER_LIB=~/xray/phenix/phenix-1.8-1069/chem_data/mon_lib/
#export CLIBD_MON=~/xray/phenix/phenix-1.8-1069/chem_data/geostd/

ABSPATH=$(cd "$(dirname "$0")"; pwd)

export CLIBD_MON=$ABSPATH/../data/mon_lib/
export MMTBX_CCP4_MONOMER_LIB=$ABSPATH/../data/mon_lib/


. $ABSPATH/../virtualenv/cctbx_build/setpaths.sh


libtbx.python ./tst_refine_tools.py $@
