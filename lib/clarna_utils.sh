#!/bin/bash


if [ $(hostname) = 'passoa' ]; then

    /xray/cctbx/cctbx_latest/cctbx_build/bin/libtbx.python clarna_utils.py $@

else

    if [ ! -d virtualenv ] ; then
        ./_create_virtualenv
        ./_install_cctbx
    fi

    . virtualenv/bin/activate
    . virtualenv/cctbx_build/setpaths.sh

    #libtbx.python -m pdb ./rbx.py $@
    libtbx.python ./clarna_utils.py $@

fi
