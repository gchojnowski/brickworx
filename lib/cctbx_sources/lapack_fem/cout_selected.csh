#! /bin/csh -f
set verbose
set lp=/net/marbles/raid1/rwgk/lapack-3.2.1
# BLAS/SRC/xerbla.f
# INSTALL/dlamch.f
fable.cout $dist/lapack_fem/{xerbla.f,dlamch_intel_x86_64.f} $lp/BLAS/SRC/{d*.f} $lp/SRC/{i*.f,d*.f} $lp/INSTALL/lsame.f --top-procedure=dgesdd,dgesvd,dsyev --namespace=lapack_fem --include-guard-suffix=_SELECTED_HPP --fortran-file-comments --no-fem-do-safe --inline-all > selected.hpp
# /usr/local_cci/fc8_x86_64/intel_11.1.072/lib/intel64/*.so
# -lgfortran
set ff=(`grep '// Fortran file: ' selected.hpp | cut -d' ' -f4 | $dist/lapack_fem/suppress_repeating_lines.py`)
cat $ff > selected.f
