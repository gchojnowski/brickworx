#include <fem.hpp> // Fortran EMulation library of fable module

namespace lapack_dsyev_fem {

using namespace fem::major_types;

struct common :
  fem::common
{
  fem::cmn_sve dlamch_sve;

  common(
    int argc,
    char const* argv[])
  :
    fem::common(argc, argv)
  {}
};

// Fortran file: /net/marbles/raid1/rwgk/dist/lapack_fem/xerbla.f
void
xerbla(
  str_cref /* srname */,
  int const& /* info */)
{
  //C
  //C  -- LAPACK auxiliary routine (preliminary version) --
  //C     Univ. of Tennessee, Univ. of California Berkeley and NAG Ltd..
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  XERBLA  is an error handler for the LAPACK routines.
  //C  It is called by an LAPACK routine if an input parameter has an
  //C  invalid value.  A message is printed and execution stops.
  //C
  //C  Installers may consider modifying the STOP statement in order to
  //C  call system-specific exception-handling facilities.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  SRNAME  (input) CHARACTER*(*)
  //C          The name of the routine which called XERBLA.
  //C
  //C  INFO    (input) INTEGER
  //C          The position of the invalid parameter in the parameter list
  //C          of the calling routine.
  //C
  //C =====================================================================
  //C
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //CFEM  WRITE( *, FMT = 9999 )SRNAME( 1:LEN_TRIM( SRNAME ) ), INFO
  //C
  FEM_STOP(0);
  //C
  //C     End of XERBLA
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/dist/lapack_fem/dlamch_intel_x86_64.f
void
dlamc2(
  int& beta,
  int& t,
  bool& rnd,
  double& eps,
  int& emin,
  double& rmin,
  int& emax,
  double& rmax)
{
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C     Univ. of Tennessee, Univ. of California Berkeley and NAG Ltd..
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DLAMC2 determines the machine parameters specified in its argument
  //C  list.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  BETA    (output) INTEGER
  //C          The base of the machine.
  //C
  //C  T       (output) INTEGER
  //C          The number of ( BETA ) digits in the mantissa.
  //C
  //C  RND     (output) LOGICAL
  //C          Specifies whether proper rounding  ( RND = .TRUE. )  or
  //C          chopping  ( RND = .FALSE. )  occurs in addition. This may not
  //C          be a reliable guide to the way in which the machine performs
  //C          its arithmetic.
  //C
  //C  EPS     (output) DOUBLE PRECISION
  //C          The smallest positive number such that
  //C
  //C             fl( 1.0 - EPS ) .LT. 1.0,
  //C
  //C          where fl denotes the computed value.
  //C
  //C  EMIN    (output) INTEGER
  //C          The minimum exponent before (gradual) underflow occurs.
  //C
  //C  RMIN    (output) DOUBLE PRECISION
  //C          The smallest normalized number for the machine, given by
  //C          BASE**( EMIN - 1 ), where  BASE  is the floating point value
  //C          of BETA.
  //C
  //C  EMAX    (output) INTEGER
  //C          The maximum exponent before overflow occurs.
  //C
  //C  RMAX    (output) DOUBLE PRECISION
  //C          The largest positive number for the machine, given by
  //C          BASE**EMAX * ( 1 - EPS ), where  BASE  is the floating point
  //C          value of BETA.
  //C
  //C  Further Details
  //C  ===============
  //C
  //C  The computation of  EPS  is based on a routine PARANOIA by
  //C  W. Kahan of the University of California at Berkeley.
  //C
  //C =====================================================================
  //C
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Functions ..
  //CFEM  DOUBLE PRECISION   DLAMC3
  //CFEM  EXTERNAL           DLAMC3
  //C     ..
  //C     .. External Subroutines ..
  //CFEM  EXTERNAL           DLAMC1, DLAMC4, DLAMC5
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Save statement ..
  //CFEM  SAVE               FIRST, IWARN, LBETA, LEMAX, LEMIN, LEPS, LRMAX,
  //CFEM $                   LRMIN, LT
  //C     ..
  //C     .. Data statements ..
  //CFEM  DATA               FIRST / .TRUE. / , IWARN / .FALSE. /
  //C     ..
  //C     .. Executable Statements ..
  //C
  //CFEM Intel x86_64 ifort 12.1, gfortran 4.1.2 output
  beta = 2;
  t = 53;
  rnd = true;
  eps = 0.11102230246251565404e-15;
  emin = -1021;
  rmin = 0.22250738585072013831e-307;
  emax = 1024;
  rmax = 0.17976931348623157081e+309;
  //C
  //C     End of DLAMC2
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/INSTALL/lsame.f
bool
lsame(
  str_cref ca,
  str_cref cb)
{
  bool return_value = fem::bool0;
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C     Univ. of Tennessee, Univ. of California Berkeley and NAG Ltd..
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  LSAME returns .TRUE. if CA is the same letter as CB regardless of
  //C  case.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  CA      (input) CHARACTER*1
  //C  CB      (input) CHARACTER*1
  //C          CA and CB specify the single characters to be compared.
  //C
  //C =====================================================================
  //C
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Test if the characters are equal
  //C
  return_value = ca == cb;
  if (return_value) {
    return return_value;
  }
  //C
  //C     Now test for equivalence if both characters are alphabetic.
  //C
  int zcode = fem::ichar("Z");
  //C
  //C     Use 'Z' rather than 'A' so that ASCII can be detected on Prime
  //C     machines, on which ICHAR returns a value with bit 8 set.
  //C     ICHAR('A') on Prime machines returns 193 which is the same as
  //C     ICHAR('A') on an EBCDIC machine.
  //C
  int inta = fem::ichar(ca);
  int intb = fem::ichar(cb);
  //C
  if (zcode == 90 || zcode == 122) {
    //C
    //C        ASCII is assumed - ZCODE is the ASCII code of either lower or
    //C        upper case 'Z'.
    //C
    if (inta >= 97 && inta <= 122) {
      inta = inta - 32;
    }
    if (intb >= 97 && intb <= 122) {
      intb = intb - 32;
    }
    //C
  }
  else if (zcode == 233 || zcode == 169) {
    //C
    //C        EBCDIC is assumed - ZCODE is the EBCDIC code of either lower or
    //C        upper case 'Z'.
    //C
    if (inta >= 129 && inta <= 137 || inta >= 145 && inta <= 153 ||
        inta >= 162 && inta <= 169) {
      inta += 64;
    }
    if (intb >= 129 && intb <= 137 || intb >= 145 && intb <= 153 ||
        intb >= 162 && intb <= 169) {
      intb += 64;
    }
    //C
  }
  else if (zcode == 218 || zcode == 250) {
    //C
    //C        ASCII is assumed, on Prime machines - ZCODE is the ASCII code
    //C        plus 128 of either lower or upper case 'Z'.
    //C
    if (inta >= 225 && inta <= 250) {
      inta = inta - 32;
    }
    if (intb >= 225 && intb <= 250) {
      intb = intb - 32;
    }
  }
  return_value = inta == intb;
  return return_value;
  //C
  //C     RETURN
  //C
  //C     End of LSAME
  //C
}

struct dlamch_save
{
  double base;
  double emax;
  double emin;
  double eps;
  bool first;
  double prec;
  double rmax;
  double rmin;
  double rnd;
  double sfmin;
  double t;

  dlamch_save() :
    base(fem::double0),
    emax(fem::double0),
    emin(fem::double0),
    eps(fem::double0),
    first(fem::bool0),
    prec(fem::double0),
    rmax(fem::double0),
    rmin(fem::double0),
    rnd(fem::double0),
    sfmin(fem::double0),
    t(fem::double0)
  {}
};

// Fortran file: /net/marbles/raid1/rwgk/dist/lapack_fem/dlamch_intel_x86_64.f
double
dlamch(
  common& cmn,
  str_cref cmach)
{
  double return_value = fem::double0;
  FEM_CMN_SVE(dlamch);
  // SAVE
  double& base = sve.base;
  double& emax = sve.emax;
  double& emin = sve.emin;
  double& eps = sve.eps;
  bool& first = sve.first;
  double& prec = sve.prec;
  double& rmax = sve.rmax;
  double& rmin = sve.rmin;
  double& rnd = sve.rnd;
  double& sfmin = sve.sfmin;
  double& t = sve.t;
  //
  if (is_called_first_time) {
    first = true;
  }
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C     Univ. of Tennessee, Univ. of California Berkeley and NAG Ltd..
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DLAMCH determines double precision machine parameters.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  CMACH   (input) CHARACTER*1
  //C          Specifies the value to be returned by DLAMCH:
  //C          = 'E' or 'e',   DLAMCH := eps
  //C          = 'S' or 's ,   DLAMCH := sfmin
  //C          = 'B' or 'b',   DLAMCH := base
  //C          = 'P' or 'p',   DLAMCH := eps*base
  //C          = 'N' or 'n',   DLAMCH := t
  //C          = 'R' or 'r',   DLAMCH := rnd
  //C          = 'M' or 'm',   DLAMCH := emin
  //C          = 'U' or 'u',   DLAMCH := rmin
  //C          = 'L' or 'l',   DLAMCH := emax
  //C          = 'O' or 'o',   DLAMCH := rmax
  //C
  //C          where
  //C
  //C          eps   = relative machine precision
  //C          sfmin = safe minimum, such that 1/sfmin does not overflow
  //C          base  = base of the machine
  //C          prec  = eps*base
  //C          t     = number of (base) digits in the mantissa
  //C          rnd   = 1.0 when rounding occurs in addition, 0.0 otherwise
  //C          emin  = minimum exponent before (gradual) underflow
  //C          rmin  = underflow threshold - base**(emin-1)
  //C          emax  = largest exponent before overflow
  //C          rmax  = overflow threshold  - (base**emax)*(1-eps)
  //C
  //C =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Save statement ..
  //C     ..
  //C     .. Data statements ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  int beta = fem::int0;
  int it = fem::int0;
  bool lrnd = fem::bool0;
  int imin = fem::int0;
  int imax = fem::int0;
  const double one = 1.0e+0;
  const double zero = 0.0e+0;
  double small = fem::double0;
  if (first) {
    dlamc2(beta, it, lrnd, eps, imin, rmin, imax, rmax);
    base = beta;
    t = it;
    if (lrnd) {
      rnd = one;
      eps = (fem::pow(base, (1 - it))) / 2;
    }
    else {
      rnd = zero;
      eps = fem::pow(base, (1 - it));
    }
    prec = eps * base;
    emin = imin;
    emax = imax;
    sfmin = rmin;
    small = one / rmax;
    if (small >= sfmin) {
      //C
      //C           Use SMALL plus a bit, to avoid the possibility of rounding
      //C           causing overflow when computing  1/sfmin.
      //C
      sfmin = small * (one + eps);
    }
  }
  //C
  double rmach = fem::double0;
  if (lsame(cmach, "E")) {
    rmach = eps;
  }
  else if (lsame(cmach, "S")) {
    rmach = sfmin;
  }
  else if (lsame(cmach, "B")) {
    rmach = base;
  }
  else if (lsame(cmach, "P")) {
    rmach = prec;
  }
  else if (lsame(cmach, "N")) {
    rmach = t;
  }
  else if (lsame(cmach, "R")) {
    rmach = rnd;
  }
  else if (lsame(cmach, "M")) {
    rmach = emin;
  }
  else if (lsame(cmach, "U")) {
    rmach = rmin;
  }
  else if (lsame(cmach, "L")) {
    rmach = emax;
  }
  else if (lsame(cmach, "O")) {
    rmach = rmax;
  }
  //C
  return_value = rmach;
  first = false;
  return return_value;
  //C
  //C     End of DLAMCH
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/BLAS/SRC/dscal.f
void
dscal(
  int const& n,
  double const& da,
  arr_ref<double> dx,
  int const& incx)
{
  dx(dimension(star));
  int nincx = fem::int0;
  int i = fem::int0;
  int m = fem::int0;
  int mp1 = fem::int0;
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C     DSCAL scales a vector by a constant.
  //C     uses unrolled loops for increment equal to one.
  //C
  //C  Further Details
  //C  ===============
  //C
  //C     jack dongarra, linpack, 3/11/78.
  //C     modified 3/93 to return if incx .le. 0.
  //C     modified 12/3/93, array(1) declarations changed to array(*)
  //C
  //C  =====================================================================
  //C
  //C     .. Local Scalars ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  if (n <= 0 || incx <= 0) {
    return;
  }
  if (incx == 1) {
    goto statement_20;
  }
  //C
  //C        code for increment not equal to 1
  //C
  nincx = n * incx;
  FEM_DOSTEP(i, 1, nincx, incx) {
    dx(i) = da * dx(i);
  }
  return;
  //C
  //C        code for increment equal to 1
  //C
  //C        clean-up loop
  //C
  statement_20:
  m = fem::mod(n, 5);
  if (m == 0) {
    goto statement_40;
  }
  FEM_DO(i, 1, m) {
    dx(i) = da * dx(i);
  }
  if (n < 5) {
    return;
  }
  statement_40:
  mp1 = m + 1;
  FEM_DOSTEP(i, mp1, n, 5) {
    dx(i) = da * dx(i);
    dx(i + 1) = da * dx(i + 1);
    dx(i + 2) = da * dx(i + 2);
    dx(i + 3) = da * dx(i + 3);
    dx(i + 4) = da * dx(i + 4);
  }
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/ieeeck.f
int
ieeeck(
  int const& ispec,
  float const& zero,
  float const& one)
{
  int return_value = fem::int0;
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  IEEECK is called from the ILAENV to verify that Infinity and
  //C  possibly NaN arithmetic is safe (i.e. will not trap).
  //C
  //C  Arguments
  //C  =========
  //C
  //C  ISPEC   (input) INTEGER
  //C          Specifies whether to test just for inifinity arithmetic
  //C          or whether to test for infinity and NaN arithmetic.
  //C          = 0: Verify infinity arithmetic only.
  //C          = 1: Verify infinity and NaN arithmetic.
  //C
  //C  ZERO    (input) REAL
  //C          Must contain the value 0.0
  //C          This is passed to prevent the compiler from optimizing
  //C          away this code.
  //C
  //C  ONE     (input) REAL
  //C          Must contain the value 1.0
  //C          This is passed to prevent the compiler from optimizing
  //C          away this code.
  //C
  //C  RETURN VALUE:  INTEGER
  //C          = 0:  Arithmetic failed to produce the correct answers
  //C          = 1:  Arithmetic produced the correct answers
  //C
  //C     .. Local Scalars ..
  //C     ..
  //C     .. Executable Statements ..
  return_value = 1;
  //C
  float posinf = one / zero;
  if (posinf <= one) {
    return_value = 0;
    return return_value;
  }
  //C
  float neginf = -one / zero;
  if (neginf >= zero) {
    return_value = 0;
    return return_value;
  }
  //C
  float negzro = one / (neginf + one);
  if (negzro != zero) {
    return_value = 0;
    return return_value;
  }
  //C
  neginf = one / negzro;
  if (neginf >= zero) {
    return_value = 0;
    return return_value;
  }
  //C
  float newzro = negzro + zero;
  if (newzro != zero) {
    return_value = 0;
    return return_value;
  }
  //C
  posinf = one / newzro;
  if (posinf <= one) {
    return_value = 0;
    return return_value;
  }
  //C
  neginf = neginf * posinf;
  if (neginf >= zero) {
    return_value = 0;
    return return_value;
  }
  //C
  posinf = posinf * posinf;
  if (posinf <= one) {
    return_value = 0;
    return return_value;
  }
  //C
  //C     Return if we were only asked to check infinity arithmetic
  //C
  if (ispec == 0) {
    return return_value;
  }
  //C
  float nan1 = posinf + neginf;
  //C
  float nan2 = posinf / neginf;
  //C
  float nan3 = posinf / posinf;
  //C
  float nan4 = posinf * zero;
  //C
  float nan5 = neginf * negzro;
  //C
  float nan6 = nan5 * 0.0f;
  //C
  if (nan1 == nan1) {
    return_value = 0;
    return return_value;
  }
  //C
  if (nan2 == nan2) {
    return_value = 0;
    return return_value;
  }
  //C
  if (nan3 == nan3) {
    return_value = 0;
    return return_value;
  }
  //C
  if (nan4 == nan4) {
    return_value = 0;
    return return_value;
  }
  //C
  if (nan5 == nan5) {
    return_value = 0;
    return return_value;
  }
  //C
  if (nan6 == nan6) {
    return_value = 0;
    return return_value;
  }
  //C
  return return_value;
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/iparmq.f
int
iparmq(
  int const& ispec,
  str_cref /* name */,
  str_cref /* opts */,
  int const& /* n */,
  int const& ilo,
  int const& ihi,
  int const& /* lwork */)
{
  int return_value = fem::int0;
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C       This program sets problem and machine dependent parameters
  //C       useful for xHSEQR and its subroutines. It is called whenever
  //C       ILAENV is called with 12 <= ISPEC <= 16
  //C
  //C  Arguments
  //C  =========
  //C
  //C       ISPEC  (input) integer scalar
  //C              ISPEC specifies which tunable parameter IPARMQ should
  //C              return.
  //C
  //C              ISPEC=12: (INMIN)  Matrices of order nmin or less
  //C                        are sent directly to xLAHQR, the implicit
  //C                        double shift QR algorithm.  NMIN must be
  //C                        at least 11.
  //C
  //C              ISPEC=13: (INWIN)  Size of the deflation window.
  //C                        This is best set greater than or equal to
  //C                        the number of simultaneous shifts NS.
  //C                        Larger matrices benefit from larger deflation
  //C                        windows.
  //C
  //C              ISPEC=14: (INIBL) Determines when to stop nibbling and
  //C                        invest in an (expensive) multi-shift QR sweep.
  //C                        If the aggressive early deflation subroutine
  //C                        finds LD converged eigenvalues from an order
  //C                        NW deflation window and LD.GT.(NW*NIBBLE)/100,
  //C                        then the next QR sweep is skipped and early
  //C                        deflation is applied immediately to the
  //C                        remaining active diagonal block.  Setting
  //C                        IPARMQ(ISPEC=14) = 0 causes TTQRE to skip a
  //C                        multi-shift QR sweep whenever early deflation
  //C                        finds a converged eigenvalue.  Setting
  //C                        IPARMQ(ISPEC=14) greater than or equal to 100
  //C                        prevents TTQRE from skipping a multi-shift
  //C                        QR sweep.
  //C
  //C              ISPEC=15: (NSHFTS) The number of simultaneous shifts in
  //C                        a multi-shift QR iteration.
  //C
  //C              ISPEC=16: (IACC22) IPARMQ is set to 0, 1 or 2 with the
  //C                        following meanings.
  //C                        0:  During the multi-shift QR sweep,
  //C                            xLAQR5 does not accumulate reflections and
  //C                            does not use matrix-matrix multiply to
  //C                            update the far-from-diagonal matrix
  //C                            entries.
  //C                        1:  During the multi-shift QR sweep,
  //C                            xLAQR5 and/or xLAQRaccumulates reflections and uses
  //C                            matrix-matrix multiply to update the
  //C                            far-from-diagonal matrix entries.
  //C                        2:  During the multi-shift QR sweep.
  //C                            xLAQR5 accumulates reflections and takes
  //C                            advantage of 2-by-2 block structure during
  //C                            matrix-matrix multiplies.
  //C                        (If xTRMM is slower than xGEMM, then
  //C                        IPARMQ(ISPEC=16)=1 may be more efficient than
  //C                        IPARMQ(ISPEC=16)=2 despite the greater level of
  //C                        arithmetic work implied by the latter choice.)
  //C
  //C       NAME    (input) character string
  //C               Name of the calling subroutine
  //C
  //C       OPTS    (input) character string
  //C               This is a concatenation of the string arguments to
  //C               TTQRE.
  //C
  //C       N       (input) integer scalar
  //C               N is the order of the Hessenberg matrix H.
  //C
  //C       ILO     (input) INTEGER
  //C       IHI     (input) INTEGER
  //C               It is assumed that H is already upper triangular
  //C               in rows and columns 1:ILO-1 and IHI+1:N.
  //C
  //C       LWORK   (input) integer scalar
  //C               The amount of workspace available.
  //C
  //C  Further Details
  //C  ===============
  //C
  //C       Little is known about how best to choose these parameters.
  //C       It is possible to use different values of the parameters
  //C       for each of CHSEQR, DHSEQR, SHSEQR and ZHSEQR.
  //C
  //C       It is probably best to choose different parameters for
  //C       different matrices and different parameters at different
  //C       times during the iteration, but this has not been
  //C       implemented --- yet.
  //C
  //C       The best choices of most of the parameters depend
  //C       in an ill-understood way on the relative execution
  //C       rate of xLAQR3 and xLAQR5 and on the nature of each
  //C       particular eigenvalue problem.  Experiment may be the
  //C       only practical way to determine which choices are most
  //C       effective.
  //C
  //C       Following is a list of default values supplied by IPARMQ.
  //C       These defaults may be adjusted in order to attain better
  //C       performance in any particular computational environment.
  //C
  //C       IPARMQ(ISPEC=12) The xLAHQR vs xLAQR0 crossover point.
  //C                        Default: 75. (Must be at least 11.)
  //C
  //C       IPARMQ(ISPEC=13) Recommended deflation window size.
  //C                        This depends on ILO, IHI and NS, the
  //C                        number of simultaneous shifts returned
  //C                        by IPARMQ(ISPEC=15).  The default for
  //C                        (IHI-ILO+1).LE.500 is NS.  The default
  //C                        for (IHI-ILO+1).GT.500 is 3*NS/2.
  //C
  //C       IPARMQ(ISPEC=14) Nibble crossover point.  Default: 14.
  //C
  //C       IPARMQ(ISPEC=15) Number of simultaneous shifts, NS.
  //C                        a multi-shift QR iteration.
  //C
  //C                        If IHI-ILO+1 is ...
  //C
  //C                        greater than      ...but less    ... the
  //C                        or equal to ...      than        default is
  //C
  //C                                0               30       NS =   2+
  //C                               30               60       NS =   4+
  //C                               60              150       NS =  10
  //C                              150              590       NS =  **
  //C                              590             3000       NS =  64
  //C                             3000             6000       NS = 128
  //C                             6000             infinity   NS = 256
  //C
  //C                    (+)  By default matrices of this order are
  //C                         passed to the implicit double shift routine
  //C                         xLAHQR.  See IPARMQ(ISPEC=12) above.   These
  //C                         values of NS are used only in case of a rare
  //C                         xLAHQR failure.
  //C
  //C                    (**) The asterisks (**) indicate an ad-hoc
  //C                         function increasing from 10 to 64.
  //C
  //C       IPARMQ(ISPEC=16) Select structured matrix multiply.
  //C                        (See ISPEC=16 above for details.)
  //C                        Default: 3.
  //C
  //C     ================================================================
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Executable Statements ..
  const int ishfts = 15;
  const int inwin = 13;
  const int iacc22 = 16;
  int nh = fem::int0;
  int ns = fem::int0;
  const float two = 2.0f;
  if ((ispec == ishfts) || (ispec == inwin) || (ispec == iacc22)) {
    //C
    //C        ==== Set the number simultaneous shifts ====
    //C
    nh = ihi - ilo + 1;
    ns = 2;
    if (nh >= 30) {
      ns = 4;
    }
    if (nh >= 60) {
      ns = 10;
    }
    if (nh >= 150) {
      ns = fem::max(10, nh / fem::nint(fem::log(fem::real(nh)) /
        fem::log(two)));
    }
    if (nh >= 590) {
      ns = 64;
    }
    if (nh >= 3000) {
      ns = 128;
    }
    if (nh >= 6000) {
      ns = 256;
    }
    ns = fem::max(2, ns - fem::mod(ns, 2));
  }
  //C
  const int inmin = 12;
  const int nmin = 75;
  const int inibl = 14;
  const int nibble = 14;
  const int knwswp = 500;
  const int kacmin = 14;
  const int k22min = 14;
  if (ispec == inmin) {
    //C
    //C        ===== Matrices of order smaller than NMIN get sent
    //C        .     to xLAHQR, the classic double shift algorithm.
    //C        .     This must be at least 11. ====
    //C
    return_value = nmin;
    //C
  }
  else if (ispec == inibl) {
    //C
    //C        ==== INIBL: skip a multi-shift qr iteration and
    //C        .    whenever aggressive early deflation finds
    //C        .    at least (NIBBLE*(window size)/100) deflations. ====
    //C
    return_value = nibble;
    //C
  }
  else if (ispec == ishfts) {
    //C
    //C        ==== NSHFTS: The number of simultaneous shifts =====
    //C
    return_value = ns;
    //C
  }
  else if (ispec == inwin) {
    //C
    //C        ==== NW: deflation window size.  ====
    //C
    if (nh <= knwswp) {
      return_value = ns;
    }
    else {
      return_value = 3 * ns / 2;
    }
    //C
  }
  else if (ispec == iacc22) {
    //C
    //C        ==== IACC22: Whether to accumulate reflections
    //C        .     before updating the far-from-diagonal elements
    //C        .     and whether to use 2-by-2 block structure while
    //C        .     doing it.  A small amount of work could be saved
    //C        .     by making this choice dependent also upon the
    //C        .     NH=IHI-ILO+1.
    //C
    return_value = 0;
    if (ns >= kacmin) {
      return_value = 1;
    }
    if (ns >= k22min) {
      return_value = 2;
    }
    //C
  }
  else {
    //C        ===== invalid value of ispec =====
    return_value = -1;
    //C
  }
  return return_value;
  //C
  //C     ==== End of IPARMQ ====
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/ilaenv.f
int
ilaenv(
  int const& ispec,
  str_cref name,
  str_cref opts,
  int const& n1,
  int const& n2,
  int const& n3,
  int const& n4)
{
  int return_value = fem::int0;
  fem::str<6> subnam = fem::char0;
  int ic = fem::int0;
  int iz = fem::int0;
  int i = fem::int0;
  fem::str<1> c1 = fem::char0;
  bool sname = fem::bool0;
  bool cname = fem::bool0;
  fem::str<2> c2 = fem::char0;
  fem::str<3> c3 = fem::char0;
  fem::str<2> c4 = fem::char0;
  int nb = fem::int0;
  int nbmin = fem::int0;
  int nx = fem::int0;
  //C
  //C  -- LAPACK auxiliary routine (version 3.2.1)                        --
  //C
  //C  -- April 2009                                                      --
  //C
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  ILAENV is called from the LAPACK routines to choose problem-dependent
  //C  parameters for the local environment.  See ISPEC for a description of
  //C  the parameters.
  //C
  //C  ILAENV returns an INTEGER
  //C  if ILAENV >= 0: ILAENV returns the value of the parameter specified by ISPEC
  //C  if ILAENV < 0:  if ILAENV = -k, the k-th argument had an illegal value.
  //C
  //C  This version provides a set of parameters which should give good,
  //C  but not optimal, performance on many of the currently available
  //C  computers.  Users are encouraged to modify this subroutine to set
  //C  the tuning parameters for their particular machine using the option
  //C  and problem size information in the arguments.
  //C
  //C  This routine will not function correctly if it is converted to all
  //C  lower case.  Converting it to all upper case is allowed.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  ISPEC   (input) INTEGER
  //C          Specifies the parameter to be returned as the value of
  //C          ILAENV.
  //C          = 1: the optimal blocksize; if this value is 1, an unblocked
  //C               algorithm will give the best performance.
  //C          = 2: the minimum block size for which the block routine
  //C               should be used; if the usable block size is less than
  //C               this value, an unblocked routine should be used.
  //C          = 3: the crossover point (in a block routine, for N less
  //C               than this value, an unblocked routine should be used)
  //C          = 4: the number of shifts, used in the nonsymmetric
  //C               eigenvalue routines (DEPRECATED)
  //C          = 5: the minimum column dimension for blocking to be used;
  //C               rectangular blocks must have dimension at least k by m,
  //C               where k is given by ILAENV(2,...) and m by ILAENV(5,...)
  //C          = 6: the crossover point for the SVD (when reducing an m by n
  //C               matrix to bidiagonal form, if max(m,n)/min(m,n) exceeds
  //C               this value, a QR factorization is used first to reduce
  //C               the matrix to a triangular form.)
  //C          = 7: the number of processors
  //C          = 8: the crossover point for the multishift QR method
  //C               for nonsymmetric eigenvalue problems (DEPRECATED)
  //C          = 9: maximum size of the subproblems at the bottom of the
  //C               computation tree in the divide-and-conquer algorithm
  //C               (used by xGELSD and xGESDD)
  //C          =10: ieee NaN arithmetic can be trusted not to trap
  //C          =11: infinity arithmetic can be trusted not to trap
  //C          12 <= ISPEC <= 16:
  //C               xHSEQR or one of its subroutines,
  //C               see IPARMQ for detailed explanation
  //C
  //C  NAME    (input) CHARACTER*(*)
  //C          The name of the calling subroutine, in either upper case or
  //C          lower case.
  //C
  //C  OPTS    (input) CHARACTER*(*)
  //C          The character options to the subroutine NAME, concatenated
  //C          into a single character string.  For example, UPLO = 'U',
  //C          TRANS = 'T', and DIAG = 'N' for a triangular routine would
  //C          be specified as OPTS = 'UTN'.
  //C
  //C  N1      (input) INTEGER
  //C  N2      (input) INTEGER
  //C  N3      (input) INTEGER
  //C  N4      (input) INTEGER
  //C          Problem dimensions for the subroutine NAME; these may not all
  //C          be required.
  //C
  //C  Further Details
  //C  ===============
  //C
  //C  The following conventions have been used when calling ILAENV from the
  //C  LAPACK routines:
  //C  1)  OPTS is a concatenation of all of the character options to
  //C      subroutine NAME, in the same order that they appear in the
  //C      argument list for NAME, even if they are not used in determining
  //C      the value of the parameter specified by ISPEC.
  //C  2)  The problem dimensions N1, N2, N3, N4 are specified in the order
  //C      that they appear in the argument list for NAME.  N1 is used
  //C      first, N2 second, and so on, and unused problem dimensions are
  //C      passed a value of -1.
  //C  3)  The parameter value returned by ILAENV is checked for validity in
  //C      the calling subroutine.  For example, ILAENV is used to retrieve
  //C      the optimal blocksize for STRTRI as follows:
  //C
  //C      NB = ILAENV( 1, 'STRTRI', UPLO // DIAG, N, -1, -1, -1 )
  //C      IF( NB.LE.1 ) NB = MAX( 1, N )
  //C
  //C  =====================================================================
  //C
  //C     .. Local Scalars ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  switch (ispec) {
    case 1: goto statement_10;
    case 2: goto statement_10;
    case 3: goto statement_10;
    case 4: goto statement_80;
    case 5: goto statement_90;
    case 6: goto statement_100;
    case 7: goto statement_110;
    case 8: goto statement_120;
    case 9: goto statement_130;
    case 10: goto statement_140;
    case 11: goto statement_150;
    case 12: goto statement_160;
    case 13: goto statement_160;
    case 14: goto statement_160;
    case 15: goto statement_160;
    case 16: goto statement_160;
    default: break;
  }
  //C
  //C     Invalid value for ISPEC
  //C
  return_value = -1;
  return return_value;
  //C
  statement_10:
  //C
  //C     Convert NAME to upper case if the first character is lower case.
  //C
  return_value = 1;
  subnam = name;
  ic = fem::ichar(subnam(1, 1));
  iz = fem::ichar("Z");
  if (iz == 90 || iz == 122) {
    //C
    //C        ASCII character set
    //C
    if (ic >= 97 && ic <= 122) {
      subnam(1, 1) = fem::fchar(ic - 32);
      FEM_DO(i, 2, 6) {
        ic = fem::ichar(subnam(i, i));
        if (ic >= 97 && ic <= 122) {
          subnam(i, i) = fem::fchar(ic - 32);
        }
      }
    }
    //C
  }
  else if (iz == 233 || iz == 169) {
    //C
    //C        EBCDIC character set
    //C
    if ((ic >= 129 && ic <= 137) || (ic >= 145 && ic <= 153) || (
        ic >= 162 && ic <= 169)) {
      subnam(1, 1) = fem::fchar(ic + 64);
      FEM_DO(i, 2, 6) {
        ic = fem::ichar(subnam(i, i));
        if ((ic >= 129 && ic <= 137) || (ic >= 145 && ic <= 153) || (
            ic >= 162 && ic <= 169)) {
          subnam(i, i) = fem::fchar(ic + 64);
        }
      }
    }
    //C
  }
  else if (iz == 218 || iz == 250) {
    //C
    //C        Prime machines:  ASCII+128
    //C
    if (ic >= 225 && ic <= 250) {
      subnam(1, 1) = fem::fchar(ic - 32);
      FEM_DO(i, 2, 6) {
        ic = fem::ichar(subnam(i, i));
        if (ic >= 225 && ic <= 250) {
          subnam(i, i) = fem::fchar(ic - 32);
        }
      }
    }
  }
  //C
  c1 = subnam(1, 1);
  sname = c1 == "S" || c1 == "D";
  cname = c1 == "C" || c1 == "Z";
  if (!(cname || sname)) {
    return return_value;
  }
  c2 = subnam(2, 3);
  c3 = subnam(4, 6);
  c4 = c3(2, 3);
  //C
  switch (ispec) {
    case 1: goto statement_50;
    case 2: goto statement_60;
    case 3: goto statement_70;
    default: break;
  }
  //C
  statement_50:
  //C
  //C     ISPEC = 1:  block size
  //C
  //C     In these examples, separate code is provided for setting NB for
  //C     real and complex.  We assume that NB will take the same value in
  //C     single or double precision.
  //C
  nb = 1;
  //C
  if (c2 == "GE") {
    if (c3 == "TRF") {
      if (sname) {
        nb = 64;
      }
      else {
        nb = 64;
      }
    }
    else if (c3 == "QRF" || c3 == "RQF" || c3 == "LQF" || c3 == "QLF") {
      if (sname) {
        nb = 32;
      }
      else {
        nb = 32;
      }
    }
    else if (c3 == "HRD") {
      if (sname) {
        nb = 32;
      }
      else {
        nb = 32;
      }
    }
    else if (c3 == "BRD") {
      if (sname) {
        nb = 32;
      }
      else {
        nb = 32;
      }
    }
    else if (c3 == "TRI") {
      if (sname) {
        nb = 64;
      }
      else {
        nb = 64;
      }
    }
  }
  else if (c2 == "PO") {
    if (c3 == "TRF") {
      if (sname) {
        nb = 64;
      }
      else {
        nb = 64;
      }
    }
  }
  else if (c2 == "SY") {
    if (c3 == "TRF") {
      if (sname) {
        nb = 64;
      }
      else {
        nb = 64;
      }
    }
    else if (sname && c3 == "TRD") {
      nb = 32;
    }
    else if (sname && c3 == "GST") {
      nb = 64;
    }
  }
  else if (cname && c2 == "HE") {
    if (c3 == "TRF") {
      nb = 64;
    }
    else if (c3 == "TRD") {
      nb = 32;
    }
    else if (c3 == "GST") {
      nb = 64;
    }
  }
  else if (sname && c2 == "OR") {
    if (c3(1, 1) == "G") {
      if (c4 == "QR" || c4 == "RQ" || c4 == "LQ" || c4 == "QL" ||
          c4 == "HR" || c4 == "TR" || c4 == "BR") {
        nb = 32;
      }
    }
    else if (c3(1, 1) == "M") {
      if (c4 == "QR" || c4 == "RQ" || c4 == "LQ" || c4 == "QL" ||
          c4 == "HR" || c4 == "TR" || c4 == "BR") {
        nb = 32;
      }
    }
  }
  else if (cname && c2 == "UN") {
    if (c3(1, 1) == "G") {
      if (c4 == "QR" || c4 == "RQ" || c4 == "LQ" || c4 == "QL" ||
          c4 == "HR" || c4 == "TR" || c4 == "BR") {
        nb = 32;
      }
    }
    else if (c3(1, 1) == "M") {
      if (c4 == "QR" || c4 == "RQ" || c4 == "LQ" || c4 == "QL" ||
          c4 == "HR" || c4 == "TR" || c4 == "BR") {
        nb = 32;
      }
    }
  }
  else if (c2 == "GB") {
    if (c3 == "TRF") {
      if (sname) {
        if (n4 <= 64) {
          nb = 1;
        }
        else {
          nb = 32;
        }
      }
      else {
        if (n4 <= 64) {
          nb = 1;
        }
        else {
          nb = 32;
        }
      }
    }
  }
  else if (c2 == "PB") {
    if (c3 == "TRF") {
      if (sname) {
        if (n2 <= 64) {
          nb = 1;
        }
        else {
          nb = 32;
        }
      }
      else {
        if (n2 <= 64) {
          nb = 1;
        }
        else {
          nb = 32;
        }
      }
    }
  }
  else if (c2 == "TR") {
    if (c3 == "TRI") {
      if (sname) {
        nb = 64;
      }
      else {
        nb = 64;
      }
    }
  }
  else if (c2 == "LA") {
    if (c3 == "UUM") {
      if (sname) {
        nb = 64;
      }
      else {
        nb = 64;
      }
    }
  }
  else if (sname && c2 == "ST") {
    if (c3 == "EBZ") {
      nb = 1;
    }
  }
  return_value = nb;
  return return_value;
  //C
  statement_60:
  //C
  //C     ISPEC = 2:  minimum block size
  //C
  nbmin = 2;
  if (c2 == "GE") {
    if (c3 == "QRF" || c3 == "RQF" || c3 == "LQF" || c3 == "QLF") {
      if (sname) {
        nbmin = 2;
      }
      else {
        nbmin = 2;
      }
    }
    else if (c3 == "HRD") {
      if (sname) {
        nbmin = 2;
      }
      else {
        nbmin = 2;
      }
    }
    else if (c3 == "BRD") {
      if (sname) {
        nbmin = 2;
      }
      else {
        nbmin = 2;
      }
    }
    else if (c3 == "TRI") {
      if (sname) {
        nbmin = 2;
      }
      else {
        nbmin = 2;
      }
    }
  }
  else if (c2 == "SY") {
    if (c3 == "TRF") {
      if (sname) {
        nbmin = 8;
      }
      else {
        nbmin = 8;
      }
    }
    else if (sname && c3 == "TRD") {
      nbmin = 2;
    }
  }
  else if (cname && c2 == "HE") {
    if (c3 == "TRD") {
      nbmin = 2;
    }
  }
  else if (sname && c2 == "OR") {
    if (c3(1, 1) == "G") {
      if (c4 == "QR" || c4 == "RQ" || c4 == "LQ" || c4 == "QL" ||
          c4 == "HR" || c4 == "TR" || c4 == "BR") {
        nbmin = 2;
      }
    }
    else if (c3(1, 1) == "M") {
      if (c4 == "QR" || c4 == "RQ" || c4 == "LQ" || c4 == "QL" ||
          c4 == "HR" || c4 == "TR" || c4 == "BR") {
        nbmin = 2;
      }
    }
  }
  else if (cname && c2 == "UN") {
    if (c3(1, 1) == "G") {
      if (c4 == "QR" || c4 == "RQ" || c4 == "LQ" || c4 == "QL" ||
          c4 == "HR" || c4 == "TR" || c4 == "BR") {
        nbmin = 2;
      }
    }
    else if (c3(1, 1) == "M") {
      if (c4 == "QR" || c4 == "RQ" || c4 == "LQ" || c4 == "QL" ||
          c4 == "HR" || c4 == "TR" || c4 == "BR") {
        nbmin = 2;
      }
    }
  }
  return_value = nbmin;
  return return_value;
  //C
  statement_70:
  //C
  //C     ISPEC = 3:  crossover point
  //C
  nx = 0;
  if (c2 == "GE") {
    if (c3 == "QRF" || c3 == "RQF" || c3 == "LQF" || c3 == "QLF") {
      if (sname) {
        nx = 128;
      }
      else {
        nx = 128;
      }
    }
    else if (c3 == "HRD") {
      if (sname) {
        nx = 128;
      }
      else {
        nx = 128;
      }
    }
    else if (c3 == "BRD") {
      if (sname) {
        nx = 128;
      }
      else {
        nx = 128;
      }
    }
  }
  else if (c2 == "SY") {
    if (sname && c3 == "TRD") {
      nx = 32;
    }
  }
  else if (cname && c2 == "HE") {
    if (c3 == "TRD") {
      nx = 32;
    }
  }
  else if (sname && c2 == "OR") {
    if (c3(1, 1) == "G") {
      if (c4 == "QR" || c4 == "RQ" || c4 == "LQ" || c4 == "QL" ||
          c4 == "HR" || c4 == "TR" || c4 == "BR") {
        nx = 128;
      }
    }
  }
  else if (cname && c2 == "UN") {
    if (c3(1, 1) == "G") {
      if (c4 == "QR" || c4 == "RQ" || c4 == "LQ" || c4 == "QL" ||
          c4 == "HR" || c4 == "TR" || c4 == "BR") {
        nx = 128;
      }
    }
  }
  return_value = nx;
  return return_value;
  //C
  statement_80:
  //C
  //C     ISPEC = 4:  number of shifts (used by xHSEQR)
  //C
  return_value = 6;
  return return_value;
  //C
  statement_90:
  //C
  //C     ISPEC = 5:  minimum column dimension (not used)
  //C
  return_value = 2;
  return return_value;
  //C
  statement_100:
  //C
  //C     ISPEC = 6:  crossover point for SVD (used by xGELSS and xGESVD)
  //C
  return_value = fem::fint(fem::real(fem::min(n1, n2)) * 1.6e0f);
  return return_value;
  //C
  statement_110:
  //C
  //C     ISPEC = 7:  number of processors (not used)
  //C
  return_value = 1;
  return return_value;
  //C
  statement_120:
  //C
  //C     ISPEC = 8:  crossover point for multishift (used by xHSEQR)
  //C
  return_value = 50;
  return return_value;
  //C
  statement_130:
  //C
  //C     ISPEC = 9:  maximum size of the subproblems at the bottom of the
  //C                 computation tree in the divide-and-conquer algorithm
  //C                 (used by xGELSD and xGESDD)
  //C
  return_value = 25;
  return return_value;
  //C
  statement_140:
  //C
  //C     ISPEC = 10: ieee NaN arithmetic can be trusted not to trap
  //C
  //C     ILAENV = 0
  return_value = 1;
  if (return_value == 1) {
    return_value = ieeeck(1, 0.0f, 1.0f);
  }
  return return_value;
  //C
  statement_150:
  //C
  //C     ISPEC = 11: infinity arithmetic can be trusted not to trap
  //C
  //C     ILAENV = 0
  return_value = 1;
  if (return_value == 1) {
    return_value = ieeeck(0, 0.0f, 1.0f);
  }
  return return_value;
  //C
  statement_160:
  //C
  //C     12 <= ISPEC <= 16: xHSEQR or one of its subroutines.
  //C
  return_value = iparmq(ispec, name, opts, n1, n2, n3, n4);
  return return_value;
  //C
  //C     End of ILAENV
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dlassq.f
void
dlassq(
  int const& n,
  arr_cref<double> x,
  int const& incx,
  double& scale,
  double& sumsq)
{
  x(dimension(star));
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DLASSQ  returns the values  scl  and  smsq  such that
  //C
  //C     ( scl**2 )*smsq = x( 1 )**2 +...+ x( n )**2 + ( scale**2 )*sumsq,
  //C
  //C  where  x( i ) = X( 1 + ( i - 1 )*INCX ). The value of  sumsq  is
  //C  assumed to be non-negative and  scl  returns the value
  //C
  //C     scl = max( scale, abs( x( i ) ) ).
  //C
  //C  scale and sumsq must be supplied in SCALE and SUMSQ and
  //C  scl and smsq are overwritten on SCALE and SUMSQ respectively.
  //C
  //C  The routine makes only one pass through the vector x.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  N       (input) INTEGER
  //C          The number of elements to be used from the vector X.
  //C
  //C  X       (input) DOUBLE PRECISION array, dimension (N)
  //C          The vector for which a scaled sum of squares is computed.
  //C             x( i )  = X( 1 + ( i - 1 )*INCX ), 1 <= i <= n.
  //C
  //C  INCX    (input) INTEGER
  //C          The increment between successive values of the vector X.
  //C          INCX > 0.
  //C
  //C  SCALE   (input/output) DOUBLE PRECISION
  //C          On entry, the value  scale  in the equation above.
  //C          On exit, SCALE is overwritten with  scl , the scaling factor
  //C          for the sum of squares.
  //C
  //C  SUMSQ   (input/output) DOUBLE PRECISION
  //C          On entry, the value  sumsq  in the equation above.
  //C          On exit, SUMSQ is overwritten with  smsq , the basic sum of
  //C          squares from which  scl  has been factored out.
  //C
  //C =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  int ix = fem::int0;
  const double zero = 0.0e+0;
  double absxi = fem::double0;
  if (n > 0) {
    FEM_DOSTEP(ix, 1, 1 + (n - 1) * incx, incx) {
      if (x(ix) != zero) {
        absxi = fem::abs(x(ix));
        if (scale < absxi) {
          sumsq = 1 + sumsq * fem::pow2((scale / absxi));
          scale = absxi;
        }
        else {
          sumsq += fem::pow2((absxi / scale));
        }
      }
    }
  }
  //C
  //C     End of DLASSQ
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dlansy.f
double
dlansy(
  str_cref norm,
  str_cref uplo,
  int const& n,
  arr_cref<double, 2> a,
  int const& lda,
  arr_ref<double> work)
{
  double return_value = fem::double0;
  a(dimension(lda, star));
  work(dimension(star));
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DLANSY  returns the value of the one norm,  or the Frobenius norm, or
  //C  the  infinity norm,  or the  element of  largest absolute value  of a
  //C  real symmetric matrix A.
  //C
  //C  Description
  //C  ===========
  //C
  //C  DLANSY returns the value
  //C
  //C     DLANSY = ( max(abs(A(i,j))), NORM = 'M' or 'm'
  //C              (
  //C              ( norm1(A),         NORM = '1', 'O' or 'o'
  //C              (
  //C              ( normI(A),         NORM = 'I' or 'i'
  //C              (
  //C              ( normF(A),         NORM = 'F', 'f', 'E' or 'e'
  //C
  //C  where  norm1  denotes the  one norm of a matrix (maximum column sum),
  //C  normI  denotes the  infinity norm  of a matrix  (maximum row sum) and
  //C  normF  denotes the  Frobenius norm of a matrix (square root of sum of
  //C  squares).  Note that  max(abs(A(i,j)))  is not a consistent matrix norm.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  NORM    (input) CHARACTER*1
  //C          Specifies the value to be returned in DLANSY as described
  //C          above.
  //C
  //C  UPLO    (input) CHARACTER*1
  //C          Specifies whether the upper or lower triangular part of the
  //C          symmetric matrix A is to be referenced.
  //C          = 'U':  Upper triangular part of A is referenced
  //C          = 'L':  Lower triangular part of A is referenced
  //C
  //C  N       (input) INTEGER
  //C          The order of the matrix A.  N >= 0.  When N = 0, DLANSY is
  //C          set to zero.
  //C
  //C  A       (input) DOUBLE PRECISION array, dimension (LDA,N)
  //C          The symmetric matrix A.  If UPLO = 'U', the leading n by n
  //C          upper triangular part of A contains the upper triangular part
  //C          of the matrix A, and the strictly lower triangular part of A
  //C          is not referenced.  If UPLO = 'L', the leading n by n lower
  //C          triangular part of A contains the lower triangular part of
  //C          the matrix A, and the strictly upper triangular part of A is
  //C          not referenced.
  //C
  //C  LDA     (input) INTEGER
  //C          The leading dimension of the array A.  LDA >= max(N,1).
  //C
  //C  WORK    (workspace) DOUBLE PRECISION array, dimension (MAX(1,LWORK)),
  //C          where LWORK >= N when NORM = 'I' or '1' or 'O'; otherwise,
  //C          WORK is not referenced.
  //C
  //C =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  const double zero = 0.0e+0;
  double value = fem::double0;
  int j = fem::int0;
  int i = fem::int0;
  double sum = fem::double0;
  double absa = fem::double0;
  double scale = fem::double0;
  const double one = 1.0e+0;
  if (n == 0) {
    value = zero;
  }
  else if (lsame(norm, "M")) {
    //C
    //C        Find max(abs(A(i,j))).
    //C
    value = zero;
    if (lsame(uplo, "U")) {
      FEM_DO(j, 1, n) {
        FEM_DO(i, 1, j) {
          value = fem::max(value, fem::abs(a(i, j)));
        }
      }
    }
    else {
      FEM_DO(j, 1, n) {
        FEM_DO(i, j, n) {
          value = fem::max(value, fem::abs(a(i, j)));
        }
      }
    }
  }
  else if ((lsame(norm, "I")) || (lsame(norm, "O")) || (norm == "1")) {
    //C
    //C        Find normI(A) ( = norm1(A), since A is symmetric).
    //C
    value = zero;
    if (lsame(uplo, "U")) {
      FEM_DO(j, 1, n) {
        sum = zero;
        {
          int fem_do_last = j - 1;
          FEM_DO(i, 1, fem_do_last) {
            absa = fem::abs(a(i, j));
            sum += absa;
            work(i) += absa;
          }
        }
        work(j) = sum + fem::abs(a(j, j));
      }
      FEM_DO(i, 1, n) {
        value = fem::max(value, work(i));
      }
    }
    else {
      FEM_DO(i, 1, n) {
        work(i) = zero;
      }
      FEM_DO(j, 1, n) {
        sum = work(j) + fem::abs(a(j, j));
        FEM_DO(i, j + 1, n) {
          absa = fem::abs(a(i, j));
          sum += absa;
          work(i) += absa;
        }
        value = fem::max(value, sum);
      }
    }
  }
  else if ((lsame(norm, "F")) || (lsame(norm, "E"))) {
    //C
    //C        Find normF(A).
    //C
    scale = zero;
    sum = one;
    if (lsame(uplo, "U")) {
      FEM_DO(j, 2, n) {
        dlassq(j - 1, a(1, j), 1, scale, sum);
      }
    }
    else {
      {
        int fem_do_last = n - 1;
        FEM_DO(j, 1, fem_do_last) {
          dlassq(n - j, a(j + 1, j), 1, scale, sum);
        }
      }
    }
    sum = 2 * sum;
    dlassq(n, a, lda + 1, scale, sum);
    value = scale * fem::sqrt(sum);
  }
  //C
  return_value = value;
  return return_value;
  //C
  //C     End of DLANSY
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dlaisnan.f
bool
dlaisnan(
  double const& din1,
  double const& din2)
{
  bool return_value = fem::bool0;
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  This routine is not for general use.  It exists solely to avoid
  //C  over-optimization in DISNAN.
  //C
  //C  DLAISNAN checks for NaNs by comparing its two arguments for
  //C  inequality.  NaN is the only floating-point value where NaN != NaN
  //C  returns .TRUE.  To check for NaNs, pass the same variable as both
  //C  arguments.
  //C
  //C  A compiler must assume that the two arguments are
  //C  not the same variable, and the test will not be optimized away.
  //C  Interprocedural or whole-program optimization may delete this
  //C  test.  The ISNAN functions will be replaced by the correct
  //C  Fortran 03 intrinsic once the intrinsic is widely available.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  DIN1     (input) DOUBLE PRECISION
  //C  DIN2     (input) DOUBLE PRECISION
  //C          Two numbers to compare for inequality.
  //C
  //C  =====================================================================
  //C
  //C  .. Executable Statements ..
  return_value = (din1 != din2);
  return return_value;
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/disnan.f
bool
disnan(
  double const& din)
{
  bool return_value = fem::bool0;
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DISNAN returns .TRUE. if its argument is NaN, and .FALSE.
  //C  otherwise.  To be replaced by the Fortran 2003 intrinsic in the
  //C  future.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  DIN      (input) DOUBLE PRECISION
  //C          Input to test for NaN.
  //C
  //C  =====================================================================
  //C
  //C  .. External Functions ..
  //C  ..
  //C  .. Executable Statements ..
  return_value = dlaisnan(din, din);
  return return_value;
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dlascl.f
void
dlascl(
  common& cmn,
  str_cref type,
  int const& kl,
  int const& ku,
  double const& cfrom,
  double const& cto,
  int const& m,
  int const& n,
  arr_ref<double, 2> a,
  int const& lda,
  int& info)
{
  a(dimension(lda, star));
  int itype = fem::int0;
  const double zero = 0.0e0;
  double smlnum = fem::double0;
  const double one = 1.0e0;
  double bignum = fem::double0;
  double cfromc = fem::double0;
  double ctoc = fem::double0;
  double cfrom1 = fem::double0;
  double mul = fem::double0;
  bool done = fem::bool0;
  double cto1 = fem::double0;
  int j = fem::int0;
  int i = fem::int0;
  int k3 = fem::int0;
  int k4 = fem::int0;
  int k1 = fem::int0;
  int k2 = fem::int0;
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DLASCL multiplies the M by N real matrix A by the real scalar
  //C  CTO/CFROM.  This is done without over/underflow as long as the final
  //C  result CTO*A(I,J)/CFROM does not over/underflow. TYPE specifies that
  //C  A may be full, upper triangular, lower triangular, upper Hessenberg,
  //C  or banded.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  TYPE    (input) CHARACTER*1
  //C          TYPE indices the storage type of the input matrix.
  //C          = 'G':  A is a full matrix.
  //C          = 'L':  A is a lower triangular matrix.
  //C          = 'U':  A is an upper triangular matrix.
  //C          = 'H':  A is an upper Hessenberg matrix.
  //C          = 'B':  A is a symmetric band matrix with lower bandwidth KL
  //C                  and upper bandwidth KU and with the only the lower
  //C                  half stored.
  //C          = 'Q':  A is a symmetric band matrix with lower bandwidth KL
  //C                  and upper bandwidth KU and with the only the upper
  //C                  half stored.
  //C          = 'Z':  A is a band matrix with lower bandwidth KL and upper
  //C                  bandwidth KU.
  //C
  //C  KL      (input) INTEGER
  //C          The lower bandwidth of A.  Referenced only if TYPE = 'B',
  //C          'Q' or 'Z'.
  //C
  //C  KU      (input) INTEGER
  //C          The upper bandwidth of A.  Referenced only if TYPE = 'B',
  //C          'Q' or 'Z'.
  //C
  //C  CFROM   (input) DOUBLE PRECISION
  //C  CTO     (input) DOUBLE PRECISION
  //C          The matrix A is multiplied by CTO/CFROM. A(I,J) is computed
  //C          without over/underflow if the final result CTO*A(I,J)/CFROM
  //C          can be represented without over/underflow.  CFROM must be
  //C          nonzero.
  //C
  //C  M       (input) INTEGER
  //C          The number of rows of the matrix A.  M >= 0.
  //C
  //C  N       (input) INTEGER
  //C          The number of columns of the matrix A.  N >= 0.
  //C
  //C  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //C          The matrix to be multiplied by CTO/CFROM.  See TYPE for the
  //C          storage type.
  //C
  //C  LDA     (input) INTEGER
  //C          The leading dimension of the array A.  LDA >= max(1,M).
  //C
  //C  INFO    (output) INTEGER
  //C          0  - successful exit
  //C          <0 - if INFO = -i, the i-th argument had an illegal value.
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Test the input arguments
  //C
  info = 0;
  //C
  if (lsame(type, "G")) {
    itype = 0;
  }
  else if (lsame(type, "L")) {
    itype = 1;
  }
  else if (lsame(type, "U")) {
    itype = 2;
  }
  else if (lsame(type, "H")) {
    itype = 3;
  }
  else if (lsame(type, "B")) {
    itype = 4;
  }
  else if (lsame(type, "Q")) {
    itype = 5;
  }
  else if (lsame(type, "Z")) {
    itype = 6;
  }
  else {
    itype = -1;
  }
  //C
  if (itype ==  - 1) {
    info = -1;
  }
  else if (cfrom == zero || disnan(cfrom)) {
    info = -4;
  }
  else if (disnan(cto)) {
    info = -5;
  }
  else if (m < 0) {
    info = -6;
  }
  else if (n < 0 || (itype == 4 && n != m) || (itype == 5 && n != m)) {
    info = -7;
  }
  else if (itype <= 3 && lda < fem::max(1, m)) {
    info = -9;
  }
  else if (itype >= 4) {
    if (kl < 0 || kl > fem::max(m - 1, 0)) {
      info = -2;
    }
    else if (ku < 0 || ku > fem::max(n - 1, 0) || ((itype == 4 ||
      itype == 5) && kl != ku)) {
      info = -3;
    }
    else if ((itype == 4 && lda < kl + 1) || (itype == 5 &&
      lda < ku + 1) || (itype == 6 && lda < 2 * kl + ku + 1)) {
      info = -9;
    }
  }
  //C
  if (info != 0) {
    xerbla("DLASCL", -info);
    return;
  }
  //C
  //C     Quick return if possible
  //C
  if (n == 0 || m == 0) {
    return;
  }
  //C
  //C     Get machine parameters
  //C
  smlnum = dlamch(cmn, "S");
  bignum = one / smlnum;
  //C
  cfromc = cfrom;
  ctoc = cto;
  //C
  statement_10:
  cfrom1 = cfromc * smlnum;
  if (cfrom1 == cfromc) {
    //C        CFROMC is an inf.  Multiply by a correctly signed zero for
    //C        finite CTOC, or a NaN if CTOC is infinite.
    mul = ctoc / cfromc;
    done = true;
    cto1 = ctoc;
  }
  else {
    cto1 = ctoc / bignum;
    if (cto1 == ctoc) {
      //C           CTOC is either 0 or an inf.  In both cases, CTOC itself
      //C           serves as the correct multiplication factor.
      mul = ctoc;
      done = true;
      cfromc = one;
    }
    else if (fem::abs(cfrom1) > fem::abs(ctoc) && ctoc != zero) {
      mul = smlnum;
      done = false;
      cfromc = cfrom1;
    }
    else if (fem::abs(cto1) > fem::abs(cfromc)) {
      mul = bignum;
      done = false;
      ctoc = cto1;
    }
    else {
      mul = ctoc / cfromc;
      done = true;
    }
  }
  //C
  if (itype == 0) {
    //C
    //C        Full matrix
    //C
    FEM_DO(j, 1, n) {
      FEM_DO(i, 1, m) {
        a(i, j) = a(i, j) * mul;
      }
    }
    //C
  }
  else if (itype == 1) {
    //C
    //C        Lower triangular matrix
    //C
    FEM_DO(j, 1, n) {
      FEM_DO(i, j, m) {
        a(i, j) = a(i, j) * mul;
      }
    }
    //C
  }
  else if (itype == 2) {
    //C
    //C        Upper triangular matrix
    //C
    FEM_DO(j, 1, n) {
      {
        int fem_do_last = fem::min(j, m);
        FEM_DO(i, 1, fem_do_last) {
          a(i, j) = a(i, j) * mul;
        }
      }
    }
    //C
  }
  else if (itype == 3) {
    //C
    //C        Upper Hessenberg matrix
    //C
    FEM_DO(j, 1, n) {
      {
        int fem_do_last = fem::min(j + 1, m);
        FEM_DO(i, 1, fem_do_last) {
          a(i, j) = a(i, j) * mul;
        }
      }
    }
    //C
  }
  else if (itype == 4) {
    //C
    //C        Lower half of a symmetric band matrix
    //C
    k3 = kl + 1;
    k4 = n + 1;
    FEM_DO(j, 1, n) {
      {
        int fem_do_last = fem::min(k3, k4 - j);
        FEM_DO(i, 1, fem_do_last) {
          a(i, j) = a(i, j) * mul;
        }
      }
    }
    //C
  }
  else if (itype == 5) {
    //C
    //C        Upper half of a symmetric band matrix
    //C
    k1 = ku + 2;
    k3 = ku + 1;
    FEM_DO(j, 1, n) {
      FEM_DO(i, fem::max(k1 - j, 1), k3) {
        a(i, j) = a(i, j) * mul;
      }
    }
    //C
  }
  else if (itype == 6) {
    //C
    //C        Band matrix
    //C
    k1 = kl + ku + 2;
    k2 = kl + 1;
    k3 = 2 * kl + ku + 1;
    k4 = kl + ku + 1 + m;
    FEM_DO(j, 1, n) {
      {
        int fem_do_last = fem::min(k3, k4 - j);
        FEM_DO(i, fem::max(k1 - j, k2), fem_do_last) {
          a(i, j) = a(i, j) * mul;
        }
      }
    }
    //C
  }
  //C
  if (!done) {
    goto statement_10;
  }
  //C
  //C     End of DLASCL
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/BLAS/SRC/dcopy.f
void
dcopy(
  int const& n,
  arr_cref<double> dx,
  int const& incx,
  arr_ref<double> dy,
  int const& incy)
{
  dx(dimension(star));
  dy(dimension(star));
  int ix = fem::int0;
  int iy = fem::int0;
  int i = fem::int0;
  int m = fem::int0;
  int mp1 = fem::int0;
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C     DCOPY copies a vector, x, to a vector, y.
  //C     uses unrolled loops for increments equal to one.
  //C
  //C  Further Details
  //C  ===============
  //C
  //C     jack dongarra, linpack, 3/11/78.
  //C     modified 12/3/93, array(1) declarations changed to array(*)
  //C
  //C  =====================================================================
  //C
  //C     .. Local Scalars ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  if (n <= 0) {
    return;
  }
  if (incx == 1 && incy == 1) {
    goto statement_20;
  }
  //C
  //C        code for unequal increments or equal increments
  //C          not equal to 1
  //C
  ix = 1;
  iy = 1;
  if (incx < 0) {
    ix = (-n + 1) * incx + 1;
  }
  if (incy < 0) {
    iy = (-n + 1) * incy + 1;
  }
  FEM_DO(i, 1, n) {
    dy(iy) = dx(ix);
    ix += incx;
    iy += incy;
  }
  return;
  //C
  //C        code for both increments equal to 1
  //C
  //C        clean-up loop
  //C
  statement_20:
  m = fem::mod(n, 7);
  if (m == 0) {
    goto statement_40;
  }
  FEM_DO(i, 1, m) {
    dy(i) = dx(i);
  }
  if (n < 7) {
    return;
  }
  statement_40:
  mp1 = m + 1;
  FEM_DOSTEP(i, mp1, n, 7) {
    dy(i) = dx(i);
    dy(i + 1) = dx(i + 1);
    dy(i + 2) = dx(i + 2);
    dy(i + 3) = dx(i + 3);
    dy(i + 4) = dx(i + 4);
    dy(i + 5) = dx(i + 5);
    dy(i + 6) = dx(i + 6);
  }
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/BLAS/SRC/dgemm.f
void
dgemm(
  str_cref transa,
  str_cref transb,
  int const& m,
  int const& n,
  int const& k,
  double const& alpha,
  arr_cref<double, 2> a,
  int const& lda,
  arr_cref<double, 2> b,
  int const& ldb,
  double const& beta,
  arr_ref<double, 2> c,
  int const& ldc)
{
  a(dimension(lda, star));
  b(dimension(ldb, star));
  c(dimension(ldc, star));
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DGEMM  performs one of the matrix-matrix operations
  //C
  //C     C := alpha*op( A )*op( B ) + beta*C,
  //C
  //C  where  op( X ) is one of
  //C
  //C     op( X ) = X   or   op( X ) = X',
  //C
  //C  alpha and beta are scalars, and A, B and C are matrices, with op( A )
  //C  an m by k matrix,  op( B )  a  k by n matrix and  C an m by n matrix.
  //C
  //C  Arguments
  //C  ==========
  //C
  //C  TRANSA - CHARACTER*1.
  //C           On entry, TRANSA specifies the form of op( A ) to be used in
  //C           the matrix multiplication as follows:
  //C
  //C              TRANSA = 'N' or 'n',  op( A ) = A.
  //C
  //C              TRANSA = 'T' or 't',  op( A ) = A'.
  //C
  //C              TRANSA = 'C' or 'c',  op( A ) = A'.
  //C
  //C           Unchanged on exit.
  //C
  //C  TRANSB - CHARACTER*1.
  //C           On entry, TRANSB specifies the form of op( B ) to be used in
  //C           the matrix multiplication as follows:
  //C
  //C              TRANSB = 'N' or 'n',  op( B ) = B.
  //C
  //C              TRANSB = 'T' or 't',  op( B ) = B'.
  //C
  //C              TRANSB = 'C' or 'c',  op( B ) = B'.
  //C
  //C           Unchanged on exit.
  //C
  //C  M      - INTEGER.
  //C           On entry,  M  specifies  the number  of rows  of the  matrix
  //C           op( A )  and of the  matrix  C.  M  must  be at least  zero.
  //C           Unchanged on exit.
  //C
  //C  N      - INTEGER.
  //C           On entry,  N  specifies the number  of columns of the matrix
  //C           op( B ) and the number of columns of the matrix C. N must be
  //C           at least zero.
  //C           Unchanged on exit.
  //C
  //C  K      - INTEGER.
  //C           On entry,  K  specifies  the number of columns of the matrix
  //C           op( A ) and the number of rows of the matrix op( B ). K must
  //C           be at least  zero.
  //C           Unchanged on exit.
  //C
  //C  ALPHA  - DOUBLE PRECISION.
  //C           On entry, ALPHA specifies the scalar alpha.
  //C           Unchanged on exit.
  //C
  //C  A      - DOUBLE PRECISION array of DIMENSION ( LDA, ka ), where ka is
  //C           k  when  TRANSA = 'N' or 'n',  and is  m  otherwise.
  //C           Before entry with  TRANSA = 'N' or 'n',  the leading  m by k
  //C           part of the array  A  must contain the matrix  A,  otherwise
  //C           the leading  k by m  part of the array  A  must contain  the
  //C           matrix A.
  //C           Unchanged on exit.
  //C
  //C  LDA    - INTEGER.
  //C           On entry, LDA specifies the first dimension of A as declared
  //C           in the calling (sub) program. When  TRANSA = 'N' or 'n' then
  //C           LDA must be at least  max( 1, m ), otherwise  LDA must be at
  //C           least  max( 1, k ).
  //C           Unchanged on exit.
  //C
  //C  B      - DOUBLE PRECISION array of DIMENSION ( LDB, kb ), where kb is
  //C           n  when  TRANSB = 'N' or 'n',  and is  k  otherwise.
  //C           Before entry with  TRANSB = 'N' or 'n',  the leading  k by n
  //C           part of the array  B  must contain the matrix  B,  otherwise
  //C           the leading  n by k  part of the array  B  must contain  the
  //C           matrix B.
  //C           Unchanged on exit.
  //C
  //C  LDB    - INTEGER.
  //C           On entry, LDB specifies the first dimension of B as declared
  //C           in the calling (sub) program. When  TRANSB = 'N' or 'n' then
  //C           LDB must be at least  max( 1, k ), otherwise  LDB must be at
  //C           least  max( 1, n ).
  //C           Unchanged on exit.
  //C
  //C  BETA   - DOUBLE PRECISION.
  //C           On entry,  BETA  specifies the scalar  beta.  When  BETA  is
  //C           supplied as zero then C need not be set on input.
  //C           Unchanged on exit.
  //C
  //C  C      - DOUBLE PRECISION array of DIMENSION ( LDC, n ).
  //C           Before entry, the leading  m by n  part of the array  C must
  //C           contain the matrix  C,  except when  beta  is zero, in which
  //C           case C need not be set on entry.
  //C           On exit, the array  C  is overwritten by the  m by n  matrix
  //C           ( alpha*op( A )*op( B ) + beta*C ).
  //C
  //C  LDC    - INTEGER.
  //C           On entry, LDC specifies the first dimension of C as declared
  //C           in  the  calling  (sub)  program.   LDC  must  be  at  least
  //C           max( 1, m ).
  //C           Unchanged on exit.
  //C
  //C  Further Details
  //C  ===============
  //C
  //C  Level 3 Blas routine.
  //C
  //C  -- Written on 8-February-1989.
  //C     Jack Dongarra, Argonne National Laboratory.
  //C     Iain Duff, AERE Harwell.
  //C     Jeremy Du Croz, Numerical Algorithms Group Ltd.
  //C     Sven Hammarling, Numerical Algorithms Group Ltd.
  //C
  //C  =====================================================================
  //C
  //C     .. External Functions ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. Parameters ..
  //C     ..
  //C
  //C     Set  NOTA  and  NOTB  as  true if  A  and  B  respectively are not
  //C     transposed and set  NROWA, NCOLA and  NROWB  as the number of rows
  //C     and  columns of  A  and the  number of  rows  of  B  respectively.
  //C
  bool nota = lsame(transa, "N");
  bool notb = lsame(transb, "N");
  int nrowa = fem::int0;
  int ncola = fem::int0;
  if (nota) {
    nrowa = m;
    ncola = k;
  }
  else {
    nrowa = k;
    ncola = m;
  }
  int nrowb = fem::int0;
  if (notb) {
    nrowb = k;
  }
  else {
    nrowb = n;
  }
  //C
  //C     Test the input parameters.
  //C
  int info = 0;
  if ((!nota) && (!lsame(transa, "C")) && (!lsame(transa, "T"))) {
    info = 1;
  }
  else if ((!notb) && (!lsame(transb, "C")) && (!lsame(transb, "T"))) {
    info = 2;
  }
  else if (m < 0) {
    info = 3;
  }
  else if (n < 0) {
    info = 4;
  }
  else if (k < 0) {
    info = 5;
  }
  else if (lda < fem::max(1, nrowa)) {
    info = 8;
  }
  else if (ldb < fem::max(1, nrowb)) {
    info = 10;
  }
  else if (ldc < fem::max(1, m)) {
    info = 13;
  }
  if (info != 0) {
    xerbla("DGEMM ", info);
    return;
  }
  //C
  //C     Quick return if possible.
  //C
  const double zero = 0.0e+0;
  const double one = 1.0e+0;
  if ((m == 0) || (n == 0) || (((alpha == zero) || (k == 0)) && (
      beta == one))) {
    return;
  }
  //C
  //C     And if  alpha.eq.zero.
  //C
  int j = fem::int0;
  int i = fem::int0;
  if (alpha == zero) {
    if (beta == zero) {
      FEM_DO(j, 1, n) {
        FEM_DO(i, 1, m) {
          c(i, j) = zero;
        }
      }
    }
    else {
      FEM_DO(j, 1, n) {
        FEM_DO(i, 1, m) {
          c(i, j) = beta * c(i, j);
        }
      }
    }
    return;
  }
  //C
  //C     Start the operations.
  //C
  int l = fem::int0;
  double temp = fem::double0;
  if (notb) {
    if (nota) {
      //C
      //C           Form  C := alpha*A*B + beta*C.
      //C
      FEM_DO(j, 1, n) {
        if (beta == zero) {
          FEM_DO(i, 1, m) {
            c(i, j) = zero;
          }
        }
        else if (beta != one) {
          FEM_DO(i, 1, m) {
            c(i, j) = beta * c(i, j);
          }
        }
        FEM_DO(l, 1, k) {
          if (b(l, j) != zero) {
            temp = alpha * b(l, j);
            FEM_DO(i, 1, m) {
              c(i, j) += temp * a(i, l);
            }
          }
        }
      }
    }
    else {
      //C
      //C           Form  C := alpha*A'*B + beta*C
      //C
      FEM_DO(j, 1, n) {
        FEM_DO(i, 1, m) {
          temp = zero;
          FEM_DO(l, 1, k) {
            temp += a(l, i) * b(l, j);
          }
          if (beta == zero) {
            c(i, j) = alpha * temp;
          }
          else {
            c(i, j) = alpha * temp + beta * c(i, j);
          }
        }
      }
    }
  }
  else {
    if (nota) {
      //C
      //C           Form  C := alpha*A*B' + beta*C
      //C
      FEM_DO(j, 1, n) {
        if (beta == zero) {
          FEM_DO(i, 1, m) {
            c(i, j) = zero;
          }
        }
        else if (beta != one) {
          FEM_DO(i, 1, m) {
            c(i, j) = beta * c(i, j);
          }
        }
        FEM_DO(l, 1, k) {
          if (b(j, l) != zero) {
            temp = alpha * b(j, l);
            FEM_DO(i, 1, m) {
              c(i, j) += temp * a(i, l);
            }
          }
        }
      }
    }
    else {
      //C
      //C           Form  C := alpha*A'*B' + beta*C
      //C
      FEM_DO(j, 1, n) {
        FEM_DO(i, 1, m) {
          temp = zero;
          FEM_DO(l, 1, k) {
            temp += a(l, i) * b(j, l);
          }
          if (beta == zero) {
            c(i, j) = alpha * temp;
          }
          else {
            c(i, j) = alpha * temp + beta * c(i, j);
          }
        }
      }
    }
  }
  //C
  //C     End of DGEMM .
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/BLAS/SRC/dtrmm.f
void
dtrmm(
  str_cref side,
  str_cref uplo,
  str_cref transa,
  str_cref diag,
  int const& m,
  int const& n,
  double const& alpha,
  arr_cref<double, 2> a,
  int const& lda,
  arr_ref<double, 2> b,
  int const& ldb)
{
  a(dimension(lda, star));
  b(dimension(ldb, star));
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DTRMM  performs one of the matrix-matrix operations
  //C
  //C     B := alpha*op( A )*B,   or   B := alpha*B*op( A ),
  //C
  //C  where  alpha  is a scalar,  B  is an m by n matrix,  A  is a unit, or
  //C  non-unit,  upper or lower triangular matrix  and  op( A )  is one  of
  //C
  //C     op( A ) = A   or   op( A ) = A'.
  //C
  //C  Arguments
  //C  ==========
  //C
  //C  SIDE   - CHARACTER*1.
  //C           On entry,  SIDE specifies whether  op( A ) multiplies B from
  //C           the left or right as follows:
  //C
  //C              SIDE = 'L' or 'l'   B := alpha*op( A )*B.
  //C
  //C              SIDE = 'R' or 'r'   B := alpha*B*op( A ).
  //C
  //C           Unchanged on exit.
  //C
  //C  UPLO   - CHARACTER*1.
  //C           On entry, UPLO specifies whether the matrix A is an upper or
  //C           lower triangular matrix as follows:
  //C
  //C              UPLO = 'U' or 'u'   A is an upper triangular matrix.
  //C
  //C              UPLO = 'L' or 'l'   A is a lower triangular matrix.
  //C
  //C           Unchanged on exit.
  //C
  //C  TRANSA - CHARACTER*1.
  //C           On entry, TRANSA specifies the form of op( A ) to be used in
  //C           the matrix multiplication as follows:
  //C
  //C              TRANSA = 'N' or 'n'   op( A ) = A.
  //C
  //C              TRANSA = 'T' or 't'   op( A ) = A'.
  //C
  //C              TRANSA = 'C' or 'c'   op( A ) = A'.
  //C
  //C           Unchanged on exit.
  //C
  //C  DIAG   - CHARACTER*1.
  //C           On entry, DIAG specifies whether or not A is unit triangular
  //C           as follows:
  //C
  //C              DIAG = 'U' or 'u'   A is assumed to be unit triangular.
  //C
  //C              DIAG = 'N' or 'n'   A is not assumed to be unit
  //C                                  triangular.
  //C
  //C           Unchanged on exit.
  //C
  //C  M      - INTEGER.
  //C           On entry, M specifies the number of rows of B. M must be at
  //C           least zero.
  //C           Unchanged on exit.
  //C
  //C  N      - INTEGER.
  //C           On entry, N specifies the number of columns of B.  N must be
  //C           at least zero.
  //C           Unchanged on exit.
  //C
  //C  ALPHA  - DOUBLE PRECISION.
  //C           On entry,  ALPHA specifies the scalar  alpha. When  alpha is
  //C           zero then  A is not referenced and  B need not be set before
  //C           entry.
  //C           Unchanged on exit.
  //C
  //C  A      - DOUBLE PRECISION array of DIMENSION ( LDA, k ), where k is m
  //C           when  SIDE = 'L' or 'l'  and is  n  when  SIDE = 'R' or 'r'.
  //C           Before entry  with  UPLO = 'U' or 'u',  the  leading  k by k
  //C           upper triangular part of the array  A must contain the upper
  //C           triangular matrix  and the strictly lower triangular part of
  //C           A is not referenced.
  //C           Before entry  with  UPLO = 'L' or 'l',  the  leading  k by k
  //C           lower triangular part of the array  A must contain the lower
  //C           triangular matrix  and the strictly upper triangular part of
  //C           A is not referenced.
  //C           Note that when  DIAG = 'U' or 'u',  the diagonal elements of
  //C           A  are not referenced either,  but are assumed to be  unity.
  //C           Unchanged on exit.
  //C
  //C  LDA    - INTEGER.
  //C           On entry, LDA specifies the first dimension of A as declared
  //C           in the calling (sub) program.  When  SIDE = 'L' or 'l'  then
  //C           LDA  must be at least  max( 1, m ),  when  SIDE = 'R' or 'r'
  //C           then LDA must be at least max( 1, n ).
  //C           Unchanged on exit.
  //C
  //C  B      - DOUBLE PRECISION array of DIMENSION ( LDB, n ).
  //C           Before entry,  the leading  m by n part of the array  B must
  //C           contain the matrix  B,  and  on exit  is overwritten  by the
  //C           transformed matrix.
  //C
  //C  LDB    - INTEGER.
  //C           On entry, LDB specifies the first dimension of B as declared
  //C           in  the  calling  (sub)  program.   LDB  must  be  at  least
  //C           max( 1, m ).
  //C           Unchanged on exit.
  //C
  //C  Further Details
  //C  ===============
  //C
  //C  Level 3 Blas routine.
  //C
  //C  -- Written on 8-February-1989.
  //C     Jack Dongarra, Argonne National Laboratory.
  //C     Iain Duff, AERE Harwell.
  //C     Jeremy Du Croz, Numerical Algorithms Group Ltd.
  //C     Sven Hammarling, Numerical Algorithms Group Ltd.
  //C
  //C  =====================================================================
  //C
  //C     .. External Functions ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. Parameters ..
  //C     ..
  //C
  //C     Test the input parameters.
  //C
  bool lside = lsame(side, "L");
  int nrowa = fem::int0;
  if (lside) {
    nrowa = m;
  }
  else {
    nrowa = n;
  }
  bool nounit = lsame(diag, "N");
  bool upper = lsame(uplo, "U");
  //C
  int info = 0;
  if ((!lside) && (!lsame(side, "R"))) {
    info = 1;
  }
  else if ((!upper) && (!lsame(uplo, "L"))) {
    info = 2;
  }
  else if ((!lsame(transa, "N")) && (!lsame(transa, "T")) && (!lsame(transa,
    "C"))) {
    info = 3;
  }
  else if ((!lsame(diag, "U")) && (!lsame(diag, "N"))) {
    info = 4;
  }
  else if (m < 0) {
    info = 5;
  }
  else if (n < 0) {
    info = 6;
  }
  else if (lda < fem::max(1, nrowa)) {
    info = 9;
  }
  else if (ldb < fem::max(1, m)) {
    info = 11;
  }
  if (info != 0) {
    xerbla("DTRMM ", info);
    return;
  }
  //C
  //C     Quick return if possible.
  //C
  if (m == 0 || n == 0) {
    return;
  }
  //C
  //C     And when  alpha.eq.zero.
  //C
  const double zero = 0.0e+0;
  int j = fem::int0;
  int i = fem::int0;
  if (alpha == zero) {
    FEM_DO(j, 1, n) {
      FEM_DO(i, 1, m) {
        b(i, j) = zero;
      }
    }
    return;
  }
  //C
  //C     Start the operations.
  //C
  int k = fem::int0;
  double temp = fem::double0;
  const double one = 1.0e+0;
  if (lside) {
    if (lsame(transa, "N")) {
      //C
      //C           Form  B := alpha*A*B.
      //C
      if (upper) {
        FEM_DO(j, 1, n) {
          FEM_DO(k, 1, m) {
            if (b(k, j) != zero) {
              temp = alpha * b(k, j);
              {
                int fem_do_last = k - 1;
                FEM_DO(i, 1, fem_do_last) {
                  b(i, j) += temp * a(i, k);
                }
              }
              if (nounit) {
                temp = temp * a(k, k);
              }
              b(k, j) = temp;
            }
          }
        }
      }
      else {
        FEM_DO(j, 1, n) {
          FEM_DOSTEP(k, m, 1, -1) {
            if (b(k, j) != zero) {
              temp = alpha * b(k, j);
              b(k, j) = temp;
              if (nounit) {
                b(k, j) = b(k, j) * a(k, k);
              }
              FEM_DO(i, k + 1, m) {
                b(i, j) += temp * a(i, k);
              }
            }
          }
        }
      }
    }
    else {
      //C
      //C           Form  B := alpha*A'*B.
      //C
      if (upper) {
        FEM_DO(j, 1, n) {
          FEM_DOSTEP(i, m, 1, -1) {
            temp = b(i, j);
            if (nounit) {
              temp = temp * a(i, i);
            }
            {
              int fem_do_last = i - 1;
              FEM_DO(k, 1, fem_do_last) {
                temp += a(k, i) * b(k, j);
              }
            }
            b(i, j) = alpha * temp;
          }
        }
      }
      else {
        FEM_DO(j, 1, n) {
          FEM_DO(i, 1, m) {
            temp = b(i, j);
            if (nounit) {
              temp = temp * a(i, i);
            }
            FEM_DO(k, i + 1, m) {
              temp += a(k, i) * b(k, j);
            }
            b(i, j) = alpha * temp;
          }
        }
      }
    }
  }
  else {
    if (lsame(transa, "N")) {
      //C
      //C           Form  B := alpha*B*A.
      //C
      if (upper) {
        FEM_DOSTEP(j, n, 1, -1) {
          temp = alpha;
          if (nounit) {
            temp = temp * a(j, j);
          }
          FEM_DO(i, 1, m) {
            b(i, j) = temp * b(i, j);
          }
          {
            int fem_do_last = j - 1;
            FEM_DO(k, 1, fem_do_last) {
              if (a(k, j) != zero) {
                temp = alpha * a(k, j);
                FEM_DO(i, 1, m) {
                  b(i, j) += temp * b(i, k);
                }
              }
            }
          }
        }
      }
      else {
        FEM_DO(j, 1, n) {
          temp = alpha;
          if (nounit) {
            temp = temp * a(j, j);
          }
          FEM_DO(i, 1, m) {
            b(i, j) = temp * b(i, j);
          }
          FEM_DO(k, j + 1, n) {
            if (a(k, j) != zero) {
              temp = alpha * a(k, j);
              FEM_DO(i, 1, m) {
                b(i, j) += temp * b(i, k);
              }
            }
          }
        }
      }
    }
    else {
      //C
      //C           Form  B := alpha*B*A'.
      //C
      if (upper) {
        FEM_DO(k, 1, n) {
          {
            int fem_do_last = k - 1;
            FEM_DO(j, 1, fem_do_last) {
              if (a(j, k) != zero) {
                temp = alpha * a(j, k);
                FEM_DO(i, 1, m) {
                  b(i, j) += temp * b(i, k);
                }
              }
            }
          }
          temp = alpha;
          if (nounit) {
            temp = temp * a(k, k);
          }
          if (temp != one) {
            FEM_DO(i, 1, m) {
              b(i, k) = temp * b(i, k);
            }
          }
        }
      }
      else {
        FEM_DOSTEP(k, n, 1, -1) {
          FEM_DO(j, k + 1, n) {
            if (a(j, k) != zero) {
              temp = alpha * a(j, k);
              FEM_DO(i, 1, m) {
                b(i, j) += temp * b(i, k);
              }
            }
          }
          temp = alpha;
          if (nounit) {
            temp = temp * a(k, k);
          }
          if (temp != one) {
            FEM_DO(i, 1, m) {
              b(i, k) = temp * b(i, k);
            }
          }
        }
      }
    }
  }
  //C
  //C     End of DTRMM .
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/iladlc.f
int
iladlc(
  int const& m,
  int const& n,
  arr_cref<double, 2> a,
  int const& lda)
{
  int return_value = fem::int0;
  a(dimension(lda, star));
  //C
  //C  -- LAPACK auxiliary routine (version 3.2.1)                        --
  //C
  //C  -- April 2009                                                      --
  //C
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  ILADLC scans A for its last non-zero column.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  M       (input) INTEGER
  //C          The number of rows of the matrix A.
  //C
  //C  N       (input) INTEGER
  //C          The number of columns of the matrix A.
  //C
  //C  A       (input) DOUBLE PRECISION array, dimension (LDA,N)
  //C          The m by n matrix A.
  //C
  //C  LDA     (input) INTEGER
  //C          The leading dimension of the array A. LDA >= max(1,M).
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Quick test for the common case where one corner is non-zero.
  const double zero = 0.0e+0;
  int i = fem::int0;
  if (n == 0) {
    return_value = n;
  }
  else if (a(1, n) != zero || a(m, n) != zero) {
    return_value = n;
  }
  else {
    //C     Now scan each column from the end, returning with the first non-zero.
    FEM_DOSTEP(return_value, n, 1, -1) {
      FEM_DO(i, 1, m) {
        if (a(i, return_value) != zero) {
          return return_value;
        }
      }
    }
  }
  return return_value;
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/iladlr.f
int
iladlr(
  int const& m,
  int const& n,
  arr_cref<double, 2> a,
  int const& lda)
{
  int return_value = fem::int0;
  a(dimension(lda, star));
  //C
  //C  -- LAPACK auxiliary routine (version 3.2.1)                        --
  //C
  //C  -- April 2009                                                      --
  //C
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  ILADLR scans A for its last non-zero row.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  M       (input) INTEGER
  //C          The number of rows of the matrix A.
  //C
  //C  N       (input) INTEGER
  //C          The number of columns of the matrix A.
  //C
  //C  A       (input) DOUBLE PRECISION array, dimension (LDA,N)
  //C          The m by n matrix A.
  //C
  //C  LDA     (input) INTEGER
  //C          The leading dimension of the array A. LDA >= max(1,M).
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Quick test for the common case where one corner is non-zero.
  const double zero = 0.0e+0;
  int j = fem::int0;
  int i = fem::int0;
  if (m == 0) {
    return_value = m;
  }
  else if (a(m, 1) != zero || a(m, n) != zero) {
    return_value = m;
  }
  else {
    //C     Scan up each column tracking the last zero row seen.
    return_value = 0;
    FEM_DO(j, 1, n) {
      FEM_DOSTEP(i, m, 1, -1) {
        if (a(i, j) != zero) {
          break;
        }
      }
      return_value = fem::max(return_value, i);
    }
  }
  return return_value;
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dlarfb.f
void
dlarfb(
  str_cref side,
  str_cref trans,
  str_cref direct,
  str_cref storev,
  int const& m,
  int const& n,
  int const& k,
  arr_cref<double, 2> v,
  int const& ldv,
  arr_cref<double, 2> t,
  int const& ldt,
  arr_ref<double, 2> c,
  int const& ldc,
  arr_ref<double, 2> work,
  int const& ldwork)
{
  v(dimension(ldv, star));
  t(dimension(ldt, star));
  c(dimension(ldc, star));
  work(dimension(ldwork, star));
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DLARFB applies a real block reflector H or its transpose H' to a
  //C  real m by n matrix C, from either the left or the right.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  SIDE    (input) CHARACTER*1
  //C          = 'L': apply H or H' from the Left
  //C          = 'R': apply H or H' from the Right
  //C
  //C  TRANS   (input) CHARACTER*1
  //C          = 'N': apply H (No transpose)
  //C          = 'T': apply H' (Transpose)
  //C
  //C  DIRECT  (input) CHARACTER*1
  //C          Indicates how H is formed from a product of elementary
  //C          reflectors
  //C          = 'F': H = H(1) H(2) . . . H(k) (Forward)
  //C          = 'B': H = H(k) . . . H(2) H(1) (Backward)
  //C
  //C  STOREV  (input) CHARACTER*1
  //C          Indicates how the vectors which define the elementary
  //C          reflectors are stored:
  //C          = 'C': Columnwise
  //C          = 'R': Rowwise
  //C
  //C  M       (input) INTEGER
  //C          The number of rows of the matrix C.
  //C
  //C  N       (input) INTEGER
  //C          The number of columns of the matrix C.
  //C
  //C  K       (input) INTEGER
  //C          The order of the matrix T (= the number of elementary
  //C          reflectors whose product defines the block reflector).
  //C
  //C  V       (input) DOUBLE PRECISION array, dimension
  //C                                (LDV,K) if STOREV = 'C'
  //C                                (LDV,M) if STOREV = 'R' and SIDE = 'L'
  //C                                (LDV,N) if STOREV = 'R' and SIDE = 'R'
  //C          The matrix V. See further details.
  //C
  //C  LDV     (input) INTEGER
  //C          The leading dimension of the array V.
  //C          If STOREV = 'C' and SIDE = 'L', LDV >= max(1,M);
  //C          if STOREV = 'C' and SIDE = 'R', LDV >= max(1,N);
  //C          if STOREV = 'R', LDV >= K.
  //C
  //C  T       (input) DOUBLE PRECISION array, dimension (LDT,K)
  //C          The triangular k by k matrix T in the representation of the
  //C          block reflector.
  //C
  //C  LDT     (input) INTEGER
  //C          The leading dimension of the array T. LDT >= K.
  //C
  //C  C       (input/output) DOUBLE PRECISION array, dimension (LDC,N)
  //C          On entry, the m by n matrix C.
  //C          On exit, C is overwritten by H*C or H'*C or C*H or C*H'.
  //C
  //C  LDC     (input) INTEGER
  //C          The leading dimension of the array C. LDA >= max(1,M).
  //C
  //C  WORK    (workspace) DOUBLE PRECISION array, dimension (LDWORK,K)
  //C
  //C  LDWORK  (input) INTEGER
  //C          The leading dimension of the array WORK.
  //C          If SIDE = 'L', LDWORK >= max(1,N);
  //C          if SIDE = 'R', LDWORK >= max(1,M).
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Quick return if possible
  //C
  if (m <= 0 || n <= 0) {
    return;
  }
  //C
  fem::str<1> transt = fem::char0;
  if (lsame(trans, "N")) {
    transt = "T";
  }
  else {
    transt = "N";
  }
  //C
  int lastv = fem::int0;
  int lastc = fem::int0;
  int j = fem::int0;
  const double one = 1.0e+0;
  int i = fem::int0;
  if (lsame(storev, "C")) {
    //C
    if (lsame(direct, "F")) {
      //C
      //C           Let  V =  ( V1 )    (first K rows)
      //C                     ( V2 )
      //C           where  V1  is unit lower triangular.
      //C
      if (lsame(side, "L")) {
        //C
        //C              Form  H * C  or  H' * C  where  C = ( C1 )
        //C                                                  ( C2 )
        //C
        lastv = fem::max(k, iladlr(m, k, v, ldv));
        lastc = iladlc(lastv, n, c, ldc);
        //C
        //C              W := C' * V  =  (C1'*V1 + C2'*V2)  (stored in WORK)
        //C
        //C              W := C1'
        //C
        FEM_DO(j, 1, k) {
          dcopy(lastc, c(j, 1), ldc, work(1, j), 1);
        }
        //C
        //C              W := W * V1
        //C
        dtrmm("Right", "Lower", "No transpose", "Unit", lastc, k,
          one, v, ldv, work, ldwork);
        if (lastv > k) {
          //C
          //C                 W := W + C2'*V2
          //C
          dgemm("Transpose", "No transpose", lastc, k, lastv - k,
            one, c(k + 1, 1), ldc, v(k + 1, 1), ldv, one, work,
            ldwork);
        }
        //C
        //C              W := W * T'  or  W * T
        //C
        dtrmm("Right", "Upper", transt, "Non-unit", lastc, k, one, t,
          ldt, work, ldwork);
        //C
        //C              C := C - V * W'
        //C
        if (lastv > k) {
          //C
          //C                 C2 := C2 - V2 * W'
          //C
          dgemm("No transpose", "Transpose", lastv - k, lastc, k,
            -one, v(k + 1, 1), ldv, work, ldwork, one, c(k + 1, 1),
            ldc);
        }
        //C
        //C              W := W * V1'
        //C
        dtrmm("Right", "Lower", "Transpose", "Unit", lastc, k, one,
          v, ldv, work, ldwork);
        //C
        //C              C1 := C1 - W'
        //C
        FEM_DO(j, 1, k) {
          FEM_DO(i, 1, lastc) {
            c(j, i) = c(j, i) - work(i, j);
          }
        }
        //C
      }
      else if (lsame(side, "R")) {
        //C
        //C              Form  C * H  or  C * H'  where  C = ( C1  C2 )
        //C
        lastv = fem::max(k, iladlr(n, k, v, ldv));
        lastc = iladlr(m, lastv, c, ldc);
        //C
        //C              W := C * V  =  (C1*V1 + C2*V2)  (stored in WORK)
        //C
        //C              W := C1
        //C
        FEM_DO(j, 1, k) {
          dcopy(lastc, c(1, j), 1, work(1, j), 1);
        }
        //C
        //C              W := W * V1
        //C
        dtrmm("Right", "Lower", "No transpose", "Unit", lastc, k,
          one, v, ldv, work, ldwork);
        if (lastv > k) {
          //C
          //C                 W := W + C2 * V2
          //C
          dgemm("No transpose", "No transpose", lastc, k, lastv - k,
            one, c(1, k + 1), ldc, v(k + 1, 1), ldv, one, work,
            ldwork);
        }
        //C
        //C              W := W * T  or  W * T'
        //C
        dtrmm("Right", "Upper", trans, "Non-unit", lastc, k, one, t,
          ldt, work, ldwork);
        //C
        //C              C := C - W * V'
        //C
        if (lastv > k) {
          //C
          //C                 C2 := C2 - W * V2'
          //C
          dgemm("No transpose", "Transpose", lastc, lastv - k, k,
            -one, work, ldwork, v(k + 1, 1), ldv, one, c(1, k + 1),
            ldc);
        }
        //C
        //C              W := W * V1'
        //C
        dtrmm("Right", "Lower", "Transpose", "Unit", lastc, k, one,
          v, ldv, work, ldwork);
        //C
        //C              C1 := C1 - W
        //C
        FEM_DO(j, 1, k) {
          FEM_DO(i, 1, lastc) {
            c(i, j) = c(i, j) - work(i, j);
          }
        }
      }
      //C
    }
    else {
      //C
      //C           Let  V =  ( V1 )
      //C                     ( V2 )    (last K rows)
      //C           where  V2  is unit upper triangular.
      //C
      if (lsame(side, "L")) {
        //C
        //C              Form  H * C  or  H' * C  where  C = ( C1 )
        //C                                                  ( C2 )
        //C
        lastv = fem::max(k, iladlr(m, k, v, ldv));
        lastc = iladlc(lastv, n, c, ldc);
        //C
        //C              W := C' * V  =  (C1'*V1 + C2'*V2)  (stored in WORK)
        //C
        //C              W := C2'
        //C
        FEM_DO(j, 1, k) {
          dcopy(lastc, c(lastv - k + j, 1), ldc, work(1, j), 1);
        }
        //C
        //C              W := W * V2
        //C
        dtrmm("Right", "Upper", "No transpose", "Unit", lastc, k,
          one, v(lastv - k + 1, 1), ldv, work, ldwork);
        if (lastv > k) {
          //C
          //C                 W := W + C1'*V1
          //C
          dgemm("Transpose", "No transpose", lastc, k, lastv - k,
            one, c, ldc, v, ldv, one, work, ldwork);
        }
        //C
        //C              W := W * T'  or  W * T
        //C
        dtrmm("Right", "Lower", transt, "Non-unit", lastc, k, one, t,
          ldt, work, ldwork);
        //C
        //C              C := C - V * W'
        //C
        if (lastv > k) {
          //C
          //C                 C1 := C1 - V1 * W'
          //C
          dgemm("No transpose", "Transpose", lastv - k, lastc, k,
            -one, v, ldv, work, ldwork, one, c, ldc);
        }
        //C
        //C              W := W * V2'
        //C
        dtrmm("Right", "Upper", "Transpose", "Unit", lastc, k, one, v(
          lastv - k + 1, 1), ldv, work, ldwork);
        //C
        //C              C2 := C2 - W'
        //C
        FEM_DO(j, 1, k) {
          FEM_DO(i, 1, lastc) {
            c(lastv - k + j, i) = c(lastv - k + j, i) - work(i, j);
          }
        }
        //C
      }
      else if (lsame(side, "R")) {
        //C
        //C              Form  C * H  or  C * H'  where  C = ( C1  C2 )
        //C
        lastv = fem::max(k, iladlr(n, k, v, ldv));
        lastc = iladlr(m, lastv, c, ldc);
        //C
        //C              W := C * V  =  (C1*V1 + C2*V2)  (stored in WORK)
        //C
        //C              W := C2
        //C
        FEM_DO(j, 1, k) {
          dcopy(lastc, c(1, n - k + j), 1, work(1, j), 1);
        }
        //C
        //C              W := W * V2
        //C
        dtrmm("Right", "Upper", "No transpose", "Unit", lastc, k,
          one, v(lastv - k + 1, 1), ldv, work, ldwork);
        if (lastv > k) {
          //C
          //C                 W := W + C1 * V1
          //C
          dgemm("No transpose", "No transpose", lastc, k, lastv - k,
            one, c, ldc, v, ldv, one, work, ldwork);
        }
        //C
        //C              W := W * T  or  W * T'
        //C
        dtrmm("Right", "Lower", trans, "Non-unit", lastc, k, one, t,
          ldt, work, ldwork);
        //C
        //C              C := C - W * V'
        //C
        if (lastv > k) {
          //C
          //C                 C1 := C1 - W * V1'
          //C
          dgemm("No transpose", "Transpose", lastc, lastv - k, k,
            -one, work, ldwork, v, ldv, one, c, ldc);
        }
        //C
        //C              W := W * V2'
        //C
        dtrmm("Right", "Upper", "Transpose", "Unit", lastc, k, one, v(
          lastv - k + 1, 1), ldv, work, ldwork);
        //C
        //C              C2 := C2 - W
        //C
        FEM_DO(j, 1, k) {
          FEM_DO(i, 1, lastc) {
            c(i, lastv - k + j) = c(i, lastv - k + j) - work(i, j);
          }
        }
      }
    }
    //C
  }
  else if (lsame(storev, "R")) {
    //C
    if (lsame(direct, "F")) {
      //C
      //C           Let  V =  ( V1  V2 )    (V1: first K columns)
      //C           where  V1  is unit upper triangular.
      //C
      if (lsame(side, "L")) {
        //C
        //C              Form  H * C  or  H' * C  where  C = ( C1 )
        //C                                                  ( C2 )
        //C
        lastv = fem::max(k, iladlc(k, m, v, ldv));
        lastc = iladlc(lastv, n, c, ldc);
        //C
        //C              W := C' * V'  =  (C1'*V1' + C2'*V2') (stored in WORK)
        //C
        //C              W := C1'
        //C
        FEM_DO(j, 1, k) {
          dcopy(lastc, c(j, 1), ldc, work(1, j), 1);
        }
        //C
        //C              W := W * V1'
        //C
        dtrmm("Right", "Upper", "Transpose", "Unit", lastc, k, one,
          v, ldv, work, ldwork);
        if (lastv > k) {
          //C
          //C                 W := W + C2'*V2'
          //C
          dgemm("Transpose", "Transpose", lastc, k, lastv - k, one, c(k + 1,
            1), ldc, v(1, k + 1), ldv, one, work, ldwork);
        }
        //C
        //C              W := W * T'  or  W * T
        //C
        dtrmm("Right", "Upper", transt, "Non-unit", lastc, k, one, t,
          ldt, work, ldwork);
        //C
        //C              C := C - V' * W'
        //C
        if (lastv > k) {
          //C
          //C                 C2 := C2 - V2' * W'
          //C
          dgemm("Transpose", "Transpose", lastv - k, lastc, k, -one,
            v(1, k + 1), ldv, work, ldwork, one, c(k + 1, 1), ldc);
        }
        //C
        //C              W := W * V1
        //C
        dtrmm("Right", "Upper", "No transpose", "Unit", lastc, k,
          one, v, ldv, work, ldwork);
        //C
        //C              C1 := C1 - W'
        //C
        FEM_DO(j, 1, k) {
          FEM_DO(i, 1, lastc) {
            c(j, i) = c(j, i) - work(i, j);
          }
        }
        //C
      }
      else if (lsame(side, "R")) {
        //C
        //C              Form  C * H  or  C * H'  where  C = ( C1  C2 )
        //C
        lastv = fem::max(k, iladlc(k, n, v, ldv));
        lastc = iladlr(m, lastv, c, ldc);
        //C
        //C              W := C * V'  =  (C1*V1' + C2*V2')  (stored in WORK)
        //C
        //C              W := C1
        //C
        FEM_DO(j, 1, k) {
          dcopy(lastc, c(1, j), 1, work(1, j), 1);
        }
        //C
        //C              W := W * V1'
        //C
        dtrmm("Right", "Upper", "Transpose", "Unit", lastc, k, one,
          v, ldv, work, ldwork);
        if (lastv > k) {
          //C
          //C                 W := W + C2 * V2'
          //C
          dgemm("No transpose", "Transpose", lastc, k, lastv - k,
            one, c(1, k + 1), ldc, v(1, k + 1), ldv, one, work,
            ldwork);
        }
        //C
        //C              W := W * T  or  W * T'
        //C
        dtrmm("Right", "Upper", trans, "Non-unit", lastc, k, one, t,
          ldt, work, ldwork);
        //C
        //C              C := C - W * V
        //C
        if (lastv > k) {
          //C
          //C                 C2 := C2 - W * V2
          //C
          dgemm("No transpose", "No transpose", lastc, lastv - k, k,
            -one, work, ldwork, v(1, k + 1), ldv, one, c(1, k + 1),
            ldc);
        }
        //C
        //C              W := W * V1
        //C
        dtrmm("Right", "Upper", "No transpose", "Unit", lastc, k,
          one, v, ldv, work, ldwork);
        //C
        //C              C1 := C1 - W
        //C
        FEM_DO(j, 1, k) {
          FEM_DO(i, 1, lastc) {
            c(i, j) = c(i, j) - work(i, j);
          }
        }
        //C
      }
      //C
    }
    else {
      //C
      //C           Let  V =  ( V1  V2 )    (V2: last K columns)
      //C           where  V2  is unit lower triangular.
      //C
      if (lsame(side, "L")) {
        //C
        //C              Form  H * C  or  H' * C  where  C = ( C1 )
        //C                                                  ( C2 )
        //C
        lastv = fem::max(k, iladlc(k, m, v, ldv));
        lastc = iladlc(lastv, n, c, ldc);
        //C
        //C              W := C' * V'  =  (C1'*V1' + C2'*V2') (stored in WORK)
        //C
        //C              W := C2'
        //C
        FEM_DO(j, 1, k) {
          dcopy(lastc, c(lastv - k + j, 1), ldc, work(1, j), 1);
        }
        //C
        //C              W := W * V2'
        //C
        dtrmm("Right", "Lower", "Transpose", "Unit", lastc, k, one, v(1,
          lastv - k + 1), ldv, work, ldwork);
        if (lastv > k) {
          //C
          //C                 W := W + C1'*V1'
          //C
          dgemm("Transpose", "Transpose", lastc, k, lastv - k, one,
            c, ldc, v, ldv, one, work, ldwork);
        }
        //C
        //C              W := W * T'  or  W * T
        //C
        dtrmm("Right", "Lower", transt, "Non-unit", lastc, k, one, t,
          ldt, work, ldwork);
        //C
        //C              C := C - V' * W'
        //C
        if (lastv > k) {
          //C
          //C                 C1 := C1 - V1' * W'
          //C
          dgemm("Transpose", "Transpose", lastv - k, lastc, k, -one,
            v, ldv, work, ldwork, one, c, ldc);
        }
        //C
        //C              W := W * V2
        //C
        dtrmm("Right", "Lower", "No transpose", "Unit", lastc, k,
          one, v(1, lastv - k + 1), ldv, work, ldwork);
        //C
        //C              C2 := C2 - W'
        //C
        FEM_DO(j, 1, k) {
          FEM_DO(i, 1, lastc) {
            c(lastv - k + j, i) = c(lastv - k + j, i) - work(i, j);
          }
        }
        //C
      }
      else if (lsame(side, "R")) {
        //C
        //C              Form  C * H  or  C * H'  where  C = ( C1  C2 )
        //C
        lastv = fem::max(k, iladlc(k, n, v, ldv));
        lastc = iladlr(m, lastv, c, ldc);
        //C
        //C              W := C * V'  =  (C1*V1' + C2*V2')  (stored in WORK)
        //C
        //C              W := C2
        //C
        FEM_DO(j, 1, k) {
          dcopy(lastc, c(1, lastv - k + j), 1, work(1, j), 1);
        }
        //C
        //C              W := W * V2'
        //C
        dtrmm("Right", "Lower", "Transpose", "Unit", lastc, k, one, v(1,
          lastv - k + 1), ldv, work, ldwork);
        if (lastv > k) {
          //C
          //C                 W := W + C1 * V1'
          //C
          dgemm("No transpose", "Transpose", lastc, k, lastv - k,
            one, c, ldc, v, ldv, one, work, ldwork);
        }
        //C
        //C              W := W * T  or  W * T'
        //C
        dtrmm("Right", "Lower", trans, "Non-unit", lastc, k, one, t,
          ldt, work, ldwork);
        //C
        //C              C := C - W * V
        //C
        if (lastv > k) {
          //C
          //C                 C1 := C1 - W * V1
          //C
          dgemm("No transpose", "No transpose", lastc, lastv - k, k,
            -one, work, ldwork, v, ldv, one, c, ldc);
        }
        //C
        //C              W := W * V2
        //C
        dtrmm("Right", "Lower", "No transpose", "Unit", lastc, k,
          one, v(1, lastv - k + 1), ldv, work, ldwork);
        //C
        //C              C1 := C1 - W
        //C
        FEM_DO(j, 1, k) {
          FEM_DO(i, 1, lastc) {
            c(i, lastv - k + j) = c(i, lastv - k + j) - work(i, j);
          }
        }
        //C
      }
      //C
    }
  }
  //C
  //C     End of DLARFB
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/BLAS/SRC/dgemv.f
void
dgemv(
  str_cref trans,
  int const& m,
  int const& n,
  double const& alpha,
  arr_cref<double, 2> a,
  int const& lda,
  arr_cref<double> x,
  int const& incx,
  double const& beta,
  arr_ref<double> y,
  int const& incy)
{
  a(dimension(lda, star));
  x(dimension(star));
  y(dimension(star));
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DGEMV  performs one of the matrix-vector operations
  //C
  //C     y := alpha*A*x + beta*y,   or   y := alpha*A'*x + beta*y,
  //C
  //C  where alpha and beta are scalars, x and y are vectors and A is an
  //C  m by n matrix.
  //C
  //C  Arguments
  //C  ==========
  //C
  //C  TRANS  - CHARACTER*1.
  //C           On entry, TRANS specifies the operation to be performed as
  //C           follows:
  //C
  //C              TRANS = 'N' or 'n'   y := alpha*A*x + beta*y.
  //C
  //C              TRANS = 'T' or 't'   y := alpha*A'*x + beta*y.
  //C
  //C              TRANS = 'C' or 'c'   y := alpha*A'*x + beta*y.
  //C
  //C           Unchanged on exit.
  //C
  //C  M      - INTEGER.
  //C           On entry, M specifies the number of rows of the matrix A.
  //C           M must be at least zero.
  //C           Unchanged on exit.
  //C
  //C  N      - INTEGER.
  //C           On entry, N specifies the number of columns of the matrix A.
  //C           N must be at least zero.
  //C           Unchanged on exit.
  //C
  //C  ALPHA  - DOUBLE PRECISION.
  //C           On entry, ALPHA specifies the scalar alpha.
  //C           Unchanged on exit.
  //C
  //C  A      - DOUBLE PRECISION array of DIMENSION ( LDA, n ).
  //C           Before entry, the leading m by n part of the array A must
  //C           contain the matrix of coefficients.
  //C           Unchanged on exit.
  //C
  //C  LDA    - INTEGER.
  //C           On entry, LDA specifies the first dimension of A as declared
  //C           in the calling (sub) program. LDA must be at least
  //C           max( 1, m ).
  //C           Unchanged on exit.
  //C
  //C  X      - DOUBLE PRECISION array of DIMENSION at least
  //C           ( 1 + ( n - 1 )*abs( INCX ) ) when TRANS = 'N' or 'n'
  //C           and at least
  //C           ( 1 + ( m - 1 )*abs( INCX ) ) otherwise.
  //C           Before entry, the incremented array X must contain the
  //C           vector x.
  //C           Unchanged on exit.
  //C
  //C  INCX   - INTEGER.
  //C           On entry, INCX specifies the increment for the elements of
  //C           X. INCX must not be zero.
  //C           Unchanged on exit.
  //C
  //C  BETA   - DOUBLE PRECISION.
  //C           On entry, BETA specifies the scalar beta. When BETA is
  //C           supplied as zero then Y need not be set on input.
  //C           Unchanged on exit.
  //C
  //C  Y      - DOUBLE PRECISION array of DIMENSION at least
  //C           ( 1 + ( m - 1 )*abs( INCY ) ) when TRANS = 'N' or 'n'
  //C           and at least
  //C           ( 1 + ( n - 1 )*abs( INCY ) ) otherwise.
  //C           Before entry with BETA non-zero, the incremented array Y
  //C           must contain the vector y. On exit, Y is overwritten by the
  //C           updated vector y.
  //C
  //C  INCY   - INTEGER.
  //C           On entry, INCY specifies the increment for the elements of
  //C           Y. INCY must not be zero.
  //C           Unchanged on exit.
  //C
  //C  Further Details
  //C  ===============
  //C
  //C  Level 2 Blas routine.
  //C
  //C  -- Written on 22-October-1986.
  //C     Jack Dongarra, Argonne National Lab.
  //C     Jeremy Du Croz, Nag Central Office.
  //C     Sven Hammarling, Nag Central Office.
  //C     Richard Hanson, Sandia National Labs.
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C
  //C     Test the input parameters.
  //C
  int info = 0;
  if (!lsame(trans, "N") && !lsame(trans, "T") && !lsame(trans, "C")) {
    info = 1;
  }
  else if (m < 0) {
    info = 2;
  }
  else if (n < 0) {
    info = 3;
  }
  else if (lda < fem::max(1, m)) {
    info = 6;
  }
  else if (incx == 0) {
    info = 8;
  }
  else if (incy == 0) {
    info = 11;
  }
  if (info != 0) {
    xerbla("DGEMV ", info);
    return;
  }
  //C
  //C     Quick return if possible.
  //C
  const double zero = 0.0e+0;
  const double one = 1.0e+0;
  if ((m == 0) || (n == 0) || ((alpha == zero) && (beta == one))) {
    return;
  }
  //C
  //C     Set  LENX  and  LENY, the lengths of the vectors x and y, and set
  //C     up the start points in  X  and  Y.
  //C
  int lenx = fem::int0;
  int leny = fem::int0;
  if (lsame(trans, "N")) {
    lenx = n;
    leny = m;
  }
  else {
    lenx = m;
    leny = n;
  }
  int kx = fem::int0;
  if (incx > 0) {
    kx = 1;
  }
  else {
    kx = 1 - (lenx - 1) * incx;
  }
  int ky = fem::int0;
  if (incy > 0) {
    ky = 1;
  }
  else {
    ky = 1 - (leny - 1) * incy;
  }
  //C
  //C     Start the operations. In this version the elements of A are
  //C     accessed sequentially with one pass through A.
  //C
  //C     First form  y := beta*y.
  //C
  int i = fem::int0;
  int iy = fem::int0;
  if (beta != one) {
    if (incy == 1) {
      if (beta == zero) {
        FEM_DO(i, 1, leny) {
          y(i) = zero;
        }
      }
      else {
        FEM_DO(i, 1, leny) {
          y(i) = beta * y(i);
        }
      }
    }
    else {
      iy = ky;
      if (beta == zero) {
        FEM_DO(i, 1, leny) {
          y(iy) = zero;
          iy += incy;
        }
      }
      else {
        FEM_DO(i, 1, leny) {
          y(iy) = beta * y(iy);
          iy += incy;
        }
      }
    }
  }
  if (alpha == zero) {
    return;
  }
  int jx = fem::int0;
  int j = fem::int0;
  double temp = fem::double0;
  int jy = fem::int0;
  int ix = fem::int0;
  if (lsame(trans, "N")) {
    //C
    //C        Form  y := alpha*A*x + y.
    //C
    jx = kx;
    if (incy == 1) {
      FEM_DO(j, 1, n) {
        if (x(jx) != zero) {
          temp = alpha * x(jx);
          FEM_DO(i, 1, m) {
            y(i) += temp * a(i, j);
          }
        }
        jx += incx;
      }
    }
    else {
      FEM_DO(j, 1, n) {
        if (x(jx) != zero) {
          temp = alpha * x(jx);
          iy = ky;
          FEM_DO(i, 1, m) {
            y(iy) += temp * a(i, j);
            iy += incy;
          }
        }
        jx += incx;
      }
    }
  }
  else {
    //C
    //C        Form  y := alpha*A'*x + y.
    //C
    jy = ky;
    if (incx == 1) {
      FEM_DO(j, 1, n) {
        temp = zero;
        FEM_DO(i, 1, m) {
          temp += a(i, j) * x(i);
        }
        y(jy) += alpha * temp;
        jy += incy;
      }
    }
    else {
      FEM_DO(j, 1, n) {
        temp = zero;
        ix = kx;
        FEM_DO(i, 1, m) {
          temp += a(i, j) * x(ix);
          ix += incx;
        }
        y(jy) += alpha * temp;
        jy += incy;
      }
    }
  }
  //C
  //C     End of DGEMV .
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/BLAS/SRC/dtrmv.f
void
dtrmv(
  str_cref uplo,
  str_cref trans,
  str_cref diag,
  int const& n,
  arr_cref<double, 2> a,
  int const& lda,
  arr_ref<double> x,
  int const& incx)
{
  a(dimension(lda, star));
  x(dimension(star));
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DTRMV  performs one of the matrix-vector operations
  //C
  //C     x := A*x,   or   x := A'*x,
  //C
  //C  where x is an n element vector and  A is an n by n unit, or non-unit,
  //C  upper or lower triangular matrix.
  //C
  //C  Arguments
  //C  ==========
  //C
  //C  UPLO   - CHARACTER*1.
  //C           On entry, UPLO specifies whether the matrix is an upper or
  //C           lower triangular matrix as follows:
  //C
  //C              UPLO = 'U' or 'u'   A is an upper triangular matrix.
  //C
  //C              UPLO = 'L' or 'l'   A is a lower triangular matrix.
  //C
  //C           Unchanged on exit.
  //C
  //C  TRANS  - CHARACTER*1.
  //C           On entry, TRANS specifies the operation to be performed as
  //C           follows:
  //C
  //C              TRANS = 'N' or 'n'   x := A*x.
  //C
  //C              TRANS = 'T' or 't'   x := A'*x.
  //C
  //C              TRANS = 'C' or 'c'   x := A'*x.
  //C
  //C           Unchanged on exit.
  //C
  //C  DIAG   - CHARACTER*1.
  //C           On entry, DIAG specifies whether or not A is unit
  //C           triangular as follows:
  //C
  //C              DIAG = 'U' or 'u'   A is assumed to be unit triangular.
  //C
  //C              DIAG = 'N' or 'n'   A is not assumed to be unit
  //C                                  triangular.
  //C
  //C           Unchanged on exit.
  //C
  //C  N      - INTEGER.
  //C           On entry, N specifies the order of the matrix A.
  //C           N must be at least zero.
  //C           Unchanged on exit.
  //C
  //C  A      - DOUBLE PRECISION array of DIMENSION ( LDA, n ).
  //C           Before entry with  UPLO = 'U' or 'u', the leading n by n
  //C           upper triangular part of the array A must contain the upper
  //C           triangular matrix and the strictly lower triangular part of
  //C           A is not referenced.
  //C           Before entry with UPLO = 'L' or 'l', the leading n by n
  //C           lower triangular part of the array A must contain the lower
  //C           triangular matrix and the strictly upper triangular part of
  //C           A is not referenced.
  //C           Note that when  DIAG = 'U' or 'u', the diagonal elements of
  //C           A are not referenced either, but are assumed to be unity.
  //C           Unchanged on exit.
  //C
  //C  LDA    - INTEGER.
  //C           On entry, LDA specifies the first dimension of A as declared
  //C           in the calling (sub) program. LDA must be at least
  //C           max( 1, n ).
  //C           Unchanged on exit.
  //C
  //C  X      - DOUBLE PRECISION array of dimension at least
  //C           ( 1 + ( n - 1 )*abs( INCX ) ).
  //C           Before entry, the incremented array X must contain the n
  //C           element vector x. On exit, X is overwritten with the
  //C           tranformed vector x.
  //C
  //C  INCX   - INTEGER.
  //C           On entry, INCX specifies the increment for the elements of
  //C           X. INCX must not be zero.
  //C           Unchanged on exit.
  //C
  //C  Further Details
  //C  ===============
  //C
  //C  Level 2 Blas routine.
  //C
  //C  -- Written on 22-October-1986.
  //C     Jack Dongarra, Argonne National Lab.
  //C     Jeremy Du Croz, Nag Central Office.
  //C     Sven Hammarling, Nag Central Office.
  //C     Richard Hanson, Sandia National Labs.
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C
  //C     Test the input parameters.
  //C
  int info = 0;
  if (!lsame(uplo, "U") && !lsame(uplo, "L")) {
    info = 1;
  }
  else if (!lsame(trans, "N") && !lsame(trans, "T") && !lsame(trans, "C")) {
    info = 2;
  }
  else if (!lsame(diag, "U") && !lsame(diag, "N")) {
    info = 3;
  }
  else if (n < 0) {
    info = 4;
  }
  else if (lda < fem::max(1, n)) {
    info = 6;
  }
  else if (incx == 0) {
    info = 8;
  }
  if (info != 0) {
    xerbla("DTRMV ", info);
    return;
  }
  //C
  //C     Quick return if possible.
  //C
  if (n == 0) {
    return;
  }
  //C
  bool nounit = lsame(diag, "N");
  //C
  //C     Set up the start point in X if the increment is not unity. This
  //C     will be  ( N - 1 )*INCX  too small for descending loops.
  //C
  int kx = fem::int0;
  if (incx <= 0) {
    kx = 1 - (n - 1) * incx;
  }
  else if (incx != 1) {
    kx = 1;
  }
  //C
  //C     Start the operations. In this version the elements of A are
  //C     accessed sequentially with one pass through A.
  //C
  int j = fem::int0;
  const double zero = 0.0e+0;
  double temp = fem::double0;
  int i = fem::int0;
  int jx = fem::int0;
  int ix = fem::int0;
  if (lsame(trans, "N")) {
    //C
    //C        Form  x := A*x.
    //C
    if (lsame(uplo, "U")) {
      if (incx == 1) {
        FEM_DO(j, 1, n) {
          if (x(j) != zero) {
            temp = x(j);
            {
              int fem_do_last = j - 1;
              FEM_DO(i, 1, fem_do_last) {
                x(i) += temp * a(i, j);
              }
            }
            if (nounit) {
              x(j) = x(j) * a(j, j);
            }
          }
        }
      }
      else {
        jx = kx;
        FEM_DO(j, 1, n) {
          if (x(jx) != zero) {
            temp = x(jx);
            ix = kx;
            {
              int fem_do_last = j - 1;
              FEM_DO(i, 1, fem_do_last) {
                x(ix) += temp * a(i, j);
                ix += incx;
              }
            }
            if (nounit) {
              x(jx) = x(jx) * a(j, j);
            }
          }
          jx += incx;
        }
      }
    }
    else {
      if (incx == 1) {
        FEM_DOSTEP(j, n, 1, -1) {
          if (x(j) != zero) {
            temp = x(j);
            FEM_DOSTEP(i, n, j + 1, -1) {
              x(i) += temp * a(i, j);
            }
            if (nounit) {
              x(j) = x(j) * a(j, j);
            }
          }
        }
      }
      else {
        kx += (n - 1) * incx;
        jx = kx;
        FEM_DOSTEP(j, n, 1, -1) {
          if (x(jx) != zero) {
            temp = x(jx);
            ix = kx;
            FEM_DOSTEP(i, n, j + 1, -1) {
              x(ix) += temp * a(i, j);
              ix = ix - incx;
            }
            if (nounit) {
              x(jx) = x(jx) * a(j, j);
            }
          }
          jx = jx - incx;
        }
      }
    }
  }
  else {
    //C
    //C        Form  x := A'*x.
    //C
    if (lsame(uplo, "U")) {
      if (incx == 1) {
        FEM_DOSTEP(j, n, 1, -1) {
          temp = x(j);
          if (nounit) {
            temp = temp * a(j, j);
          }
          FEM_DOSTEP(i, j - 1, 1, -1) {
            temp += a(i, j) * x(i);
          }
          x(j) = temp;
        }
      }
      else {
        jx = kx + (n - 1) * incx;
        FEM_DOSTEP(j, n, 1, -1) {
          temp = x(jx);
          ix = jx;
          if (nounit) {
            temp = temp * a(j, j);
          }
          FEM_DOSTEP(i, j - 1, 1, -1) {
            ix = ix - incx;
            temp += a(i, j) * x(ix);
          }
          x(jx) = temp;
          jx = jx - incx;
        }
      }
    }
    else {
      if (incx == 1) {
        FEM_DO(j, 1, n) {
          temp = x(j);
          if (nounit) {
            temp = temp * a(j, j);
          }
          FEM_DO(i, j + 1, n) {
            temp += a(i, j) * x(i);
          }
          x(j) = temp;
        }
      }
      else {
        jx = kx;
        FEM_DO(j, 1, n) {
          temp = x(jx);
          ix = jx;
          if (nounit) {
            temp = temp * a(j, j);
          }
          FEM_DO(i, j + 1, n) {
            ix += incx;
            temp += a(i, j) * x(ix);
          }
          x(jx) = temp;
          jx += incx;
        }
      }
    }
  }
  //C
  //C     End of DTRMV .
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dlarft.f
void
dlarft(
  str_cref direct,
  str_cref storev,
  int const& n,
  int const& k,
  arr_ref<double, 2> v,
  int const& ldv,
  arr_cref<double> tau,
  arr_ref<double, 2> t,
  int const& ldt)
{
  v(dimension(ldv, star));
  tau(dimension(star));
  t(dimension(ldt, star));
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DLARFT forms the triangular factor T of a real block reflector H
  //C  of order n, which is defined as a product of k elementary reflectors.
  //C
  //C  If DIRECT = 'F', H = H(1) H(2) . . . H(k) and T is upper triangular;
  //C
  //C  If DIRECT = 'B', H = H(k) . . . H(2) H(1) and T is lower triangular.
  //C
  //C  If STOREV = 'C', the vector which defines the elementary reflector
  //C  H(i) is stored in the i-th column of the array V, and
  //C
  //C     H  =  I - V * T * V'
  //C
  //C  If STOREV = 'R', the vector which defines the elementary reflector
  //C  H(i) is stored in the i-th row of the array V, and
  //C
  //C     H  =  I - V' * T * V
  //C
  //C  Arguments
  //C  =========
  //C
  //C  DIRECT  (input) CHARACTER*1
  //C          Specifies the order in which the elementary reflectors are
  //C          multiplied to form the block reflector:
  //C          = 'F': H = H(1) H(2) . . . H(k) (Forward)
  //C          = 'B': H = H(k) . . . H(2) H(1) (Backward)
  //C
  //C  STOREV  (input) CHARACTER*1
  //C          Specifies how the vectors which define the elementary
  //C          reflectors are stored (see also Further Details):
  //C          = 'C': columnwise
  //C          = 'R': rowwise
  //C
  //C  N       (input) INTEGER
  //C          The order of the block reflector H. N >= 0.
  //C
  //C  K       (input) INTEGER
  //C          The order of the triangular factor T (= the number of
  //C          elementary reflectors). K >= 1.
  //C
  //C  V       (input/output) DOUBLE PRECISION array, dimension
  //C                               (LDV,K) if STOREV = 'C'
  //C                               (LDV,N) if STOREV = 'R'
  //C          The matrix V. See further details.
  //C
  //C  LDV     (input) INTEGER
  //C          The leading dimension of the array V.
  //C          If STOREV = 'C', LDV >= max(1,N); if STOREV = 'R', LDV >= K.
  //C
  //C  TAU     (input) DOUBLE PRECISION array, dimension (K)
  //C          TAU(i) must contain the scalar factor of the elementary
  //C          reflector H(i).
  //C
  //C  T       (output) DOUBLE PRECISION array, dimension (LDT,K)
  //C          The k by k triangular factor T of the block reflector.
  //C          If DIRECT = 'F', T is upper triangular; if DIRECT = 'B', T is
  //C          lower triangular. The rest of the array is not used.
  //C
  //C  LDT     (input) INTEGER
  //C          The leading dimension of the array T. LDT >= K.
  //C
  //C  Further Details
  //C  ===============
  //C
  //C  The shape of the matrix V and the storage of the vectors which define
  //C  the H(i) is best illustrated by the following example with n = 5 and
  //C  k = 3. The elements equal to 1 are not stored; the corresponding
  //C  array elements are modified but restored on exit. The rest of the
  //C  array is not used.
  //C
  //C  DIRECT = 'F' and STOREV = 'C':         DIRECT = 'F' and STOREV = 'R':
  //C
  //C               V = (  1       )                 V = (  1 v1 v1 v1 v1 )
  //C                   ( v1  1    )                     (     1 v2 v2 v2 )
  //C                   ( v1 v2  1 )                     (        1 v3 v3 )
  //C                   ( v1 v2 v3 )
  //C
  //C  DIRECT = 'B' and STOREV = 'C':         DIRECT = 'B' and STOREV = 'R':
  //C
  //C               V = ( v1 v2 v3 )                 V = ( v1 v1  1       )
  //C                   ( v1 v2 v3 )                     ( v2 v2 v2  1    )
  //C                   (  1 v2 v3 )                     ( v3 v3 v3 v3  1 )
  //C                   (     1 v3 )
  //C                   (        1 )
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Quick return if possible
  //C
  if (n == 0) {
    return;
  }
  //C
  int prevlastv = fem::int0;
  int i = fem::int0;
  const double zero = 0.0e+0;
  int j = fem::int0;
  double vii = fem::double0;
  const double one = 1.0e+0;
  int lastv = fem::int0;
  if (lsame(direct, "F")) {
    prevlastv = n;
    FEM_DO(i, 1, k) {
      prevlastv = fem::max(i, prevlastv);
      if (tau(i) == zero) {
        //C
        //C              H(i)  =  I
        //C
        FEM_DO(j, 1, i) {
          t(j, i) = zero;
        }
      }
      else {
        //C
        //C              general case
        //C
        vii = v(i, i);
        v(i, i) = one;
        if (lsame(storev, "C")) {
          //C                 Skip any trailing zeros.
          FEM_DOSTEP(lastv, n, i + 1, -1) {
            if (v(lastv, i) != zero) {
              break;
            }
          }
          j = fem::min(lastv, prevlastv);
          //C
          //C                 T(1:i-1,i) := - tau(i) * V(i:j,1:i-1)' * V(i:j,i)
          //C
          dgemv("Transpose", j - i + 1, i - 1, -tau(i), v(i, 1), ldv,
            v(i, i), 1, zero, t(1, i), 1);
        }
        else {
          //C                 Skip any trailing zeros.
          FEM_DOSTEP(lastv, n, i + 1, -1) {
            if (v(i, lastv) != zero) {
              break;
            }
          }
          j = fem::min(lastv, prevlastv);
          //C
          //C                 T(1:i-1,i) := - tau(i) * V(1:i-1,i:j) * V(i,i:j)'
          //C
          dgemv("No transpose", i - 1, j - i + 1, -tau(i), v(1, i),
            ldv, v(i, i), ldv, zero, t(1, i), 1);
        }
        v(i, i) = vii;
        //C
        //C              T(1:i-1,i) := T(1:i-1,1:i-1) * T(1:i-1,i)
        //C
        dtrmv("Upper", "No transpose", "Non-unit", i - 1, t, ldt, t(1, i), 1);
        t(i, i) = tau(i);
        if (i > 1) {
          prevlastv = fem::max(prevlastv, lastv);
        }
        else {
          prevlastv = lastv;
        }
      }
    }
  }
  else {
    prevlastv = 1;
    FEM_DOSTEP(i, k, 1, -1) {
      if (tau(i) == zero) {
        //C
        //C              H(i)  =  I
        //C
        FEM_DO(j, i, k) {
          t(j, i) = zero;
        }
      }
      else {
        //C
        //C              general case
        //C
        if (i < k) {
          if (lsame(storev, "C")) {
            vii = v(n - k + i, i);
            v(n - k + i, i) = one;
            //C                    Skip any leading zeros.
            {
              int fem_do_last = i - 1;
              FEM_DO(lastv, 1, fem_do_last) {
                if (v(lastv, i) != zero) {
                  break;
                }
              }
            }
            j = fem::max(lastv, prevlastv);
            //C
            //C                    T(i+1:k,i) :=
            //C                            - tau(i) * V(j:n-k+i,i+1:k)' * V(j:n-k+i,i)
            //C
            dgemv("Transpose", n - k + i - j + 1, k - i, -tau(i), v(j,
              i + 1), ldv, v(j, i), 1, zero, t(i + 1, i), 1);
            v(n - k + i, i) = vii;
          }
          else {
            vii = v(i, n - k + i);
            v(i, n - k + i) = one;
            //C                    Skip any leading zeros.
            {
              int fem_do_last = i - 1;
              FEM_DO(lastv, 1, fem_do_last) {
                if (v(i, lastv) != zero) {
                  break;
                }
              }
            }
            j = fem::max(lastv, prevlastv);
            //C
            //C                    T(i+1:k,i) :=
            //C                            - tau(i) * V(i+1:k,j:n-k+i) * V(i,j:n-k+i)'
            //C
            dgemv("No transpose", k - i, n - k + i - j + 1, -tau(i),
              v(i + 1, j), ldv, v(i, j), ldv, zero, t(i + 1, i), 1);
            v(i, n - k + i) = vii;
          }
          //C
          //C                 T(i+1:k,i) := T(i+1:k,i+1:k) * T(i+1:k,i)
          //C
          dtrmv("Lower", "No transpose", "Non-unit", k - i, t(i + 1,
            i + 1), ldt, t(i + 1, i), 1);
          if (i > 1) {
            prevlastv = fem::min(prevlastv, lastv);
          }
          else {
            prevlastv = lastv;
          }
        }
        t(i, i) = tau(i);
      }
    }
  }
  //C
  //C     End of DLARFT
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/BLAS/SRC/dger.f
void
dger(
  int const& m,
  int const& n,
  double const& alpha,
  arr_cref<double> x,
  int const& incx,
  arr_cref<double> y,
  int const& incy,
  arr_ref<double, 2> a,
  int const& lda)
{
  x(dimension(star));
  y(dimension(star));
  a(dimension(lda, star));
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DGER   performs the rank 1 operation
  //C
  //C     A := alpha*x*y' + A,
  //C
  //C  where alpha is a scalar, x is an m element vector, y is an n element
  //C  vector and A is an m by n matrix.
  //C
  //C  Arguments
  //C  ==========
  //C
  //C  M      - INTEGER.
  //C           On entry, M specifies the number of rows of the matrix A.
  //C           M must be at least zero.
  //C           Unchanged on exit.
  //C
  //C  N      - INTEGER.
  //C           On entry, N specifies the number of columns of the matrix A.
  //C           N must be at least zero.
  //C           Unchanged on exit.
  //C
  //C  ALPHA  - DOUBLE PRECISION.
  //C           On entry, ALPHA specifies the scalar alpha.
  //C           Unchanged on exit.
  //C
  //C  X      - DOUBLE PRECISION array of dimension at least
  //C           ( 1 + ( m - 1 )*abs( INCX ) ).
  //C           Before entry, the incremented array X must contain the m
  //C           element vector x.
  //C           Unchanged on exit.
  //C
  //C  INCX   - INTEGER.
  //C           On entry, INCX specifies the increment for the elements of
  //C           X. INCX must not be zero.
  //C           Unchanged on exit.
  //C
  //C  Y      - DOUBLE PRECISION array of dimension at least
  //C           ( 1 + ( n - 1 )*abs( INCY ) ).
  //C           Before entry, the incremented array Y must contain the n
  //C           element vector y.
  //C           Unchanged on exit.
  //C
  //C  INCY   - INTEGER.
  //C           On entry, INCY specifies the increment for the elements of
  //C           Y. INCY must not be zero.
  //C           Unchanged on exit.
  //C
  //C  A      - DOUBLE PRECISION array of DIMENSION ( LDA, n ).
  //C           Before entry, the leading m by n part of the array A must
  //C           contain the matrix of coefficients. On exit, A is
  //C           overwritten by the updated matrix.
  //C
  //C  LDA    - INTEGER.
  //C           On entry, LDA specifies the first dimension of A as declared
  //C           in the calling (sub) program. LDA must be at least
  //C           max( 1, m ).
  //C           Unchanged on exit.
  //C
  //C  Further Details
  //C  ===============
  //C
  //C  Level 2 Blas routine.
  //C
  //C  -- Written on 22-October-1986.
  //C     Jack Dongarra, Argonne National Lab.
  //C     Jeremy Du Croz, Nag Central Office.
  //C     Sven Hammarling, Nag Central Office.
  //C     Richard Hanson, Sandia National Labs.
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C
  //C     Test the input parameters.
  //C
  int info = 0;
  if (m < 0) {
    info = 1;
  }
  else if (n < 0) {
    info = 2;
  }
  else if (incx == 0) {
    info = 5;
  }
  else if (incy == 0) {
    info = 7;
  }
  else if (lda < fem::max(1, m)) {
    info = 9;
  }
  if (info != 0) {
    xerbla("DGER  ", info);
    return;
  }
  //C
  //C     Quick return if possible.
  //C
  const double zero = 0.0e+0;
  if ((m == 0) || (n == 0) || (alpha == zero)) {
    return;
  }
  //C
  //C     Start the operations. In this version the elements of A are
  //C     accessed sequentially with one pass through A.
  //C
  int jy = fem::int0;
  if (incy > 0) {
    jy = 1;
  }
  else {
    jy = 1 - (n - 1) * incy;
  }
  int j = fem::int0;
  double temp = fem::double0;
  int i = fem::int0;
  int kx = fem::int0;
  int ix = fem::int0;
  if (incx == 1) {
    FEM_DO(j, 1, n) {
      if (y(jy) != zero) {
        temp = alpha * y(jy);
        FEM_DO(i, 1, m) {
          a(i, j) += x(i) * temp;
        }
      }
      jy += incy;
    }
  }
  else {
    if (incx > 0) {
      kx = 1;
    }
    else {
      kx = 1 - (m - 1) * incx;
    }
    FEM_DO(j, 1, n) {
      if (y(jy) != zero) {
        temp = alpha * y(jy);
        ix = kx;
        FEM_DO(i, 1, m) {
          a(i, j) += x(ix) * temp;
          ix += incx;
        }
      }
      jy += incy;
    }
  }
  //C
  //C     End of DGER  .
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dlarf.f
void
dlarf(
  str_cref side,
  int const& m,
  int const& n,
  arr_cref<double> v,
  int const& incv,
  double const& tau,
  arr_ref<double, 2> c,
  int const& ldc,
  arr_ref<double> work)
{
  v(dimension(star));
  c(dimension(ldc, star));
  work(dimension(star));
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DLARF applies a real elementary reflector H to a real m by n matrix
  //C  C, from either the left or the right. H is represented in the form
  //C
  //C        H = I - tau * v * v'
  //C
  //C  where tau is a real scalar and v is a real vector.
  //C
  //C  If tau = 0, then H is taken to be the unit matrix.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  SIDE    (input) CHARACTER*1
  //C          = 'L': form  H * C
  //C          = 'R': form  C * H
  //C
  //C  M       (input) INTEGER
  //C          The number of rows of the matrix C.
  //C
  //C  N       (input) INTEGER
  //C          The number of columns of the matrix C.
  //C
  //C  V       (input) DOUBLE PRECISION array, dimension
  //C                     (1 + (M-1)*abs(INCV)) if SIDE = 'L'
  //C                  or (1 + (N-1)*abs(INCV)) if SIDE = 'R'
  //C          The vector v in the representation of H. V is not used if
  //C          TAU = 0.
  //C
  //C  INCV    (input) INTEGER
  //C          The increment between elements of v. INCV <> 0.
  //C
  //C  TAU     (input) DOUBLE PRECISION
  //C          The value tau in the representation of H.
  //C
  //C  C       (input/output) DOUBLE PRECISION array, dimension (LDC,N)
  //C          On entry, the m by n matrix C.
  //C          On exit, C is overwritten by the matrix H * C if SIDE = 'L',
  //C          or C * H if SIDE = 'R'.
  //C
  //C  LDC     (input) INTEGER
  //C          The leading dimension of the array C. LDC >= max(1,M).
  //C
  //C  WORK    (workspace) DOUBLE PRECISION array, dimension
  //C                         (N) if SIDE = 'L'
  //C                      or (M) if SIDE = 'R'
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  bool applyleft = lsame(side, "L");
  int lastv = 0;
  int lastc = 0;
  const double zero = 0.0e+0;
  int i = fem::int0;
  if (tau != zero) {
    //C     Set up variables for scanning V.  LASTV begins pointing to the end
    //C     of V.
    if (applyleft) {
      lastv = m;
    }
    else {
      lastv = n;
    }
    if (incv > 0) {
      i = 1 + (lastv - 1) * incv;
    }
    else {
      i = 1;
    }
    //C     Look for the last non-zero row in V.
    while (lastv > 0 && v(i) == zero) {
      lastv = lastv - 1;
      i = i - incv;
    }
    if (applyleft) {
      //C     Scan for the last non-zero column in C(1:lastv,:).
      lastc = iladlc(lastv, n, c, ldc);
    }
    else {
      //C     Scan for the last non-zero row in C(:,1:lastv).
      lastc = iladlr(m, lastv, c, ldc);
    }
  }
  //C     Note that lastc.eq.0 renders the BLAS operations null; no special
  //C     case is needed at this level.
  const double one = 1.0e+0;
  if (applyleft) {
    //C
    //C        Form  H * C
    //C
    if (lastv > 0) {
      //C
      //C           w(1:lastc,1) := C(1:lastv,1:lastc)' * v(1:lastv,1)
      //C
      dgemv("Transpose", lastv, lastc, one, c, ldc, v, incv, zero, work, 1);
      //C
      //C           C(1:lastv,1:lastc) := C(...) - v(1:lastv,1) * w(1:lastc,1)'
      //C
      dger(lastv, lastc, -tau, v, incv, work, 1, c, ldc);
    }
  }
  else {
    //C
    //C        Form  C * H
    //C
    if (lastv > 0) {
      //C
      //C           w(1:lastc,1) := C(1:lastc,1:lastv) * v(1:lastv,1)
      //C
      dgemv("No transpose", lastc, lastv, one, c, ldc, v, incv, zero, work, 1);
      //C
      //C           C(1:lastc,1:lastv) := C(...) - w(1:lastc,1) * v(1:lastv,1)'
      //C
      dger(lastc, lastv, -tau, work, 1, v, incv, c, ldc);
    }
  }
  //C
  //C     End of DLARF
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dorg2l.f
void
dorg2l(
  int const& m,
  int const& n,
  int const& k,
  arr_ref<double, 2> a,
  int const& lda,
  arr_cref<double> tau,
  arr_ref<double> work,
  int& info)
{
  a(dimension(lda, star));
  tau(dimension(star));
  work(dimension(star));
  //C
  //C  -- LAPACK routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DORG2L generates an m by n real matrix Q with orthonormal columns,
  //C  which is defined as the last n columns of a product of k elementary
  //C  reflectors of order m
  //C
  //C        Q  =  H(k) . . . H(2) H(1)
  //C
  //C  as returned by DGEQLF.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  M       (input) INTEGER
  //C          The number of rows of the matrix Q. M >= 0.
  //C
  //C  N       (input) INTEGER
  //C          The number of columns of the matrix Q. M >= N >= 0.
  //C
  //C  K       (input) INTEGER
  //C          The number of elementary reflectors whose product defines the
  //C          matrix Q. N >= K >= 0.
  //C
  //C  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //C          On entry, the (n-k+i)-th column must contain the vector which
  //C          defines the elementary reflector H(i), for i = 1,2,...,k, as
  //C          returned by DGEQLF in the last k columns of its array
  //C          argument A.
  //C          On exit, the m by n matrix Q.
  //C
  //C  LDA     (input) INTEGER
  //C          The first dimension of the array A. LDA >= max(1,M).
  //C
  //C  TAU     (input) DOUBLE PRECISION array, dimension (K)
  //C          TAU(i) must contain the scalar factor of the elementary
  //C          reflector H(i), as returned by DGEQLF.
  //C
  //C  WORK    (workspace) DOUBLE PRECISION array, dimension (N)
  //C
  //C  INFO    (output) INTEGER
  //C          = 0: successful exit
  //C          < 0: if INFO = -i, the i-th argument has an illegal value
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Test the input arguments
  //C
  info = 0;
  if (m < 0) {
    info = -1;
  }
  else if (n < 0 || n > m) {
    info = -2;
  }
  else if (k < 0 || k > n) {
    info = -3;
  }
  else if (lda < fem::max(1, m)) {
    info = -5;
  }
  if (info != 0) {
    xerbla("DORG2L", -info);
    return;
  }
  //C
  //C     Quick return if possible
  //C
  if (n <= 0) {
    return;
  }
  //C
  //C     Initialise columns 1:n-k to columns of the unit matrix
  //C
  int j = fem::int0;
  int l = fem::int0;
  const double zero = 0.0e+0;
  const double one = 1.0e+0;
  {
    int fem_do_last = n - k;
    FEM_DO(j, 1, fem_do_last) {
      FEM_DO(l, 1, m) {
        a(l, j) = zero;
      }
      a(m - n + j, j) = one;
    }
  }
  //C
  int i = fem::int0;
  int ii = fem::int0;
  FEM_DO(i, 1, k) {
    ii = n - k + i;
    //C
    //C        Apply H(i) to A(1:m-k+i,1:n-k+i) from the left
    //C
    a(m - n + ii, ii) = one;
    dlarf("Left", m - n + ii, ii - 1, a(1, ii), 1, tau(i), a, lda, work);
    dscal(m - n + ii - 1, -tau(i), a(1, ii), 1);
    a(m - n + ii, ii) = one - tau(i);
    //C
    //C        Set A(m-k+i+1:m,n-k+i) to zero
    //C
    FEM_DO(l, m - n + ii + 1, m) {
      a(l, ii) = zero;
    }
  }
  //C
  //C     End of DORG2L
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dorgql.f
void
dorgql(
  int const& m,
  int const& n,
  int const& k,
  arr_ref<double, 2> a,
  int const& lda,
  arr_cref<double> tau,
  arr_ref<double> work,
  int const& lwork,
  int& info)
{
  a(dimension(lda, star));
  tau(dimension(star));
  work(dimension(star));
  //C
  //C  -- LAPACK routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DORGQL generates an M-by-N real matrix Q with orthonormal columns,
  //C  which is defined as the last N columns of a product of K elementary
  //C  reflectors of order M
  //C
  //C        Q  =  H(k) . . . H(2) H(1)
  //C
  //C  as returned by DGEQLF.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  M       (input) INTEGER
  //C          The number of rows of the matrix Q. M >= 0.
  //C
  //C  N       (input) INTEGER
  //C          The number of columns of the matrix Q. M >= N >= 0.
  //C
  //C  K       (input) INTEGER
  //C          The number of elementary reflectors whose product defines the
  //C          matrix Q. N >= K >= 0.
  //C
  //C  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //C          On entry, the (n-k+i)-th column must contain the vector which
  //C          defines the elementary reflector H(i), for i = 1,2,...,k, as
  //C          returned by DGEQLF in the last k columns of its array
  //C          argument A.
  //C          On exit, the M-by-N matrix Q.
  //C
  //C  LDA     (input) INTEGER
  //C          The first dimension of the array A. LDA >= max(1,M).
  //C
  //C  TAU     (input) DOUBLE PRECISION array, dimension (K)
  //C          TAU(i) must contain the scalar factor of the elementary
  //C          reflector H(i), as returned by DGEQLF.
  //C
  //C  WORK    (workspace/output) DOUBLE PRECISION array, dimension (MAX(1,LWORK))
  //C          On exit, if INFO = 0, WORK(1) returns the optimal LWORK.
  //C
  //C  LWORK   (input) INTEGER
  //C          The dimension of the array WORK. LWORK >= max(1,N).
  //C          For optimum performance LWORK >= N*NB, where NB is the
  //C          optimal blocksize.
  //C
  //C          If LWORK = -1, then a workspace query is assumed; the routine
  //C          only calculates the optimal size of the WORK array, returns
  //C          this value as the first entry of the WORK array, and no error
  //C          message related to LWORK is issued by XERBLA.
  //C
  //C  INFO    (output) INTEGER
  //C          = 0:  successful exit
  //C          < 0:  if INFO = -i, the i-th argument has an illegal value
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Test the input arguments
  //C
  info = 0;
  bool lquery = (lwork ==  - 1);
  if (m < 0) {
    info = -1;
  }
  else if (n < 0 || n > m) {
    info = -2;
  }
  else if (k < 0 || k > n) {
    info = -3;
  }
  else if (lda < fem::max(1, m)) {
    info = -5;
  }
  //C
  int lwkopt = fem::int0;
  int nb = fem::int0;
  if (info == 0) {
    if (n == 0) {
      lwkopt = 1;
    }
    else {
      nb = ilaenv(1, "DORGQL", " ", m, n, k, -1);
      lwkopt = n * nb;
    }
    work(1) = lwkopt;
    //C
    if (lwork < fem::max(1, n) && !lquery) {
      info = -8;
    }
  }
  //C
  if (info != 0) {
    xerbla("DORGQL", -info);
    return;
  }
  else if (lquery) {
    return;
  }
  //C
  //C     Quick return if possible
  //C
  if (n <= 0) {
    return;
  }
  //C
  int nbmin = 2;
  int nx = 0;
  int iws = n;
  int ldwork = fem::int0;
  if (nb > 1 && nb < k) {
    //C
    //C        Determine when to cross over from blocked to unblocked code.
    //C
    nx = fem::max(0, ilaenv(3, "DORGQL", " ", m, n, k, -1));
    if (nx < k) {
      //C
      //C           Determine if workspace is large enough for blocked code.
      //C
      ldwork = n;
      iws = ldwork * nb;
      if (lwork < iws) {
        //C
        //C              Not enough workspace to use optimal NB:  reduce NB and
        //C              determine the minimum value of NB.
        //C
        nb = lwork / ldwork;
        nbmin = fem::max(2, ilaenv(2, "DORGQL", " ", m, n, k, -1));
      }
    }
  }
  //C
  int kk = fem::int0;
  int j = fem::int0;
  int i = fem::int0;
  const double zero = 0.0e+0;
  if (nb >= nbmin && nb < k && nx < k) {
    //C
    //C        Use blocked code after the first block.
    //C        The last kk columns are handled by the block method.
    //C
    kk = fem::min(k, ((k - nx + nb - 1) / nb) * nb);
    //C
    //C        Set A(m-kk+1:m,1:n-kk) to zero.
    //C
    {
      int fem_do_last = n - kk;
      FEM_DO(j, 1, fem_do_last) {
        FEM_DO(i, m - kk + 1, m) {
          a(i, j) = zero;
        }
      }
    }
  }
  else {
    kk = 0;
  }
  //C
  //C     Use unblocked code for the first or only block.
  //C
  int iinfo = fem::int0;
  dorg2l(m - kk, n - kk, k - kk, a, lda, tau, work, iinfo);
  //C
  int ib = fem::int0;
  int l = fem::int0;
  if (kk > 0) {
    //C
    //C        Use blocked code
    //C
    FEM_DOSTEP(i, k - kk + 1, k, nb) {
      ib = fem::min(nb, k - i + 1);
      if (n - k + i > 1) {
        //C
        //C              Form the triangular factor of the block reflector
        //C              H = H(i+ib-1) . . . H(i+1) H(i)
        //C
        dlarft("Backward", "Columnwise", m - k + i + ib - 1, ib, a(1,
          n - k + i), lda, tau(i), work, ldwork);
        //C
        //C              Apply H to A(1:m-k+i+ib-1,1:n-k+i-1) from the left
        //C
        dlarfb("Left", "No transpose", "Backward", "Columnwise", m -
          k + i + ib - 1, n - k + i - 1, ib, a(1, n - k + i), lda,
          work, ldwork, a, lda, work(ib + 1), ldwork);
      }
      //C
      //C           Apply H to rows 1:m-k+i+ib-1 of current block
      //C
      dorg2l(m - k + i + ib - 1, ib, ib, a(1, n - k + i), lda, tau(i),
        work, iinfo);
      //C
      //C           Set rows m-k+i+ib:m of current block to zero
      //C
      {
        int fem_do_last = n - k + i + ib - 1;
        FEM_DO(j, n - k + i, fem_do_last) {
          FEM_DO(l, m - k + i + ib, m) {
            a(l, j) = zero;
          }
        }
      }
    }
  }
  //C
  work(1) = iws;
  //C
  //C     End of DORGQL
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dorg2r.f
void
dorg2r(
  int const& m,
  int const& n,
  int const& k,
  arr_ref<double, 2> a,
  int const& lda,
  arr_cref<double> tau,
  arr_ref<double> work,
  int& info)
{
  a(dimension(lda, star));
  tau(dimension(star));
  work(dimension(star));
  //C
  //C  -- LAPACK routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DORG2R generates an m by n real matrix Q with orthonormal columns,
  //C  which is defined as the first n columns of a product of k elementary
  //C  reflectors of order m
  //C
  //C        Q  =  H(1) H(2) . . . H(k)
  //C
  //C  as returned by DGEQRF.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  M       (input) INTEGER
  //C          The number of rows of the matrix Q. M >= 0.
  //C
  //C  N       (input) INTEGER
  //C          The number of columns of the matrix Q. M >= N >= 0.
  //C
  //C  K       (input) INTEGER
  //C          The number of elementary reflectors whose product defines the
  //C          matrix Q. N >= K >= 0.
  //C
  //C  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //C          On entry, the i-th column must contain the vector which
  //C          defines the elementary reflector H(i), for i = 1,2,...,k, as
  //C          returned by DGEQRF in the first k columns of its array
  //C          argument A.
  //C          On exit, the m-by-n matrix Q.
  //C
  //C  LDA     (input) INTEGER
  //C          The first dimension of the array A. LDA >= max(1,M).
  //C
  //C  TAU     (input) DOUBLE PRECISION array, dimension (K)
  //C          TAU(i) must contain the scalar factor of the elementary
  //C          reflector H(i), as returned by DGEQRF.
  //C
  //C  WORK    (workspace) DOUBLE PRECISION array, dimension (N)
  //C
  //C  INFO    (output) INTEGER
  //C          = 0: successful exit
  //C          < 0: if INFO = -i, the i-th argument has an illegal value
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Test the input arguments
  //C
  info = 0;
  if (m < 0) {
    info = -1;
  }
  else if (n < 0 || n > m) {
    info = -2;
  }
  else if (k < 0 || k > n) {
    info = -3;
  }
  else if (lda < fem::max(1, m)) {
    info = -5;
  }
  if (info != 0) {
    xerbla("DORG2R", -info);
    return;
  }
  //C
  //C     Quick return if possible
  //C
  if (n <= 0) {
    return;
  }
  //C
  //C     Initialise columns k+1:n to columns of the unit matrix
  //C
  int j = fem::int0;
  int l = fem::int0;
  const double zero = 0.0e+0;
  const double one = 1.0e+0;
  FEM_DO(j, k + 1, n) {
    FEM_DO(l, 1, m) {
      a(l, j) = zero;
    }
    a(j, j) = one;
  }
  //C
  int i = fem::int0;
  FEM_DOSTEP(i, k, 1, -1) {
    //C
    //C        Apply H(i) to A(i:m,i:n) from the left
    //C
    if (i < n) {
      a(i, i) = one;
      dlarf("Left", m - i + 1, n - i, a(i, i), 1, tau(i), a(i, i + 1),
        lda, work);
    }
    if (i < m) {
      dscal(m - i, -tau(i), a(i + 1, i), 1);
    }
    a(i, i) = one - tau(i);
    //C
    //C        Set A(1:i-1,i) to zero
    //C
    {
      int fem_do_last = i - 1;
      FEM_DO(l, 1, fem_do_last) {
        a(l, i) = zero;
      }
    }
  }
  //C
  //C     End of DORG2R
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dorgqr.f
void
dorgqr(
  int const& m,
  int const& n,
  int const& k,
  arr_ref<double, 2> a,
  int const& lda,
  arr_cref<double> tau,
  arr_ref<double> work,
  int const& lwork,
  int& info)
{
  a(dimension(lda, star));
  tau(dimension(star));
  work(dimension(star));
  //C
  //C  -- LAPACK routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DORGQR generates an M-by-N real matrix Q with orthonormal columns,
  //C  which is defined as the first N columns of a product of K elementary
  //C  reflectors of order M
  //C
  //C        Q  =  H(1) H(2) . . . H(k)
  //C
  //C  as returned by DGEQRF.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  M       (input) INTEGER
  //C          The number of rows of the matrix Q. M >= 0.
  //C
  //C  N       (input) INTEGER
  //C          The number of columns of the matrix Q. M >= N >= 0.
  //C
  //C  K       (input) INTEGER
  //C          The number of elementary reflectors whose product defines the
  //C          matrix Q. N >= K >= 0.
  //C
  //C  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //C          On entry, the i-th column must contain the vector which
  //C          defines the elementary reflector H(i), for i = 1,2,...,k, as
  //C          returned by DGEQRF in the first k columns of its array
  //C          argument A.
  //C          On exit, the M-by-N matrix Q.
  //C
  //C  LDA     (input) INTEGER
  //C          The first dimension of the array A. LDA >= max(1,M).
  //C
  //C  TAU     (input) DOUBLE PRECISION array, dimension (K)
  //C          TAU(i) must contain the scalar factor of the elementary
  //C          reflector H(i), as returned by DGEQRF.
  //C
  //C  WORK    (workspace/output) DOUBLE PRECISION array, dimension (MAX(1,LWORK))
  //C          On exit, if INFO = 0, WORK(1) returns the optimal LWORK.
  //C
  //C  LWORK   (input) INTEGER
  //C          The dimension of the array WORK. LWORK >= max(1,N).
  //C          For optimum performance LWORK >= N*NB, where NB is the
  //C          optimal blocksize.
  //C
  //C          If LWORK = -1, then a workspace query is assumed; the routine
  //C          only calculates the optimal size of the WORK array, returns
  //C          this value as the first entry of the WORK array, and no error
  //C          message related to LWORK is issued by XERBLA.
  //C
  //C  INFO    (output) INTEGER
  //C          = 0:  successful exit
  //C          < 0:  if INFO = -i, the i-th argument has an illegal value
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Test the input arguments
  //C
  info = 0;
  int nb = ilaenv(1, "DORGQR", " ", m, n, k, -1);
  int lwkopt = fem::max(1, n) * nb;
  work(1) = lwkopt;
  bool lquery = (lwork ==  - 1);
  if (m < 0) {
    info = -1;
  }
  else if (n < 0 || n > m) {
    info = -2;
  }
  else if (k < 0 || k > n) {
    info = -3;
  }
  else if (lda < fem::max(1, m)) {
    info = -5;
  }
  else if (lwork < fem::max(1, n) && !lquery) {
    info = -8;
  }
  if (info != 0) {
    xerbla("DORGQR", -info);
    return;
  }
  else if (lquery) {
    return;
  }
  //C
  //C     Quick return if possible
  //C
  if (n <= 0) {
    work(1) = 1;
    return;
  }
  //C
  int nbmin = 2;
  int nx = 0;
  int iws = n;
  int ldwork = fem::int0;
  if (nb > 1 && nb < k) {
    //C
    //C        Determine when to cross over from blocked to unblocked code.
    //C
    nx = fem::max(0, ilaenv(3, "DORGQR", " ", m, n, k, -1));
    if (nx < k) {
      //C
      //C           Determine if workspace is large enough for blocked code.
      //C
      ldwork = n;
      iws = ldwork * nb;
      if (lwork < iws) {
        //C
        //C              Not enough workspace to use optimal NB:  reduce NB and
        //C              determine the minimum value of NB.
        //C
        nb = lwork / ldwork;
        nbmin = fem::max(2, ilaenv(2, "DORGQR", " ", m, n, k, -1));
      }
    }
  }
  //C
  int ki = fem::int0;
  int kk = fem::int0;
  int j = fem::int0;
  int i = fem::int0;
  const double zero = 0.0e+0;
  if (nb >= nbmin && nb < k && nx < k) {
    //C
    //C        Use blocked code after the last block.
    //C        The first kk columns are handled by the block method.
    //C
    ki = ((k - nx - 1) / nb) * nb;
    kk = fem::min(k, ki + nb);
    //C
    //C        Set A(1:kk,kk+1:n) to zero.
    //C
    FEM_DO(j, kk + 1, n) {
      FEM_DO(i, 1, kk) {
        a(i, j) = zero;
      }
    }
  }
  else {
    kk = 0;
  }
  //C
  //C     Use unblocked code for the last or only block.
  //C
  int iinfo = fem::int0;
  if (kk < n) {
    dorg2r(m - kk, n - kk, k - kk, a(kk + 1, kk + 1), lda, tau(kk + 1),
      work, iinfo);
  }
  //C
  int ib = fem::int0;
  int l = fem::int0;
  if (kk > 0) {
    //C
    //C        Use blocked code
    //C
    FEM_DOSTEP(i, ki + 1, 1, -nb) {
      ib = fem::min(nb, k - i + 1);
      if (i + ib <= n) {
        //C
        //C              Form the triangular factor of the block reflector
        //C              H = H(i) H(i+1) . . . H(i+ib-1)
        //C
        dlarft("Forward", "Columnwise", m - i + 1, ib, a(i, i), lda,
          tau(i), work, ldwork);
        //C
        //C              Apply H to A(i:m,i+ib:n) from the left
        //C
        dlarfb("Left", "No transpose", "Forward", "Columnwise", m - i + 1,
          n - i - ib + 1, ib, a(i, i), lda, work, ldwork, a(i, i + ib),
          lda, work(ib + 1), ldwork);
      }
      //C
      //C           Apply H to rows i:m of current block
      //C
      dorg2r(m - i + 1, ib, ib, a(i, i), lda, tau(i), work, iinfo);
      //C
      //C           Set rows 1:i-1 of current block to zero
      //C
      {
        int fem_do_last = i + ib - 1;
        FEM_DO(j, i, fem_do_last) {
          {
            int fem_do_last = i - 1;
            FEM_DO(l, 1, fem_do_last) {
              a(l, j) = zero;
            }
          }
        }
      }
    }
  }
  //C
  work(1) = iws;
  //C
  //C     End of DORGQR
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dorgtr.f
void
dorgtr(
  str_cref uplo,
  int const& n,
  arr_ref<double, 2> a,
  int const& lda,
  arr_cref<double> tau,
  arr_ref<double> work,
  int const& lwork,
  int& info)
{
  a(dimension(lda, star));
  tau(dimension(star));
  work(dimension(star));
  //C
  //C  -- LAPACK routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DORGTR generates a real orthogonal matrix Q which is defined as the
  //C  product of n-1 elementary reflectors of order N, as returned by
  //C  DSYTRD:
  //C
  //C  if UPLO = 'U', Q = H(n-1) . . . H(2) H(1),
  //C
  //C  if UPLO = 'L', Q = H(1) H(2) . . . H(n-1).
  //C
  //C  Arguments
  //C  =========
  //C
  //C  UPLO    (input) CHARACTER*1
  //C          = 'U': Upper triangle of A contains elementary reflectors
  //C                 from DSYTRD;
  //C          = 'L': Lower triangle of A contains elementary reflectors
  //C                 from DSYTRD.
  //C
  //C  N       (input) INTEGER
  //C          The order of the matrix Q. N >= 0.
  //C
  //C  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //C          On entry, the vectors which define the elementary reflectors,
  //C          as returned by DSYTRD.
  //C          On exit, the N-by-N orthogonal matrix Q.
  //C
  //C  LDA     (input) INTEGER
  //C          The leading dimension of the array A. LDA >= max(1,N).
  //C
  //C  TAU     (input) DOUBLE PRECISION array, dimension (N-1)
  //C          TAU(i) must contain the scalar factor of the elementary
  //C          reflector H(i), as returned by DSYTRD.
  //C
  //C  WORK    (workspace/output) DOUBLE PRECISION array, dimension (MAX(1,LWORK))
  //C          On exit, if INFO = 0, WORK(1) returns the optimal LWORK.
  //C
  //C  LWORK   (input) INTEGER
  //C          The dimension of the array WORK. LWORK >= max(1,N-1).
  //C          For optimum performance LWORK >= (N-1)*NB, where NB is
  //C          the optimal blocksize.
  //C
  //C          If LWORK = -1, then a workspace query is assumed; the routine
  //C          only calculates the optimal size of the WORK array, returns
  //C          this value as the first entry of the WORK array, and no error
  //C          message related to LWORK is issued by XERBLA.
  //C
  //C  INFO    (output) INTEGER
  //C          = 0:  successful exit
  //C          < 0:  if INFO = -i, the i-th argument had an illegal value
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Test the input arguments
  //C
  info = 0;
  bool lquery = (lwork ==  - 1);
  bool upper = lsame(uplo, "U");
  if (!upper && !lsame(uplo, "L")) {
    info = -1;
  }
  else if (n < 0) {
    info = -2;
  }
  else if (lda < fem::max(1, n)) {
    info = -4;
  }
  else if (lwork < fem::max(1, n - 1) && !lquery) {
    info = -7;
  }
  //C
  int nb = fem::int0;
  int lwkopt = fem::int0;
  if (info == 0) {
    if (upper) {
      nb = ilaenv(1, "DORGQL", " ", n - 1, n - 1, n - 1, -1);
    }
    else {
      nb = ilaenv(1, "DORGQR", " ", n - 1, n - 1, n - 1, -1);
    }
    lwkopt = fem::max(1, n - 1) * nb;
    work(1) = lwkopt;
  }
  //C
  if (info != 0) {
    xerbla("DORGTR", -info);
    return;
  }
  else if (lquery) {
    return;
  }
  //C
  //C     Quick return if possible
  //C
  if (n == 0) {
    work(1) = 1;
    return;
  }
  //C
  int j = fem::int0;
  int i = fem::int0;
  const double zero = 0.0e+0;
  const double one = 1.0e+0;
  int iinfo = fem::int0;
  if (upper) {
    //C
    //C        Q was determined by a call to DSYTRD with UPLO = 'U'
    //C
    //C        Shift the vectors which define the elementary reflectors one
    //C        column to the left, and set the last row and column of Q to
    //C        those of the unit matrix
    //C
    {
      int fem_do_last = n - 1;
      FEM_DO(j, 1, fem_do_last) {
        {
          int fem_do_last = j - 1;
          FEM_DO(i, 1, fem_do_last) {
            a(i, j) = a(i, j + 1);
          }
        }
        a(n, j) = zero;
      }
    }
    {
      int fem_do_last = n - 1;
      FEM_DO(i, 1, fem_do_last) {
        a(i, n) = zero;
      }
    }
    a(n, n) = one;
    //C
    //C        Generate Q(1:n-1,1:n-1)
    //C
    dorgql(n - 1, n - 1, n - 1, a, lda, tau, work, lwork, iinfo);
    //C
  }
  else {
    //C
    //C        Q was determined by a call to DSYTRD with UPLO = 'L'.
    //C
    //C        Shift the vectors which define the elementary reflectors one
    //C        column to the right, and set the first row and column of Q to
    //C        those of the unit matrix
    //C
    FEM_DOSTEP(j, n, 2, -1) {
      a(1, j) = zero;
      FEM_DO(i, j + 1, n) {
        a(i, j) = a(i, j - 1);
      }
    }
    a(1, 1) = one;
    FEM_DO(i, 2, n) {
      a(i, 1) = zero;
    }
    if (n > 1) {
      //C
      //C           Generate Q(2:n,2:n)
      //C
      dorgqr(n - 1, n - 1, n - 1, a(2, 2), lda, tau, work, lwork, iinfo);
    }
  }
  work(1) = lwkopt;
  //C
  //C     End of DORGTR
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/BLAS/SRC/dswap.f
void
dswap(
  int const& n,
  arr_ref<double> dx,
  int const& incx,
  arr_ref<double> dy,
  int const& incy)
{
  dx(dimension(star));
  dy(dimension(star));
  int ix = fem::int0;
  int iy = fem::int0;
  int i = fem::int0;
  double dtemp = fem::double0;
  int m = fem::int0;
  int mp1 = fem::int0;
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C     interchanges two vectors.
  //C     uses unrolled loops for increments equal one.
  //C
  //C  Further Details
  //C  ===============
  //C
  //C     jack dongarra, linpack, 3/11/78.
  //C     modified 12/3/93, array(1) declarations changed to array(*)
  //C
  //C  =====================================================================
  //C
  //C     .. Local Scalars ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  if (n <= 0) {
    return;
  }
  if (incx == 1 && incy == 1) {
    goto statement_20;
  }
  //C
  //C       code for unequal increments or equal increments not equal
  //C         to 1
  //C
  ix = 1;
  iy = 1;
  if (incx < 0) {
    ix = (-n + 1) * incx + 1;
  }
  if (incy < 0) {
    iy = (-n + 1) * incy + 1;
  }
  FEM_DO(i, 1, n) {
    dtemp = dx(ix);
    dx(ix) = dy(iy);
    dy(iy) = dtemp;
    ix += incx;
    iy += incy;
  }
  return;
  //C
  //C       code for both increments equal to 1
  //C
  //C       clean-up loop
  //C
  statement_20:
  m = fem::mod(n, 3);
  if (m == 0) {
    goto statement_40;
  }
  FEM_DO(i, 1, m) {
    dtemp = dx(i);
    dx(i) = dy(i);
    dy(i) = dtemp;
  }
  if (n < 3) {
    return;
  }
  statement_40:
  mp1 = m + 1;
  FEM_DOSTEP(i, mp1, n, 3) {
    dtemp = dx(i);
    dx(i) = dy(i);
    dy(i) = dtemp;
    dtemp = dx(i + 1);
    dx(i + 1) = dy(i + 1);
    dy(i + 1) = dtemp;
    dtemp = dx(i + 2);
    dx(i + 2) = dy(i + 2);
    dy(i + 2) = dtemp;
  }
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dlae2.f
void
dlae2(
  double const& a,
  double const& b,
  double const& c,
  double& rt1,
  double& rt2)
{
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DLAE2  computes the eigenvalues of a 2-by-2 symmetric matrix
  //C     [  A   B  ]
  //C     [  B   C  ].
  //C  On return, RT1 is the eigenvalue of larger absolute value, and RT2
  //C  is the eigenvalue of smaller absolute value.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  A       (input) DOUBLE PRECISION
  //C          The (1,1) element of the 2-by-2 matrix.
  //C
  //C  B       (input) DOUBLE PRECISION
  //C          The (1,2) and (2,1) elements of the 2-by-2 matrix.
  //C
  //C  C       (input) DOUBLE PRECISION
  //C          The (2,2) element of the 2-by-2 matrix.
  //C
  //C  RT1     (output) DOUBLE PRECISION
  //C          The eigenvalue of larger absolute value.
  //C
  //C  RT2     (output) DOUBLE PRECISION
  //C          The eigenvalue of smaller absolute value.
  //C
  //C  Further Details
  //C  ===============
  //C
  //C  RT1 is accurate to a few ulps barring over/underflow.
  //C
  //C  RT2 may be inaccurate if there is massive cancellation in the
  //C  determinant A*C-B*B; higher precision or correctly rounded or
  //C  correctly truncated arithmetic would be needed to compute RT2
  //C  accurately in all cases.
  //C
  //C  Overflow is possible only if RT1 is within a factor of 5 of overflow.
  //C  Underflow is harmless if the input data is 0 or exceeds
  //C     underflow_threshold / macheps.
  //C
  //C =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Compute the eigenvalues
  //C
  double sm = a + c;
  double df = a - c;
  double adf = fem::abs(df);
  double tb = b + b;
  double ab = fem::abs(tb);
  double acmx = fem::double0;
  double acmn = fem::double0;
  if (fem::abs(a) > fem::abs(c)) {
    acmx = a;
    acmn = c;
  }
  else {
    acmx = c;
    acmn = a;
  }
  const double one = 1.0e0;
  double rt = fem::double0;
  const double two = 2.0e0;
  if (adf > ab) {
    rt = adf * fem::sqrt(one + fem::pow2((ab / adf)));
  }
  else if (adf < ab) {
    rt = ab * fem::sqrt(one + fem::pow2((adf / ab)));
  }
  else {
    //C
    //C        Includes case AB=ADF=0
    //C
    rt = ab * fem::sqrt(two);
  }
  const double zero = 0.0e0;
  const double half = 0.5e0;
  if (sm < zero) {
    rt1 = half * (sm - rt);
    //C
    //C        Order of execution important.
    //C        To get fully accurate smaller eigenvalue,
    //C        next line needs to be executed in higher precision.
    //C
    rt2 = (acmx / rt1) * acmn - (b / rt1) * b;
  }
  else if (sm > zero) {
    rt1 = half * (sm + rt);
    //C
    //C        Order of execution important.
    //C        To get fully accurate smaller eigenvalue,
    //C        next line needs to be executed in higher precision.
    //C
    rt2 = (acmx / rt1) * acmn - (b / rt1) * b;
  }
  else {
    //C
    //C        Includes case RT1 = RT2 = 0
    //C
    rt1 = half * rt;
    rt2 = -half * rt;
  }
  //C
  //C     End of DLAE2
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dlaev2.f
void
dlaev2(
  double const& a,
  double const& b,
  double const& c,
  double& rt1,
  double& rt2,
  double& cs1,
  double& sn1)
{
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DLAEV2 computes the eigendecomposition of a 2-by-2 symmetric matrix
  //C     [  A   B  ]
  //C     [  B   C  ].
  //C  On return, RT1 is the eigenvalue of larger absolute value, RT2 is the
  //C  eigenvalue of smaller absolute value, and (CS1,SN1) is the unit right
  //C  eigenvector for RT1, giving the decomposition
  //C
  //C     [ CS1  SN1 ] [  A   B  ] [ CS1 -SN1 ]  =  [ RT1  0  ]
  //C     [-SN1  CS1 ] [  B   C  ] [ SN1  CS1 ]     [  0  RT2 ].
  //C
  //C  Arguments
  //C  =========
  //C
  //C  A       (input) DOUBLE PRECISION
  //C          The (1,1) element of the 2-by-2 matrix.
  //C
  //C  B       (input) DOUBLE PRECISION
  //C          The (1,2) element and the conjugate of the (2,1) element of
  //C          the 2-by-2 matrix.
  //C
  //C  C       (input) DOUBLE PRECISION
  //C          The (2,2) element of the 2-by-2 matrix.
  //C
  //C  RT1     (output) DOUBLE PRECISION
  //C          The eigenvalue of larger absolute value.
  //C
  //C  RT2     (output) DOUBLE PRECISION
  //C          The eigenvalue of smaller absolute value.
  //C
  //C  CS1     (output) DOUBLE PRECISION
  //C  SN1     (output) DOUBLE PRECISION
  //C          The vector (CS1, SN1) is a unit right eigenvector for RT1.
  //C
  //C  Further Details
  //C  ===============
  //C
  //C  RT1 is accurate to a few ulps barring over/underflow.
  //C
  //C  RT2 may be inaccurate if there is massive cancellation in the
  //C  determinant A*C-B*B; higher precision or correctly rounded or
  //C  correctly truncated arithmetic would be needed to compute RT2
  //C  accurately in all cases.
  //C
  //C  CS1 and SN1 are accurate to a few ulps barring over/underflow.
  //C
  //C  Overflow is possible only if RT1 is within a factor of 5 of overflow.
  //C  Underflow is harmless if the input data is 0 or exceeds
  //C     underflow_threshold / macheps.
  //C
  //C =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Compute the eigenvalues
  //C
  double sm = a + c;
  double df = a - c;
  double adf = fem::abs(df);
  double tb = b + b;
  double ab = fem::abs(tb);
  double acmx = fem::double0;
  double acmn = fem::double0;
  if (fem::abs(a) > fem::abs(c)) {
    acmx = a;
    acmn = c;
  }
  else {
    acmx = c;
    acmn = a;
  }
  const double one = 1.0e0;
  double rt = fem::double0;
  const double two = 2.0e0;
  if (adf > ab) {
    rt = adf * fem::sqrt(one + fem::pow2((ab / adf)));
  }
  else if (adf < ab) {
    rt = ab * fem::sqrt(one + fem::pow2((adf / ab)));
  }
  else {
    //C
    //C        Includes case AB=ADF=0
    //C
    rt = ab * fem::sqrt(two);
  }
  const double zero = 0.0e0;
  const double half = 0.5e0;
  int sgn1 = fem::int0;
  if (sm < zero) {
    rt1 = half * (sm - rt);
    sgn1 = -1;
    //C
    //C        Order of execution important.
    //C        To get fully accurate smaller eigenvalue,
    //C        next line needs to be executed in higher precision.
    //C
    rt2 = (acmx / rt1) * acmn - (b / rt1) * b;
  }
  else if (sm > zero) {
    rt1 = half * (sm + rt);
    sgn1 = 1;
    //C
    //C        Order of execution important.
    //C        To get fully accurate smaller eigenvalue,
    //C        next line needs to be executed in higher precision.
    //C
    rt2 = (acmx / rt1) * acmn - (b / rt1) * b;
  }
  else {
    //C
    //C        Includes case RT1 = RT2 = 0
    //C
    rt1 = half * rt;
    rt2 = -half * rt;
    sgn1 = 1;
  }
  //C
  //C     Compute the eigenvector
  //C
  double cs = fem::double0;
  int sgn2 = fem::int0;
  if (df >= zero) {
    cs = df + rt;
    sgn2 = 1;
  }
  else {
    cs = df - rt;
    sgn2 = -1;
  }
  double acs = fem::abs(cs);
  double ct = fem::double0;
  double tn = fem::double0;
  if (acs > ab) {
    ct = -tb / cs;
    sn1 = one / fem::sqrt(one + ct * ct);
    cs1 = ct * sn1;
  }
  else {
    if (ab == zero) {
      cs1 = one;
      sn1 = zero;
    }
    else {
      tn = -cs / tb;
      cs1 = one / fem::sqrt(one + tn * tn);
      sn1 = tn * cs1;
    }
  }
  if (sgn1 == sgn2) {
    tn = cs1;
    cs1 = -sn1;
    sn1 = tn;
  }
  //C
  //C     End of DLAEV2
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dlanst.f
double
dlanst(
  str_cref norm,
  int const& n,
  arr_cref<double> d,
  arr_cref<double> e)
{
  double return_value = fem::double0;
  d(dimension(star));
  e(dimension(star));
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DLANST  returns the value of the one norm,  or the Frobenius norm, or
  //C  the  infinity norm,  or the  element of  largest absolute value  of a
  //C  real symmetric tridiagonal matrix A.
  //C
  //C  Description
  //C  ===========
  //C
  //C  DLANST returns the value
  //C
  //C     DLANST = ( max(abs(A(i,j))), NORM = 'M' or 'm'
  //C              (
  //C              ( norm1(A),         NORM = '1', 'O' or 'o'
  //C              (
  //C              ( normI(A),         NORM = 'I' or 'i'
  //C              (
  //C              ( normF(A),         NORM = 'F', 'f', 'E' or 'e'
  //C
  //C  where  norm1  denotes the  one norm of a matrix (maximum column sum),
  //C  normI  denotes the  infinity norm  of a matrix  (maximum row sum) and
  //C  normF  denotes the  Frobenius norm of a matrix (square root of sum of
  //C  squares).  Note that  max(abs(A(i,j)))  is not a consistent matrix norm.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  NORM    (input) CHARACTER*1
  //C          Specifies the value to be returned in DLANST as described
  //C          above.
  //C
  //C  N       (input) INTEGER
  //C          The order of the matrix A.  N >= 0.  When N = 0, DLANST is
  //C          set to zero.
  //C
  //C  D       (input) DOUBLE PRECISION array, dimension (N)
  //C          The diagonal elements of A.
  //C
  //C  E       (input) DOUBLE PRECISION array, dimension (N-1)
  //C          The (n-1) sub-diagonal or super-diagonal elements of A.
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  const double zero = 0.0e+0;
  double anorm = fem::double0;
  int i = fem::int0;
  double scale = fem::double0;
  const double one = 1.0e+0;
  double sum = fem::double0;
  if (n <= 0) {
    anorm = zero;
  }
  else if (lsame(norm, "M")) {
    //C
    //C        Find max(abs(A(i,j))).
    //C
    anorm = fem::abs(d(n));
    {
      int fem_do_last = n - 1;
      FEM_DO(i, 1, fem_do_last) {
        anorm = fem::max(anorm, fem::abs(d(i)));
        anorm = fem::max(anorm, fem::abs(e(i)));
      }
    }
  }
  else if (lsame(norm, "O") || norm == "1" || lsame(norm, "I")) {
    //C
    //C        Find norm1(A).
    //C
    if (n == 1) {
      anorm = fem::abs(d(1));
    }
    else {
      anorm = fem::max(fem::abs(d(1)) + fem::abs(e(1)), fem::abs(e(
        n - 1)) + fem::abs(d(n)));
      {
        int fem_do_last = n - 1;
        FEM_DO(i, 2, fem_do_last) {
          anorm = fem::max(anorm, fem::abs(d(i)) + fem::abs(e(i)) +
            fem::abs(e(i - 1)));
        }
      }
    }
  }
  else if ((lsame(norm, "F")) || (lsame(norm, "E"))) {
    //C
    //C        Find normF(A).
    //C
    scale = zero;
    sum = one;
    if (n > 1) {
      dlassq(n - 1, e, 1, scale, sum);
      sum = 2 * sum;
    }
    dlassq(n, d, 1, scale, sum);
    anorm = scale * fem::sqrt(sum);
  }
  //C
  return_value = anorm;
  return return_value;
  //C
  //C     End of DLANST
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dlapy2.f
double
dlapy2(
  double const& x,
  double const& y)
{
  double return_value = fem::double0;
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DLAPY2 returns sqrt(x**2+y**2), taking care not to cause unnecessary
  //C  overflow.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  X       (input) DOUBLE PRECISION
  //C  Y       (input) DOUBLE PRECISION
  //C          X and Y specify the values x and y.
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  double xabs = fem::abs(x);
  double yabs = fem::abs(y);
  double w = fem::max(xabs, yabs);
  double z = fem::min(xabs, yabs);
  const double zero = 0.0e0;
  const double one = 1.0e0;
  if (z == zero) {
    return_value = w;
  }
  else {
    return_value = w * fem::sqrt(one + fem::pow2((z / w)));
  }
  return return_value;
  //C
  //C     End of DLAPY2
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dlartg.f
void
dlartg(
  common& cmn,
  double const& f,
  double const& g,
  double& cs,
  double& sn,
  double& r)
{
  double safmin = fem::double0;
  double eps = fem::double0;
  const double two = 2.0e0;
  double safmn2 = fem::double0;
  const double one = 1.0e0;
  double safmx2 = fem::double0;
  const double zero = 0.0e0;
  double f1 = fem::double0;
  double g1 = fem::double0;
  double scale = fem::double0;
  int count = fem::int0;
  int i = fem::int0;
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DLARTG generate a plane rotation so that
  //C
  //C     [  CS  SN  ]  .  [ F ]  =  [ R ]   where CS**2 + SN**2 = 1.
  //C     [ -SN  CS  ]     [ G ]     [ 0 ]
  //C
  //C  This is a slower, more accurate version of the BLAS1 routine DROTG,
  //C  with the following other differences:
  //C     F and G are unchanged on return.
  //C     If G=0, then CS=1 and SN=0.
  //C     If F=0 and (G .ne. 0), then CS=0 and SN=1 without doing any
  //C        floating point operations (saves work in DBDSQR when
  //C        there are zeros on the diagonal).
  //C
  //C  If F exceeds G in magnitude, CS will be positive.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  F       (input) DOUBLE PRECISION
  //C          The first component of vector to be rotated.
  //C
  //C  G       (input) DOUBLE PRECISION
  //C          The second component of vector to be rotated.
  //C
  //C  CS      (output) DOUBLE PRECISION
  //C          The cosine of the rotation.
  //C
  //C  SN      (output) DOUBLE PRECISION
  //C          The sine of the rotation.
  //C
  //C  R       (output) DOUBLE PRECISION
  //C          The nonzero component of the rotated vector.
  //C
  //C  This version has a few statements commented out for thread safety
  //C  (machine parameters are computed on each entry). 10 feb 03, SJH.
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     LOGICAL            FIRST
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Save statement ..
  //C     SAVE               FIRST, SAFMX2, SAFMIN, SAFMN2
  //C     ..
  //C     .. Data statements ..
  //C     DATA               FIRST / .TRUE. /
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     IF( FIRST ) THEN
  safmin = dlamch(cmn, "S");
  eps = dlamch(cmn, "E");
  safmn2 = fem::pow(dlamch(cmn, "B"), fem::fint(fem::log(safmin /
    eps) / fem::log(dlamch(cmn, "B")) / two));
  safmx2 = one / safmn2;
  //C        FIRST = .FALSE.
  //C     END IF
  if (g == zero) {
    cs = one;
    sn = zero;
    r = f;
  }
  else if (f == zero) {
    cs = zero;
    sn = one;
    r = g;
  }
  else {
    f1 = f;
    g1 = g;
    scale = fem::max(fem::abs(f1), fem::abs(g1));
    if (scale >= safmx2) {
      count = 0;
      statement_10:
      count++;
      f1 = f1 * safmn2;
      g1 = g1 * safmn2;
      scale = fem::max(fem::abs(f1), fem::abs(g1));
      if (scale >= safmx2) {
        goto statement_10;
      }
      r = fem::sqrt(fem::pow2(f1) + fem::pow2(g1));
      cs = f1 / r;
      sn = g1 / r;
      FEM_DO(i, 1, count) {
        r = r * safmx2;
      }
    }
    else if (scale <= safmn2) {
      count = 0;
      statement_30:
      count++;
      f1 = f1 * safmx2;
      g1 = g1 * safmx2;
      scale = fem::max(fem::abs(f1), fem::abs(g1));
      if (scale <= safmn2) {
        goto statement_30;
      }
      r = fem::sqrt(fem::pow2(f1) + fem::pow2(g1));
      cs = f1 / r;
      sn = g1 / r;
      FEM_DO(i, 1, count) {
        r = r * safmn2;
      }
    }
    else {
      r = fem::sqrt(fem::pow2(f1) + fem::pow2(g1));
      cs = f1 / r;
      sn = g1 / r;
    }
    if (fem::abs(f) > fem::abs(g) && cs < zero) {
      cs = -cs;
      sn = -sn;
      r = -r;
    }
  }
  //C
  //C     End of DLARTG
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dlaset.f
void
dlaset(
  str_cref uplo,
  int const& m,
  int const& n,
  double const& alpha,
  double const& beta,
  arr_ref<double, 2> a,
  int const& lda)
{
  a(dimension(lda, star));
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DLASET initializes an m-by-n matrix A to BETA on the diagonal and
  //C  ALPHA on the offdiagonals.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  UPLO    (input) CHARACTER*1
  //C          Specifies the part of the matrix A to be set.
  //C          = 'U':      Upper triangular part is set; the strictly lower
  //C                      triangular part of A is not changed.
  //C          = 'L':      Lower triangular part is set; the strictly upper
  //C                      triangular part of A is not changed.
  //C          Otherwise:  All of the matrix A is set.
  //C
  //C  M       (input) INTEGER
  //C          The number of rows of the matrix A.  M >= 0.
  //C
  //C  N       (input) INTEGER
  //C          The number of columns of the matrix A.  N >= 0.
  //C
  //C  ALPHA   (input) DOUBLE PRECISION
  //C          The constant to which the offdiagonal elements are to be set.
  //C
  //C  BETA    (input) DOUBLE PRECISION
  //C          The constant to which the diagonal elements are to be set.
  //C
  //C  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //C          On exit, the leading m-by-n submatrix of A is set as follows:
  //C
  //C          if UPLO = 'U', A(i,j) = ALPHA, 1<=i<=j-1, 1<=j<=n,
  //C          if UPLO = 'L', A(i,j) = ALPHA, j+1<=i<=m, 1<=j<=n,
  //C          otherwise,     A(i,j) = ALPHA, 1<=i<=m, 1<=j<=n, i.ne.j,
  //C
  //C          and, for all UPLO, A(i,i) = BETA, 1<=i<=min(m,n).
  //C
  //C  LDA     (input) INTEGER
  //C          The leading dimension of the array A.  LDA >= max(1,M).
  //C
  //C =====================================================================
  //C
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  int j = fem::int0;
  int i = fem::int0;
  if (lsame(uplo, "U")) {
    //C
    //C        Set the strictly upper triangular or trapezoidal part of the
    //C        array to ALPHA.
    //C
    FEM_DO(j, 2, n) {
      {
        int fem_do_last = fem::min(j - 1, m);
        FEM_DO(i, 1, fem_do_last) {
          a(i, j) = alpha;
        }
      }
    }
    //C
  }
  else if (lsame(uplo, "L")) {
    //C
    //C        Set the strictly lower triangular or trapezoidal part of the
    //C        array to ALPHA.
    //C
    {
      int fem_do_last = fem::min(m, n);
      FEM_DO(j, 1, fem_do_last) {
        FEM_DO(i, j + 1, m) {
          a(i, j) = alpha;
        }
      }
    }
    //C
  }
  else {
    //C
    //C        Set the leading m-by-n submatrix to ALPHA.
    //C
    FEM_DO(j, 1, n) {
      FEM_DO(i, 1, m) {
        a(i, j) = alpha;
      }
    }
  }
  //C
  //C     Set the first min(M,N) diagonal elements to BETA.
  //C
  {
    int fem_do_last = fem::min(m, n);
    FEM_DO(i, 1, fem_do_last) {
      a(i, i) = beta;
    }
  }
  //C
  //C     End of DLASET
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dlasr.f
void
dlasr(
  str_cref side,
  str_cref pivot,
  str_cref direct,
  int const& m,
  int const& n,
  arr_cref<double> c,
  arr_cref<double> s,
  arr_ref<double, 2> a,
  int const& lda)
{
  c(dimension(star));
  s(dimension(star));
  a(dimension(lda, star));
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DLASR applies a sequence of plane rotations to a real matrix A,
  //C  from either the left or the right.
  //C
  //C  When SIDE = 'L', the transformation takes the form
  //C
  //C     A := P*A
  //C
  //C  and when SIDE = 'R', the transformation takes the form
  //C
  //C     A := A*P**T
  //C
  //C  where P is an orthogonal matrix consisting of a sequence of z plane
  //C  rotations, with z = M when SIDE = 'L' and z = N when SIDE = 'R',
  //C  and P**T is the transpose of P.
  //C
  //C  When DIRECT = 'F' (Forward sequence), then
  //C
  //C     P = P(z-1) * ... * P(2) * P(1)
  //C
  //C  and when DIRECT = 'B' (Backward sequence), then
  //C
  //C     P = P(1) * P(2) * ... * P(z-1)
  //C
  //C  where P(k) is a plane rotation matrix defined by the 2-by-2 rotation
  //C
  //C     R(k) = (  c(k)  s(k) )
  //C          = ( -s(k)  c(k) ).
  //C
  //C  When PIVOT = 'V' (Variable pivot), the rotation is performed
  //C  for the plane (k,k+1), i.e., P(k) has the form
  //C
  //C     P(k) = (  1                                            )
  //C            (       ...                                     )
  //C            (              1                                )
  //C            (                   c(k)  s(k)                  )
  //C            (                  -s(k)  c(k)                  )
  //C            (                                1              )
  //C            (                                     ...       )
  //C            (                                            1  )
  //C
  //C  where R(k) appears as a rank-2 modification to the identity matrix in
  //C  rows and columns k and k+1.
  //C
  //C  When PIVOT = 'T' (Top pivot), the rotation is performed for the
  //C  plane (1,k+1), so P(k) has the form
  //C
  //C     P(k) = (  c(k)                    s(k)                 )
  //C            (         1                                     )
  //C            (              ...                              )
  //C            (                     1                         )
  //C            ( -s(k)                    c(k)                 )
  //C            (                                 1             )
  //C            (                                      ...      )
  //C            (                                             1 )
  //C
  //C  where R(k) appears in rows and columns 1 and k+1.
  //C
  //C  Similarly, when PIVOT = 'B' (Bottom pivot), the rotation is
  //C  performed for the plane (k,z), giving P(k) the form
  //C
  //C     P(k) = ( 1                                             )
  //C            (      ...                                      )
  //C            (             1                                 )
  //C            (                  c(k)                    s(k) )
  //C            (                         1                     )
  //C            (                              ...              )
  //C            (                                     1         )
  //C            (                 -s(k)                    c(k) )
  //C
  //C  where R(k) appears in rows and columns k and z.  The rotations are
  //C  performed without ever forming P(k) explicitly.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  SIDE    (input) CHARACTER*1
  //C          Specifies whether the plane rotation matrix P is applied to
  //C          A on the left or the right.
  //C          = 'L':  Left, compute A := P*A
  //C          = 'R':  Right, compute A:= A*P**T
  //C
  //C  PIVOT   (input) CHARACTER*1
  //C          Specifies the plane for which P(k) is a plane rotation
  //C          matrix.
  //C          = 'V':  Variable pivot, the plane (k,k+1)
  //C          = 'T':  Top pivot, the plane (1,k+1)
  //C          = 'B':  Bottom pivot, the plane (k,z)
  //C
  //C  DIRECT  (input) CHARACTER*1
  //C          Specifies whether P is a forward or backward sequence of
  //C          plane rotations.
  //C          = 'F':  Forward, P = P(z-1)*...*P(2)*P(1)
  //C          = 'B':  Backward, P = P(1)*P(2)*...*P(z-1)
  //C
  //C  M       (input) INTEGER
  //C          The number of rows of the matrix A.  If m <= 1, an immediate
  //C          return is effected.
  //C
  //C  N       (input) INTEGER
  //C          The number of columns of the matrix A.  If n <= 1, an
  //C          immediate return is effected.
  //C
  //C  C       (input) DOUBLE PRECISION array, dimension
  //C                  (M-1) if SIDE = 'L'
  //C                  (N-1) if SIDE = 'R'
  //C          The cosines c(k) of the plane rotations.
  //C
  //C  S       (input) DOUBLE PRECISION array, dimension
  //C                  (M-1) if SIDE = 'L'
  //C                  (N-1) if SIDE = 'R'
  //C          The sines s(k) of the plane rotations.  The 2-by-2 plane
  //C          rotation part of the matrix P(k), R(k), has the form
  //C          R(k) = (  c(k)  s(k) )
  //C                 ( -s(k)  c(k) ).
  //C
  //C  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //C          The M-by-N matrix A.  On exit, A is overwritten by P*A if
  //C          SIDE = 'R' or by A*P**T if SIDE = 'L'.
  //C
  //C  LDA     (input) INTEGER
  //C          The leading dimension of the array A.  LDA >= max(1,M).
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Test the input parameters
  //C
  int info = 0;
  if (!(lsame(side, "L") || lsame(side, "R"))) {
    info = 1;
  }
  else if (!(lsame(pivot, "V") || lsame(pivot, "T") || lsame(pivot, "B"))) {
    info = 2;
  }
  else if (!(lsame(direct, "F") || lsame(direct, "B"))) {
    info = 3;
  }
  else if (m < 0) {
    info = 4;
  }
  else if (n < 0) {
    info = 5;
  }
  else if (lda < fem::max(1, m)) {
    info = 9;
  }
  if (info != 0) {
    xerbla("DLASR ", info);
    return;
  }
  //C
  //C     Quick return if possible
  //C
  if ((m == 0) || (n == 0)) {
    return;
  }
  int j = fem::int0;
  double ctemp = fem::double0;
  double stemp = fem::double0;
  const double one = 1.0e+0;
  const double zero = 0.0e+0;
  int i = fem::int0;
  double temp = fem::double0;
  if (lsame(side, "L")) {
    //C
    //C        Form  P * A
    //C
    if (lsame(pivot, "V")) {
      if (lsame(direct, "F")) {
        {
          int fem_do_last = m - 1;
          FEM_DO(j, 1, fem_do_last) {
            ctemp = c(j);
            stemp = s(j);
            if ((ctemp != one) || (stemp != zero)) {
              FEM_DO(i, 1, n) {
                temp = a(j + 1, i);
                a(j + 1, i) = ctemp * temp - stemp * a(j, i);
                a(j, i) = stemp * temp + ctemp * a(j, i);
              }
            }
          }
        }
      }
      else if (lsame(direct, "B")) {
        FEM_DOSTEP(j, m - 1, 1, -1) {
          ctemp = c(j);
          stemp = s(j);
          if ((ctemp != one) || (stemp != zero)) {
            FEM_DO(i, 1, n) {
              temp = a(j + 1, i);
              a(j + 1, i) = ctemp * temp - stemp * a(j, i);
              a(j, i) = stemp * temp + ctemp * a(j, i);
            }
          }
        }
      }
    }
    else if (lsame(pivot, "T")) {
      if (lsame(direct, "F")) {
        FEM_DO(j, 2, m) {
          ctemp = c(j - 1);
          stemp = s(j - 1);
          if ((ctemp != one) || (stemp != zero)) {
            FEM_DO(i, 1, n) {
              temp = a(j, i);
              a(j, i) = ctemp * temp - stemp * a(1, i);
              a(1, i) = stemp * temp + ctemp * a(1, i);
            }
          }
        }
      }
      else if (lsame(direct, "B")) {
        FEM_DOSTEP(j, m, 2, -1) {
          ctemp = c(j - 1);
          stemp = s(j - 1);
          if ((ctemp != one) || (stemp != zero)) {
            FEM_DO(i, 1, n) {
              temp = a(j, i);
              a(j, i) = ctemp * temp - stemp * a(1, i);
              a(1, i) = stemp * temp + ctemp * a(1, i);
            }
          }
        }
      }
    }
    else if (lsame(pivot, "B")) {
      if (lsame(direct, "F")) {
        {
          int fem_do_last = m - 1;
          FEM_DO(j, 1, fem_do_last) {
            ctemp = c(j);
            stemp = s(j);
            if ((ctemp != one) || (stemp != zero)) {
              FEM_DO(i, 1, n) {
                temp = a(j, i);
                a(j, i) = stemp * a(m, i) + ctemp * temp;
                a(m, i) = ctemp * a(m, i) - stemp * temp;
              }
            }
          }
        }
      }
      else if (lsame(direct, "B")) {
        FEM_DOSTEP(j, m - 1, 1, -1) {
          ctemp = c(j);
          stemp = s(j);
          if ((ctemp != one) || (stemp != zero)) {
            FEM_DO(i, 1, n) {
              temp = a(j, i);
              a(j, i) = stemp * a(m, i) + ctemp * temp;
              a(m, i) = ctemp * a(m, i) - stemp * temp;
            }
          }
        }
      }
    }
  }
  else if (lsame(side, "R")) {
    //C
    //C        Form A * P'
    //C
    if (lsame(pivot, "V")) {
      if (lsame(direct, "F")) {
        {
          int fem_do_last = n - 1;
          FEM_DO(j, 1, fem_do_last) {
            ctemp = c(j);
            stemp = s(j);
            if ((ctemp != one) || (stemp != zero)) {
              FEM_DO(i, 1, m) {
                temp = a(i, j + 1);
                a(i, j + 1) = ctemp * temp - stemp * a(i, j);
                a(i, j) = stemp * temp + ctemp * a(i, j);
              }
            }
          }
        }
      }
      else if (lsame(direct, "B")) {
        FEM_DOSTEP(j, n - 1, 1, -1) {
          ctemp = c(j);
          stemp = s(j);
          if ((ctemp != one) || (stemp != zero)) {
            FEM_DO(i, 1, m) {
              temp = a(i, j + 1);
              a(i, j + 1) = ctemp * temp - stemp * a(i, j);
              a(i, j) = stemp * temp + ctemp * a(i, j);
            }
          }
        }
      }
    }
    else if (lsame(pivot, "T")) {
      if (lsame(direct, "F")) {
        FEM_DO(j, 2, n) {
          ctemp = c(j - 1);
          stemp = s(j - 1);
          if ((ctemp != one) || (stemp != zero)) {
            FEM_DO(i, 1, m) {
              temp = a(i, j);
              a(i, j) = ctemp * temp - stemp * a(i, 1);
              a(i, 1) = stemp * temp + ctemp * a(i, 1);
            }
          }
        }
      }
      else if (lsame(direct, "B")) {
        FEM_DOSTEP(j, n, 2, -1) {
          ctemp = c(j - 1);
          stemp = s(j - 1);
          if ((ctemp != one) || (stemp != zero)) {
            FEM_DO(i, 1, m) {
              temp = a(i, j);
              a(i, j) = ctemp * temp - stemp * a(i, 1);
              a(i, 1) = stemp * temp + ctemp * a(i, 1);
            }
          }
        }
      }
    }
    else if (lsame(pivot, "B")) {
      if (lsame(direct, "F")) {
        {
          int fem_do_last = n - 1;
          FEM_DO(j, 1, fem_do_last) {
            ctemp = c(j);
            stemp = s(j);
            if ((ctemp != one) || (stemp != zero)) {
              FEM_DO(i, 1, m) {
                temp = a(i, j);
                a(i, j) = stemp * a(i, n) + ctemp * temp;
                a(i, n) = ctemp * a(i, n) - stemp * temp;
              }
            }
          }
        }
      }
      else if (lsame(direct, "B")) {
        FEM_DOSTEP(j, n - 1, 1, -1) {
          ctemp = c(j);
          stemp = s(j);
          if ((ctemp != one) || (stemp != zero)) {
            FEM_DO(i, 1, m) {
              temp = a(i, j);
              a(i, j) = stemp * a(i, n) + ctemp * temp;
              a(i, n) = ctemp * a(i, n) - stemp * temp;
            }
          }
        }
      }
    }
  }
  //C
  //C     End of DLASR
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dlasrt.f
void
dlasrt(
  str_cref id,
  int const& n,
  arr_ref<double> d,
  int& info)
{
  d(dimension(star));
  int dir = fem::int0;
  int stkpnt = fem::int0;
  arr_2d<2, 32, int> stack(fem::fill0);
  int start = fem::int0;
  int endd = fem::int0;
  const int select = 20;
  int i = fem::int0;
  int j = fem::int0;
  double dmnmx = fem::double0;
  double d1 = fem::double0;
  double d2 = fem::double0;
  double d3 = fem::double0;
  double tmp = fem::double0;
  //C
  //C  -- LAPACK routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  Sort the numbers in D in increasing order (if ID = 'I') or
  //C  in decreasing order (if ID = 'D' ).
  //C
  //C  Use Quick Sort, reverting to Insertion sort on arrays of
  //C  size <= 20. Dimension of STACK limits N to about 2**32.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  ID      (input) CHARACTER*1
  //C          = 'I': sort D in increasing order;
  //C          = 'D': sort D in decreasing order.
  //C
  //C  N       (input) INTEGER
  //C          The length of the array D.
  //C
  //C  D       (input/output) DOUBLE PRECISION array, dimension (N)
  //C          On entry, the array to be sorted.
  //C          On exit, D has been sorted into increasing order
  //C          (D(1) <= ... <= D(N) ) or into decreasing order
  //C          (D(1) >= ... >= D(N) ), depending on ID.
  //C
  //C  INFO    (output) INTEGER
  //C          = 0:  successful exit
  //C          < 0:  if INFO = -i, the i-th argument had an illegal value
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. Local Arrays ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Test the input paramters.
  //C
  info = 0;
  dir = -1;
  if (lsame(id, "D")) {
    dir = 0;
  }
  else if (lsame(id, "I")) {
    dir = 1;
  }
  if (dir ==  - 1) {
    info = -1;
  }
  else if (n < 0) {
    info = -2;
  }
  if (info != 0) {
    xerbla("DLASRT", -info);
    return;
  }
  //C
  //C     Quick return if possible
  //C
  if (n <= 1) {
    return;
  }
  //C
  stkpnt = 1;
  stack(1, 1) = 1;
  stack(2, 1) = n;
  statement_10:
  start = stack(1, stkpnt);
  endd = stack(2, stkpnt);
  stkpnt = stkpnt - 1;
  if (endd - start <= select && endd - start > 0) {
    //C
    //C        Do Insertion sort on D( START:ENDD )
    //C
    if (dir == 0) {
      //C
      //C           Sort into decreasing order
      //C
      FEM_DO(i, start + 1, endd) {
        FEM_DOSTEP(j, i, start + 1, -1) {
          if (d(j) > d(j - 1)) {
            dmnmx = d(j);
            d(j) = d(j - 1);
            d(j - 1) = dmnmx;
          }
          else {
            goto statement_30;
          }
        }
        statement_30:;
      }
      //C
    }
    else {
      //C
      //C           Sort into increasing order
      //C
      FEM_DO(i, start + 1, endd) {
        FEM_DOSTEP(j, i, start + 1, -1) {
          if (d(j) < d(j - 1)) {
            dmnmx = d(j);
            d(j) = d(j - 1);
            d(j - 1) = dmnmx;
          }
          else {
            goto statement_50;
          }
        }
        statement_50:;
      }
      //C
    }
    //C
  }
  else if (endd - start > select) {
    //C
    //C        Partition D( START:ENDD ) and stack parts, largest one first
    //C
    //C        Choose partition entry as median of 3
    //C
    d1 = d(start);
    d2 = d(endd);
    i = (start + endd) / 2;
    d3 = d(i);
    if (d1 < d2) {
      if (d3 < d1) {
        dmnmx = d1;
      }
      else if (d3 < d2) {
        dmnmx = d3;
      }
      else {
        dmnmx = d2;
      }
    }
    else {
      if (d3 < d2) {
        dmnmx = d2;
      }
      else if (d3 < d1) {
        dmnmx = d3;
      }
      else {
        dmnmx = d1;
      }
    }
    //C
    if (dir == 0) {
      //C
      //C           Sort into decreasing order
      //C
      i = start - 1;
      j = endd + 1;
      statement_60:
      statement_70:
      j = j - 1;
      if (d(j) < dmnmx) {
        goto statement_70;
      }
      statement_80:
      i++;
      if (d(i) > dmnmx) {
        goto statement_80;
      }
      if (i < j) {
        tmp = d(i);
        d(i) = d(j);
        d(j) = tmp;
        goto statement_60;
      }
      if (j - start > endd - j - 1) {
        stkpnt++;
        stack(1, stkpnt) = start;
        stack(2, stkpnt) = j;
        stkpnt++;
        stack(1, stkpnt) = j + 1;
        stack(2, stkpnt) = endd;
      }
      else {
        stkpnt++;
        stack(1, stkpnt) = j + 1;
        stack(2, stkpnt) = endd;
        stkpnt++;
        stack(1, stkpnt) = start;
        stack(2, stkpnt) = j;
      }
    }
    else {
      //C
      //C           Sort into increasing order
      //C
      i = start - 1;
      j = endd + 1;
      statement_90:
      statement_100:
      j = j - 1;
      if (d(j) > dmnmx) {
        goto statement_100;
      }
      statement_110:
      i++;
      if (d(i) < dmnmx) {
        goto statement_110;
      }
      if (i < j) {
        tmp = d(i);
        d(i) = d(j);
        d(j) = tmp;
        goto statement_90;
      }
      if (j - start > endd - j - 1) {
        stkpnt++;
        stack(1, stkpnt) = start;
        stack(2, stkpnt) = j;
        stkpnt++;
        stack(1, stkpnt) = j + 1;
        stack(2, stkpnt) = endd;
      }
      else {
        stkpnt++;
        stack(1, stkpnt) = j + 1;
        stack(2, stkpnt) = endd;
        stkpnt++;
        stack(1, stkpnt) = start;
        stack(2, stkpnt) = j;
      }
    }
  }
  if (stkpnt > 0) {
    goto statement_10;
  }
  //C
  //C     End of DLASRT
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dsteqr.f
void
dsteqr(
  common& cmn,
  str_cref compz,
  int const& n,
  arr_ref<double> d,
  arr_ref<double> e,
  arr_ref<double, 2> z,
  int const& ldz,
  arr_ref<double> work,
  int& info)
{
  d(dimension(star));
  e(dimension(star));
  z(dimension(ldz, star));
  work(dimension(star));
  int icompz = fem::int0;
  const double one = 1.0e0;
  double eps = fem::double0;
  double eps2 = fem::double0;
  double safmin = fem::double0;
  double safmax = fem::double0;
  const double three = 3.0e0;
  double ssfmax = fem::double0;
  double ssfmin = fem::double0;
  const double zero = 0.0e0;
  const int maxit = 30;
  int nmaxit = fem::int0;
  int jtot = fem::int0;
  int l1 = fem::int0;
  int nm1 = fem::int0;
  int m = fem::int0;
  double tst = fem::double0;
  int l = fem::int0;
  int lsv = fem::int0;
  int lend = fem::int0;
  int lendsv = fem::int0;
  double anorm = fem::double0;
  int iscale = fem::int0;
  int lendm1 = fem::int0;
  double p = fem::double0;
  double rt1 = fem::double0;
  double rt2 = fem::double0;
  double c = fem::double0;
  double s = fem::double0;
  const double two = 2.0e0;
  double g = fem::double0;
  double r = fem::double0;
  int mm1 = fem::int0;
  int i = fem::int0;
  double f = fem::double0;
  double b = fem::double0;
  int mm = fem::int0;
  int lendp1 = fem::int0;
  int lm1 = fem::int0;
  int ii = fem::int0;
  int k = fem::int0;
  int j = fem::int0;
  //C
  //C  -- LAPACK routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DSTEQR computes all eigenvalues and, optionally, eigenvectors of a
  //C  symmetric tridiagonal matrix using the implicit QL or QR method.
  //C  The eigenvectors of a full or band symmetric matrix can also be found
  //C  if DSYTRD or DSPTRD or DSBTRD has been used to reduce this matrix to
  //C  tridiagonal form.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  COMPZ   (input) CHARACTER*1
  //C          = 'N':  Compute eigenvalues only.
  //C          = 'V':  Compute eigenvalues and eigenvectors of the original
  //C                  symmetric matrix.  On entry, Z must contain the
  //C                  orthogonal matrix used to reduce the original matrix
  //C                  to tridiagonal form.
  //C          = 'I':  Compute eigenvalues and eigenvectors of the
  //C                  tridiagonal matrix.  Z is initialized to the identity
  //C                  matrix.
  //C
  //C  N       (input) INTEGER
  //C          The order of the matrix.  N >= 0.
  //C
  //C  D       (input/output) DOUBLE PRECISION array, dimension (N)
  //C          On entry, the diagonal elements of the tridiagonal matrix.
  //C          On exit, if INFO = 0, the eigenvalues in ascending order.
  //C
  //C  E       (input/output) DOUBLE PRECISION array, dimension (N-1)
  //C          On entry, the (n-1) subdiagonal elements of the tridiagonal
  //C          matrix.
  //C          On exit, E has been destroyed.
  //C
  //C  Z       (input/output) DOUBLE PRECISION array, dimension (LDZ, N)
  //C          On entry, if  COMPZ = 'V', then Z contains the orthogonal
  //C          matrix used in the reduction to tridiagonal form.
  //C          On exit, if INFO = 0, then if  COMPZ = 'V', Z contains the
  //C          orthonormal eigenvectors of the original symmetric matrix,
  //C          and if COMPZ = 'I', Z contains the orthonormal eigenvectors
  //C          of the symmetric tridiagonal matrix.
  //C          If COMPZ = 'N', then Z is not referenced.
  //C
  //C  LDZ     (input) INTEGER
  //C          The leading dimension of the array Z.  LDZ >= 1, and if
  //C          eigenvectors are desired, then  LDZ >= max(1,N).
  //C
  //C  WORK    (workspace) DOUBLE PRECISION array, dimension (max(1,2*N-2))
  //C          If COMPZ = 'N', then WORK is not referenced.
  //C
  //C  INFO    (output) INTEGER
  //C          = 0:  successful exit
  //C          < 0:  if INFO = -i, the i-th argument had an illegal value
  //C          > 0:  the algorithm has failed to find all the eigenvalues in
  //C                a total of 30*N iterations; if INFO = i, then i
  //C                elements of E have not converged to zero; on exit, D
  //C                and E contain the elements of a symmetric tridiagonal
  //C                matrix which is orthogonally similar to the original
  //C                matrix.
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Test the input parameters.
  //C
  info = 0;
  //C
  if (lsame(compz, "N")) {
    icompz = 0;
  }
  else if (lsame(compz, "V")) {
    icompz = 1;
  }
  else if (lsame(compz, "I")) {
    icompz = 2;
  }
  else {
    icompz = -1;
  }
  if (icompz < 0) {
    info = -1;
  }
  else if (n < 0) {
    info = -2;
  }
  else if ((ldz < 1) || (icompz > 0 && ldz < fem::max(1, n))) {
    info = -6;
  }
  if (info != 0) {
    xerbla("DSTEQR", -info);
    return;
  }
  //C
  //C     Quick return if possible
  //C
  if (n == 0) {
    return;
  }
  //C
  if (n == 1) {
    if (icompz == 2) {
      z(1, 1) = one;
    }
    return;
  }
  //C
  //C     Determine the unit roundoff and over/underflow thresholds.
  //C
  eps = dlamch(cmn, "E");
  eps2 = fem::pow2(eps);
  safmin = dlamch(cmn, "S");
  safmax = one / safmin;
  ssfmax = fem::sqrt(safmax) / three;
  ssfmin = fem::sqrt(safmin) / eps2;
  //C
  //C     Compute the eigenvalues and eigenvectors of the tridiagonal
  //C     matrix.
  //C
  if (icompz == 2) {
    dlaset("Full", n, n, zero, one, z, ldz);
  }
  //C
  nmaxit = n * maxit;
  jtot = 0;
  //C
  //C     Determine where the matrix splits and choose QL or QR iteration
  //C     for each block, according to whether top or bottom diagonal
  //C     element is smaller.
  //C
  l1 = 1;
  nm1 = n - 1;
  //C
  statement_10:
  if (l1 > n) {
    goto statement_160;
  }
  if (l1 > 1) {
    e(l1 - 1) = zero;
  }
  if (l1 <= nm1) {
    FEM_DO(m, l1, nm1) {
      tst = fem::abs(e(m));
      if (tst == zero) {
        goto statement_30;
      }
      if (tst <= (fem::sqrt(fem::abs(d(m))) * fem::sqrt(fem::abs(d(
          m + 1)))) * eps) {
        e(m) = zero;
        goto statement_30;
      }
    }
  }
  m = n;
  //C
  statement_30:
  l = l1;
  lsv = l;
  lend = m;
  lendsv = lend;
  l1 = m + 1;
  if (lend == l) {
    goto statement_10;
  }
  //C
  //C     Scale submatrix in rows and columns L to LEND
  //C
  anorm = dlanst("I", lend - l + 1, d(l), e(l));
  iscale = 0;
  if (anorm == zero) {
    goto statement_10;
  }
  if (anorm > ssfmax) {
    iscale = 1;
    dlascl(cmn, "G", 0, 0, anorm, ssfmax, lend - l + 1, 1, d(l), n, info);
    dlascl(cmn, "G", 0, 0, anorm, ssfmax, lend - l, 1, e(l), n, info);
  }
  else if (anorm < ssfmin) {
    iscale = 2;
    dlascl(cmn, "G", 0, 0, anorm, ssfmin, lend - l + 1, 1, d(l), n, info);
    dlascl(cmn, "G", 0, 0, anorm, ssfmin, lend - l, 1, e(l), n, info);
  }
  //C
  //C     Choose between QL and QR iteration
  //C
  if (fem::abs(d(lend)) < fem::abs(d(l))) {
    lend = lsv;
    l = lendsv;
  }
  //C
  if (lend > l) {
    //C
    //C        QL Iteration
    //C
    //C        Look for small subdiagonal element.
    //C
    statement_40:
    if (l != lend) {
      lendm1 = lend - 1;
      FEM_DO(m, l, lendm1) {
        tst = fem::pow2(fem::abs(e(m)));
        if (tst <= (eps2 * fem::abs(d(m))) * fem::abs(d(m + 1)) + safmin) {
          goto statement_60;
        }
      }
    }
    //C
    m = lend;
    //C
    statement_60:
    if (m < lend) {
      e(m) = zero;
    }
    p = d(l);
    if (m == l) {
      goto statement_80;
    }
    //C
    //C        If remaining matrix is 2-by-2, use DLAE2 or SLAEV2
    //C        to compute its eigensystem.
    //C
    if (m == l + 1) {
      if (icompz > 0) {
        dlaev2(d(l), e(l), d(l + 1), rt1, rt2, c, s);
        work(l) = c;
        work(n - 1 + l) = s;
        dlasr("R", "V", "B", n, 2, work(l), work(n - 1 + l), z(1, l), ldz);
      }
      else {
        dlae2(d(l), e(l), d(l + 1), rt1, rt2);
      }
      d(l) = rt1;
      d(l + 1) = rt2;
      e(l) = zero;
      l += 2;
      if (l <= lend) {
        goto statement_40;
      }
      goto statement_140;
    }
    //C
    if (jtot == nmaxit) {
      goto statement_140;
    }
    jtot++;
    //C
    //C        Form shift.
    //C
    g = (d(l + 1) - p) / (two * e(l));
    r = dlapy2(g, one);
    g = d(m) - p + (e(l) / (g + fem::sign(r, g)));
    //C
    s = one;
    c = one;
    p = zero;
    //C
    //C        Inner loop
    //C
    mm1 = m - 1;
    FEM_DOSTEP(i, mm1, l, -1) {
      f = s * e(i);
      b = c * e(i);
      dlartg(cmn, g, f, c, s, r);
      if (i != m - 1) {
        e(i + 1) = r;
      }
      g = d(i + 1) - p;
      r = (d(i) - g) * s + two * c * b;
      p = s * r;
      d(i + 1) = g + p;
      g = c * r - b;
      //C
      //C           If eigenvectors are desired, then save rotations.
      //C
      if (icompz > 0) {
        work(i) = c;
        work(n - 1 + i) = -s;
      }
      //C
    }
    //C
    //C        If eigenvectors are desired, then apply saved rotations.
    //C
    if (icompz > 0) {
      mm = m - l + 1;
      dlasr("R", "V", "B", n, mm, work(l), work(n - 1 + l), z(1, l), ldz);
    }
    //C
    d(l) = d(l) - p;
    e(l) = g;
    goto statement_40;
    //C
    //C        Eigenvalue found.
    //C
    statement_80:
    d(l) = p;
    //C
    l++;
    if (l <= lend) {
      goto statement_40;
    }
    goto statement_140;
    //C
  }
  else {
    //C
    //C        QR Iteration
    //C
    //C        Look for small superdiagonal element.
    //C
    statement_90:
    if (l != lend) {
      lendp1 = lend + 1;
      FEM_DOSTEP(m, l, lendp1, -1) {
        tst = fem::pow2(fem::abs(e(m - 1)));
        if (tst <= (eps2 * fem::abs(d(m))) * fem::abs(d(m - 1)) + safmin) {
          goto statement_110;
        }
      }
    }
    //C
    m = lend;
    //C
    statement_110:
    if (m > lend) {
      e(m - 1) = zero;
    }
    p = d(l);
    if (m == l) {
      goto statement_130;
    }
    //C
    //C        If remaining matrix is 2-by-2, use DLAE2 or SLAEV2
    //C        to compute its eigensystem.
    //C
    if (m == l - 1) {
      if (icompz > 0) {
        dlaev2(d(l - 1), e(l - 1), d(l), rt1, rt2, c, s);
        work(m) = c;
        work(n - 1 + m) = s;
        dlasr("R", "V", "F", n, 2, work(m), work(n - 1 + m), z(1, l - 1), ldz);
      }
      else {
        dlae2(d(l - 1), e(l - 1), d(l), rt1, rt2);
      }
      d(l - 1) = rt1;
      d(l) = rt2;
      e(l - 1) = zero;
      l = l - 2;
      if (l >= lend) {
        goto statement_90;
      }
      goto statement_140;
    }
    //C
    if (jtot == nmaxit) {
      goto statement_140;
    }
    jtot++;
    //C
    //C        Form shift.
    //C
    g = (d(l - 1) - p) / (two * e(l - 1));
    r = dlapy2(g, one);
    g = d(m) - p + (e(l - 1) / (g + fem::sign(r, g)));
    //C
    s = one;
    c = one;
    p = zero;
    //C
    //C        Inner loop
    //C
    lm1 = l - 1;
    FEM_DO(i, m, lm1) {
      f = s * e(i);
      b = c * e(i);
      dlartg(cmn, g, f, c, s, r);
      if (i != m) {
        e(i - 1) = r;
      }
      g = d(i) - p;
      r = (d(i + 1) - g) * s + two * c * b;
      p = s * r;
      d(i) = g + p;
      g = c * r - b;
      //C
      //C           If eigenvectors are desired, then save rotations.
      //C
      if (icompz > 0) {
        work(i) = c;
        work(n - 1 + i) = s;
      }
      //C
    }
    //C
    //C        If eigenvectors are desired, then apply saved rotations.
    //C
    if (icompz > 0) {
      mm = l - m + 1;
      dlasr("R", "V", "F", n, mm, work(m), work(n - 1 + m), z(1, m), ldz);
    }
    //C
    d(l) = d(l) - p;
    e(lm1) = g;
    goto statement_90;
    //C
    //C        Eigenvalue found.
    //C
    statement_130:
    d(l) = p;
    //C
    l = l - 1;
    if (l >= lend) {
      goto statement_90;
    }
    goto statement_140;
    //C
  }
  //C
  //C     Undo scaling if necessary
  //C
  statement_140:
  if (iscale == 1) {
    dlascl(cmn, "G", 0, 0, ssfmax, anorm, lendsv - lsv + 1, 1, d(lsv), n, info);
    dlascl(cmn, "G", 0, 0, ssfmax, anorm, lendsv - lsv, 1, e(lsv), n, info);
  }
  else if (iscale == 2) {
    dlascl(cmn, "G", 0, 0, ssfmin, anorm, lendsv - lsv + 1, 1, d(lsv), n, info);
    dlascl(cmn, "G", 0, 0, ssfmin, anorm, lendsv - lsv, 1, e(lsv), n, info);
  }
  //C
  //C     Check for no convergence to an eigenvalue after a total
  //C     of N*MAXIT iterations.
  //C
  if (jtot < nmaxit) {
    goto statement_10;
  }
  {
    int fem_do_last = n - 1;
    FEM_DO(i, 1, fem_do_last) {
      if (e(i) != zero) {
        info++;
      }
    }
  }
  goto statement_190;
  //C
  //C     Order eigenvalues and eigenvectors.
  //C
  statement_160:
  if (icompz == 0) {
    //C
    //C        Use Quick Sort
    //C
    dlasrt("I", n, d, info);
    //C
  }
  else {
    //C
    //C        Use Selection Sort to minimize swaps of eigenvectors
    //C
    FEM_DO(ii, 2, n) {
      i = ii - 1;
      k = i;
      p = d(i);
      FEM_DO(j, ii, n) {
        if (d(j) < p) {
          k = j;
          p = d(j);
        }
      }
      if (k != i) {
        d(k) = d(i);
        d(i) = p;
        dswap(n, z(1, i), 1, z(1, k), 1);
      }
    }
  }
  //C
  statement_190:;
  //C
  //C     End of DSTEQR
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dsterf.f
void
dsterf(
  common& cmn,
  int const& n,
  arr_ref<double> d,
  arr_ref<double> e,
  int& info)
{
  d(dimension(star));
  e(dimension(star));
  double eps = fem::double0;
  double eps2 = fem::double0;
  double safmin = fem::double0;
  const double one = 1.0e0;
  double safmax = fem::double0;
  const double three = 3.0e0;
  double ssfmax = fem::double0;
  double ssfmin = fem::double0;
  const int maxit = 30;
  int nmaxit = fem::int0;
  const double zero = 0.0e0;
  double sigma = fem::double0;
  int jtot = fem::int0;
  int l1 = fem::int0;
  int m = fem::int0;
  int l = fem::int0;
  int lsv = fem::int0;
  int lend = fem::int0;
  int lendsv = fem::int0;
  double anorm = fem::double0;
  int iscale = fem::int0;
  int i = fem::int0;
  double p = fem::double0;
  double rte = fem::double0;
  double rt1 = fem::double0;
  double rt2 = fem::double0;
  const double two = 2.0e0;
  double r = fem::double0;
  double c = fem::double0;
  double s = fem::double0;
  double gamma = fem::double0;
  double bb = fem::double0;
  double oldc = fem::double0;
  double oldgam = fem::double0;
  double alpha = fem::double0;
  //C
  //C  -- LAPACK routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DSTERF computes all eigenvalues of a symmetric tridiagonal matrix
  //C  using the Pal-Walker-Kahan variant of the QL or QR algorithm.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  N       (input) INTEGER
  //C          The order of the matrix.  N >= 0.
  //C
  //C  D       (input/output) DOUBLE PRECISION array, dimension (N)
  //C          On entry, the n diagonal elements of the tridiagonal matrix.
  //C          On exit, if INFO = 0, the eigenvalues in ascending order.
  //C
  //C  E       (input/output) DOUBLE PRECISION array, dimension (N-1)
  //C          On entry, the (n-1) subdiagonal elements of the tridiagonal
  //C          matrix.
  //C          On exit, E has been destroyed.
  //C
  //C  INFO    (output) INTEGER
  //C          = 0:  successful exit
  //C          < 0:  if INFO = -i, the i-th argument had an illegal value
  //C          > 0:  the algorithm failed to find all of the eigenvalues in
  //C                a total of 30*N iterations; if INFO = i, then i
  //C                elements of E have not converged to zero.
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Test the input parameters.
  //C
  info = 0;
  //C
  //C     Quick return if possible
  //C
  if (n < 0) {
    info = -1;
    xerbla("DSTERF", -info);
    return;
  }
  if (n <= 1) {
    return;
  }
  //C
  //C     Determine the unit roundoff for this environment.
  //C
  eps = dlamch(cmn, "E");
  eps2 = fem::pow2(eps);
  safmin = dlamch(cmn, "S");
  safmax = one / safmin;
  ssfmax = fem::sqrt(safmax) / three;
  ssfmin = fem::sqrt(safmin) / eps2;
  //C
  //C     Compute the eigenvalues of the tridiagonal matrix.
  //C
  nmaxit = n * maxit;
  sigma = zero;
  jtot = 0;
  //C
  //C     Determine where the matrix splits and choose QL or QR iteration
  //C     for each block, according to whether top or bottom diagonal
  //C     element is smaller.
  //C
  l1 = 1;
  //C
  statement_10:
  if (l1 > n) {
    goto statement_170;
  }
  if (l1 > 1) {
    e(l1 - 1) = zero;
  }
  {
    int fem_do_last = n - 1;
    FEM_DO(m, l1, fem_do_last) {
      if (fem::abs(e(m)) <= (fem::sqrt(fem::abs(d(m))) * fem::sqrt(
          fem::abs(d(m + 1)))) * eps) {
        e(m) = zero;
        goto statement_30;
      }
    }
  }
  m = n;
  //C
  statement_30:
  l = l1;
  lsv = l;
  lend = m;
  lendsv = lend;
  l1 = m + 1;
  if (lend == l) {
    goto statement_10;
  }
  //C
  //C     Scale submatrix in rows and columns L to LEND
  //C
  anorm = dlanst("I", lend - l + 1, d(l), e(l));
  iscale = 0;
  if (anorm > ssfmax) {
    iscale = 1;
    dlascl(cmn, "G", 0, 0, anorm, ssfmax, lend - l + 1, 1, d(l), n, info);
    dlascl(cmn, "G", 0, 0, anorm, ssfmax, lend - l, 1, e(l), n, info);
  }
  else if (anorm < ssfmin) {
    iscale = 2;
    dlascl(cmn, "G", 0, 0, anorm, ssfmin, lend - l + 1, 1, d(l), n, info);
    dlascl(cmn, "G", 0, 0, anorm, ssfmin, lend - l, 1, e(l), n, info);
  }
  //C
  {
    int fem_do_last = lend - 1;
    FEM_DO(i, l, fem_do_last) {
      e(i) = fem::pow2(e(i));
    }
  }
  //C
  //C     Choose between QL and QR iteration
  //C
  if (fem::abs(d(lend)) < fem::abs(d(l))) {
    lend = lsv;
    l = lendsv;
  }
  //C
  if (lend >= l) {
    //C
    //C        QL Iteration
    //C
    //C        Look for small subdiagonal element.
    //C
    statement_50:
    if (l != lend) {
      {
        int fem_do_last = lend - 1;
        FEM_DO(m, l, fem_do_last) {
          if (fem::abs(e(m)) <= eps2 * fem::abs(d(m) * d(m + 1))) {
            goto statement_70;
          }
        }
      }
    }
    m = lend;
    //C
    statement_70:
    if (m < lend) {
      e(m) = zero;
    }
    p = d(l);
    if (m == l) {
      goto statement_90;
    }
    //C
    //C        If remaining matrix is 2 by 2, use DLAE2 to compute its
    //C        eigenvalues.
    //C
    if (m == l + 1) {
      rte = fem::sqrt(e(l));
      dlae2(d(l), rte, d(l + 1), rt1, rt2);
      d(l) = rt1;
      d(l + 1) = rt2;
      e(l) = zero;
      l += 2;
      if (l <= lend) {
        goto statement_50;
      }
      goto statement_150;
    }
    //C
    if (jtot == nmaxit) {
      goto statement_150;
    }
    jtot++;
    //C
    //C        Form shift.
    //C
    rte = fem::sqrt(e(l));
    sigma = (d(l + 1) - p) / (two * rte);
    r = dlapy2(sigma, one);
    sigma = p - (rte / (sigma + fem::sign(r, sigma)));
    //C
    c = one;
    s = zero;
    gamma = d(m) - sigma;
    p = gamma * gamma;
    //C
    //C        Inner loop
    //C
    FEM_DOSTEP(i, m - 1, l, -1) {
      bb = e(i);
      r = p + bb;
      if (i != m - 1) {
        e(i + 1) = s * r;
      }
      oldc = c;
      c = p / r;
      s = bb / r;
      oldgam = gamma;
      alpha = d(i);
      gamma = c * (alpha - sigma) - s * oldgam;
      d(i + 1) = oldgam + (alpha - gamma);
      if (c != zero) {
        p = (gamma * gamma) / c;
      }
      else {
        p = oldc * bb;
      }
    }
    //C
    e(l) = s * p;
    d(l) = sigma + gamma;
    goto statement_50;
    //C
    //C        Eigenvalue found.
    //C
    statement_90:
    d(l) = p;
    //C
    l++;
    if (l <= lend) {
      goto statement_50;
    }
    goto statement_150;
    //C
  }
  else {
    //C
    //C        QR Iteration
    //C
    //C        Look for small superdiagonal element.
    //C
    statement_100:
    FEM_DOSTEP(m, l, lend + 1, -1) {
      if (fem::abs(e(m - 1)) <= eps2 * fem::abs(d(m) * d(m - 1))) {
        goto statement_120;
      }
    }
    m = lend;
    //C
    statement_120:
    if (m > lend) {
      e(m - 1) = zero;
    }
    p = d(l);
    if (m == l) {
      goto statement_140;
    }
    //C
    //C        If remaining matrix is 2 by 2, use DLAE2 to compute its
    //C        eigenvalues.
    //C
    if (m == l - 1) {
      rte = fem::sqrt(e(l - 1));
      dlae2(d(l), rte, d(l - 1), rt1, rt2);
      d(l) = rt1;
      d(l - 1) = rt2;
      e(l - 1) = zero;
      l = l - 2;
      if (l >= lend) {
        goto statement_100;
      }
      goto statement_150;
    }
    //C
    if (jtot == nmaxit) {
      goto statement_150;
    }
    jtot++;
    //C
    //C        Form shift.
    //C
    rte = fem::sqrt(e(l - 1));
    sigma = (d(l - 1) - p) / (two * rte);
    r = dlapy2(sigma, one);
    sigma = p - (rte / (sigma + fem::sign(r, sigma)));
    //C
    c = one;
    s = zero;
    gamma = d(m) - sigma;
    p = gamma * gamma;
    //C
    //C        Inner loop
    //C
    {
      int fem_do_last = l - 1;
      FEM_DO(i, m, fem_do_last) {
        bb = e(i);
        r = p + bb;
        if (i != m) {
          e(i - 1) = s * r;
        }
        oldc = c;
        c = p / r;
        s = bb / r;
        oldgam = gamma;
        alpha = d(i + 1);
        gamma = c * (alpha - sigma) - s * oldgam;
        d(i) = oldgam + (alpha - gamma);
        if (c != zero) {
          p = (gamma * gamma) / c;
        }
        else {
          p = oldc * bb;
        }
      }
    }
    //C
    e(l - 1) = s * p;
    d(l) = sigma + gamma;
    goto statement_100;
    //C
    //C        Eigenvalue found.
    //C
    statement_140:
    d(l) = p;
    //C
    l = l - 1;
    if (l >= lend) {
      goto statement_100;
    }
    goto statement_150;
    //C
  }
  //C
  //C     Undo scaling if necessary
  //C
  statement_150:
  if (iscale == 1) {
    dlascl(cmn, "G", 0, 0, ssfmax, anorm, lendsv - lsv + 1, 1, d(lsv), n, info);
  }
  if (iscale == 2) {
    dlascl(cmn, "G", 0, 0, ssfmin, anorm, lendsv - lsv + 1, 1, d(lsv), n, info);
  }
  //C
  //C     Check for no convergence to an eigenvalue after a total
  //C     of N*MAXIT iterations.
  //C
  if (jtot < nmaxit) {
    goto statement_10;
  }
  {
    int fem_do_last = n - 1;
    FEM_DO(i, 1, fem_do_last) {
      if (e(i) != zero) {
        info++;
      }
    }
  }
  goto statement_180;
  //C
  //C     Sort eigenvalues in increasing order.
  //C
  statement_170:
  dlasrt("I", n, d, info);
  //C
  statement_180:;
  //C
  //C     End of DSTERF
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/BLAS/SRC/dsyr2k.f
void
dsyr2k(
  str_cref uplo,
  str_cref trans,
  int const& n,
  int const& k,
  double const& alpha,
  arr_cref<double, 2> a,
  int const& lda,
  arr_cref<double, 2> b,
  int const& ldb,
  double const& beta,
  arr_ref<double, 2> c,
  int const& ldc)
{
  a(dimension(lda, star));
  b(dimension(ldb, star));
  c(dimension(ldc, star));
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DSYR2K  performs one of the symmetric rank 2k operations
  //C
  //C     C := alpha*A*B' + alpha*B*A' + beta*C,
  //C
  //C  or
  //C
  //C     C := alpha*A'*B + alpha*B'*A + beta*C,
  //C
  //C  where  alpha and beta  are scalars, C is an  n by n  symmetric matrix
  //C  and  A and B  are  n by k  matrices  in the  first  case  and  k by n
  //C  matrices in the second case.
  //C
  //C  Arguments
  //C  ==========
  //C
  //C  UPLO   - CHARACTER*1.
  //C           On  entry,   UPLO  specifies  whether  the  upper  or  lower
  //C           triangular  part  of the  array  C  is to be  referenced  as
  //C           follows:
  //C
  //C              UPLO = 'U' or 'u'   Only the  upper triangular part of  C
  //C                                  is to be referenced.
  //C
  //C              UPLO = 'L' or 'l'   Only the  lower triangular part of  C
  //C                                  is to be referenced.
  //C
  //C           Unchanged on exit.
  //C
  //C  TRANS  - CHARACTER*1.
  //C           On entry,  TRANS  specifies the operation to be performed as
  //C           follows:
  //C
  //C              TRANS = 'N' or 'n'   C := alpha*A*B' + alpha*B*A' +
  //C                                        beta*C.
  //C
  //C              TRANS = 'T' or 't'   C := alpha*A'*B + alpha*B'*A +
  //C                                        beta*C.
  //C
  //C              TRANS = 'C' or 'c'   C := alpha*A'*B + alpha*B'*A +
  //C                                        beta*C.
  //C
  //C           Unchanged on exit.
  //C
  //C  N      - INTEGER.
  //C           On entry,  N specifies the order of the matrix C.  N must be
  //C           at least zero.
  //C           Unchanged on exit.
  //C
  //C  K      - INTEGER.
  //C           On entry with  TRANS = 'N' or 'n',  K  specifies  the number
  //C           of  columns  of the  matrices  A and B,  and on  entry  with
  //C           TRANS = 'T' or 't' or 'C' or 'c',  K  specifies  the  number
  //C           of rows of the matrices  A and B.  K must be at least  zero.
  //C           Unchanged on exit.
  //C
  //C  ALPHA  - DOUBLE PRECISION.
  //C           On entry, ALPHA specifies the scalar alpha.
  //C           Unchanged on exit.
  //C
  //C  A      - DOUBLE PRECISION array of DIMENSION ( LDA, ka ), where ka is
  //C           k  when  TRANS = 'N' or 'n',  and is  n  otherwise.
  //C           Before entry with  TRANS = 'N' or 'n',  the  leading  n by k
  //C           part of the array  A  must contain the matrix  A,  otherwise
  //C           the leading  k by n  part of the array  A  must contain  the
  //C           matrix A.
  //C           Unchanged on exit.
  //C
  //C  LDA    - INTEGER.
  //C           On entry, LDA specifies the first dimension of A as declared
  //C           in  the  calling  (sub)  program.   When  TRANS = 'N' or 'n'
  //C           then  LDA must be at least  max( 1, n ), otherwise  LDA must
  //C           be at least  max( 1, k ).
  //C           Unchanged on exit.
  //C
  //C  B      - DOUBLE PRECISION array of DIMENSION ( LDB, kb ), where kb is
  //C           k  when  TRANS = 'N' or 'n',  and is  n  otherwise.
  //C           Before entry with  TRANS = 'N' or 'n',  the  leading  n by k
  //C           part of the array  B  must contain the matrix  B,  otherwise
  //C           the leading  k by n  part of the array  B  must contain  the
  //C           matrix B.
  //C           Unchanged on exit.
  //C
  //C  LDB    - INTEGER.
  //C           On entry, LDB specifies the first dimension of B as declared
  //C           in  the  calling  (sub)  program.   When  TRANS = 'N' or 'n'
  //C           then  LDB must be at least  max( 1, n ), otherwise  LDB must
  //C           be at least  max( 1, k ).
  //C           Unchanged on exit.
  //C
  //C  BETA   - DOUBLE PRECISION.
  //C           On entry, BETA specifies the scalar beta.
  //C           Unchanged on exit.
  //C
  //C  C      - DOUBLE PRECISION array of DIMENSION ( LDC, n ).
  //C           Before entry  with  UPLO = 'U' or 'u',  the leading  n by n
  //C           upper triangular part of the array C must contain the upper
  //C           triangular part  of the  symmetric matrix  and the strictly
  //C           lower triangular part of C is not referenced.  On exit, the
  //C           upper triangular part of the array  C is overwritten by the
  //C           upper triangular part of the updated matrix.
  //C           Before entry  with  UPLO = 'L' or 'l',  the leading  n by n
  //C           lower triangular part of the array C must contain the lower
  //C           triangular part  of the  symmetric matrix  and the strictly
  //C           upper triangular part of C is not referenced.  On exit, the
  //C           lower triangular part of the array  C is overwritten by the
  //C           lower triangular part of the updated matrix.
  //C
  //C  LDC    - INTEGER.
  //C           On entry, LDC specifies the first dimension of C as declared
  //C           in  the  calling  (sub)  program.   LDC  must  be  at  least
  //C           max( 1, n ).
  //C           Unchanged on exit.
  //C
  //C  Further Details
  //C  ===============
  //C
  //C  Level 3 Blas routine.
  //C
  //C  -- Written on 8-February-1989.
  //C     Jack Dongarra, Argonne National Laboratory.
  //C     Iain Duff, AERE Harwell.
  //C     Jeremy Du Croz, Numerical Algorithms Group Ltd.
  //C     Sven Hammarling, Numerical Algorithms Group Ltd.
  //C
  //C  =====================================================================
  //C
  //C     .. External Functions ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. Parameters ..
  //C     ..
  //C
  //C     Test the input parameters.
  //C
  int nrowa = fem::int0;
  if (lsame(trans, "N")) {
    nrowa = n;
  }
  else {
    nrowa = k;
  }
  bool upper = lsame(uplo, "U");
  //C
  int info = 0;
  if ((!upper) && (!lsame(uplo, "L"))) {
    info = 1;
  }
  else if ((!lsame(trans, "N")) && (!lsame(trans, "T")) && (!lsame(trans,
    "C"))) {
    info = 2;
  }
  else if (n < 0) {
    info = 3;
  }
  else if (k < 0) {
    info = 4;
  }
  else if (lda < fem::max(1, nrowa)) {
    info = 7;
  }
  else if (ldb < fem::max(1, nrowa)) {
    info = 9;
  }
  else if (ldc < fem::max(1, n)) {
    info = 12;
  }
  if (info != 0) {
    xerbla("DSYR2K", info);
    return;
  }
  //C
  //C     Quick return if possible.
  //C
  const double zero = 0.0e+0;
  const double one = 1.0e+0;
  if ((n == 0) || (((alpha == zero) || (k == 0)) && (beta == one))) {
    return;
  }
  //C
  //C     And when  alpha.eq.zero.
  //C
  int j = fem::int0;
  int i = fem::int0;
  if (alpha == zero) {
    if (upper) {
      if (beta == zero) {
        FEM_DO(j, 1, n) {
          FEM_DO(i, 1, j) {
            c(i, j) = zero;
          }
        }
      }
      else {
        FEM_DO(j, 1, n) {
          FEM_DO(i, 1, j) {
            c(i, j) = beta * c(i, j);
          }
        }
      }
    }
    else {
      if (beta == zero) {
        FEM_DO(j, 1, n) {
          FEM_DO(i, j, n) {
            c(i, j) = zero;
          }
        }
      }
      else {
        FEM_DO(j, 1, n) {
          FEM_DO(i, j, n) {
            c(i, j) = beta * c(i, j);
          }
        }
      }
    }
    return;
  }
  //C
  //C     Start the operations.
  //C
  int l = fem::int0;
  double temp1 = fem::double0;
  double temp2 = fem::double0;
  if (lsame(trans, "N")) {
    //C
    //C        Form  C := alpha*A*B' + alpha*B*A' + C.
    //C
    if (upper) {
      FEM_DO(j, 1, n) {
        if (beta == zero) {
          FEM_DO(i, 1, j) {
            c(i, j) = zero;
          }
        }
        else if (beta != one) {
          FEM_DO(i, 1, j) {
            c(i, j) = beta * c(i, j);
          }
        }
        FEM_DO(l, 1, k) {
          if ((a(j, l) != zero) || (b(j, l) != zero)) {
            temp1 = alpha * b(j, l);
            temp2 = alpha * a(j, l);
            FEM_DO(i, 1, j) {
              c(i, j) += a(i, l) * temp1 + b(i, l) * temp2;
            }
          }
        }
      }
    }
    else {
      FEM_DO(j, 1, n) {
        if (beta == zero) {
          FEM_DO(i, j, n) {
            c(i, j) = zero;
          }
        }
        else if (beta != one) {
          FEM_DO(i, j, n) {
            c(i, j) = beta * c(i, j);
          }
        }
        FEM_DO(l, 1, k) {
          if ((a(j, l) != zero) || (b(j, l) != zero)) {
            temp1 = alpha * b(j, l);
            temp2 = alpha * a(j, l);
            FEM_DO(i, j, n) {
              c(i, j) += a(i, l) * temp1 + b(i, l) * temp2;
            }
          }
        }
      }
    }
  }
  else {
    //C
    //C        Form  C := alpha*A'*B + alpha*B'*A + C.
    //C
    if (upper) {
      FEM_DO(j, 1, n) {
        FEM_DO(i, 1, j) {
          temp1 = zero;
          temp2 = zero;
          FEM_DO(l, 1, k) {
            temp1 += a(l, i) * b(l, j);
            temp2 += b(l, i) * a(l, j);
          }
          if (beta == zero) {
            c(i, j) = alpha * temp1 + alpha * temp2;
          }
          else {
            c(i, j) = beta * c(i, j) + alpha * temp1 + alpha * temp2;
          }
        }
      }
    }
    else {
      FEM_DO(j, 1, n) {
        FEM_DO(i, j, n) {
          temp1 = zero;
          temp2 = zero;
          FEM_DO(l, 1, k) {
            temp1 += a(l, i) * b(l, j);
            temp2 += b(l, i) * a(l, j);
          }
          if (beta == zero) {
            c(i, j) = alpha * temp1 + alpha * temp2;
          }
          else {
            c(i, j) = beta * c(i, j) + alpha * temp1 + alpha * temp2;
          }
        }
      }
    }
  }
  //C
  //C     End of DSYR2K.
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/BLAS/SRC/daxpy.f
void
daxpy(
  int const& n,
  double const& da,
  arr_cref<double> dx,
  int const& incx,
  arr_ref<double> dy,
  int const& incy)
{
  dx(dimension(star));
  dy(dimension(star));
  int ix = fem::int0;
  int iy = fem::int0;
  int i = fem::int0;
  int m = fem::int0;
  int mp1 = fem::int0;
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C     DAXPY constant times a vector plus a vector.
  //C     uses unrolled loops for increments equal to one.
  //C
  //C  Further Details
  //C  ===============
  //C
  //C     jack dongarra, linpack, 3/11/78.
  //C     modified 12/3/93, array(1) declarations changed to array(*)
  //C
  //C  =====================================================================
  //C
  //C     .. Local Scalars ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  if (n <= 0) {
    return;
  }
  if (da == 0.0e0) {
    return;
  }
  if (incx == 1 && incy == 1) {
    goto statement_20;
  }
  //C
  //C        code for unequal increments or equal increments
  //C          not equal to 1
  //C
  ix = 1;
  iy = 1;
  if (incx < 0) {
    ix = (-n + 1) * incx + 1;
  }
  if (incy < 0) {
    iy = (-n + 1) * incy + 1;
  }
  FEM_DO(i, 1, n) {
    dy(iy) += da * dx(ix);
    ix += incx;
    iy += incy;
  }
  return;
  //C
  //C        code for both increments equal to 1
  //C
  //C        clean-up loop
  //C
  statement_20:
  m = fem::mod(n, 4);
  if (m == 0) {
    goto statement_40;
  }
  FEM_DO(i, 1, m) {
    dy(i) += da * dx(i);
  }
  if (n < 4) {
    return;
  }
  statement_40:
  mp1 = m + 1;
  FEM_DOSTEP(i, mp1, n, 4) {
    dy(i) += da * dx(i);
    dy(i + 1) += da * dx(i + 1);
    dy(i + 2) += da * dx(i + 2);
    dy(i + 3) += da * dx(i + 3);
  }
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/BLAS/SRC/ddot.f
double
ddot(
  int const& n,
  arr_cref<double> dx,
  int const& incx,
  arr_cref<double> dy,
  int const& incy)
{
  double return_value = fem::double0;
  dx(dimension(star));
  dy(dimension(star));
  double dtemp = fem::double0;
  int ix = fem::int0;
  int iy = fem::int0;
  int i = fem::int0;
  int m = fem::int0;
  int mp1 = fem::int0;
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C     DDOT forms the dot product of two vectors.
  //C     uses unrolled loops for increments equal to one.
  //C
  //C  Further Details
  //C  ===============
  //C
  //C     jack dongarra, linpack, 3/11/78.
  //C     modified 12/3/93, array(1) declarations changed to array(*)
  //C
  //C  =====================================================================
  //C
  //C     .. Local Scalars ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  return_value = 0.0e0;
  dtemp = 0.0e0;
  if (n <= 0) {
    return return_value;
  }
  if (incx == 1 && incy == 1) {
    goto statement_20;
  }
  //C
  //C        code for unequal increments or equal increments
  //C          not equal to 1
  //C
  ix = 1;
  iy = 1;
  if (incx < 0) {
    ix = (-n + 1) * incx + 1;
  }
  if (incy < 0) {
    iy = (-n + 1) * incy + 1;
  }
  FEM_DO(i, 1, n) {
    dtemp += dx(ix) * dy(iy);
    ix += incx;
    iy += incy;
  }
  return_value = dtemp;
  return return_value;
  //C
  //C        code for both increments equal to 1
  //C
  //C        clean-up loop
  //C
  statement_20:
  m = fem::mod(n, 5);
  if (m == 0) {
    goto statement_40;
  }
  FEM_DO(i, 1, m) {
    dtemp += dx(i) * dy(i);
  }
  if (n < 5) {
    goto statement_60;
  }
  statement_40:
  mp1 = m + 1;
  FEM_DOSTEP(i, mp1, n, 5) {
    dtemp += dx(i) * dy(i) + dx(i + 1) * dy(i + 1) + dx(i + 2) * dy(
      i + 2) + dx(i + 3) * dy(i + 3) + dx(i + 4) * dy(i + 4);
  }
  statement_60:
  return_value = dtemp;
  return return_value;
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/BLAS/SRC/dsymv.f
void
dsymv(
  str_cref uplo,
  int const& n,
  double const& alpha,
  arr_cref<double, 2> a,
  int const& lda,
  arr_cref<double> x,
  int const& incx,
  double const& beta,
  arr_ref<double> y,
  int const& incy)
{
  a(dimension(lda, star));
  x(dimension(star));
  y(dimension(star));
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DSYMV  performs the matrix-vector  operation
  //C
  //C     y := alpha*A*x + beta*y,
  //C
  //C  where alpha and beta are scalars, x and y are n element vectors and
  //C  A is an n by n symmetric matrix.
  //C
  //C  Arguments
  //C  ==========
  //C
  //C  UPLO   - CHARACTER*1.
  //C           On entry, UPLO specifies whether the upper or lower
  //C           triangular part of the array A is to be referenced as
  //C           follows:
  //C
  //C              UPLO = 'U' or 'u'   Only the upper triangular part of A
  //C                                  is to be referenced.
  //C
  //C              UPLO = 'L' or 'l'   Only the lower triangular part of A
  //C                                  is to be referenced.
  //C
  //C           Unchanged on exit.
  //C
  //C  N      - INTEGER.
  //C           On entry, N specifies the order of the matrix A.
  //C           N must be at least zero.
  //C           Unchanged on exit.
  //C
  //C  ALPHA  - DOUBLE PRECISION.
  //C           On entry, ALPHA specifies the scalar alpha.
  //C           Unchanged on exit.
  //C
  //C  A      - DOUBLE PRECISION array of DIMENSION ( LDA, n ).
  //C           Before entry with  UPLO = 'U' or 'u', the leading n by n
  //C           upper triangular part of the array A must contain the upper
  //C           triangular part of the symmetric matrix and the strictly
  //C           lower triangular part of A is not referenced.
  //C           Before entry with UPLO = 'L' or 'l', the leading n by n
  //C           lower triangular part of the array A must contain the lower
  //C           triangular part of the symmetric matrix and the strictly
  //C           upper triangular part of A is not referenced.
  //C           Unchanged on exit.
  //C
  //C  LDA    - INTEGER.
  //C           On entry, LDA specifies the first dimension of A as declared
  //C           in the calling (sub) program. LDA must be at least
  //C           max( 1, n ).
  //C           Unchanged on exit.
  //C
  //C  X      - DOUBLE PRECISION array of dimension at least
  //C           ( 1 + ( n - 1 )*abs( INCX ) ).
  //C           Before entry, the incremented array X must contain the n
  //C           element vector x.
  //C           Unchanged on exit.
  //C
  //C  INCX   - INTEGER.
  //C           On entry, INCX specifies the increment for the elements of
  //C           X. INCX must not be zero.
  //C           Unchanged on exit.
  //C
  //C  BETA   - DOUBLE PRECISION.
  //C           On entry, BETA specifies the scalar beta. When BETA is
  //C           supplied as zero then Y need not be set on input.
  //C           Unchanged on exit.
  //C
  //C  Y      - DOUBLE PRECISION array of dimension at least
  //C           ( 1 + ( n - 1 )*abs( INCY ) ).
  //C           Before entry, the incremented array Y must contain the n
  //C           element vector y. On exit, Y is overwritten by the updated
  //C           vector y.
  //C
  //C  INCY   - INTEGER.
  //C           On entry, INCY specifies the increment for the elements of
  //C           Y. INCY must not be zero.
  //C           Unchanged on exit.
  //C
  //C  Further Details
  //C  ===============
  //C
  //C  Level 2 Blas routine.
  //C
  //C  -- Written on 22-October-1986.
  //C     Jack Dongarra, Argonne National Lab.
  //C     Jeremy Du Croz, Nag Central Office.
  //C     Sven Hammarling, Nag Central Office.
  //C     Richard Hanson, Sandia National Labs.
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C
  //C     Test the input parameters.
  //C
  int info = 0;
  if (!lsame(uplo, "U") && !lsame(uplo, "L")) {
    info = 1;
  }
  else if (n < 0) {
    info = 2;
  }
  else if (lda < fem::max(1, n)) {
    info = 5;
  }
  else if (incx == 0) {
    info = 7;
  }
  else if (incy == 0) {
    info = 10;
  }
  if (info != 0) {
    xerbla("DSYMV ", info);
    return;
  }
  //C
  //C     Quick return if possible.
  //C
  const double zero = 0.0e+0;
  const double one = 1.0e+0;
  if ((n == 0) || ((alpha == zero) && (beta == one))) {
    return;
  }
  //C
  //C     Set up the start points in  X  and  Y.
  //C
  int kx = fem::int0;
  if (incx > 0) {
    kx = 1;
  }
  else {
    kx = 1 - (n - 1) * incx;
  }
  int ky = fem::int0;
  if (incy > 0) {
    ky = 1;
  }
  else {
    ky = 1 - (n - 1) * incy;
  }
  //C
  //C     Start the operations. In this version the elements of A are
  //C     accessed sequentially with one pass through the triangular part
  //C     of A.
  //C
  //C     First form  y := beta*y.
  //C
  int i = fem::int0;
  int iy = fem::int0;
  if (beta != one) {
    if (incy == 1) {
      if (beta == zero) {
        FEM_DO(i, 1, n) {
          y(i) = zero;
        }
      }
      else {
        FEM_DO(i, 1, n) {
          y(i) = beta * y(i);
        }
      }
    }
    else {
      iy = ky;
      if (beta == zero) {
        FEM_DO(i, 1, n) {
          y(iy) = zero;
          iy += incy;
        }
      }
      else {
        FEM_DO(i, 1, n) {
          y(iy) = beta * y(iy);
          iy += incy;
        }
      }
    }
  }
  if (alpha == zero) {
    return;
  }
  int j = fem::int0;
  double temp1 = fem::double0;
  double temp2 = fem::double0;
  int jx = fem::int0;
  int jy = fem::int0;
  int ix = fem::int0;
  if (lsame(uplo, "U")) {
    //C
    //C        Form  y  when A is stored in upper triangle.
    //C
    if ((incx == 1) && (incy == 1)) {
      FEM_DO(j, 1, n) {
        temp1 = alpha * x(j);
        temp2 = zero;
        {
          int fem_do_last = j - 1;
          FEM_DO(i, 1, fem_do_last) {
            y(i) += temp1 * a(i, j);
            temp2 += a(i, j) * x(i);
          }
        }
        y(j) += temp1 * a(j, j) + alpha * temp2;
      }
    }
    else {
      jx = kx;
      jy = ky;
      FEM_DO(j, 1, n) {
        temp1 = alpha * x(jx);
        temp2 = zero;
        ix = kx;
        iy = ky;
        {
          int fem_do_last = j - 1;
          FEM_DO(i, 1, fem_do_last) {
            y(iy) += temp1 * a(i, j);
            temp2 += a(i, j) * x(ix);
            ix += incx;
            iy += incy;
          }
        }
        y(jy) += temp1 * a(j, j) + alpha * temp2;
        jx += incx;
        jy += incy;
      }
    }
  }
  else {
    //C
    //C        Form  y  when A is stored in lower triangle.
    //C
    if ((incx == 1) && (incy == 1)) {
      FEM_DO(j, 1, n) {
        temp1 = alpha * x(j);
        temp2 = zero;
        y(j) += temp1 * a(j, j);
        FEM_DO(i, j + 1, n) {
          y(i) += temp1 * a(i, j);
          temp2 += a(i, j) * x(i);
        }
        y(j) += alpha * temp2;
      }
    }
    else {
      jx = kx;
      jy = ky;
      FEM_DO(j, 1, n) {
        temp1 = alpha * x(jx);
        temp2 = zero;
        y(jy) += temp1 * a(j, j);
        ix = jx;
        iy = jy;
        FEM_DO(i, j + 1, n) {
          ix += incx;
          iy += incy;
          y(iy) += temp1 * a(i, j);
          temp2 += a(i, j) * x(ix);
        }
        y(jy) += alpha * temp2;
        jx += incx;
        jy += incy;
      }
    }
  }
  //C
  //C     End of DSYMV .
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/BLAS/SRC/dnrm2.f
double
dnrm2(
  int const& n,
  arr_cref<double> x,
  int const& incx)
{
  double return_value = fem::double0;
  x(dimension(star));
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DNRM2 returns the euclidean norm of a vector via the function
  //C  name, so that
  //C
  //C     DNRM2 := sqrt( x'*x )
  //C
  //C  Further Details
  //C  ===============
  //C
  //C  -- This version written on 25-October-1982.
  //C     Modified on 14-October-1993 to inline the call to DLASSQ.
  //C     Sven Hammarling, Nag Ltd.
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  const double zero = 0.0e+0;
  double norm = fem::double0;
  double scale = fem::double0;
  const double one = 1.0e+0;
  double ssq = fem::double0;
  int ix = fem::int0;
  double absxi = fem::double0;
  if (n < 1 || incx < 1) {
    norm = zero;
  }
  else if (n == 1) {
    norm = fem::abs(x(1));
  }
  else {
    scale = zero;
    ssq = one;
    //C        The following loop is equivalent to this call to the LAPACK
    //C        auxiliary routine:
    //C        CALL DLASSQ( N, X, INCX, SCALE, SSQ )
    //C
    FEM_DOSTEP(ix, 1, 1 + (n - 1) * incx, incx) {
      if (x(ix) != zero) {
        absxi = fem::abs(x(ix));
        if (scale < absxi) {
          ssq = one + ssq * fem::pow2((scale / absxi));
          scale = absxi;
        }
        else {
          ssq += fem::pow2((absxi / scale));
        }
      }
    }
    norm = scale * fem::sqrt(ssq);
  }
  //C
  return_value = norm;
  return return_value;
  //C
  //C     End of DNRM2.
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dlarfg.f
void
dlarfg(
  common& cmn,
  int const& n,
  double& alpha,
  arr_ref<double> x,
  int const& incx,
  double& tau)
{
  x(dimension(star));
  const double zero = 0.0e+0;
  double xnorm = fem::double0;
  double beta = fem::double0;
  double safmin = fem::double0;
  int knt = fem::int0;
  const double one = 1.0e+0;
  double rsafmn = fem::double0;
  int j = fem::int0;
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DLARFG generates a real elementary reflector H of order n, such
  //C  that
  //C
  //C        H * ( alpha ) = ( beta ),   H' * H = I.
  //C            (   x   )   (   0  )
  //C
  //C  where alpha and beta are scalars, and x is an (n-1)-element real
  //C  vector. H is represented in the form
  //C
  //C        H = I - tau * ( 1 ) * ( 1 v' ) ,
  //C                      ( v )
  //C
  //C  where tau is a real scalar and v is a real (n-1)-element
  //C  vector.
  //C
  //C  If the elements of x are all zero, then tau = 0 and H is taken to be
  //C  the unit matrix.
  //C
  //C  Otherwise  1 <= tau <= 2.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  N       (input) INTEGER
  //C          The order of the elementary reflector.
  //C
  //C  ALPHA   (input/output) DOUBLE PRECISION
  //C          On entry, the value alpha.
  //C          On exit, it is overwritten with the value beta.
  //C
  //C  X       (input/output) DOUBLE PRECISION array, dimension
  //C                         (1+(N-2)*abs(INCX))
  //C          On entry, the vector x.
  //C          On exit, it is overwritten with the vector v.
  //C
  //C  INCX    (input) INTEGER
  //C          The increment between elements of X. INCX > 0.
  //C
  //C  TAU     (output) DOUBLE PRECISION
  //C          The value tau.
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  if (n <= 1) {
    tau = zero;
    return;
  }
  //C
  xnorm = dnrm2(n - 1, x, incx);
  //C
  if (xnorm == zero) {
    //C
    //C        H  =  I
    //C
    tau = zero;
  }
  else {
    //C
    //C        general case
    //C
    beta = -fem::sign(dlapy2(alpha, xnorm), alpha);
    safmin = dlamch(cmn, "S") / dlamch(cmn, "E");
    knt = 0;
    if (fem::abs(beta) < safmin) {
      //C
      //C           XNORM, BETA may be inaccurate; scale X and recompute them
      //C
      rsafmn = one / safmin;
      statement_10:
      knt++;
      dscal(n - 1, rsafmn, x, incx);
      beta = beta * rsafmn;
      alpha = alpha * rsafmn;
      if (fem::abs(beta) < safmin) {
        goto statement_10;
      }
      //C
      //C           New BETA is at most 1, at least SAFMIN
      //C
      xnorm = dnrm2(n - 1, x, incx);
      beta = -fem::sign(dlapy2(alpha, xnorm), alpha);
    }
    tau = (beta - alpha) / beta;
    dscal(n - 1, one / (alpha - beta), x, incx);
    //C
    //C        If ALPHA is subnormal, it may lose relative accuracy
    //C
    FEM_DO(j, 1, knt) {
      beta = beta * safmin;
    }
    alpha = beta;
  }
  //C
  //C     End of DLARFG
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dlatrd.f
void
dlatrd(
  common& cmn,
  str_cref uplo,
  int const& n,
  int const& nb,
  arr_ref<double, 2> a,
  int const& lda,
  arr_ref<double> e,
  arr_ref<double> tau,
  arr_ref<double, 2> w,
  int const& ldw)
{
  a(dimension(lda, star));
  e(dimension(star));
  tau(dimension(star));
  w(dimension(ldw, star));
  //C
  //C  -- LAPACK auxiliary routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DLATRD reduces NB rows and columns of a real symmetric matrix A to
  //C  symmetric tridiagonal form by an orthogonal similarity
  //C  transformation Q' * A * Q, and returns the matrices V and W which are
  //C  needed to apply the transformation to the unreduced part of A.
  //C
  //C  If UPLO = 'U', DLATRD reduces the last NB rows and columns of a
  //C  matrix, of which the upper triangle is supplied;
  //C  if UPLO = 'L', DLATRD reduces the first NB rows and columns of a
  //C  matrix, of which the lower triangle is supplied.
  //C
  //C  This is an auxiliary routine called by DSYTRD.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  UPLO    (input) CHARACTER*1
  //C          Specifies whether the upper or lower triangular part of the
  //C          symmetric matrix A is stored:
  //C          = 'U': Upper triangular
  //C          = 'L': Lower triangular
  //C
  //C  N       (input) INTEGER
  //C          The order of the matrix A.
  //C
  //C  NB      (input) INTEGER
  //C          The number of rows and columns to be reduced.
  //C
  //C  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //C          On entry, the symmetric matrix A.  If UPLO = 'U', the leading
  //C          n-by-n upper triangular part of A contains the upper
  //C          triangular part of the matrix A, and the strictly lower
  //C          triangular part of A is not referenced.  If UPLO = 'L', the
  //C          leading n-by-n lower triangular part of A contains the lower
  //C          triangular part of the matrix A, and the strictly upper
  //C          triangular part of A is not referenced.
  //C          On exit:
  //C          if UPLO = 'U', the last NB columns have been reduced to
  //C            tridiagonal form, with the diagonal elements overwriting
  //C            the diagonal elements of A; the elements above the diagonal
  //C            with the array TAU, represent the orthogonal matrix Q as a
  //C            product of elementary reflectors;
  //C          if UPLO = 'L', the first NB columns have been reduced to
  //C            tridiagonal form, with the diagonal elements overwriting
  //C            the diagonal elements of A; the elements below the diagonal
  //C            with the array TAU, represent the  orthogonal matrix Q as a
  //C            product of elementary reflectors.
  //C          See Further Details.
  //C
  //C  LDA     (input) INTEGER
  //C          The leading dimension of the array A.  LDA >= (1,N).
  //C
  //C  E       (output) DOUBLE PRECISION array, dimension (N-1)
  //C          If UPLO = 'U', E(n-nb:n-1) contains the superdiagonal
  //C          elements of the last NB columns of the reduced matrix;
  //C          if UPLO = 'L', E(1:nb) contains the subdiagonal elements of
  //C          the first NB columns of the reduced matrix.
  //C
  //C  TAU     (output) DOUBLE PRECISION array, dimension (N-1)
  //C          The scalar factors of the elementary reflectors, stored in
  //C          TAU(n-nb:n-1) if UPLO = 'U', and in TAU(1:nb) if UPLO = 'L'.
  //C          See Further Details.
  //C
  //C  W       (output) DOUBLE PRECISION array, dimension (LDW,NB)
  //C          The n-by-nb matrix W required to update the unreduced part
  //C          of A.
  //C
  //C  LDW     (input) INTEGER
  //C          The leading dimension of the array W. LDW >= max(1,N).
  //C
  //C  Further Details
  //C  ===============
  //C
  //C  If UPLO = 'U', the matrix Q is represented as a product of elementary
  //C  reflectors
  //C
  //C     Q = H(n) H(n-1) . . . H(n-nb+1).
  //C
  //C  Each H(i) has the form
  //C
  //C     H(i) = I - tau * v * v'
  //C
  //C  where tau is a real scalar, and v is a real vector with
  //C  v(i:n) = 0 and v(i-1) = 1; v(1:i-1) is stored on exit in A(1:i-1,i),
  //C  and tau in TAU(i-1).
  //C
  //C  If UPLO = 'L', the matrix Q is represented as a product of elementary
  //C  reflectors
  //C
  //C     Q = H(1) H(2) . . . H(nb).
  //C
  //C  Each H(i) has the form
  //C
  //C     H(i) = I - tau * v * v'
  //C
  //C  where tau is a real scalar, and v is a real vector with
  //C  v(1:i) = 0 and v(i+1) = 1; v(i+1:n) is stored on exit in A(i+1:n,i),
  //C  and tau in TAU(i).
  //C
  //C  The elements of the vectors v together form the n-by-nb matrix V
  //C  which is needed, with W, to apply the transformation to the unreduced
  //C  part of the matrix, using a symmetric rank-2k update of the form:
  //C  A := A - V*W' - W*V'.
  //C
  //C  The contents of A on exit are illustrated by the following examples
  //C  with n = 5 and nb = 2:
  //C
  //C  if UPLO = 'U':                       if UPLO = 'L':
  //C
  //C    (  a   a   a   v4  v5 )              (  d                  )
  //C    (      a   a   v4  v5 )              (  1   d              )
  //C    (          a   1   v5 )              (  v1  1   a          )
  //C    (              d   1  )              (  v1  v2  a   a      )
  //C    (                  d  )              (  v1  v2  a   a   a  )
  //C
  //C  where d denotes a diagonal element of the reduced matrix, a denotes
  //C  an element of the original matrix that is unchanged, and vi denotes
  //C  an element of the vector defining H(i).
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Quick return if possible
  //C
  if (n <= 0) {
    return;
  }
  //C
  int i = fem::int0;
  int iw = fem::int0;
  const double one = 1.0e+0;
  const double zero = 0.0e+0;
  const double half = 0.5e+0;
  double alpha = fem::double0;
  if (lsame(uplo, "U")) {
    //C
    //C        Reduce last NB columns of upper triangle
    //C
    FEM_DOSTEP(i, n, n - nb + 1, -1) {
      iw = i - n + nb;
      if (i < n) {
        //C
        //C              Update A(1:i,i)
        //C
        dgemv("No transpose", i, n - i, -one, a(1, i + 1), lda, w(i,
          iw + 1), ldw, one, a(1, i), 1);
        dgemv("No transpose", i, n - i, -one, w(1, iw + 1), ldw, a(i,
          i + 1), lda, one, a(1, i), 1);
      }
      if (i > 1) {
        //C
        //C              Generate elementary reflector H(i) to annihilate
        //C              A(1:i-2,i)
        //C
        dlarfg(cmn, i - 1, a(i - 1, i), a(1, i), 1, tau(i - 1));
        e(i - 1) = a(i - 1, i);
        a(i - 1, i) = one;
        //C
        //C              Compute W(1:i-1,i)
        //C
        dsymv("Upper", i - 1, one, a, lda, a(1, i), 1, zero, w(1, iw), 1);
        if (i < n) {
          dgemv("Transpose", i - 1, n - i, one, w(1, iw + 1), ldw, a(1,
            i), 1, zero, w(i + 1, iw), 1);
          dgemv("No transpose", i - 1, n - i, -one, a(1, i + 1), lda,
            w(i + 1, iw), 1, one, w(1, iw), 1);
          dgemv("Transpose", i - 1, n - i, one, a(1, i + 1), lda, a(1,
            i), 1, zero, w(i + 1, iw), 1);
          dgemv("No transpose", i - 1, n - i, -one, w(1, iw + 1),
            ldw, w(i + 1, iw), 1, one, w(1, iw), 1);
        }
        dscal(i - 1, tau(i - 1), w(1, iw), 1);
        alpha = -half * tau(i - 1) * ddot(i - 1, w(1, iw), 1, a(1, i), 1);
        daxpy(i - 1, alpha, a(1, i), 1, w(1, iw), 1);
      }
      //C
    }
  }
  else {
    //C
    //C        Reduce first NB columns of lower triangle
    //C
    FEM_DO(i, 1, nb) {
      //C
      //C           Update A(i:n,i)
      //C
      dgemv("No transpose", n - i + 1, i - 1, -one, a(i, 1), lda, w(i,
        1), ldw, one, a(i, i), 1);
      dgemv("No transpose", n - i + 1, i - 1, -one, w(i, 1), ldw, a(i,
        1), lda, one, a(i, i), 1);
      if (i < n) {
        //C
        //C              Generate elementary reflector H(i) to annihilate
        //C              A(i+2:n,i)
        //C
        dlarfg(cmn, n - i, a(i + 1, i), a(fem::min(i + 2, n), i), 1, tau(i));
        e(i) = a(i + 1, i);
        a(i + 1, i) = one;
        //C
        //C              Compute W(i+1:n,i)
        //C
        dsymv("Lower", n - i, one, a(i + 1, i + 1), lda, a(i + 1, i),
          1, zero, w(i + 1, i), 1);
        dgemv("Transpose", n - i, i - 1, one, w(i + 1, 1), ldw, a(i + 1,
          i), 1, zero, w(1, i), 1);
        dgemv("No transpose", n - i, i - 1, -one, a(i + 1, 1), lda, w(1,
          i), 1, one, w(i + 1, i), 1);
        dgemv("Transpose", n - i, i - 1, one, a(i + 1, 1), lda, a(i + 1,
          i), 1, zero, w(1, i), 1);
        dgemv("No transpose", n - i, i - 1, -one, w(i + 1, 1), ldw, w(1,
          i), 1, one, w(i + 1, i), 1);
        dscal(n - i, tau(i), w(i + 1, i), 1);
        alpha = -half * tau(i) * ddot(n - i, w(i + 1, i), 1, a(i + 1, i), 1);
        daxpy(n - i, alpha, a(i + 1, i), 1, w(i + 1, i), 1);
      }
      //C
    }
  }
  //C
  //C     End of DLATRD
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/BLAS/SRC/dsyr2.f
void
dsyr2(
  str_cref uplo,
  int const& n,
  double const& alpha,
  arr_cref<double> x,
  int const& incx,
  arr_cref<double> y,
  int const& incy,
  arr_ref<double, 2> a,
  int const& lda)
{
  x(dimension(star));
  y(dimension(star));
  a(dimension(lda, star));
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DSYR2  performs the symmetric rank 2 operation
  //C
  //C     A := alpha*x*y' + alpha*y*x' + A,
  //C
  //C  where alpha is a scalar, x and y are n element vectors and A is an n
  //C  by n symmetric matrix.
  //C
  //C  Arguments
  //C  ==========
  //C
  //C  UPLO   - CHARACTER*1.
  //C           On entry, UPLO specifies whether the upper or lower
  //C           triangular part of the array A is to be referenced as
  //C           follows:
  //C
  //C              UPLO = 'U' or 'u'   Only the upper triangular part of A
  //C                                  is to be referenced.
  //C
  //C              UPLO = 'L' or 'l'   Only the lower triangular part of A
  //C                                  is to be referenced.
  //C
  //C           Unchanged on exit.
  //C
  //C  N      - INTEGER.
  //C           On entry, N specifies the order of the matrix A.
  //C           N must be at least zero.
  //C           Unchanged on exit.
  //C
  //C  ALPHA  - DOUBLE PRECISION.
  //C           On entry, ALPHA specifies the scalar alpha.
  //C           Unchanged on exit.
  //C
  //C  X      - DOUBLE PRECISION array of dimension at least
  //C           ( 1 + ( n - 1 )*abs( INCX ) ).
  //C           Before entry, the incremented array X must contain the n
  //C           element vector x.
  //C           Unchanged on exit.
  //C
  //C  INCX   - INTEGER.
  //C           On entry, INCX specifies the increment for the elements of
  //C           X. INCX must not be zero.
  //C           Unchanged on exit.
  //C
  //C  Y      - DOUBLE PRECISION array of dimension at least
  //C           ( 1 + ( n - 1 )*abs( INCY ) ).
  //C           Before entry, the incremented array Y must contain the n
  //C           element vector y.
  //C           Unchanged on exit.
  //C
  //C  INCY   - INTEGER.
  //C           On entry, INCY specifies the increment for the elements of
  //C           Y. INCY must not be zero.
  //C           Unchanged on exit.
  //C
  //C  A      - DOUBLE PRECISION array of DIMENSION ( LDA, n ).
  //C           Before entry with  UPLO = 'U' or 'u', the leading n by n
  //C           upper triangular part of the array A must contain the upper
  //C           triangular part of the symmetric matrix and the strictly
  //C           lower triangular part of A is not referenced. On exit, the
  //C           upper triangular part of the array A is overwritten by the
  //C           upper triangular part of the updated matrix.
  //C           Before entry with UPLO = 'L' or 'l', the leading n by n
  //C           lower triangular part of the array A must contain the lower
  //C           triangular part of the symmetric matrix and the strictly
  //C           upper triangular part of A is not referenced. On exit, the
  //C           lower triangular part of the array A is overwritten by the
  //C           lower triangular part of the updated matrix.
  //C
  //C  LDA    - INTEGER.
  //C           On entry, LDA specifies the first dimension of A as declared
  //C           in the calling (sub) program. LDA must be at least
  //C           max( 1, n ).
  //C           Unchanged on exit.
  //C
  //C  Further Details
  //C  ===============
  //C
  //C  Level 2 Blas routine.
  //C
  //C  -- Written on 22-October-1986.
  //C     Jack Dongarra, Argonne National Lab.
  //C     Jeremy Du Croz, Nag Central Office.
  //C     Sven Hammarling, Nag Central Office.
  //C     Richard Hanson, Sandia National Labs.
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C
  //C     Test the input parameters.
  //C
  int info = 0;
  if (!lsame(uplo, "U") && !lsame(uplo, "L")) {
    info = 1;
  }
  else if (n < 0) {
    info = 2;
  }
  else if (incx == 0) {
    info = 5;
  }
  else if (incy == 0) {
    info = 7;
  }
  else if (lda < fem::max(1, n)) {
    info = 9;
  }
  if (info != 0) {
    xerbla("DSYR2 ", info);
    return;
  }
  //C
  //C     Quick return if possible.
  //C
  const double zero = 0.0e+0;
  if ((n == 0) || (alpha == zero)) {
    return;
  }
  //C
  //C     Set up the start points in X and Y if the increments are not both
  //C     unity.
  //C
  int kx = fem::int0;
  int ky = fem::int0;
  int jx = fem::int0;
  int jy = fem::int0;
  if ((incx != 1) || (incy != 1)) {
    if (incx > 0) {
      kx = 1;
    }
    else {
      kx = 1 - (n - 1) * incx;
    }
    if (incy > 0) {
      ky = 1;
    }
    else {
      ky = 1 - (n - 1) * incy;
    }
    jx = kx;
    jy = ky;
  }
  //C
  //C     Start the operations. In this version the elements of A are
  //C     accessed sequentially with one pass through the triangular part
  //C     of A.
  //C
  int j = fem::int0;
  double temp1 = fem::double0;
  double temp2 = fem::double0;
  int i = fem::int0;
  int ix = fem::int0;
  int iy = fem::int0;
  if (lsame(uplo, "U")) {
    //C
    //C        Form  A  when A is stored in the upper triangle.
    //C
    if ((incx == 1) && (incy == 1)) {
      FEM_DO(j, 1, n) {
        if ((x(j) != zero) || (y(j) != zero)) {
          temp1 = alpha * y(j);
          temp2 = alpha * x(j);
          FEM_DO(i, 1, j) {
            a(i, j) += x(i) * temp1 + y(i) * temp2;
          }
        }
      }
    }
    else {
      FEM_DO(j, 1, n) {
        if ((x(jx) != zero) || (y(jy) != zero)) {
          temp1 = alpha * y(jy);
          temp2 = alpha * x(jx);
          ix = kx;
          iy = ky;
          FEM_DO(i, 1, j) {
            a(i, j) += x(ix) * temp1 + y(iy) * temp2;
            ix += incx;
            iy += incy;
          }
        }
        jx += incx;
        jy += incy;
      }
    }
  }
  else {
    //C
    //C        Form  A  when A is stored in the lower triangle.
    //C
    if ((incx == 1) && (incy == 1)) {
      FEM_DO(j, 1, n) {
        if ((x(j) != zero) || (y(j) != zero)) {
          temp1 = alpha * y(j);
          temp2 = alpha * x(j);
          FEM_DO(i, j, n) {
            a(i, j) += x(i) * temp1 + y(i) * temp2;
          }
        }
      }
    }
    else {
      FEM_DO(j, 1, n) {
        if ((x(jx) != zero) || (y(jy) != zero)) {
          temp1 = alpha * y(jy);
          temp2 = alpha * x(jx);
          ix = jx;
          iy = jy;
          FEM_DO(i, j, n) {
            a(i, j) += x(ix) * temp1 + y(iy) * temp2;
            ix += incx;
            iy += incy;
          }
        }
        jx += incx;
        jy += incy;
      }
    }
  }
  //C
  //C     End of DSYR2 .
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dsytd2.f
void
dsytd2(
  common& cmn,
  str_cref uplo,
  int const& n,
  arr_ref<double, 2> a,
  int const& lda,
  arr_ref<double> d,
  arr_ref<double> e,
  arr_ref<double> tau,
  int& info)
{
  a(dimension(lda, star));
  d(dimension(star));
  e(dimension(star));
  tau(dimension(star));
  //C
  //C  -- LAPACK routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DSYTD2 reduces a real symmetric matrix A to symmetric tridiagonal
  //C  form T by an orthogonal similarity transformation: Q' * A * Q = T.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  UPLO    (input) CHARACTER*1
  //C          Specifies whether the upper or lower triangular part of the
  //C          symmetric matrix A is stored:
  //C          = 'U':  Upper triangular
  //C          = 'L':  Lower triangular
  //C
  //C  N       (input) INTEGER
  //C          The order of the matrix A.  N >= 0.
  //C
  //C  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //C          On entry, the symmetric matrix A.  If UPLO = 'U', the leading
  //C          n-by-n upper triangular part of A contains the upper
  //C          triangular part of the matrix A, and the strictly lower
  //C          triangular part of A is not referenced.  If UPLO = 'L', the
  //C          leading n-by-n lower triangular part of A contains the lower
  //C          triangular part of the matrix A, and the strictly upper
  //C          triangular part of A is not referenced.
  //C          On exit, if UPLO = 'U', the diagonal and first superdiagonal
  //C          of A are overwritten by the corresponding elements of the
  //C          tridiagonal matrix T, and the elements above the first
  //C          superdiagonal, with the array TAU, represent the orthogonal
  //C          matrix Q as a product of elementary reflectors; if UPLO
  //C          = 'L', the diagonal and first subdiagonal of A are over-
  //C          written by the corresponding elements of the tridiagonal
  //C          matrix T, and the elements below the first subdiagonal, with
  //C          the array TAU, represent the orthogonal matrix Q as a product
  //C          of elementary reflectors. See Further Details.
  //C
  //C  LDA     (input) INTEGER
  //C          The leading dimension of the array A.  LDA >= max(1,N).
  //C
  //C  D       (output) DOUBLE PRECISION array, dimension (N)
  //C          The diagonal elements of the tridiagonal matrix T:
  //C          D(i) = A(i,i).
  //C
  //C  E       (output) DOUBLE PRECISION array, dimension (N-1)
  //C          The off-diagonal elements of the tridiagonal matrix T:
  //C          E(i) = A(i,i+1) if UPLO = 'U', E(i) = A(i+1,i) if UPLO = 'L'.
  //C
  //C  TAU     (output) DOUBLE PRECISION array, dimension (N-1)
  //C          The scalar factors of the elementary reflectors (see Further
  //C          Details).
  //C
  //C  INFO    (output) INTEGER
  //C          = 0:  successful exit
  //C          < 0:  if INFO = -i, the i-th argument had an illegal value.
  //C
  //C  Further Details
  //C  ===============
  //C
  //C  If UPLO = 'U', the matrix Q is represented as a product of elementary
  //C  reflectors
  //C
  //C     Q = H(n-1) . . . H(2) H(1).
  //C
  //C  Each H(i) has the form
  //C
  //C     H(i) = I - tau * v * v'
  //C
  //C  where tau is a real scalar, and v is a real vector with
  //C  v(i+1:n) = 0 and v(i) = 1; v(1:i-1) is stored on exit in
  //C  A(1:i-1,i+1), and tau in TAU(i).
  //C
  //C  If UPLO = 'L', the matrix Q is represented as a product of elementary
  //C  reflectors
  //C
  //C     Q = H(1) H(2) . . . H(n-1).
  //C
  //C  Each H(i) has the form
  //C
  //C     H(i) = I - tau * v * v'
  //C
  //C  where tau is a real scalar, and v is a real vector with
  //C  v(1:i) = 0 and v(i+1) = 1; v(i+2:n) is stored on exit in A(i+2:n,i),
  //C  and tau in TAU(i).
  //C
  //C  The contents of A on exit are illustrated by the following examples
  //C  with n = 5:
  //C
  //C  if UPLO = 'U':                       if UPLO = 'L':
  //C
  //C    (  d   e   v2  v3  v4 )              (  d                  )
  //C    (      d   e   v3  v4 )              (  e   d              )
  //C    (          d   e   v4 )              (  v1  e   d          )
  //C    (              d   e  )              (  v1  v2  e   d      )
  //C    (                  d  )              (  v1  v2  v3  e   d  )
  //C
  //C  where d and e denote diagonal and off-diagonal elements of T, and vi
  //C  denotes an element of the vector defining H(i).
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Test the input parameters
  //C
  info = 0;
  bool upper = lsame(uplo, "U");
  if (!upper && !lsame(uplo, "L")) {
    info = -1;
  }
  else if (n < 0) {
    info = -2;
  }
  else if (lda < fem::max(1, n)) {
    info = -4;
  }
  if (info != 0) {
    xerbla("DSYTD2", -info);
    return;
  }
  //C
  //C     Quick return if possible
  //C
  if (n <= 0) {
    return;
  }
  //C
  int i = fem::int0;
  double taui = fem::double0;
  const double zero = 0.0e0;
  const double one = 1.0e0;
  const double half = 1.0e0 / 2.0e0;
  double alpha = fem::double0;
  if (upper) {
    //C
    //C        Reduce the upper triangle of A
    //C
    FEM_DOSTEP(i, n - 1, 1, -1) {
      //C
      //C           Generate elementary reflector H(i) = I - tau * v * v'
      //C           to annihilate A(1:i-1,i+1)
      //C
      dlarfg(cmn, i, a(i, i + 1), a(1, i + 1), 1, taui);
      e(i) = a(i, i + 1);
      //C
      if (taui != zero) {
        //C
        //C              Apply H(i) from both sides to A(1:i,1:i)
        //C
        a(i, i + 1) = one;
        //C
        //C              Compute  x := tau * A * v  storing x in TAU(1:i)
        //C
        dsymv(uplo, i, taui, a, lda, a(1, i + 1), 1, zero, tau, 1);
        //C
        //C              Compute  w := x - 1/2 * tau * (x'*v) * v
        //C
        alpha = -half * taui * ddot(i, tau, 1, a(1, i + 1), 1);
        daxpy(i, alpha, a(1, i + 1), 1, tau, 1);
        //C
        //C              Apply the transformation as a rank-2 update:
        //C                 A := A - v * w' - w * v'
        //C
        dsyr2(uplo, i, -one, a(1, i + 1), 1, tau, 1, a, lda);
        //C
        a(i, i + 1) = e(i);
      }
      d(i + 1) = a(i + 1, i + 1);
      tau(i) = taui;
    }
    d(1) = a(1, 1);
  }
  else {
    //C
    //C        Reduce the lower triangle of A
    //C
    {
      int fem_do_last = n - 1;
      FEM_DO(i, 1, fem_do_last) {
        //C
        //C           Generate elementary reflector H(i) = I - tau * v * v'
        //C           to annihilate A(i+2:n,i)
        //C
        dlarfg(cmn, n - i, a(i + 1, i), a(fem::min(i + 2, n), i), 1, taui);
        e(i) = a(i + 1, i);
        //C
        if (taui != zero) {
          //C
          //C              Apply H(i) from both sides to A(i+1:n,i+1:n)
          //C
          a(i + 1, i) = one;
          //C
          //C              Compute  x := tau * A * v  storing y in TAU(i:n-1)
          //C
          dsymv(uplo, n - i, taui, a(i + 1, i + 1), lda, a(i + 1, i),
            1, zero, tau(i), 1);
          //C
          //C              Compute  w := x - 1/2 * tau * (x'*v) * v
          //C
          alpha = -half * taui * ddot(n - i, tau(i), 1, a(i + 1, i), 1);
          daxpy(n - i, alpha, a(i + 1, i), 1, tau(i), 1);
          //C
          //C              Apply the transformation as a rank-2 update:
          //C                 A := A - v * w' - w * v'
          //C
          dsyr2(uplo, n - i, -one, a(i + 1, i), 1, tau(i), 1, a(i + 1,
            i + 1), lda);
          //C
          a(i + 1, i) = e(i);
        }
        d(i) = a(i, i);
        tau(i) = taui;
      }
    }
    d(n) = a(n, n);
  }
  //C
  //C     End of DSYTD2
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dsytrd.f
void
dsytrd(
  common& cmn,
  str_cref uplo,
  int const& n,
  arr_ref<double, 2> a,
  int const& lda,
  arr_ref<double> d,
  arr_ref<double> e,
  arr_ref<double> tau,
  arr_ref<double> work,
  int const& lwork,
  int& info)
{
  a(dimension(lda, star));
  d(dimension(star));
  e(dimension(star));
  tau(dimension(star));
  work(dimension(star));
  //C
  //C  -- LAPACK routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DSYTRD reduces a real symmetric matrix A to real symmetric
  //C  tridiagonal form T by an orthogonal similarity transformation:
  //C  Q**T * A * Q = T.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  UPLO    (input) CHARACTER*1
  //C          = 'U':  Upper triangle of A is stored;
  //C          = 'L':  Lower triangle of A is stored.
  //C
  //C  N       (input) INTEGER
  //C          The order of the matrix A.  N >= 0.
  //C
  //C  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //C          On entry, the symmetric matrix A.  If UPLO = 'U', the leading
  //C          N-by-N upper triangular part of A contains the upper
  //C          triangular part of the matrix A, and the strictly lower
  //C          triangular part of A is not referenced.  If UPLO = 'L', the
  //C          leading N-by-N lower triangular part of A contains the lower
  //C          triangular part of the matrix A, and the strictly upper
  //C          triangular part of A is not referenced.
  //C          On exit, if UPLO = 'U', the diagonal and first superdiagonal
  //C          of A are overwritten by the corresponding elements of the
  //C          tridiagonal matrix T, and the elements above the first
  //C          superdiagonal, with the array TAU, represent the orthogonal
  //C          matrix Q as a product of elementary reflectors; if UPLO
  //C          = 'L', the diagonal and first subdiagonal of A are over-
  //C          written by the corresponding elements of the tridiagonal
  //C          matrix T, and the elements below the first subdiagonal, with
  //C          the array TAU, represent the orthogonal matrix Q as a product
  //C          of elementary reflectors. See Further Details.
  //C
  //C  LDA     (input) INTEGER
  //C          The leading dimension of the array A.  LDA >= max(1,N).
  //C
  //C  D       (output) DOUBLE PRECISION array, dimension (N)
  //C          The diagonal elements of the tridiagonal matrix T:
  //C          D(i) = A(i,i).
  //C
  //C  E       (output) DOUBLE PRECISION array, dimension (N-1)
  //C          The off-diagonal elements of the tridiagonal matrix T:
  //C          E(i) = A(i,i+1) if UPLO = 'U', E(i) = A(i+1,i) if UPLO = 'L'.
  //C
  //C  TAU     (output) DOUBLE PRECISION array, dimension (N-1)
  //C          The scalar factors of the elementary reflectors (see Further
  //C          Details).
  //C
  //C  WORK    (workspace/output) DOUBLE PRECISION array, dimension (MAX(1,LWORK))
  //C          On exit, if INFO = 0, WORK(1) returns the optimal LWORK.
  //C
  //C  LWORK   (input) INTEGER
  //C          The dimension of the array WORK.  LWORK >= 1.
  //C          For optimum performance LWORK >= N*NB, where NB is the
  //C          optimal blocksize.
  //C
  //C          If LWORK = -1, then a workspace query is assumed; the routine
  //C          only calculates the optimal size of the WORK array, returns
  //C          this value as the first entry of the WORK array, and no error
  //C          message related to LWORK is issued by XERBLA.
  //C
  //C  INFO    (output) INTEGER
  //C          = 0:  successful exit
  //C          < 0:  if INFO = -i, the i-th argument had an illegal value
  //C
  //C  Further Details
  //C  ===============
  //C
  //C  If UPLO = 'U', the matrix Q is represented as a product of elementary
  //C  reflectors
  //C
  //C     Q = H(n-1) . . . H(2) H(1).
  //C
  //C  Each H(i) has the form
  //C
  //C     H(i) = I - tau * v * v'
  //C
  //C  where tau is a real scalar, and v is a real vector with
  //C  v(i+1:n) = 0 and v(i) = 1; v(1:i-1) is stored on exit in
  //C  A(1:i-1,i+1), and tau in TAU(i).
  //C
  //C  If UPLO = 'L', the matrix Q is represented as a product of elementary
  //C  reflectors
  //C
  //C     Q = H(1) H(2) . . . H(n-1).
  //C
  //C  Each H(i) has the form
  //C
  //C     H(i) = I - tau * v * v'
  //C
  //C  where tau is a real scalar, and v is a real vector with
  //C  v(1:i) = 0 and v(i+1) = 1; v(i+2:n) is stored on exit in A(i+2:n,i),
  //C  and tau in TAU(i).
  //C
  //C  The contents of A on exit are illustrated by the following examples
  //C  with n = 5:
  //C
  //C  if UPLO = 'U':                       if UPLO = 'L':
  //C
  //C    (  d   e   v2  v3  v4 )              (  d                  )
  //C    (      d   e   v3  v4 )              (  e   d              )
  //C    (          d   e   v4 )              (  v1  e   d          )
  //C    (              d   e  )              (  v1  v2  e   d      )
  //C    (                  d  )              (  v1  v2  v3  e   d  )
  //C
  //C  where d and e denote diagonal and off-diagonal elements of T, and vi
  //C  denotes an element of the vector defining H(i).
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Test the input parameters
  //C
  info = 0;
  bool upper = lsame(uplo, "U");
  bool lquery = (lwork ==  - 1);
  if (!upper && !lsame(uplo, "L")) {
    info = -1;
  }
  else if (n < 0) {
    info = -2;
  }
  else if (lda < fem::max(1, n)) {
    info = -4;
  }
  else if (lwork < 1 && !lquery) {
    info = -9;
  }
  //C
  int nb = fem::int0;
  int lwkopt = fem::int0;
  if (info == 0) {
    //C
    //C        Determine the block size.
    //C
    nb = ilaenv(1, "DSYTRD", uplo, n, -1, -1, -1);
    lwkopt = n * nb;
    work(1) = lwkopt;
  }
  //C
  if (info != 0) {
    xerbla("DSYTRD", -info);
    return;
  }
  else if (lquery) {
    return;
  }
  //C
  //C     Quick return if possible
  //C
  if (n == 0) {
    work(1) = 1;
    return;
  }
  //C
  int nx = n;
  int iws = 1;
  int ldwork = fem::int0;
  int nbmin = fem::int0;
  if (nb > 1 && nb < n) {
    //C
    //C        Determine when to cross over from blocked to unblocked code
    //C        (last block is always handled by unblocked code).
    //C
    nx = fem::max(nb, ilaenv(3, "DSYTRD", uplo, n, -1, -1, -1));
    if (nx < n) {
      //C
      //C           Determine if workspace is large enough for blocked code.
      //C
      ldwork = n;
      iws = ldwork * nb;
      if (lwork < iws) {
        //C
        //C              Not enough workspace to use optimal NB:  determine the
        //C              minimum value of NB, and reduce NB or force use of
        //C              unblocked code by setting NX = N.
        //C
        nb = fem::max(lwork / ldwork, 1);
        nbmin = ilaenv(2, "DSYTRD", uplo, n, -1, -1, -1);
        if (nb < nbmin) {
          nx = n;
        }
      }
    }
    else {
      nx = n;
    }
  }
  else {
    nb = 1;
  }
  //C
  int kk = fem::int0;
  int i = fem::int0;
  const double one = 1.0e+0;
  int j = fem::int0;
  int iinfo = fem::int0;
  if (upper) {
    //C
    //C        Reduce the upper triangle of A.
    //C        Columns 1:kk are handled by the unblocked method.
    //C
    kk = n - ((n - nx + nb - 1) / nb) * nb;
    FEM_DOSTEP(i, n - nb + 1, kk + 1, -nb) {
      //C
      //C           Reduce columns i:i+nb-1 to tridiagonal form and form the
      //C           matrix W which is needed to update the unreduced part of
      //C           the matrix
      //C
      dlatrd(cmn, uplo, i + nb - 1, nb, a, lda, e, tau, work, ldwork);
      //C
      //C           Update the unreduced submatrix A(1:i-1,1:i-1), using an
      //C           update of the form:  A := A - V*W' - W*V'
      //C
      dsyr2k(uplo, "No transpose", i - 1, nb, -one, a(1, i), lda,
        work, ldwork, one, a, lda);
      //C
      //C           Copy superdiagonal elements back into A, and diagonal
      //C           elements into D
      //C
      {
        int fem_do_last = i + nb - 1;
        FEM_DO(j, i, fem_do_last) {
          a(j - 1, j) = e(j - 1);
          d(j) = a(j, j);
        }
      }
    }
    //C
    //C        Use unblocked code to reduce the last or only block
    //C
    dsytd2(cmn, uplo, kk, a, lda, d, e, tau, iinfo);
  }
  else {
    //C
    //C        Reduce the lower triangle of A
    //C
    FEM_DOSTEP(i, 1, n - nx, nb) {
      //C
      //C           Reduce columns i:i+nb-1 to tridiagonal form and form the
      //C           matrix W which is needed to update the unreduced part of
      //C           the matrix
      //C
      dlatrd(cmn, uplo, n - i + 1, nb, a(i, i), lda, e(i), tau(i),
        work, ldwork);
      //C
      //C           Update the unreduced submatrix A(i+ib:n,i+ib:n), using
      //C           an update of the form:  A := A - V*W' - W*V'
      //C
      dsyr2k(uplo, "No transpose", n - i - nb + 1, nb, -one, a(i + nb,
        i), lda, work(nb + 1), ldwork, one, a(i + nb, i + nb), lda);
      //C
      //C           Copy subdiagonal elements back into A, and diagonal
      //C           elements into D
      //C
      {
        int fem_do_last = i + nb - 1;
        FEM_DO(j, i, fem_do_last) {
          a(j + 1, j) = e(j);
          d(j) = a(j, j);
        }
      }
    }
    //C
    //C        Use unblocked code to reduce the last or only block
    //C
    dsytd2(cmn, uplo, n - i + 1, a(i, i), lda, d(i), e(i), tau(i), iinfo);
  }
  //C
  work(1) = lwkopt;
  //C
  //C     End of DSYTRD
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/lapack-3.2.1/SRC/dsyev.f
void
dsyev(
  common& cmn,
  str_cref jobz,
  str_cref uplo,
  int const& n,
  arr_ref<double, 2> a,
  int const& lda,
  arr_ref<double> w,
  arr_ref<double> work,
  int const& lwork,
  int& info)
{
  a(dimension(lda, star));
  w(dimension(star));
  work(dimension(star));
  //C
  //C  -- LAPACK driver routine (version 3.2) --
  //C  -- LAPACK is a software package provided by Univ. of Tennessee,    --
  //C  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
  //C     November 2006
  //C
  //C     .. Scalar Arguments ..
  //C     ..
  //C     .. Array Arguments ..
  //C     ..
  //C
  //C  Purpose
  //C  =======
  //C
  //C  DSYEV computes all eigenvalues and, optionally, eigenvectors of a
  //C  real symmetric matrix A.
  //C
  //C  Arguments
  //C  =========
  //C
  //C  JOBZ    (input) CHARACTER*1
  //C          = 'N':  Compute eigenvalues only;
  //C          = 'V':  Compute eigenvalues and eigenvectors.
  //C
  //C  UPLO    (input) CHARACTER*1
  //C          = 'U':  Upper triangle of A is stored;
  //C          = 'L':  Lower triangle of A is stored.
  //C
  //C  N       (input) INTEGER
  //C          The order of the matrix A.  N >= 0.
  //C
  //C  A       (input/output) DOUBLE PRECISION array, dimension (LDA, N)
  //C          On entry, the symmetric matrix A.  If UPLO = 'U', the
  //C          leading N-by-N upper triangular part of A contains the
  //C          upper triangular part of the matrix A.  If UPLO = 'L',
  //C          the leading N-by-N lower triangular part of A contains
  //C          the lower triangular part of the matrix A.
  //C          On exit, if JOBZ = 'V', then if INFO = 0, A contains the
  //C          orthonormal eigenvectors of the matrix A.
  //C          If JOBZ = 'N', then on exit the lower triangle (if UPLO='L')
  //C          or the upper triangle (if UPLO='U') of A, including the
  //C          diagonal, is destroyed.
  //C
  //C  LDA     (input) INTEGER
  //C          The leading dimension of the array A.  LDA >= max(1,N).
  //C
  //C  W       (output) DOUBLE PRECISION array, dimension (N)
  //C          If INFO = 0, the eigenvalues in ascending order.
  //C
  //C  WORK    (workspace/output) DOUBLE PRECISION array, dimension (MAX(1,LWORK))
  //C          On exit, if INFO = 0, WORK(1) returns the optimal LWORK.
  //C
  //C  LWORK   (input) INTEGER
  //C          The length of the array WORK.  LWORK >= max(1,3*N-1).
  //C          For optimal efficiency, LWORK >= (NB+2)*N,
  //C          where NB is the blocksize for DSYTRD returned by ILAENV.
  //C
  //C          If LWORK = -1, then a workspace query is assumed; the routine
  //C          only calculates the optimal size of the WORK array, returns
  //C          this value as the first entry of the WORK array, and no error
  //C          message related to LWORK is issued by XERBLA.
  //C
  //C  INFO    (output) INTEGER
  //C          = 0:  successful exit
  //C          < 0:  if INFO = -i, the i-th argument had an illegal value
  //C          > 0:  if INFO = i, the algorithm failed to converge; i
  //C                off-diagonal elements of an intermediate tridiagonal
  //C                form did not converge to zero.
  //C
  //C  =====================================================================
  //C
  //C     .. Parameters ..
  //C     ..
  //C     .. Local Scalars ..
  //C     ..
  //C     .. External Functions ..
  //C     ..
  //C     .. External Subroutines ..
  //C     ..
  //C     .. Intrinsic Functions ..
  //C     ..
  //C     .. Executable Statements ..
  //C
  //C     Test the input parameters.
  //C
  bool wantz = lsame(jobz, "V");
  bool lower = lsame(uplo, "L");
  bool lquery = (lwork ==  - 1);
  //C
  info = 0;
  if (!(wantz || lsame(jobz, "N"))) {
    info = -1;
  }
  else if (!(lower || lsame(uplo, "U"))) {
    info = -2;
  }
  else if (n < 0) {
    info = -3;
  }
  else if (lda < fem::max(1, n)) {
    info = -5;
  }
  //C
  int nb = fem::int0;
  int lwkopt = fem::int0;
  if (info == 0) {
    nb = ilaenv(1, "DSYTRD", uplo, n, -1, -1, -1);
    lwkopt = fem::max(1, (nb + 2) * n);
    work(1) = lwkopt;
    //C
    if (lwork < fem::max(1, 3 * n - 1) && !lquery) {
      info = -8;
    }
  }
  //C
  if (info != 0) {
    xerbla("DSYEV ", -info);
    return;
  }
  else if (lquery) {
    return;
  }
  //C
  //C     Quick return if possible
  //C
  if (n == 0) {
    return;
  }
  //C
  const double one = 1.0e0;
  if (n == 1) {
    w(1) = a(1, 1);
    work(1) = 2;
    if (wantz) {
      a(1, 1) = one;
    }
    return;
  }
  //C
  //C     Get machine constants.
  //C
  double safmin = dlamch(cmn, "Safe minimum");
  double eps = dlamch(cmn, "Precision");
  double smlnum = safmin / eps;
  double bignum = one / smlnum;
  double rmin = fem::sqrt(smlnum);
  double rmax = fem::sqrt(bignum);
  //C
  //C     Scale matrix to allowable range, if necessary.
  //C
  double anrm = dlansy("M", uplo, n, a, lda, work);
  int iscale = 0;
  const double zero = 0.0e0;
  double sigma = fem::double0;
  if (anrm > zero && anrm < rmin) {
    iscale = 1;
    sigma = rmin / anrm;
  }
  else if (anrm > rmax) {
    iscale = 1;
    sigma = rmax / anrm;
  }
  if (iscale == 1) {
    dlascl(cmn, uplo, 0, 0, one, sigma, n, n, a, lda, info);
  }
  //C
  //C     Call DSYTRD to reduce symmetric matrix to tridiagonal form.
  //C
  int inde = 1;
  int indtau = inde + n;
  int indwrk = indtau + n;
  int llwork = lwork - indwrk + 1;
  int iinfo = fem::int0;
  dsytrd(cmn, uplo, n, a, lda, w, work(inde), work(indtau), work(indwrk),
    llwork, iinfo);
  //C
  //C     For eigenvalues only, call DSTERF.  For eigenvectors, first call
  //C     DORGTR to generate the orthogonal matrix, then call DSTEQR.
  //C
  if (!wantz) {
    dsterf(cmn, n, w, work(inde), info);
  }
  else {
    dorgtr(uplo, n, a, lda, work(indtau), work(indwrk), llwork, iinfo);
    dsteqr(cmn, jobz, n, w, work(inde), a, lda, work(indtau), info);
  }
  //C
  //C     If matrix was scaled, then rescale eigenvalues appropriately.
  //C
  int imax = fem::int0;
  if (iscale == 1) {
    if (info == 0) {
      imax = n;
    }
    else {
      imax = info - 1;
    }
    dscal(imax, one / sigma, w, 1);
  }
  //C
  //C     Set WORK(1) to optimal workspace size.
  //C
  work(1) = lwkopt;
  //C
  //C     End of DSYEV
  //C
}

// Fortran file: /net/marbles/raid1/rwgk/dist/lapack_fem/dsyev_driver.f
void
program_dsyev_driver(
  int argc,
  char const* argv[])
{
  common cmn(argc, argv);
  common_write write(cmn);
  int jr = 0;
  int i = fem::int0;
  const int n = 800;
  int j = fem::int0;
  arr<double, 2> a(dimension(n, n), fem::fill0);
  FEM_DO(i, 1, n) {
    FEM_DO(j, 1, n) {
      jr = fem::mod(jr * 1366 + 150889, 714025);
      a(i, j) = (fem::mod(jr, 400000) - 200000) / 100000.e0;
    }
  }
  arr<double> w(dimension(n), fem::fill0);
  const int lwork = 3 * n - 1;
  arr<double> work(dimension(lwork), fem::fill0);
  int info = fem::int0;
  dsyev(cmn, "V", "U", n, a, n, w, work, lwork, info);
  double val_min = w(1);
  double val_max = w(1);
  FEM_DO(i, 1, n) {
    val_min = fem::min(val_min, w(i));
    val_max = fem::max(val_max, w(i));
  }
  write(6, "(f8.3,1x,f8.3)"), val_min, val_max;
  val_min = a(1, 1);
  val_max = a(1, 1);
  FEM_DO(i, 1, n) {
    FEM_DO(j, 1, n) {
      val_min = fem::min(val_min, a(i, j));
      val_max = fem::max(val_max, a(i, j));
    }
  }
  write(6, "(f8.3,1x,f8.3)"), val_min, val_max;
}

} // namespace lapack_dsyev_fem

int
main(
  int argc,
  char const* argv[])
{
  return fem::main_with_catch(
    argc, argv,
    lapack_dsyev_fem::program_dsyev_driver);
}
