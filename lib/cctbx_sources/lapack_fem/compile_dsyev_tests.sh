#! /bin/bash -v

ifort -o dsyev_test_ifort -O dsyev_test.f

gfortran -o dsyev_test_gfortran -O3 -ffast-math dsyev_test.f

icpc -o dsyev_test_icpc -I. -O -no-prec-div dsyev_test.cpp

g++ -o dsyev_test_g++ -I. -O3 -ffast-math dsyev_test.cpp

clang++ -o dsyev_test_clang++ -I. -O3 -ffast-math dsyev_test.cpp

