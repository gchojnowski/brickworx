#! /bin/sh

tag="$1"
t="lapack_fem_$tag"
mkdir "$t"

cp -a dsyev_test.{f,cpp} "$t"
cp -a compile_dsyev_tests.sh "$t"
cp -a time_dsyev_tests.sh "$t"

cp -a xerbla.f dlamch_intel_x86_64.f dsyev_driver.f "$t"
cp -a cout_dsyev_test.csh "$t"

cp -a "`libtbx.show_dist_paths tbxx`" "$t"
cp -a "`libtbx.show_dist_paths fable`/fem.hpp" "$t"
cp -a "`libtbx.show_dist_paths fable`/fem" "$t"

cd "$t"
find . -depth -name .svn -exec rm -rf {} \;
find . -depth -name '*.pyc' -exec rm -rf {} \;
cd ..

tar zcf "$t.tgz" "$t"
