#! /usr/bin/env python

import os
op = os.path

def run(args):
  if (len(args) < 2):
    from libtbx.utils import Usage
    raise Usage(
      "%s fable_cout_file [...] path_name.a" % op.basename(sys.argv[0]))
  path_name_a = args.pop()
  cpp_comment = "// Fortran file: "
  fortran_files = set()
  for file_name in args:
    for line in open(file_name).read().splitlines():
      if (line.startswith(cpp_comment)):
        fortran_files.add(line[len(cpp_comment):])
  fortran_files_common_prefix = op.commonprefix(fortran_files)
  #
  from libtbx.str_utils import show_string
  commands = []
  obj_files = []
  for file_name in fortran_files:
    if (len(fortran_files) == 1):
      part = op.basename(file_name)
    else:
      part = file_name[len(fortran_files_common_prefix):]
    obj_file = op.splitext(part)[0].replace(os.sep, "_") + ".o"
    if (0):
      cmd = "gfortran -fPIC -o %s -O3 -ffast-math -fstrict-aliasing -c %s"
    else:
      cmd = "ifort -fPIC -o %s -O -c %s"
    commands.append(cmd % (obj_file, show_string(file_name)))
    obj_files.append(obj_file)
  #
  if (op.exists(path_name_a)):
    os.remove(path_name_a)
  for file_name in obj_files:
    if (op.exists(file_name)):
      os.remove(file_name)
  #
  import libtbx.introspection
  n_proc = min(len(commands), libtbx.introspection.number_of_processors())
  import multiprocessing
  mp_pool = multiprocessing.Pool(processes=n_proc)
  from libtbx import easy_run
  print "Number of processors:", n_proc
  print "Number of files to compile:", len(commands)
  print commands[0]
  mp_pool.map(easy_run.call, commands)
  #
  n_missing = 0
  for file_name in obj_files:
    if (not op.exists(file_name)):
      print "Missing:", file_name
      n_missing += 1
  if (n_missing == 0):
    cmd = "ar rc %s" % path_name_a
    print cmd, "..."
    cmd += " %s" % " ".join(obj_files)
    easy_run.call(cmd)

if (__name__ == "__main__"):
  import sys
  run(args=sys.argv[1:])
