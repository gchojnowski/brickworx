      program dsyev_driver
      parameter(n=800)
      double precision a(n, n)
      double precision w(n)
      parameter(lwork=3*n-1)
      double precision work(lwork)
      double precision val_min, val_max
      jr = 0
      do i=1,n
        do j=1,n
          jr = mod(jr*1366+150889, 714025)
          a(i,j) = (mod(jr, 400000) - 200000) / 100000.d0
        enddo
      enddo
      call dsyev(
     &  'V',
     &  'U',
     &  n,
     &  a,
     &  n,
     &  w,
     &  work,
     &  lwork,
     &  info)
      val_min = w(1)
      val_max = w(1)
      do i=1,n
        val_min = min(val_min, w(i))
        val_max = max(val_max, w(i))
      enddo
      write(6, '(f8.3,1x,f8.3)') val_min, val_max
      val_min = a(1,1)
      val_max = a(1,1)
      do i=1,n
        do j=1,n
          val_min = min(val_min, a(i,j))
          val_max = max(val_max, a(i,j))
        enddo
      enddo
      write(6, '(f8.3,1x,f8.3)') val_min, val_max
      end
