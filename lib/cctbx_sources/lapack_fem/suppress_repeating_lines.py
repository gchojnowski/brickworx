#! /usr/bin/env python

import sys

def run(args):
  old = {}
  def process(f):
    for line in f.read().splitlines():
      if (line not in old):
        old[line] = 0
        print line
  if (len(args) == 0):
    process(f=sys.stdin)
  else:
    for file_name in args:
      process(f=open(file_name))

if (__name__ == "__main__"):
  run(args=sys.argv[1:])
