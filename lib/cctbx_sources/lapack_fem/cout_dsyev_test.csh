#! /bin/csh -f
set verbose
set lp=/net/marbles/raid1/rwgk/lapack-3.2.1
# BLAS/SRC/xerbla.f
# INSTALL/dlamch.f
fable.cout $dist/lapack_fem/{xerbla.f,dlamch_intel_x86_64.f,dsyev_driver.f} $lp/BLAS/SRC/{d*.f} $lp/SRC/{i*.f,d*.f} $lp/INSTALL/lsame.f --top-procedure=dsyev_driver --namespace=lapack_dsyev_fem --fortran-file-comments --no-fem-do-safe > dsyev_test.cpp
set ff=(`grep '// Fortran file: ' dsyev_test.cpp | cut -d' ' -f4 | $dist/lapack_fem/suppress_repeating_lines.py`)
cat $ff > dsyev_test.f
