import os
op = os.path

def run(args):
  assert len(args) == 1, "directory_with_lapack_sources"
  lapack_dir = args[0]
  file_names = []
  def collect_file_names(src_dir):
    for file_name in os.listdir(src_dir):
      if (not file_name.endswith(".f")): continue
      if (file_name.endswith("vxx.f")): continue
      if (file_name.endswith("fsx.f")): continue
      if (file_name.endswith("_extended.f")): continue
      if (file_name in ["dlamch.f", "xerbla.f", "xerbla_array.f"]): continue
      file_names.append(op.join(src_dir, file_name))
  collect_file_names(src_dir=op.join(lapack_dir, "BLAS", "SRC"))
  collect_file_names(src_dir=op.join(lapack_dir, "SRC"))
  src_dir = op.dirname(__file__)
  for file_name in ["dlamch.f", "slamch.f", "xerbla.f"]:
    file_names.append(op.join(src_dir, file_name))
  import fable.cout
  fable.cout.process(
    file_names=file_names,
    debug=True)

if (__name__ == "__main__"):
  import sys
  run(args=sys.argv[1:])
