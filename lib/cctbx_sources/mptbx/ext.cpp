#include <boost/python/module.hpp>

#include <mptbx/wrap.hpp>

namespace mptbx {
namespace boost_python {

// -----------------------------------------------------------------------------

void wrap_model();
void wrap_radial_functions();
void wrap_jn();
void wrap_rf_single_zeta();
void wrap_sf4();
void wrap_xd2006();
void wrap_lcs();

void init_module()
{
  wrap_model();
  wrap_radial_functions();
  wrap_jn();
  wrap_rf_single_zeta();
  wrap_sf4();
  wrap_xd2006();
  wrap_lcs();
};

// -----------------------------------------------------------------------------

}} // namespace mptbx::boost_python

BOOST_PYTHON_MODULE(mptbx_ext)
{
  mptbx::boost_python::init_module();
}
