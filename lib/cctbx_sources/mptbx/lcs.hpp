#ifndef MPTBX_LCS_HPP
#define MPTBX_LCS_HPP
//
//                      Local Coordinate Systems
//       calculated from (dummy)atom pairs using XD2006 conventions
//

#include <tbxx/error_utils.hpp>

#include <scitbx/array_family/shared.h>
#include <scitbx/vec3.h>
#include <scitbx/mat3.h>

namespace mptbx {
namespace lcs {

namespace af = scitbx::af;

// -----------------------------------------------------------------------------

typedef enum {X=1, Y=2, Z=3} axis;
typedef enum {plus=1, minus=-1} axis_sign;
typedef enum {R=1, L=-1} orientation;

struct definition
{
  //! indices of ATOM0, ATOM1, ATOM2
  int i0, i1, i2;
  //! AX1 axis assignment (axis without sign)
  axis ax1;
  //! AX1 axis assignment (sign)
  axis_sign ax1_sign;
  //! AX2 axis assignment
  axis ax2;
  orientation rl;

  definition(){};
  definition(int         i0,
             axis        ax1,
             axis_sign   ax1_sign,
             int         i1,
             int         i2,
             axis        ax2,
             orientation rl)
  : i0(i0), ax1(ax1), ax1_sign(ax1_sign), i1(i1), i2(i2), ax2(ax2), rl(rl) {};
};

// -----------------------------------------------------------------------------

template <typename FloatType=double>
struct calculator
{
  typedef FloatType f_t;
  typedef scitbx::vec3<f_t> v_3;

  calculator(){};

  calculator(
    af::const_ref<definition> const &  definitions,
    af::const_ref<v_3> const &         sites_cart,
    af::const_ref<v_3> const &         dummy_sites_cart
    )
  {
    TBXX_ASSERT((definitions.size() == sites_cart.size()));
    size_t n_atoms = sites_cart.size();
    size_t n_dummy = dummy_sites_cart.size();
    size_t n = n_atoms+n_dummy;
    af::shared<v_3> xyz(n);
    lcss_cart = af::shared<v_3>(3*n_atoms);
    for(size_t i=0;i<n_atoms;i++) xyz[i] = sites_cart[i];
    for(size_t i=0;i<n_dummy;i++) xyz[n_atoms+i] = dummy_sites_cart[i];
    for(size_t i=0;i<n_atoms;i++)
    {
      TBXX_ASSERT(((definitions[i].i0>=0)&&(definitions[i].i0<n)));
      TBXX_ASSERT(((definitions[i].i1>=0)&&(definitions[i].i1<n)));
      TBXX_ASSERT(((definitions[i].i2>=0)&&(definitions[i].i2<n)));
      v_3 r = xyz[i];
      v_3 r0 = xyz[definitions[i].i0];
      v_3 r1 = xyz[definitions[i].i1];
      v_3 r2 = xyz[definitions[i].i2];
      v_3 v1 = (r0 - r) * static_cast<f_t>(definitions[i].ax1_sign);
      v_3 v2 = r2 - r1;
      v_3 v3 = v2.cross(v1);

      int iax1 = definitions[i].ax1;
      int iax2 = definitions[i].ax2;
      int iax3 = 6 - iax1 - iax2;

      int perm_sign = 2 * ((2 + iax2 - iax1) % 3) - 1;
      v3 *= static_cast<f_t>(perm_sign);

      v_3 e_ax3 = v3.normalize();
      v_3 e_ax1 = v1.normalize();
      v_3 e_ax2 = e_ax1.cross(e_ax3).normalize();

      e_ax2 *= static_cast<f_t>(perm_sign);
      e_ax3 *= static_cast<f_t>(definitions[i].rl);

      lcss_cart[3*i+iax1-1] = e_ax1;
      lcss_cart[3*i+iax2-1] = e_ax2;
      lcss_cart[3*i+iax3-1] = e_ax3;
    }

  };

  af::shared<v_3> lcss_cart;

};

// -----------------------------------------------------------------------------

} // end lcs namespace
} // end mptbx namespace

#endif // GUARD
