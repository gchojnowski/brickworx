#include <mptbx/wrap.hpp>
#include <mptbx/jn.hpp>

namespace mptbx {
namespace boost_python {

// -----------------------------------------------------------------------------

template <typename FloatType>
struct jn_direct_wrappers
{
  typedef jn_direct<FloatType> w_t;
  static void
  wrap()
  {
    using namespace boost::python;
    class_<w_t, boost::noncopyable>("jn_direct", no_init)
      .def(init<int,FloatType,FloatType>())
      .def("as_packed_l", &w_t::as_packed_l)
      .def("__call__", &w_t::operator())
      ;
  }
};

// -----------------------------------------------------------------------------

template <typename FloatType>
struct jndjn_direct_wrappers
{
  typedef jndjn_direct<FloatType> w_t;
  static void
  wrap()
  {
    using namespace boost::python;
    class_<w_t, bases<jn_direct<FloatType> >,boost::noncopyable>
         ("jndjn_direct", no_init)
      .def(init<int,FloatType,FloatType>())
      .def("djnk_as_packed_l", &w_t::as_packed_l)
      .def("__call__", &w_t::operator())
      ;
  }
};

// -----------------------------------------------------------------------------

void wrap_jn()
{
  jn_direct_wrappers<MPTBX_FLOAT_TYPE>::wrap();
  jndjn_direct_wrappers<MPTBX_FLOAT_TYPE>::wrap();
};

// -----------------------------------------------------------------------------

}} // namespace mptbx::boost_python
