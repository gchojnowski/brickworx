#ifndef MPTBX_MODEL_HPP
#define MPTBX_MODEL_HPP

#include <tbxx/error_utils.hpp>

#include <scitbx/array_family/shared.h>
#include <scitbx/mat3.h>

namespace mptbx {

namespace af = scitbx::af;

// -----------------------------------------------------------------------------

//  More elegant C++0x version requires gcc 4.7
//template <int MAXL>
//using populations = af::tiny<double, 1+(MAXL+1)*(MAXL+1)>;

// Valence, 1xM, 3xD, 5xQ, 7xO, 9xH
template <int MAXL, typename FloatType>
class populations : public af::tiny<FloatType, 1+(MAXL+1)*(MAXL+1)>
{};

// -----------------------------------------------------------------------------

template <int MAXL=4, typename FloatType=double>
struct multipole_scatterer
{
  typedef FloatType float_type;

  //! atomic multiplicity
  FloatType                    amult;
  //! maximum l (spherical harmonic degree) for a given atom
  int                          lmax;
  //! array of populations, first element is a valence population
  populations<MAXL, FloatType> pop;
  //! local coordinate system
  scitbx::mat3<FloatType>      lcs;
  //! index into table of kappa sets (also implies radial function set)
  int                          kappa_idx;

  multipole_scatterer(){};
  multipole_scatterer(FloatType                   amult,
                      int                         lmax,
                      populations<MAXL,FloatType> pop,
                      int                         kappa_idx)
    : amult(amult), lmax(lmax), pop(pop), kappa_idx(kappa_idx),
      lcs(0.0, 0.0, 0.0) {};
};

// -----------------------------------------------------------------------------

// kappa values for Valence, M, D, Q, O, H
template <int MAXL, typename FloatType>
class kappa_set : public af::tiny<FloatType,MAXL+2>
{
public:
  static const int n_kappas = MAXL+2;
  static const int n_kappas_deformation = MAXL+1;
  typedef af::tiny<FloatType,n_kappas> tiny_type;
  typedef af::tiny<FloatType,n_kappas_deformation> tiny_type_deformation;

  kappa_set() {};

  kappa_set(tiny_type a)
  : tiny_type(a) {};

  FloatType get_valence_kappa()
  {
    return tiny_type::elems[0];
  };

  void set_valence_kappa(FloatType kappa)
  {
    tiny_type::elems[0] = kappa;
  };

  tiny_type_deformation get_deformation_kappas()
  {
    tiny_type_deformation kappas;
    for(int i=0; i<n_kappas_deformation; i++)
      kappas.elems[i] = tiny_type::elems[i+1];
    return kappas;
  };

  void set_deformation_kappas(tiny_type_deformation kappas)
  {
    for(std::size_t i=0; i<n_kappas_deformation; i++)
      tiny_type::elems[i+1] = kappas.elems[i];
  };

};

// -----------------------------------------------------------------------------

// Core, Valence, M, D, Q, O, H
// There will be as many rf_values_sets as kappa_sets!
template <int MAXL, typename FloatType>
struct rf_values_set : public af::tiny<FloatType, MAXL+3>
{
  typedef af::tiny<FloatType, MAXL+3> tiny_type;

  rf_values_set() {};

  rf_values_set(tiny_type a)
  : tiny_type(a) {};

};

// -----------------------------------------------------------------------------

} // end mptbx namespace

#endif // GUARD
