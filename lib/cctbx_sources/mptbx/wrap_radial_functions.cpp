#include <mptbx/wrap.hpp>
#include <mptbx/radial_functions.hpp>

namespace mptbx {
namespace boost_python {

// -----------------------------------------------------------------------------

template <typename FloatType>
struct radial_function_wrappers
{
  typedef radial_function<FloatType> w_t;
  static void
  wrap()
  {
    using namespace boost::python;
    class_<w_t, boost::noncopyable>("radial_function", no_init)
    ;
  }
};

// -----------------------------------------------------------------------------

template <typename FloatType>
struct radial_function_zero_wrappers
{
  typedef radial_function_zero<FloatType> w_t;
  static void
  wrap()
  {
    using namespace boost::python;
    class_<w_t, bases<radial_function<FloatType> >, boost::noncopyable>(
         "radial_function_zero", no_init)
      .def(init<>())
      .def("__call__", &w_t::operator())
      ;
  }
};

// -----------------------------------------------------------------------------

template <typename FloatType>
struct radial_function_tabulated_wrappers
{
  typedef radial_function_tabulated<FloatType> w_t;
  static void
  wrap()
  {
    using namespace boost::python;
    class_<w_t, bases<radial_function<FloatType> >, boost::noncopyable>(
         "radial_function_tabulated", no_init)
      .def(init<af::shared<FloatType>, FloatType, int>())
      .def("__call__", &w_t::operator())
      ;
  }
};

// -----------------------------------------------------------------------------

template <int MAXL, typename FloatType>
struct rf_calculator_wrappers
{
  typedef rf_calculator<MAXL,FloatType> w_t;
  static void
  wrap()
  {
    using namespace boost::python;
    class_<w_t, boost::noncopyable>(
         add_maxl_suffix("rf_calculator",MAXL), no_init)
      .def(init<object,
               af::shared< kappa_set<MAXL,FloatType> >,
               af::shared<int> > ())
      .def("calculate", &w_t::calculate)
      ;
  }
};

// -----------------------------------------------------------------------------

void wrap_radial_functions()
{
  radial_function_wrappers<MPTBX_FLOAT_TYPE>::wrap();
  radial_function_zero_wrappers<MPTBX_FLOAT_TYPE>::wrap();
  radial_function_tabulated_wrappers<MPTBX_FLOAT_TYPE>::wrap();
  rf_calculator_wrappers<MPTBX_MAXL,MPTBX_FLOAT_TYPE>::wrap();
};

// -----------------------------------------------------------------------------

}} // namespace mptbx::boost_python
