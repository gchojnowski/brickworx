from __future__ import division

from cctbx import miller
import mmtbx.f_model
import mptbx

# ------------------------------------------------------------------------------

class multipolar_model_4:
  
  def __init__(self, 
               structure,
               multipole_scatterers,
               rf_calculator,
               lcs_manager):
    self.multipole_scatterers = multipole_scatterers
    self.update_structure(structure)
    self.rf_calculator = rf_calculator
    self.lcs_manager = lcs_manager
    self.sf4 = None

  def compute_structure_factors(self, miller_array):
    assert not miller_array is None
    sf4 = mptbx.sf4(
                    self.structure.unit_cell(),
                    self.structure.space_group(),
                    self.structure.scatterers(),
                    self.multipole_scatterers,
                    self.rf_calculator,
                    miller_array.indices())
    self.sf4 = sf4
    self.as_ma = lambda data: miller.array(miller_array, data=data)
    return sf4

  def f_mpl(self):
    assert self.sf4
    return self.as_ma(self.sf4.f_mpl())

  def f_sph(self):
    assert self.sf4
    return self.as_ma(self.sf4.f_sph())

  def notify_sites_updated(self):
    self.sf4 = None
    self.lcs_manager.calculate(self.structure.scatterers())
    self.lcs_manager.apply_to(self.multipole_scatterers)
       
  def update_structure(self,structure):
    assert structure.scatterers().size() == self.multipole_scatterers.size()
    self.structure = structure
    self.sf4 = None

  def compute_packed_gradients(self, miller_array, d_target_d_f_calc):
    gradients = mptbx.sf4_gradients(
                                     self.structure.unit_cell(), 
                                     self.structure.space_group(),
                                     self.structure.scatterers(), 
                                     self.multipole_scatterers,
                                     self.rf_calculator,
                                     miller_array.indices(),
                                     d_target_d_f_calc
                                   )
    return gradients.packed()
    
# ------------------------------------------------------------------------------
  