#include <mptbx/wrap.hpp>
#include <mptbx/model.hpp>
#include <mptbx/xd2006.hpp>

namespace mptbx {
namespace boost_python {

// -----------------------------------------------------------------------------

// Wrapping xd2006 subroutines (assuming LMAX = 4)

template <typename FloatType>
af::tiny<FloatType,34> prod_wrapper(af::tiny<FloatType,3> const h,
                                    int const                   lmax)
{
  TBXX_ASSERT(((lmax >= 0) && (lmax <= 4)));
  af::tiny<FloatType,34> r;
  for(short i=0; i < 3; i++) r.elems[i] = h.elems[i];
  for(short i=3; i < 34; i++) r.elems[i] = 0.0;
  mptbx::xd2006::prod(r.begin(), lmax);
  return r;
};

template <typename FloatType>
af::tiny<FloatType,24> angular_wrapper(af::tiny<FloatType,3> const   h,
                                       scitbx::mat3<FloatType> const lcs,
                                       int const                     lmax)
{
  TBXX_ASSERT(((lmax >= 0) && (lmax <= 4)));
  af::tiny<FloatType,34> r;
  af::tiny<FloatType,24> ang;
  for(short i=0; i < 24; i++) ang.elems[i] = 0.0;
  mptbx::xd2006::angular(h.begin(), lcs.begin(), lmax, r.begin(), ang.begin());
  return ang;
};

template <typename FloatType>
af::tiny<FloatType,3> mulpol_wrapper(rf_values_set<4, FloatType> const rfs,
                                     populations<4, FloatType> const   pop,
                                     af::tiny<FloatType,24> const      ang,
                                     int     const                     lmax)
{
  TBXX_ASSERT(((lmax >= 0) && (lmax <= 4)));
  FloatType a, b, sph;
  mptbx::xd2006::mulpol(rfs.begin(), pop.begin(), ang.begin(), lmax, a, b, sph);
  return af::tiny<FloatType,3>(a,b,sph);
};

// -----------------------------------------------------------------------------

void wrap_xd2006()
{
  using namespace boost::python;
  conditional_tt_wrap< af::tiny<MPTBX_FLOAT_TYPE,24> >();
  conditional_tt_wrap< af::tiny<MPTBX_FLOAT_TYPE,34> >();
  def("xd2006_prod", prod_wrapper<MPTBX_FLOAT_TYPE>,
      ("h", arg("lmax")=4),
      "calculates coordinate products");
  def("xd2006_angular", angular_wrapper<MPTBX_FLOAT_TYPE>,
      ("h", "lcs", arg("lmax")=4),
      "calculates spherical harmonics");
  def("xd2006_mulpol", mulpol_wrapper<MPTBX_FLOAT_TYPE>,
      ("rfs", "pop", "ang", arg("lmax")=4),
      "calculates angular contribution from values of radial functions, "
      "populations and harmonics");
};

// -----------------------------------------------------------------------------

}} // namespace mptbx::boost_python

