#!/usr/bin/env python

from __future__ import division

import re

import read_mas

# ------------------------------------------------------------------------------

pat_elements = ' Element -(.*?)\n' + '-' * 80
pat_element = '''(.*?)df' =(.*?)df'' =(.*?)$
 CORE(.*?)$
 CONF(.*?)$
(.*?)$
 SPHV(.*?)Valence electrons :(.*?)$
 CONF(.*?)$
(.*?)$
( DEFV .*)'''

pat_input = \
"""-{34}START INPUT-{34}
(.*?)
-{34} END INPUT -{34}"""

# ------------------------------------------------------------------------------

def process_element(t):
    d = {}
    t =  re.match(pat_element, t, re.DOTALL | re.MULTILINE).groups()
    e, dfp, dfpp, c, cc, ct, s, sve, sc, st, D = t
    e, d['dfp'], d['dfpp'] = e.strip(), float(dfp), float(dfpp)
    d['CORE'] = c.strip()
    d['CORE_CONFIG'] = cc.strip()
    d['CORE_TABLE'] = map(float, ct.split())
    d['SPHV'] = s.strip()
    d['SPHV_CONFIG'] = sc.strip()
    d['SPHV_VALENCE_ELECTRONS'] = int(sve.strip())
    d['SPHV_TABLE'] = map(float, st.split())
    L = []
    for l in D.split('\n'):
        if l.startswith(' DEFV'):
            L.append((l, []))
        else:
            L[-1][1].append(l)
    L = [(t[0], ' '.join(t[1]).replace('\n', ' ')) for t in L]
    d['DEFVs'] = [(t[0], map(float, t[1].split())) for t in L]
    return e, d

# ------------------------------------------------------------------------------

def read_xdlsm_out_elements(filename):
    txt = file(filename).read().replace('\s','')
    l = re.findall(pat_elements, txt, re.DOTALL)
    DATA = []
    for t in l:
        e, d = process_element(t)
        DATA.append((e,d))
    return DATA

# ------------------------------------------------------------------------------

def read_xdlsm_out_input(filename, **kw):
    txt = file(filename).read().replace('\s','')
    l = re.findall(pat_input, txt, re.DOTALL)[0]
    txt = '\n'.join([t[1:] for t in l.split('\n')])
    d_mas = read_mas.parse_mas(txt, **kw)
    return d_mas

# ------------------------------------------------------------------------------

if __name__ == '__main__':
    import os, pprint, sys
    pp = pprint.PrettyPrinter(indent=2, width=200)

    if len(sys.argv) > 1:
        filename = sys.argv[1]
    else:
        filename = os.path.join(os.path.split(__file__)[0],
                                'sample_data', 'xd_lsm.out__test')

    print 'DATA = \\'
    d = read_xdlsm_out_elements(filename)
    pp.pprint(d)

    print 'd_mas = \\'
    d_mas = read_xdlsm_out_input(filename)
    pp.pprint(d_mas)
