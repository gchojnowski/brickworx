#!/usr/bin/env python

from __future__ import division

# for XD implementation see: xdlsm/rdwbn.f

# ------------------------------------------------------------------------------

try:
  from libtbx.utils import Sorry
except ImportError:
  class Sorry(ValueError):
    pass

# ------------------------------------------------------------------------------

orbt = ('1S','2S','3S','4S', '2P','3P','4P', '3D','4D',
        '4F','5S','5P','6S','6P','5D','7S','6D','5F')

# ------------------------------------------------------------------------------

def read_bnk(fname):
    ll = file(fname).readlines()
    COMMENT = []
    DATA = {}
    L = []
    for i, l in enumerate(ll):
        if l.lstrip().startswith('!'):
            COMMENT.append(l)
        elif l.lstrip().startswith(':'):
            L.append([l])
        elif len(l.strip()):
            L[-1].append(l)
    for ll in L:
        d = {}
        # 1.
        l = ll[0].split()
        ELEM = l[0][1:]
        d['Z'] = int(l[1])
        d['W'] = float(l[2])
        d['DFPMo'] = float(l[3])
        d['DDFPMo'] = float(l[4])
        d['DFPCu'] = float(l[5])
        d['DDFPCu'] = float(l[6])
        d['SCTL'] = float(l[7])
        d['IRA'] = float(l[8])
        d['IRC'] = float(l[9])
        d['IRN'] = float(l[10])
        d['sctl'] = d['SCTL'] / 10.0
        d['rcv'] = d['IRC'] / 100.0
        # 2.
        l = map(float, ll[1].split()[1:])
        a = tuple(l[0:-1:2])
        b = tuple(l[1:-1:2])
        c = l[-1]
        d['SPH'] = {'a': a, 'b': b,  'c': c}
        # 3.
        xl = 0
        if '=' in ll[2]:
            l = '%s %s' % (ll[2].rstrip()[:-1], ll[3])
            xl += 1
        else:
            l = ll[2]
        d['SZ'] = tuple(map(float, l.split()[1:]))
        # 4.
        if '=' in ll[3+xl]:
            l = '%s %s' % (ll[3+xl].rstrip()[:-1], ll[4+xl])
            xl += 1
        else:
            l = ll[3+xl]
        l = l.split()[1:]
        ORB = l[0::3]
        IOC = map(int, l[1::3])
        NBF = map(int, l[2::3])
        STO = []
        # 5.
        l = reduce(lambda a,b: a+b, [l.split() for l in ll[4+xl:]])
        I = 0
        for orb, ioc, nbf in zip(ORB, IOC, NBF):
            d_STO = {'ORB': orb, 'IOC': ioc, 'NBF': nbf}
            d_STO['BC'] = tuple(map(float, l[I+0:I+3*nbf:3]))
            d_STO['BX'] = tuple(map(float, l[I+1:I+3*nbf:3]))
            d_STO['NR'] = tuple(map(int,   l[I+2:I+3*nbf:3]))
            I += 3*nbf
            STO.append(d_STO)
        if not I == len(l):
            raise Sorry('read_bnk(): Unexpected inconsistency in the bank file.')
        d['STO'] = STO
        DATA[ELEM] = d

    return COMMENT, DATA

# ------------------------------------------------------------------------------

if __name__ == '__main__':
    import os, pprint, sys
    pp = pprint.PrettyPrinter(indent=4, width=120)

    if len(sys.argv) > 1:
        filename = sys.argv[1]
    else:
        filename = os.path.join(os.path.split(__file__)[0],
                                'sample_data', 'xd.bnk_RHF_CR')

    c, d = read_bnk(filename)

    pp.pprint(d)

