#!/usr/bin/env python

from __future__ import division

# ------------------------------------------------------------------------------

key6 = ('h', 'k', 'l', 'iscgrp', 'obs', 'sigobs')
keyx = ('tbar', 'u1', 'u2', 'u3', 'v1', 'v2', 'v3')
key7 = ('phase-angle')

def read_hkl(filename):
    ll = file(filename).readlines()
    cid, fcod, NDAT, ndat = ll[0].split()
    ndat = int(ndat)
    d = {'cid': cid, 'fcod': fcod, 'ndat': ndat}
    if ndat == -7:
        key = key6 + key7
    else:
        key = key6 + keyx
        key = key[:ndat]
    for k in key:
        d[k] = []
    for l in ll[1:]:
        t = l.split()
        t = map(int, t[:4]) + map(float, t[4:])
        for i,k in enumerate(key):
            d[k].append(t[i])
    return d

# ------------------------------------------------------------------------------

if __name__ == '__main__':
    import os, pprint, sys
    pp = pprint.PrettyPrinter(indent=4, width=200)

    if len(sys.argv) > 1:
        filename = sys.argv[1]
    else:
        filename = os.path.join(os.path.split(__file__)[0],
                                'sample_data', 'xd.hkl')

    d = read_hkl(filename)
    pp.pprint(d)
