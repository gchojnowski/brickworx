#!/usr/bin/env python

from __future__ import division

from struct import unpack

# ------------------------------------------------------------------------------

def read_fou(filename, fortran_record_size=4, verbose=False):
    """
    This routine was written by nearly random tweaking of the code.
    It should not work. In case it does, it can read xd.fou files.
    (If XD was compiled with gfortran and was run on a PC and
     and compiler -frecord-marker= option matches fortran_record_size
     and [...] )
    """
    f = file(filename, mode="rb")
    f.read(fortran_record_size)
    HKLs = []
    FOBS, SIG, PHASE, AMOD1, BMOD1, AMOD2, BMOD2 = [], [], [], [], [], [], []
    while 1:
        h, k, l = unpack('iii', f.read(3 * 4))
        fobs, sig, phase, amod1, bmod1, amod2, bmod2 = unpack('fffffff', f.read(7 * 4))
        f.read(fortran_record_size)
        if verbose:
            print '%4d %4d %4d   %12.6f %12.6f   %12.6f   %12.6f %12.6f   %12.6f %12.6f' % \
                (h, k, l, fobs, sig, phase, amod1, bmod1, amod2, bmod2)
        HKLs.append((h,k,l))
        map(lambda l,v: l.append(v),
            (FOBS, SIG, PHASE, AMOD1, BMOD1, AMOD2, BMOD2),
            (fobs, sig, phase, amod1, bmod1, amod2, bmod2))
        if h == 0 and k == 0 and l == 0:
            break
        f.read(fortran_record_size)
    return HKLs, FOBS, SIG, PHASE, AMOD1, BMOD1, AMOD2, BMOD2

# ------------------------------------------------------------------------------

if __name__ == '__main__':
    import os, optparse
    usage = "usage: %prog [options] [xd.fou_file]"
    parser = optparse.OptionParser(usage=usage)
    parser.add_option("-r", "--record-size", type="int", dest="r", default=8,
                  help="Fortran record size (look for -frecord-marker= in gfortran manpage...).")
    (options, args) = parser.parse_args()
    if len(args) == 2:
        filename = os.path.join(os.path.split(__file__)[0], 'sample_data', 'xd.fou')
    else:
        filename = args[0]

    d = read_fou(filename, verbose=True, fortran_record_size=options.r)

