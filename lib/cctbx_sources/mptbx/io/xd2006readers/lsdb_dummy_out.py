#!/usr/bin/env python

from __future__ import division

try:
  from libtbx.utils import Sorry
except ImportError:
  class Sorry(ValueError):
    pass

# ------------------------------------------------------------------------------

_header = 'DUMMY No  Av   at1      at2      at3      at4      at5      at6'

def read_lsdb_dummy_out(f_name):
  ll = file(f_name).readlines()
  if not ll[0].strip() == _header:
    raise Sorry('Unsupported file.')
  ll = [l.split() for l in ll[1:]]
  d = {}
  for l in ll:
    assert l[0] == 'DUM'
    n_o = int(l[1])
    if n_o == 0: 
      continue
    n = int(l[2])
    atoms = l[3:]
    assert len(atoms) == n
    d['%s%d' % ('DUM',n_o)] = atoms
  return d 
  

# ------------------------------------------------------------------------------

if __name__ == '__main__':
    import os, pprint, sys
    pp = pprint.PrettyPrinter(indent=4, width=120)

    if len(sys.argv) > 1:
        filename = sys.argv[1]
    else:
        filename = os.path.join(os.path.split(__file__)[0],
                                'sample_data', 'dummy.out')

    d = read_lsdb_dummy_out(filename)
    
    pp.pprint(d)

