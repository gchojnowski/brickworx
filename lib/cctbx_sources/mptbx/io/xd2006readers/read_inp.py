#!/usr/bin/env python

from __future__ import division

import re, sys

# ------------------------------------------------------------------------------

try:
  from libtbx.utils import Sorry
except ImportError:
  class Sorry(ValueError):
    pass

# ------------------------------------------------------------------------------

# software limits:
limits = ['nat', 'ntx', 'lmx', 'nzz', 'nto', 'nsc', 'ntb', 'nov']

# dimensions of arrays:
usage = [
    'na',      # number of atoms
    'ntmx',    # number of displacement tensor components
    'npolmax', # maximum level of multipole expansion
    'nz',      # number of kappa sets
    'nto',     # not used
    'nq',      # scale factors
    'next',    # extinction model
    'ncon',    # number of constraints
    'ntbl',    # number of scattering factor tables
    'ns',      # number of symmetry cards
    'nv',      # number of variables
    'nqq',     #
    'nc',      # number of cycles
    'nad'      # number of dummy atoms
]

statistics = ['r1o', 'r2o', 'r1', 'r2', 'r1w', 'r2w', 'gof', 'sig']
parameters = ['a', 'b', 'c', 'd', 'e', 'f']

atomline = [
        'atom',         # atom name (character*8)
        'icor1',        # code integers for defining the site coordinate system
        'icor2',        #
        'nax',          #
        'nay1',         #
        'nay2',         #
        'jtf',          # the order of displacement tensor
        'itbl',         # scattering factor number
        'isfz',         # kappa set
        'lmax',         # max. level of spherical harmonics used
        'isym',         # site symmetry code
        'ichcon',       # chemical constraint
        'x', 'y', 'z',  # Coordinates
        'amult'         # multiplicity
]

# ------------------------------------------------------------------------------

pat_0 = 'XDPARFILEVERSION(.*)'
pat_1 = '   (.{8}) MODEL (.{3})(.{3})(.{3})(.{3})'
pat_2 = 'LIMITS nat (.{5}) ntx (.{3}) lmx (.{2}) nzz (.{3}) nto (.{2}) nsc (.{3}) ntb (.{3}) nov (.{6})'
#pat_2 = 'LIMITS nat (.{*?}) ntx (.{*?}) lmx (.{*?}) nzz (.{*?}) nto (.{*?}) nsc (.{*?}) ntb (.{*?}) nov (.{*})'
pat_3 = 'USAGE (.{6})(.{4})(.{3})(.{4})(.{4})(.{4})(.{4})(.{4})(.{4})(.{4})(.{6})(.{4})(.{4})(.{3})'
pat_4 = '(.{10})' * 7 + '(.{10})' * 1
pat_5 = '(.{10})' * 6
pat_d = '(.{15})' * 3
pat_a = '(.{8}) (.{2})(.{2})(.{5})(.{4})(.{4})(.{2})(.{3})(.{3})(.{2})(.{3})(.{4})(.{10})(.{10})(.{10})(.{7})'
pat_u = '(.{10})' * 6

"""
based on parmio.f:

 1020 Format(3X, A8, ' MODEL ', 4I3)
 1030 Format('LIMITS nat ', I5,  ' ntx ', I3, ' lmx ', I2,
     @     ' nzz ', I3, ' nto ', I2, ' nsc ', I3, ' ntb ', I3,
     @     ' nov ', I6)
 1035 Format('USAGE ',
     @     I6,  I4,   I3,  I4,   I4,  I4,  I4,   I4,   I4,  I4,
 1040 Format(7F10.6, E10.3)
 1050 Format(6F10.6)
 1001 Format(3F15.6)
      Write(lp,1001) atom, (kp(j),j=1,11), coord, amul
 1001 Format(A8,1X,2I2,I5,2I4,I2,2I3,I2,I3,I4,3F10.6,F7.4)

      Write(lp,1002) (utens(j),j=1,6)
 1002 Format(6F10.6)
      If (nt .Ge. 3) Then
         Write(lp,1003) (utens(j),j=7,16)
 1003    Format(5F15.6)
         If (nt .Ge. 4) Then
            Write(lp,1003) (utens(j),j=17,31)

"""

# ------------------------------------------------------------------------------

def read_inp(filename, log=None, silent=False):
    ll = [l.replace('\r','') for l in file(filename)
                             if not l.rstrip().startswith('!')]
    d = {}
    #
    msg_fallback = ''
    #
    if re.search(pat_0, ll[0].replace(' ','')).groups()[0] != '2':
        print ll[0], re.search(pat_0, ll[0].replace(' ','')).groups()
        raise Sorry('read_inp: Only XDPARFILE VERSION  2 files are supported')
    #
    try:
        _MODEL = re.match(pat_1, ll[1]).groups()
        d['cid'] = _MODEL[0].strip()
        d['MODEL'] = map(int, _MODEL[1:])
    except AttributeError:
        msg_fallback += 'read_inp: MODEL line inconsistent with Fortran ' \
                        'format used by XD 5.36. Trying fallback...\n'
        d['cid'] = ll[1].split()[0]
        d['MODEL'] = map(int, ll[1].split()[2:])
    #
    try:
        _LIMITS = map(int, re.match(pat_2, ll[2]).groups())
        d['LIMITS'] = dict(zip(limits, _LIMITS))
    except AttributeError:
        msg_fallback += 'read_inp: LIMITS line inconsistent with Fortran ' \
                        'format used by XD 5.36. Trying fallback...\n'
        l = ll[2].split()[1:]
        d['LIMITS'] = dict(zip(l[0::2], l[1::2]))
    #
    try:
        _USAGE = map(int, re.match(pat_3, ll[3]).groups())
        d['USAGE'] = dict(zip(usage, _USAGE))
    except AttributeError:
        msg_fallback += 'read_inp: USAGE line inconsistent with Fortran ' \
                        'format used by XD5.36. Trying fallback...\n'
        d['USAGE'] = dict(zip(usage, map(int, ll[3].split()[1:])))
    #
    _statistics = map(float, re.match(pat_4, ll[4]).groups())
    d['statistics'] = dict(zip(statistics,  _statistics))
    #
    _parameters = map(float, re.match(pat_5, ll[5]).groups())
    d['parameters'] = dict(zip(parameters, _parameters))
    #
    I = 6
    dummy_atoms = []
    for i in range(d['USAGE']['nad']):
        try:
            _xyz = tuple(map(float, re.match(pat_d, ll[I]).groups()))
        except AttributeError:
            msg_fallback += 'read_inp: dummy atom line inconsistent with ' \
                            'Fortran format used by XD 5.36. ' \
                            'Trying fallback... (%s)\n' % ll[I].rstrip()
            _xyz = tuple(map(float, ll[I].split()))
        dummy_atoms.append(_xyz)
        I += 1
    d['dummy_atoms'] = dummy_atoms
    #
    atoms = []
    for n in range(d['USAGE']['na']):
        try:
            _atom = re.match(pat_a, ll[I]).groups()
            _atom = [_atom[0]] + map(int, _atom[1:-4])  + map(float, _atom[-4:])
        except: # intentional
            msg_fallback += 'read_inp: atom line inconsistent with Fortran ' \
                             'format used by XD 5.36. ' \
                             'Trying fallback... (%s)\n' % ll[I].rstrip()
            _atom = ll[I].split()
            _atom = [_atom[0]] + map(int, _atom[1:-4])  + map(float, _atom[-4:])
        atom = dict(zip(atomline[0:-4] + atomline[-1:], _atom[0:-4] + _atom[-1:]))
        atom['xyz'] = tuple(_atom[-4:-1])
        I += 1
        atom['U'] = tuple(map(float, re.match(pat_u, ll[I]).groups()))
        I += 1
        if atom['jtf'] >= 3:
            raise Sorry('Can not read C_{ijk} yet...')
            I += 2
        if atom['jtf'] >= 4:
            raise Sorry('Can not read D_{ijkl} yet...')
            I += 3
        lmax = d['MODEL'][0]
        npx = lmax * lmax + 2 * lmax + 2
        _n_p_lines = npx // 10
        _n_p_atend = npx - _n_p_lines * 10
        _p = []
        for i in range(_n_p_lines):
            _p += map(float, re.match('(.{8})' * 10, ll[I]).groups())
            I += 1
        if _n_p_atend > 0:
            _p += map(float, re.match('(.{8})' * _n_p_atend, ll[I]).groups())
            I += 1
        atom['pv'] = _p[0]
        atom['pop'] = tuple(_p[1:])
        atoms.append(atom)
    d['atoms'] = atoms
    if not silent and msg_fallback:
      print >> log, msg_fallback
    #
    lmax = d['MODEL'][0]
    kappas = []
    for i in range(d['USAGE']['nz']):
        _t = ll[I].split()
        kappas.append((int(_t[0]), tuple(map(float, _t[1:]))))
        I += 1
    d['kappas'] = kappas
    #
    d['extcn'] = tuple(map(float, ll[I].split()))
    d['out'] = float(ll[I+1])
    d['sc'] = tuple(map(float, ll[I+2].split()))
    return d

# ------------------------------------------------------------------------------

if __name__ == '__main__':
    import os, pprint, sys
    pp = pprint.PrettyPrinter(indent=4, width=250)

    if len(sys.argv) > 1:
        filename = sys.argv[1]
    else:
        filename = os.path.join(os.path.split(__file__)[0],
                                'sample_data', 'xd.inp')

    d = read_inp(filename)
    pp.pprint(d)
