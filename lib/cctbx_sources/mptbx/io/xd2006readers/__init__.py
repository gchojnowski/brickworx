from __future__ import division

__doc__ = \
"""Functions contained in submodules provide a low level readers for files
used by XD2006 as of version 5.36. They will return Python data structures
that will closey mirror content of the original files.

Each reader module is also an executable script.
"""
