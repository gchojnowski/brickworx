#!/usr/bin/env python

from __future__ import division

# ------------------------------------------------------------------------------

atom_line = 'ATOM     ATOM0    AX1 ATOM1    ATOM2   AX2 R/L TP TBL KAP LMX SITESYM  CHEMCON'
scat_line = "SCAT CORE SPHV DEFV   1S  2S  3S  4S  2P  3P  4P  3D  4D  4F  5S  5P  6S  6P  5D  7S  6D  5F  DELF'   DELF''  NSCT"
atom_line_l = atom_line.split()
scat_line_l = scat_line.split()
atom_line = atom_line.replace(' ', '')
scat_line = scat_line.replace(' ', '')

# ------------------------------------------------------------------------------

def parse_mas(txt, ignore_dum0=False):
    ll = txt.split('\n')
    d = {}
    LATT = None
    SYMM = []
    SCAT = []
    in_SCAT = False
    ATOM = []
    DUMM = []
    in_ATOM = False
    #
    for l in ll:
        lru = l.rstrip().upper()
        if lru.startswith('!'):
            continue
        elif lru.startswith('LATT'):
            LATT = tuple(l.split()[1:])
        elif lru.startswith('SYMM'):
            sl = lru[4:].split(';')
            for s in sl:
                SYMM.append(s)
        elif lru.startswith('BANK'):
            d['BANK'] = l.split()[1]
        elif lru.startswith('CELL '):
            d['CELL'] = tuple(map(float, l.split()[1:]))
        elif lru.startswith('CELLSD'):
            d['CELLSD'] = tuple(map(float, l.split()[1:]))
        elif lru.startswith('WAVE'):
            d['WAVE'] = float(l.split()[1])
        # -- SCAT --
        elif lru.replace(' ', '').startswith(scat_line):
            in_SCAT = True
        elif lru.replace(' ', '').startswith('ENDSCAT'):
            in_SCAT = False
        elif in_SCAT:
            ds = dict(zip(scat_line_l, l.split()))
            for k in "DELF'", "DELF''", "NSCT":
                ds[k] =  float(ds[k])
            SCAT.append(ds)
        # -- ATOM --
        elif lru.replace(' ', '').startswith(atom_line):
            in_ATOM = True
        elif lru.replace(' ', '').startswith('ENDATOM'):
            in_ATOM = False
        elif in_ATOM:
            if lru.startswith('DUM'):
                t = l.split()
                t = (t[0], float(t[1]), float(t[2]), float(t[3]))
                if ignore_dum0 and t[1:] == (0.0, 0.0, 0.0):
                    continue
                DUMM.append(t)
            else:
                da = dict(zip(atom_line_l, l.split()))
                for k in ('TP', 'TBL', 'KAP', 'LMX'):
                    da[k] = int(da[k])
                ATOM.append(da)
        #
        else:
            pass
    #
    if LATT is None:
        LATT = ('A', 'P')
    d['LATT'] = LATT
    d['SYMM'] = SYMM
    d['SCAT'] = SCAT
    d['ATOM'] = ATOM
    d['DUMM'] = DUMM
    return d

# ------------------------------------------------------------------------------

def read_mas(fname, **kw):
    return parse_mas(file(fname).read(), **kw)

# ------------------------------------------------------------------------------

if __name__ == '__main__':
    import os, pprint, sys
    pp = pprint.PrettyPrinter(indent=4, width=350)

    if len(sys.argv) > 1:
        filename = sys.argv[1]
    else:
        filename = os.path.join(os.path.split(__file__)[0],
                                'sample_data', 'xd.mas')

    d = read_mas(filename)
    pp.pprint(d)

