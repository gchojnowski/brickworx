from __future__ import division

import sys

import cctbx
from cctbx.array_family import flex
from cctbx import crystal, sgtbx, uctbx, miller, xray
from cctbx.adptbx import u_cif_as_u_iso, u_iso_as_u_star, u_cif_as_u_star
from libtbx.utils import Sorry

import xd2006readers.read_mas
import xd2006readers.read_inp
import xd2006readers.read_hkl
import xd2006readers.read_bnk
import xd2006readers.read_xdlsm_out_test
import mptbx
import mptbx.data.banks
import mptbx.data.tables
from mptbx.lcs_manager import lcs_manager
from mptbx.command_line.collect_rf_tables import collect_rf_tables

# ------------------------------------------------------------------------------

__doc__ = \
""" Functions in this module process "raw" data (usually dictionaries) produced
by reader functions from xd2006readers module and build cctbx and mptbx objects
using distributed or user supplied parametrization data banks.
"""

# ------------------------------------------------------------------------------

def get_rf_key(bank, scat):
  """Produces a key string of an atom configuration defined by a single
  SCAT line from mas file. Argument 'bank' can be a bank code or None."""
  key1 = [scat[k].upper() for k in 'SCAT CORE SPHV DEFV'.split()]
  l = '1S  2S  3S  4S  2P  3P  4P  3D  4D  4F  5S  5P  6S  6P  5D  7S  6D  5F'
  key2 = [scat[k] for k in l.split()]
  key = key1 + key2
  if not bank is None:
    key.append(bank.upper())
  return '_'.join(key).replace('-','m')

def get_tables(fname, skip_bank=False):
  """Based on an output (xd_lsm.out) of xdlsm program run with *test option
  enabled produces a dictionary of tabulated radial functions."""
  DATA = xd2006readers.read_xdlsm_out_test.read_xdlsm_out_elements(fname)
  d_mas = xd2006readers.read_xdlsm_out_test.read_xdlsm_out_input(fname)
  tables = {}
  for scat, (e,d) in zip(d_mas['SCAT'], DATA):
    if e.upper() != scat['SCAT']:
      raise Sorry('get_tables(): Inconsistent data.')
    if skip_bank:
      bank = None
    else:
      bank = d_mas['BANK']
    rf_key = get_rf_key(bank, scat)
    tables[rf_key] = d
  return tables

# ------------------------------------------------------------------------------

def get_crystall_symmetry(d_mas):
  """Builds cctbx.crystal.symmetry from dictionary returned by
  xd2006readers.read_mas.read_mas function."""
  SYMM, LATT = d_mas['SYMM'], d_mas['LATT']
  CELL = d_mas['CELL']
  space_group = sgtbx.space_group()
  if LATT[0].upper() == 'C':
    space_group.expand_inv(sgtbx.tr_vec((0, 0, 0)))
  space_group.expand_conventional_centring_type(LATT[1])
  for symm_string in SYMM:
    space_group.expand_smx(symm_string)
  unit_cell = uctbx.unit_cell(CELL)
  crystal_symmetry = crystal.symmetry(unit_cell=unit_cell,
                       space_group=space_group)
  return crystal_symmetry

# ------------------------------------------------------------------------------

def get_miller_array(crystal_symmetry, d_hkl):
  """Builds cctbx.miller.array from cctbx.crystall.symmetry object and
  dictionary returned by xd2006readers.read_hkl.read_hkl function."""
  # as_flex_int = lambda a: flex.int(a.astype('int32'))  <-- numpy version
  as_flex_int = lambda a: flex.int(a)
  h,k,l = map(as_flex_int, (d_hkl['h'], d_hkl['k'], d_hkl['l']))
  indices = flex.miller_index(h,k,l)
  data = flex.double(d_hkl['obs'])
  sigmas = flex.double(d_hkl['sigobs'])
  miller_set = miller.set(crystal_symmetry, indices).auto_anomalous()
  miller_array = miller.array(miller_set, data, sigmas)
  if d_hkl['fcod'].upper() == 'F^2':
    miller_array.set_observation_type_xray_intensity()
  elif d_hkl['fcod'].upper() == 'F':
    miller_array.set_observation_type_xray_amplitude()
  else:
    raise Sorry('fcod == %s is not implemented.' % d_hkl['fcod'])
  return miller_array

# ------------------------------------------------------------------------------

def get_scatterers(unit_cell, d_mas, d_inp, log=None, silent=False):
  atom_sites_frac = flex.vec3_double([d['xyz'] for d in d_inp['atoms']])
  scatterers = flex.xray_scatterer()
  if len(d_mas['ATOM']) != len(d_inp['atoms']):
    raise Sorry('Inconsistent data: number of atoms in mas and inp files.')
  scatterers.reserve(len(d_mas['ATOM']))
  for i, (dm, di) in enumerate(zip(d_mas['ATOM'], d_inp['atoms'])):
    kwds = {
        'label': dm['ATOM'],
        'scattering_type': dm['ATOM'].split('(')[0],  # WARNING: This is an additional assumption!
        }
    # U
    jtf = di['jtf'] # U model selected in mas
    tp = dm['TP']  # U data in inp
    if tp == 1:
      if jtf == 1:
        u_iso = di['U'][0]
      elif jtf == 2:
        if not silent:
          print >> log, 'Conversion u_cif_as_u_iso (atom %s).' % dm['ATOM']
        u_cif = list(di['U'])
        u_iso = u_cif_as_u_iso(unit_cell, u_cif)
      else:
        msg = 'Can not handle jtf=%d (xd.inp) and tp=%d (xd.mas) for atom %s.'
        raise Sorry(msg % (jtf, tp, dm['ATOM']))
      kwds['u'] = u_iso
    elif tp == 2:
      if jtf == 1:
        if not silent:
          print >> log, 'Conversion u_iso_as_u_star (atom %s).' % dm['ATOM']
        u_iso = di['U'][0]
        u_star = u_iso_as_u_star(unit_cell, u_iso)
      elif jtf == 2:
        u_cif = list(di['U'])
        u_star = u_cif_as_u_star(unit_cell, u_cif)
      else:
        msg = 'Can not handle jtf=%d (xd.inp) and tp=%d (xd.mas) for atom %s.'
        raise Sorry(msg % (jtf, tp, dm['ATOM']))
      kwds['u'] = u_star
    elif tp > 2:
      msg = 'Can not handle tp=%d > 2 (xd.inp) for atom %s.'
      raise Sorry(msg % (tp, dm['ATOM']))
    elif jtf > 2:
      msg = 'Can not handle jtf=%d > 2 (xd.mas) for atom %s.'
      raise Sorry(msg % (jtf, dm['ATOM']))
    # anomalous scattering
    tbl = dm['TBL'] - 1
    kwds['fp'] = d_mas['SCAT'][tbl]["DELF'"]
    kwds['fdp'] = d_mas['SCAT'][tbl]["DELF''"]
    #
    scaterer = xray.scatterer(**kwds)
    scatterers.append(scaterer)
  scatterers.set_sites(atom_sites_frac)
  return scatterers

# ------------------------------------------------------------------------------

def get_multipole_scatterers(d_mas, d_inp):
  kappa2table = [ks[0] - 1 for ks in d_inp['kappas']]
  mpscl = []
  for i_atom, (ATOM, atom) in enumerate(zip(d_mas['ATOM'], d_inp['atoms'])):
    amult = atom['amult']
    lmax = atom['lmax']
    pop = (atom['pv'],) + tuple(atom['pop'])
    kappa_idx = ATOM['KAP'] - 1
    if len(pop) != 26:
      msg = 'Atom %s has unsupported number of population parameters (%d).' % (
            ATOM['ATOM'], len(pop))
      raise Sorry(msg)
    if amult > 0.0:
      pop = [p / amult for p in pop]
    else:
      pop = (0.0,) * 26
    ms = mptbx.multipole_scatterer(amult, lmax, pop, kappa_idx)
    mpscl.append(ms)
  multipole_scatterers = mptbx.shared_multipole_scatterer(mpscl)
  return multipole_scatterers

# ------------------------------------------------------------------------------

def get_names2idx(d_mas):
  return dict([(d['ATOM'], i) for i, d in enumerate(d_mas['ATOM'])])

def get_names2idx_dumm(d_mas):
  return dict([(t[0], i) for i, t in enumerate(d_mas['DUMM'])])

# ------------------------------------------------------------------------------

_axes = {
  'X': mptbx.lcs_axis.X,
  'Y': mptbx.lcs_axis.Y,
  'Z': mptbx.lcs_axis.Z
}

_axes_signed = {
  '-X': (mptbx.lcs_axis.X, mptbx.lcs_axis_sign.minus),
  '-Y': (mptbx.lcs_axis.Y, mptbx.lcs_axis_sign.minus),
  '-Z': (mptbx.lcs_axis.Z, mptbx.lcs_axis_sign.minus),
   'X': (mptbx.lcs_axis.X, mptbx.lcs_axis_sign.plus),
   'Y': (mptbx.lcs_axis.Y, mptbx.lcs_axis_sign.plus),
   'Z': (mptbx.lcs_axis.Z, mptbx.lcs_axis_sign.plus)
}

def get_lcs_definitions(d_mas):
  names2idx = get_names2idx(d_mas)
  names2idx_dumm = get_names2idx_dumm(d_mas)
  n_atoms = len(d_mas['ATOM'])
  lcsdefs = mptbx.shared_lcs_definition()
  lcsdefs.reserve(n_atoms)
  for i, atom in enumerate(d_mas['ATOM']):
    if not atom['ATOM0'].upper().startswith('DUM'):
      i0 = names2idx[atom['ATOM0']]
    else:
      i0 = n_atoms + names2idx_dumm[atom['ATOM0']]
    if not atom['ATOM1'].upper().startswith('DUM'):
      i1 = names2idx[atom['ATOM1']]
    else:
      i1 = n_atoms + names2idx_dumm[atom['ATOM1']]
    if not atom['ATOM2'].upper().startswith('DUM'):
      i2 = names2idx[atom['ATOM2']]
    else:
      i2 = n_atoms + names2idx_dumm[atom['ATOM2']]
    ax1 = atom['AX1'].upper()
    if not ax1 in _axes_signed.keys():
      raise Sorry('Unrecognized AX1 for atom %s' % str(atom))
    ax1, ax1_sign = _axes_signed[ax1]
    ax2 = atom['AX2'].upper()
    if not ax2 in _axes.keys():
      raise Sorry('Unrecognized AX2 for atom %s' % str(atom))
    ax2 = _axes[ax2]
    rl = atom['R/L'].upper()
    if not rl in ('R', 'L'):
      raise Sorry('Unrecognized R/L for atom %s.' % str(atom))
    rl = getattr(mptbx.lcs_orientation, rl)
    lcsdefs.append(mptbx.lcs_definition(i0, ax1, ax1_sign, i1, i2, ax2, rl))
  return lcsdefs

# ------------------------------------------------------------------------------

def get_dumm_sites_frac(d_mas):
  dumm_pos = [t[1:] for t in d_mas['DUMM']]
  return flex.vec3_double(dumm_pos)

def get_lcs_manager(d_mas, unit_cell):
  lcs_definitions = get_lcs_definitions(d_mas)
  dumm_sites_frac = get_dumm_sites_frac(d_mas)
  dumm_sites_cart = unit_cell.orthogonalize(dumm_sites_frac)
  lcsmgr = lcs_manager(lcs_definitions, unit_cell, dumm_sites_cart)
  return lcsmgr

# ==============================================================================

class xd_data:

  def __init__(self, d_mas, d_inp, d_bnk=None, log=None, silent=False):
    self.d_mas = d_mas
    self.d_inp = d_inp
    self.d_bnk = d_bnk
    self.crystal_symmetry = get_crystall_symmetry(d_mas)
    self.unit_cell = self.crystal_symmetry.unit_cell()
    scatterers = get_scatterers(self.unit_cell, d_mas, d_inp,
                                log=log, silent=silent)
    self.structure = xray.structure(crystal_symmetry=self.crystal_symmetry,
                                    scatterers=scatterers)
    self.kappa2rf = [ks[0] - 1 for ks in d_inp['kappas']]
    self.kappa_table = mptbx.shared_kappa_set([ks[1] for ks in d_inp['kappas']])
    self.multipole_scatterers = get_multipole_scatterers(d_mas, d_inp)
    # initial LCSs:
    self.find_lcs()
    # input consistency check:
    for i_atom, (ATOM, atom) in enumerate(zip(d_mas['ATOM'], d_inp['atoms'])):
      rf_idx = ATOM['TBL'] - 1
      kappa_idx = ATOM['KAP'] - 1
      if rf_idx != self.kappa2rf[kappa_idx]:
        msg = 'Inconsistent KAP & TB entry for atom %s vs kappas in inp' % atom
        raise Sorry(msg)

# ------------------------------------------------------------------------------

  def find_lcs(self):
    self.lcs_manager = get_lcs_manager(self.d_mas, self.unit_cell)
    self.lcs_manager.calculate(self.structure.scatterers())
    self.lcs_manager.apply_to(self.multipole_scatterers)

# ------------------------------------------------------------------------------

  def get_rf_collection_tabulated(self, bank, tables, grd, lmax=4):
    rf_collection = []
    for scat in self.d_mas['SCAT']:
      rf_key = get_rf_key(bank, scat)
      tbl = tables[rf_key]
      rf_core = mptbx.radial_function_tabulated(tbl['CORE_TABLE'], grd, 0)
      rf_valence = mptbx.radial_function_tabulated(tbl['SPHV_TABLE'], grd, 0)
      rf_list = [rf_core, rf_valence]
      for l in range(lmax + 1):
        if l < len(tbl['DEFVs']):
          rf = mptbx.radial_function_tabulated(tbl['DEFVs'][l][1], grd, l)
        else:
          rf = mptbx.radial_function_zero()
        rf_list.append(rf)
      rf_collection.append(rf_list)
    return rf_collection

# ------------------------------------------------------------------------------

  def get_single_zeta_n_z(self):
    if self.d_bnk is None:
      raise Sorry('No d_bnk in xd_data.')
    from mptbx.io.xd2006readers.read_bnk import orbt
    nz = {}
    for i_sct, sct in enumerate(self.d_mas['SCAT']):
      symbol = sct['SCAT']
      bnk = self.d_bnk[symbol]
      Z = 0.0
      S = 0
      jorb = 0
      nl = 0
      for i, z in enumerate(bnk['SZ']):
          v = int(sct[orbt[i]])
          if z > 0.0 and v < 0:
              Z += z * (-v)
              S -= v
              jorb += 1
              nl += int(orbt[i][0]) - 1
      if jorb >= 1:
          nl = int(2 * float(nl) / float(jorb))
      else:
        msg = 'get_single_zeta_n_z: not implemented (jorb=%s,nl=%s).'%(jorb, nl)
        raise Sorry(msg)
      Z = 2.0 * Z / float(S)
      Z /= 0.5291772  # Bohr^-1 -> Ang^-1 (value from data statement in xdlsm.f)
      NZ = (nl, Z)
      nz[i_sct] = NZ
      nz[symbol] = NZ
    return nz

  # CORE:tabulated, SPHV:tabulated, DEFV:analytical
  def get_rf_collection_tta(self, bank, tables, grd, lmax=4):
    rf_collection = []
    nz = self.get_single_zeta_n_z()
    for scat in self.d_mas['SCAT']:
      rf_key = get_rf_key(bank, scat)
      tbl = tables[rf_key]
      rf_core = mptbx.radial_function_tabulated(tbl['CORE_TABLE'], grd, 0)
      rf_valence = mptbx.radial_function_tabulated(tbl['SPHV_TABLE'], grd, 0)
      lm = len(tbl['DEFVs']) - 1
      rf_def = mptbx.make_single_zeta_rf_set(lm,*nz[scat['SCAT']])
      rf_list = [rf_core, rf_valence] + rf_def
      n_zero = lmax - lm
      if n_zero:
        msg = 'Padding rf_list with %d instance(s) of radial_function_zero.' \
              % n_zero
        sys.stderr.write(msg+'\n')
        rf_list += [mptbx.radial_function_zero()] * n_zero
      rf_collection.append(rf_list)
    return rf_collection


# ==============================================================================

xd2006_params = """

xd {
  inp = xd.inp
    .type = path
    .help = "xd.inp or xd.res file"
  mas = xd.mas
    .type = path
  bnk = None
    .type = path
    .help = "Path to bank file (*.bnk). Overrides BANK setting in xd.mas."
  rf_set = tabulated *tta
    .type = choice
    .help = "tta = CORE:tabulated, SPHV:tabulated, DEFV:analytical"
  test = None
    .type = path
    .multiple = True
    .help = "Reference xd_lsm.out file(s) generated by xdlsm run with *test"
            "option. Used for extraction of tabulated radial functions."
  grd = None
    .type = float
    .help = "grid spacing for reading tables from xdlsm *test run."
            "XD2006v5.36's compile-time default is 0.05."
  tables = *grd_0_05 None
    .type = choice
    .help = "Default collection of tabulated radial functions."
            "Will be overridden by test entries!"
  hkl = None
    .type = path
    .help = "Reflections in XD2006 format."

  silent = False
    .type = bool

}

"""

# ------------------------------------------------------------------------------

def xd_setup(params, log=None):

  silent = params.xd.silent

  if not params.xd.bnk is None and not params.xd.test:
    msg = 'When using custom bank also xdlsm *test output(s) must be provided.'
    raise Sorry(msg)
  if params.xd.test and params.xd.grd is None:
    msg = 'When providing xdlsm *test output(s) grd value must be provided.'
    raise Sorry(msg)
  if not params.xd.test and params.xd.grd:
    msg = 'When not providing xdlsm *test output(s) grd value is meaningless.'
    raise Sorry(msg)
  if not params.xd.test and params.xd.tables is None:
    msg = 'xdlsm *test output(s) and/or non-empty table collection '
    msg += 'must be provided.'
    raise Sorry(msg)

  d_mas = xd2006readers.read_mas.read_mas(params.xd.mas)
  d_inp = xd2006readers.read_inp.read_inp(params.xd.inp, log=log, silent=silent)

  if not params.xd.bnk is None:
    bank = None
    d_bnk = xd2006readers.read_bnk.read_bnk(params.xd.bnk)[1]  #(comment,d_bnk)
  else:
    try:
      bank = d_mas['BANK'].upper()
    except KeyError:
      bank = 'CR'
      if not silent:
        print >> log, "NOTE: No BANK entry in mas file. Using 'CR'."
    d_bnk = mptbx.data.banks.load(bank)

  xd_data = globals()['xd_data'](d_mas, d_inp, d_bnk, log=log, silent=silent)

  if not bank is None and not params.xd.tables is None:
    if not silent:
      print >> log, 'LOADING TABLES:', params.xd.tables
    grd, tables = mptbx.data.tables.load(params.xd.tables)
  else:
    if not silent:
      print >> log, 'USING EMPTY COLLECTION'
    grd, tables = None, {}

  if not grd is None and not params.xd.grd is None and grd != params.xd.grd:
    msg = 'Provided grd value is inconsistent with selected collection '
    msg += 'of tables: %s.' % params.xd.tables
    raise Sorry(msg)

  if params.xd.test:
    grd = params.xd.grd
    tables.update(collect_rf_tables(params.xd.test, bank is None))
    if not silent:
      print >> log, 'UPDATING WITH TEST: ', params.xd.test, 'WITH grd =', grd

  if params.xd.rf_set == 'tabulated':
    rf_collection = xd_data.get_rf_collection_tabulated(bank, tables, grd, 4)
  elif params.xd.rf_set == 'tta':
    rf_collection = xd_data.get_rf_collection_tta(bank, tables, grd, 4)
  else:
    raise Sorry('Unexpected rf_set: %s.' % params.xd.rf_set)

  rf_calculator = mptbx.rf_calculator_4(rf_collection,
                                        xd_data.kappa_table,
                                        xd_data.kappa2rf)

  if not params.xd.hkl is None:
    d_hkl = xd2006readers.read_hkl.read_hkl(params.xd.hkl)
    miller_array = get_miller_array(xd_data.crystal_symmetry, d_hkl)
  else:
    d_hkl = None
    miller_array = None

  return type('xd_setup_container',(object,),vars())()

# ==============================================================================

if __name__ == '__main__':
  import os, pprint, sys
  pp = pprint.PrettyPrinter(indent=4, width=350)
  import libtbx.phil

  topdir = os.path.split(mptbx.__file__)[0]
  dname = os.path.join(topdir,'io', 'xd2006readers','sample_data')
  f_mas = os.path.join(dname, 'xd.mas')
  f_inp = os.path.join(dname, 'xd.inp')
  f_hkl = os.path.join(dname, 'xd.hkl')
  f_bnk = os.path.join(dname, 'xd.bnk_RHF_CR')
  f_tst = os.path.join(dname, 'xd_lsm.out__test')
  defaults = """ xd { inp = %s ; mas = %s ; hkl = %s ; bnk = %s ; test = %s ;
  rf_set=tta ; grd=0.05 } """ % (f_inp, f_mas, f_hkl, f_bnk, f_tst)
  params = libtbx.phil.parse(xd2006_params).fetch(libtbx.phil.parse(defaults))
  argument_interpreter = params.command_line_argument_interpreter()
  phil_objects = argument_interpreter.process_args(sys.argv[1:])
  params = params.fetch(sources=phil_objects)
  print 'Using:' ; params.show()

  xds = xd_setup(params.extract())
  xd = xds.xd_data

  sg = xd.crystal_symmetry.space_group()
  sgi = sgtbx.space_group_info(group=sg)
  print sgi
  for s in sg:
    print s
  print '----------------------------------------------------------------------'
  print xd.structure.show_summary().show_scatterers()
#  print '----------------------------------------------------------------------'
#  for i, lcs in enumerate(xd.lcs_manager.lcss_frac):
#    print '-- %d --' % i
#    u,v,w = lcs
#    print 'u =', u
#    print 'v =', v
#    print 'w =', w
  print '----------------------------------------------------------------------'
  for rfvs in xds.rf_calculator.calculate(0.333):
    print ('%15.10f'*7) % rfvs.as_tuple()
  print '----------------------------------------------------------------------'
  print xd.get_single_zeta_n_z()
