#include <mptbx/wrap.hpp>
#include <mptbx/lcs.hpp>

namespace mptbx {
namespace boost_python {

// -----------------------------------------------------------------------------

void wrap_lcs_enums()
{
  using namespace boost::python;
  enum_<lcs::axis>("lcs_axis")
      .value("X", lcs::X)
      .value("Y", lcs::Y)
      .value("Z", lcs::Z);
  enum_<lcs::axis_sign>("lcs_axis_sign")
      .value("plus", lcs::plus)
      .value("minus", lcs::minus);
  enum_<lcs::orientation>("lcs_orientation")
      .value("R", lcs::R)
      .value("L", lcs::L);
}

// -----------------------------------------------------------------------------

boost::python::object lcs_definition_as_tuple(lcs::definition lcsd)
{
  return boost::python::make_tuple(
      lcsd.i0,
      lcsd.ax1,
      lcsd.ax1_sign,
      lcsd.i1,
      lcsd.i2,
      lcsd.ax2,
      lcsd.rl);
};

struct lcs_definition_wrapper
{
  typedef lcs::definition w_t;
  static void
  wrap()
  {
    using namespace boost::python;
    typedef return_value_policy<return_by_value> rbv;
    typedef default_call_policies dcp;
    class_<w_t, boost::noncopyable>("lcs_definition", no_init)
      .def(init<
                 int,
                 lcs::axis,
                 lcs::axis_sign,
                 int,
                 int,
                 lcs::axis,
                 lcs::orientation
                 >())
      .add_property("i0",
                    make_getter(&w_t::i0, rbv()),
                    make_setter(&w_t::i0, dcp()))
      .add_property("i1",
                    make_getter(&w_t::i1, rbv()),
                    make_setter(&w_t::i1, dcp()))
      .add_property("i2",
                    make_getter(&w_t::i2, rbv()),
                    make_setter(&w_t::i2, dcp()))
      .add_property("ax1",
                    make_getter(&w_t::ax1, rbv()),
                    make_setter(&w_t::ax1, dcp()))
      .add_property("ax1_sign",
                    make_getter(&w_t::ax1_sign, rbv()),
                    make_setter(&w_t::ax1_sign, dcp()))
      .add_property("ax2",
                    make_getter(&w_t::ax2, rbv()),
                    make_setter(&w_t::ax2, dcp()))
      .add_property("rl",
                    make_getter(&w_t::rl, rbv()),
                    make_setter(&w_t::rl, dcp()))
      .def("as_tuple", lcs_definition_as_tuple)
      ;
  }
};


// -----------------------------------------------------------------------------

void wrap_shared_lcs_definition()
{
  typedef lcs::definition w_t;
  namespace bp = boost::python;
  namespace afbp = scitbx::af::boost_python;
  typedef bp::return_internal_reference<> rir;
  afbp::shared_wrapper<w_t, rir>::wrap("shared_lcs_definition");
}

template <typename FloatType>
struct lcs_calculator_wrapper
{
  typedef lcs::calculator<FloatType> w_t;
  static void
  wrap()
  {
    using namespace boost::python;
    typedef return_value_policy<return_by_value> rbv;
    typedef default_call_policies dcp;
    class_<w_t, boost::noncopyable>("lcs_calculator", no_init)
      .def(init<
                 af::const_ref<lcs::definition> const &,
                 af::const_ref< scitbx::vec3<FloatType> > const &,
                 af::const_ref< scitbx::vec3<FloatType> > const &
                 >())
      .add_property("lcss_cart", make_getter(&w_t::lcss_cart, rbv()))
      ;
  }
};

// -----------------------------------------------------------------------------

void wrap_lcs()
{
  wrap_lcs_enums();
  lcs_definition_wrapper::wrap();
  def("lcs_definition_as_tuple", lcs_definition_as_tuple);
  wrap_shared_lcs_definition();
  lcs_calculator_wrapper<MPTBX_FLOAT_TYPE>::wrap();
};

// -----------------------------------------------------------------------------

}} // namespace mptbx::boost_python
