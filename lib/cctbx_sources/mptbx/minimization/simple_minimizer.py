from __future__ import division
import libtbx.load_env
import os
import random
import mmtbx.f_model
from scitbx.array_family import flex
from libtbx import group_args
from cctbx import xray
import scitbx.lbfgs
from cctbx import adptbx, miller
from libtbx import adopt_init_args
import cctbx

import libtbx.phil

import mptbx
from mptbx.io.xd2006 import xd2006_params, xd_setup
import mptbx.mp4 as mp4

# ------------------------------------------------------------------------------

class minimizer(object):

  def __init__(
                 self,
                 fmodel,
                 mp4_model,
                 max_iterations=25,
                 scale = None,
                 sites = False,
                 adps = False
              ):
    
    self.fmodel = fmodel
    self.mp4_model = mp4_model
    assert fmodel.xray_structure is mp4_model.structure
    self.scale = scale
    
    self.fmodel.xray_structure.scatterers().flags_set_grads(state=False)    
    if sites:
      xray.set_scatterer_grad_flags(
        scatterers = self.fmodel.xray_structure.scatterers(),
        site       = True)
    if adps:
      self.fmodel.xray_structure.scatterers().flags_set_grad_u_aniso(
        iselection = self.fmodel.xray_structure.use_u_aniso().iselection())
      self.fmodel.xray_structure.scatterers().flags_set_grad_u_iso(
        iselection = self.fmodel.xray_structure.use_u_iso().iselection())
    
    self.x = flex.double(self.fmodel.xray_structure.n_parameters(), 0)
    self._scatterers_start = self.fmodel.xray_structure.scatterers()
    self.call_count = 0
    
    self.minimizer = scitbx.lbfgs.run(
      target_evaluator=self,
      termination_params=scitbx.lbfgs.termination_parameters(
        max_iterations=max_iterations),
      exception_handling_params=scitbx.lbfgs.exception_handling_parameters(
        ignore_line_search_failed_rounding_errors=True,
        ignore_line_search_failed_step_at_lower_bound=True,
        ignore_line_search_failed_maxfev=True))
    
    self.fmodel.xray_structure.tidy_us()
    self.apply_shifts()
    del self._scatterers_start
    self.mp4_model.compute_structure_factors(self.fmodel.f_obs())
    self.fmodel.update(f_calc=self.mp4_model.f_mpl())


  def apply_shifts(self):
    apply_shifts_result = xray.ext.minimization_apply_shifts(
      unit_cell      = self.fmodel.xray_structure.unit_cell(),
      scatterers     = self._scatterers_start,
      shifts         = self.x)
    scatterers_shifted = apply_shifts_result.shifted_scatterers
    self.fmodel.xray_structure.replace_scatterers(
      scatterers = scatterers_shifted)
    self.mp4_model.compute_structure_factors(self.fmodel.f_obs())
    self.fmodel.update(f_calc=self.mp4_model.f_mpl())

  def compute_functional_and_gradients(self):
    self.apply_shifts()
    tgx = func(fmodel=self.fmodel, mp4_model=self.mp4_model, scale=self.scale)
    f = tgx.target_work()
    g = tgx.grads_u_anisos()
    xray.minimization.add_gradients(
      scatterers        = self.fmodel.xray_structure.scatterers(),
      xray_gradients    = g)
    self.call_count += 1
    return f, g

# ------------------------------------------------------------------------------

class func(object):
  def __init__(self, fmodel, mp4_model, scale=None):
    adopt_init_args(self, locals())
    weights = flex.double(self.fmodel.f_obs().data().size(), 1.0)
    if scale is None:
      scale = fmodel.scale_k1()
    self.core = xray.target_functors.least_squares(
      compute_scale_using_all_data = False,
      f_obs                        = self.fmodel.f_obs(),
      r_free_flags                 = self.fmodel.r_free_flags(),
      weights                      = weights,
      scale_factor                 = scale)
    self.r = self.core(f_calc = self.fmodel.f_model(), compute_gradients=True)
    self.d_target_d_f_calc = self.r.gradients_work() # XXX needs scales

  def target_work(self):
    return self.r.target_work()

  def grads_u_anisos(self):
    return self.mp4_model.compute_packed_gradients(self.fmodel.f_obs(), 
                                                   self.d_target_d_f_calc)

# ------------------------------------------------------------------------------
