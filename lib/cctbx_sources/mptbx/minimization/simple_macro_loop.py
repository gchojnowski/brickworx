from __future__ import division

import libtbx.phil

from simple_minimizer import minimizer

# ------------------------------------------------------------------------------

macro_loop_params = """

macro_loop {

  n_cycles = 10
    .type = int

  refine_sites = True
    .type = bool

  refine_adps = True
    .type = bool

  file_trj = None
    .type = path

}

"""

default_macro_loop_params = libtbx.phil.parse(macro_loop_params)

# ------------------------------------------------------------------------------

def macro_loop(
               mp4_model,
               fmodel, 
               params,
               minimizer_kw={},
               ):
  print "start r_factor: %6.4f" % fmodel.r_work()
  total_call_count = 0
  if params.macro_loop.file_trj:
    with file(params.macro_loop.file_trj, 'a') as f: pass
  for macro_cycle in xrange(params.macro_loop.n_cycles):
    if params.macro_loop.refine_sites:
      minimized = minimizer(fmodel=fmodel, 
                            mp4_model=mp4_model, 
                            sites=True,
                            **minimizer_kw)
      msg = "macro_cycle %3d (sites)   r_factor: %6.4f  call_count=%d  scale=%f" % \
            (macro_cycle, fmodel.r_work(), minimized.call_count, fmodel.scale_k1())
      print '    ', msg
      total_call_count += minimized.call_count
      if params.macro_loop.file_trj:
        with file(params.macro_loop.file_trj, 'a') as f:
          f.write('REMARK %s\n' % msg)
          f.write(fmodel.xray_structure.as_pdb_file())
    if params.macro_loop.refine_adps:
      minimized = minimizer(fmodel=fmodel, 
                            mp4_model=mp4_model, 
                            adps=True,
                            **minimizer_kw)
      msg = "macro_cycle %3d (adps)    r_factor: %6.4f  call_count=%d  scale=%f" % \
            (macro_cycle, fmodel.r_work(), minimized.call_count, fmodel.scale_k1())
      print '    ', msg
      total_call_count += minimized.call_count
      if params.macro_loop.file_trj:
        with file(params.macro_loop.file_trj, 'a') as f:
          f.write('REMARK %s\n' % msg)
          f.write(fmodel.xray_structure.as_pdb_file())
  return total_call_count
    
# ------------------------------------------------------------------------------

