#include <mptbx/wrap.hpp>
#include <mptbx/sf4.hpp>
#include <mptbx/sf4_gradients.hpp>

namespace mptbx {
namespace boost_python {

// -----------------------------------------------------------------------------

template < typename ScattererType=cctbx::xray::scatterer<>,
           typename MultipoleScattererType=mptbx::multipole_scatterer<4> >
struct sf4_one_h_one_scatterer_wrappers
{
  typedef typename ScattererType::float_type float_type;
  typedef sf4::one_h_one_scatterer<ScattererType,MultipoleScattererType> w_t;
  static void
  wrap()
  {
    using namespace boost::python;
    typedef return_value_policy<return_by_value> rbv;
    typedef default_call_policies dcp;
    class_<w_t, boost::noncopyable>("sf4_one_h_one_scatterer", no_init)
      .def(init<
                 cctbx::sgtbx::space_group const&,
                 cctbx::miller::index<> const&,
                 float_type const&,
                 ScattererType const&,
                 MultipoleScattererType,
                 rf_values_set<4, float_type> const&
                 >())
      .add_property("f_mpl", make_getter(&w_t::f_mpl, rbv()))
      .add_property("f_sph", make_getter(&w_t::f_sph, rbv()))
      ;
  }
};

// -----------------------------------------------------------------------------

template < typename ScattererType=cctbx::xray::scatterer<>,
           typename MultipoleScattererType=mptbx::multipole_scatterer<4> >
struct sf4_one_h_wrappers
{
  typedef typename ScattererType::float_type float_type;
  typedef sf4::one_h<ScattererType,MultipoleScattererType> w_t;
  static void
  wrap()
  {
    using namespace boost::python;
    typedef return_value_policy<return_by_value> rbv;
    typedef default_call_policies dcp;
    class_<w_t, boost::noncopyable>("sf4_one_h", no_init)
      .def(init<
                 cctbx::sgtbx::space_group const&,
                 cctbx::miller::index<> const&,
                 float_type const&,
                 af::const_ref<ScattererType> const&,
                 af::const_ref<MultipoleScattererType> const&,
                 af::const_ref<rf_values_set<4,float_type> > const&
                 >())
      .add_property("f_mpl", make_getter(&w_t::f_mpl, rbv()))
      .add_property("f_sph", make_getter(&w_t::f_sph, rbv()))
      ;
  }
};

// -----------------------------------------------------------------------------

template < typename ScattererType=cctbx::xray::scatterer<>,
           typename MultipoleScattererType=mptbx::multipole_scatterer<4> >
struct sf4_wrappers
{
  typedef typename ScattererType::float_type float_type;
  typedef sf4::structure_factors<ScattererType,MultipoleScattererType> w_t;
  static void
  wrap()
  {
    using namespace boost::python;
    typedef return_value_policy<return_by_value> rbv;
    typedef default_call_policies dcp;
    class_<w_t, boost::noncopyable>("sf4", no_init)
      .def(init<
                 cctbx::uctbx::unit_cell const&,
                 cctbx::sgtbx::space_group const&,
                 af::const_ref<ScattererType> const&,
                 af::const_ref<MultipoleScattererType> const&,
                 mptbx::rf_calculator<4,float_type>,
                 af::const_ref<cctbx::miller::index<> > const&
                 >())
      .def("f_mpl", &w_t::f_mpl)
      .def("f_sph", &w_t::f_sph)
      ;
  }
};

// -----------------------------------------------------------------------------

template < typename ScattererType=cctbx::xray::scatterer<>,
           typename MultipoleScattererType=mptbx::multipole_scatterer<4> >
struct sf4_gradients_one_h_one_scatterer_wrappers
{
  typedef typename ScattererType::float_type float_type;
  typedef sf4::gradients_one_h_one_scatterer<ScattererType,
                                             MultipoleScattererType> w_t;
  static void
  wrap()
  {
    conditional_tt_wrap<cctbx::fractional<std::complex<float_type> > >();
    conditional_tt_wrap<scitbx::sym_mat3<std::complex<float_type> > >();
    using namespace boost::python;
    typedef return_value_policy<return_by_value> rbv;
    typedef default_call_policies dcp;
    class_<w_t, boost::noncopyable>("sf4_gradients_one_h_one_scatterer", no_init)
      .def(init<
                cctbx::sgtbx::space_group const&,
                cctbx::miller::index<> const&,
                float_type const &,
                ScattererType const&,
                MultipoleScattererType const&,
                rf_values_set<4,float_type> const&,
                std::complex<float_type> const&
                 >())
      .add_property("d_target_d_site_frac",
          make_getter(&w_t::d_target_d_site_frac, rbv()))
      .add_property("d_target_d_u_iso",
           make_getter(&w_t::d_target_d_u_iso, rbv()))
      .add_property("d_target_d_u_star",
           make_getter(&w_t::d_target_d_u_star, rbv()))
      .add_property("f_mpl",
           make_getter(&w_t::f_mpl, rbv()))
      .add_property("d_f_d_site_frac",
           make_getter(&w_t::d_f_d_site_frac, rbv()))
      .add_property("d_f_d_u_iso",
           make_getter(&w_t::d_f_d_u_iso, rbv()))
      .add_property("d_f_d_u_star",
           make_getter(&w_t::d_f_d_u_star, rbv()))
      ;
  }
};

// -----------------------------------------------------------------------------

template < typename ScattererType=cctbx::xray::scatterer<> >
struct sf4_gradient_storage_wrappers
{
  typedef typename ScattererType::float_type float_type;
  typedef sf4::gradient_storage<ScattererType> w_t;
  static void
  wrap()
  {
    using namespace boost::python;
    typedef return_value_policy<return_by_value> rbv;
    typedef default_call_policies dcp;
    class_<w_t, boost::noncopyable>("sf4_gradient_storage", no_init)
      .def(init<af::const_ref<ScattererType> const&>())
      .def("d_target_d_site_frac", &w_t::d_target_d_site_frac)
      .def("d_target_d_u_iso", &w_t::d_target_d_u_iso)
      .def("d_target_d_u_star", &w_t::d_target_d_u_star)
      .def("packed", &w_t::packed)
      ;
  }
};

// -----------------------------------------------------------------------------

template < typename ScattererType=cctbx::xray::scatterer<>,
           typename MultipoleScattererType=mptbx::multipole_scatterer<4> >
struct  sf4_gradient_wrappers
{
  typedef typename ScattererType::float_type float_type;
  typedef std::complex<float_type> complex_type;
  typedef sf4::gradients<ScattererType,MultipoleScattererType> w_t;
  static void
  wrap()
  {
    using namespace boost::python;
    typedef return_value_policy<return_by_value> rbv;
    typedef default_call_policies dcp;
    class_<w_t, bases<sf4::gradient_storage<ScattererType> >,
           boost::noncopyable>("sf4_gradients", no_init)
      .def(init<
                 cctbx::uctbx::unit_cell const&,
                 cctbx::sgtbx::space_group const&,
                 af::const_ref<ScattererType> const&,
                 af::const_ref<MultipoleScattererType> const&,
                 mptbx::rf_calculator<4,float_type>,
                 af::const_ref<cctbx::miller::index<> > const&,
                 af::const_ref<complex_type> const&
                 >())
      ;
  }
};

// -----------------------------------------------------------------------------

void wrap_sf4()
{
  sf4_one_h_one_scatterer_wrappers<>::wrap();
  sf4_one_h_wrappers<>::wrap();
  sf4_wrappers<>::wrap();
  sf4_gradients_one_h_one_scatterer_wrappers<>::wrap();
  sf4_gradient_storage_wrappers<>::wrap();
  sf4_gradient_wrappers<>::wrap();
};

// -----------------------------------------------------------------------------

}} // namespace mptbx::boost_python
