#ifndef MPTBX_WRAP_HPP
#define MPTBX_WRAP_HPP

#include <boost/python/class.hpp>
#include <boost/python/def.hpp>
#include <boost/python/args.hpp>
#include <boost/python/return_value_policy.hpp>
#include <boost/python/reference_existing_object.hpp>
#include <boost/python.hpp>

#include <boost/lexical_cast.hpp>

#include <tbxx/error_utils.hpp>
#include <cctbx/error.h>

#include <scitbx/array_family/tiny.h>
#include <scitbx/array_family/shared.h>
#include <scitbx/array_family/boost_python/shared_wrapper.h>

#include <scitbx/boost_python/container_conversions.h>
#include <iostream>

namespace mptbx {
namespace boost_python {

// -----------------------------------------------------------------------------

namespace af = scitbx::af;
namespace bp = boost::python;
namespace afbp = scitbx::af::boost_python;

// -----------------------------------------------------------------------------

template<typename tiny_type>
void conditional_tt_wrap()
{
  namespace cc = scitbx::boost_python::container_conversions;
  namespace bp = boost::python;
  const bp::converter::registration* reg =
    bp::converter::registry::query(bp::type_id<tiny_type>());
  if((!reg)||(!(reg->rvalue_chain)))
    cc::tuple_mapping_fixed_size<tiny_type>();
};

// -----------------------------------------------------------------------------

inline const char * add_maxl_suffix(std::string s, int l)
{
  std::string ls = boost::lexical_cast<std::string>(l);
  return (std::string(s) + std::string("_") + ls).c_str();
}

// -----------------------------------------------------------------------------

#define MPTBX_FLOAT_TYPE   double
#define MPTBX_MAXL         4

// while model data templates can be specialized for any maxl value
// actual structure factors implementation currently exists only for maxl=4

// -----------------------------------------------------------------------------

}} // namespace mptbx::boost_python

#endif // GUARD
