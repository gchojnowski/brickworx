from __future__ import division

import mptbx

class lcs_manager:

  def __init__(self, lcs_definitions, unit_cell, dumm_sites_cart):
    self.lcs_definitions = lcs_definitions
    self.unit_cell = unit_cell
    self.dumm_sites_cart = dumm_sites_cart

  def calculate(self, scatterers):
    sites_cart = self.unit_cell.orthogonalize(scatterers.extract_sites())
    lcss_calculator = mptbx.lcs_calculator(self.lcs_definitions,
                                           sites_cart,
                                           self.dumm_sites_cart)
    self.lcss_cart = lcss_calculator.lcss_cart
    self.lcss_frac = self.unit_cell.fractionalize(self.lcss_cart)
    return self

  def apply_to(self, multipole_scatterers):
    multipole_scatterers.set_lcss_3vec3(self.lcss_frac)
