#ifndef MPTBX_RADIAL_FUNCTIONS_HPP
#define MPTBX_RADIAL_FUNCTIONS_HPP

#include <tbxx/error_utils.hpp>

#include <mptbx/model.hpp>
#include <mptbx/xd2006.hpp>

namespace mptbx {

namespace af = scitbx::af;

// -----------------------------------------------------------------------------

template <typename FloatType>
class radial_function
{
  public:

  virtual af::tiny<FloatType,2> operator() (FloatType stol, FloatType kappa)
  {
    throw TBXX_NOT_IMPLEMENTED();
  };

};

// -----------------------------------------------------------------------------

template <typename FloatType>
class radial_function_zero : public radial_function<FloatType>
{
  public:

  radial_function_zero()
  {};

  virtual af::tiny<FloatType,2> operator() (FloatType stol, FloatType kappa)
  {
    return af::tiny<FloatType,2> (0.0, 0.0);
  };

};

// -----------------------------------------------------------------------------

template <typename FloatType>
class radial_function_tabulated : public radial_function<FloatType>
{
  private:

  af::shared<FloatType> table;
  FloatType spacing;
  unsigned int l;

  public:

  radial_function_tabulated(af::shared<FloatType> table,
                            FloatType             spacing,
                            int                   l)
  : table(table), spacing(spacing), l(l)
  {};

  virtual af::tiny<FloatType,2> operator() (FloatType stol, FloatType kappa)
  {
    unsigned int n = table.size();
    FloatType f,df;
    xd2006::inter(stol, table.begin(), kappa, l, spacing, n, f, df);
    return af::tiny<FloatType,2>(f,df);
  };

};

// -----------------------------------------------------------------------------

template <int MAXL, typename FloatType>
class rf_calculator
{
  public:

  typedef af::shared< kappa_set<MAXL,FloatType> > kappa_table_type;
  typedef af::const_ref< kappa_set<MAXL,FloatType> > kappa_table_cref;
  typedef af::shared< rf_values_set<MAXL,FloatType> > rf_table_type;
  typedef af::ref< rf_values_set<MAXL,FloatType> > rf_table_ref;

  rf_calculator() {};

  rf_calculator(boost::python::object  rf_list,
                kappa_table_type       kappa_table,
                af::shared<int>        kappa2rf)
  : rf_list_(rf_list),
    kappa_table_(kappa_table),
    kappa2rf_(kappa2rf)
  {
    TBXX_ASSERT((kappa2rf.size() == kappa_table.size()));
    rf_table_.resize(kappa_table.size());
  };

  rf_table_type calculate(FloatType sinthl)
  {
    using boost::python::len;
    af::const_ref<int> kappa2rf = kappa2rf_.const_ref();
    kappa_table_cref kappa_cref = kappa_table_.const_ref();
    rf_table_ref rf_ref = rf_table_.ref();

    for(int i_kappa=0; i_kappa < kappa_table_.size(); i_kappa++)
    {
      af::const_ref<FloatType> kappas = kappa_cref[i_kappa].const_ref();
      int i_table = kappa2rf[i_kappa];
      boost::python::object rf_funs = rf_list_[i_table];
      af::ref<FloatType> rf_vals = rf_ref[i_kappa].ref();
      boost::python::object f_df = rf_funs[0](sinthl, 1.0);
      FloatType f = boost::python::extract<FloatType>(f_df[0]);
      rf_vals[0] = f;
      for(int i_rf = 1; i_rf < len(rf_funs); i_rf++)
      {
        boost::python::object f_df = rf_funs[i_rf](sinthl, kappas[i_rf-1]);
        FloatType f = boost::python::extract<FloatType>(f_df[0]);
        rf_vals[i_rf] = f;
      }
    }

    return rf_table_;
  }


  protected:

  boost::python::object rf_list_;
  af::shared<int> kappa2rf_;
  kappa_table_type kappa_table_;

  rf_table_type rf_table_;

};

// -----------------------------------------------------------------------------


} // end mptbx namespace

#endif // GUARD
