from __future__ import division

def JN(N,S,Z):
  JNk = {}
  SZ = float(S**2 + Z**2)
  JNk[(1,0)] = 1.0 / SZ
  c1 = lambda nu: (2*nu*S) / SZ
  for nu in xrange(1, N):
    JNk[(nu+1,nu)] = c1(nu) * JNk[(nu,nu-1)]
  c2 = lambda nu: Z * (2*nu+2) / SZ
  for nu in xrange(0, N-1):
    JNk[(nu+2,nu)] = c2(nu) * JNk[(nu+1,nu)]
  c3b = lambda mu,nu: (mu+nu)*(mu-nu-1)
  c3c = lambda mu: 2*mu*Z
  for nu in xrange(0, N-2):
    for mu in xrange(nu+2, N):
      JNk[(mu+1,nu)] = (c3c(mu)*JNk[mu,nu] - c3b(mu,nu)*JNk[mu-1,nu]) / SZ
  return JNk

if __name__ == '__main__':
  N = 4
  jn = JN(N, 2.0, 3.0)
  try:
    import numpy as np
    a = np.zeros((N+1,N+1))
    for (i,j),v in jn.iteritems():
      a[i,j] = v
    print a
  except ImportError:
    import pprint
    pp = pprint.PrettyPrinter()
    pp.pprint(jn)
