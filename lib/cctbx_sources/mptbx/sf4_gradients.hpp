#ifndef MPTBX_SF4_GRADIENTS_HPP
#define MPTBX_SF4_GRADIENTS_HPP

#include <boost/mpl/assert.hpp>
#include <boost/type_traits.hpp>

#include <cctbx/sgtbx/miller_ops.h>
#include <cctbx/sgtbx/space_group.h>
#include <cctbx/xray/scatterer.h>

#include <mptbx/model.hpp>
#include <mptbx/xd2006.hpp>

// #include <stdio.h>

namespace mptbx {
namespace sf4 {

namespace af = scitbx::af;

using cctbx::adptbx::debye_waller_factor_u_iso;
using cctbx::adptbx::debye_waller_factor_u_star;
using cctbx::adptbx::debye_waller_factor_u_star_gradient_coefficients;
using cctbx::adptbx::grad_u_star_as_u_cart;
using scitbx::constants::two_pi;
using scitbx::constants::two_pi_sq;

// -----------------------------------------------------------------------------

template <typename ScattererType, typename MultipoleScattererType>
struct gradients_one_h_one_scatterer
{
  typedef typename ScattererType::float_type f_t;
  typedef typename MultipoleScattererType::float_type mp_float_type;
  BOOST_MPL_ASSERT((boost::is_same<f_t,mp_float_type >));
  typedef std::complex<f_t> c_t;

  gradients_one_h_one_scatterer(
    cctbx::sgtbx::space_group const&     space_group,
    cctbx::miller::index<> const&        h,
    f_t const &                          stol_sq,
    ScattererType const&                 scatterer,
    MultipoleScattererType const&        mp_scatterer,
    rf_values_set<4,f_t> const&          rfs,
    c_t const&                           d_target_d_f_calc)
  :
    d_target_d_site_frac(0,0,0),
    d_target_d_u_iso(0),
    d_target_d_u_star(0,0,0,0,0,0),
    f_mpl(0),
    d_f_d_site_frac(0,0,0),
    d_f_d_u_iso(0),
    d_f_d_u_star(0,0,0,0,0,0)
  {
    af::tiny<f_t,34> prod;
    af::tiny<f_t,24> ang;
    f_t a, b, sph;

    f_t f_core = rfs[0];
    c_t f_fpfdp(scatterer.fp, scatterer.fdp);

    f_t dw_iso(1.0);
    if (scatterer.flags.use_u_iso())
      dw_iso = debye_waller_factor_u_iso(stol_sq, scatterer.u_iso);

    for(std::size_t i_smx=0; i_smx < space_group.order_z(); i_smx++)
    {
      cctbx::sgtbx::rt_mx s = space_group(i_smx);
      cctbx::miller::index<> hr = h * s.r();
      f_t hrx = hr * scatterer.site;
      f_t ht = f_t(h * s.t()) / space_group.t_den();
      f_t phase = two_pi * (hrx + ht);
      c_t e_j_phase(std::cos(phase), std::sin(phase));

      f_t dw_u_star(1.0);
      if (scatterer.flags.use_u_aniso())
        dw_u_star = debye_waller_factor_u_star(hr, scatterer.u_star);
      f_t dw = dw_iso * dw_u_star;

      cctbx::miller::index<f_t> hr__f_t(hr);
      mptbx::xd2006::angular(hr__f_t.begin(),
                             mp_scatterer.lcs.begin(),
                             mp_scatterer.lmax,
                             prod.begin(),
                             ang.begin());
      mptbx::xd2006::mulpol(rfs.begin(),
                            mp_scatterer.pop.begin(),
                            ang.begin(),
                            mp_scatterer.lmax,
                            a, b, sph);
      c_t f_angular(a,b);

      c_t f0_fp_fd = f_core + f_angular + f_fpfdp;
      c_t f0_fp_fdp_w = f0_fp_fd * mp_scatterer.amult;
      c_t f_calc = e_j_phase * dw * f0_fp_fdp_w;
      f_mpl += f_calc;

      if (scatterer.flags.grad_site())
      {
        cctbx::fractional<c_t> hr__c_t(hr__f_t);
        d_f_d_site_frac += two_pi * c_t(0,1) * hr__c_t * f_calc;
        d_target_d_site_frac +=
              d_target_d_f_calc.real() * fractional_real(d_f_d_site_frac)
            + d_target_d_f_calc.imag() * fractional_imag(d_f_d_site_frac);
      }

      if (scatterer.flags.grad_u_iso() && scatterer.flags.use_u_iso())
      {
        d_f_d_u_iso += -two_pi_sq * f_calc * 4.0 * stol_sq;
        d_target_d_u_iso +=
              d_target_d_f_calc.real() * d_f_d_u_iso.real()
            + d_target_d_f_calc.imag() * d_f_d_u_iso.imag();
      }

      if (scatterer.flags.grad_u_aniso() && scatterer.flags.use_u_aniso())
      {
        scitbx::sym_mat3<c_t> dw_coeff(
          debye_waller_factor_u_star_gradient_coefficients(
          hr, scitbx::type_holder<f_t>()));
        d_f_d_u_star += -two_pi_sq * f_calc * dw_coeff;
        d_target_d_u_star +=
              d_target_d_f_calc.real() * smat3_real(d_f_d_u_star)
            + d_target_d_f_calc.imag() * smat3_imag(d_f_d_u_star);
      }

    }

  }

  // output:
  cctbx::fractional<f_t> d_target_d_site_frac;
  f_t                    d_target_d_u_iso;
  scitbx::sym_mat3<f_t>  d_target_d_u_star;
  // auxiliary:
  c_t                    f_mpl;
  cctbx::fractional<c_t> d_f_d_site_frac;
  c_t                    d_f_d_u_iso;
  scitbx::sym_mat3<c_t>  d_f_d_u_star;

  private:

  cctbx::fractional<f_t> inline
  fractional_real(cctbx::fractional<c_t> v)
  {
    cctbx::fractional<f_t> result;
    for(short i=0; i < 3; i++) result[i] = v[i].real();
    return result;
  }

  cctbx::fractional<f_t> inline
  fractional_imag(cctbx::fractional<c_t> v)
  {
    cctbx::fractional<f_t> result;
    for(short i=0; i < 3; i++) result[i] = v[i].imag();
    return result;
  }

  scitbx::sym_mat3<f_t> inline
  smat3_real(scitbx::sym_mat3<c_t> v)
  {
    scitbx::sym_mat3<f_t> result;
    for(short i=0; i < 6; i++) result[i] = v[i].real();
    return result;
  }

  scitbx::sym_mat3<f_t> inline
  smat3_imag(scitbx::sym_mat3<c_t> v)
  {
    scitbx::sym_mat3<f_t> result;
    for(short i=0; i < 6; i++) result[i] = v[i].imag();
    return result;
  }

};

// -----------------------------------------------------------------------------

template <typename FloatType>
struct gradient_refs
{
  gradient_refs(
    af::ref<scitbx::vec3<FloatType> >     site,
    af::ref<FloatType>                    u_iso,
    af::ref<scitbx::sym_mat3<FloatType> > u_star)
  :
    site(site),
    u_iso(u_iso),
    u_star(u_star)
  {}

  af::ref<scitbx::vec3<FloatType> >     site;
  af::ref<FloatType>                    u_iso;
  af::ref<scitbx::sym_mat3<FloatType> > u_star;
};

// -----------------------------------------------------------------------------

template <typename ScattererType>
struct gradient_storage
{
  typedef typename ScattererType::float_type f_t;

  gradient_storage(){};

  gradient_storage(af::const_ref<ScattererType> const& scatterers)
  {
    cctbx::xray::scatterer_grad_flags_counts grad_flags_counts(scatterers);

    if (grad_flags_counts.site != 0)
      d_target_d_site_frac_.resize(
        scatterers.size(), scitbx::vec3<f_t>(0,0,0));

    if (grad_flags_counts.u_iso != 0)
      d_target_d_u_iso_.resize(scatterers.size(), 0);

    if (grad_flags_counts.u_aniso != 0)
      d_target_d_u_star_.resize(
        scatterers.size(), scitbx::sym_mat3<f_t>(0,0,0,0,0,0));

    packed_.reserve(grad_flags_counts.site +
                    grad_flags_counts.u_iso +
                    grad_flags_counts.u_aniso);
  };

  af::shared<scitbx::vec3<f_t> >
  d_target_d_site_frac() const { return d_target_d_site_frac_; }

  af::shared<f_t>
  d_target_d_u_iso() const { return d_target_d_u_iso_; }

  af::shared<scitbx::sym_mat3<f_t> >
  d_target_d_u_star() const { return d_target_d_u_star_; }

  af::shared<f_t>
  packed() const { return packed_; }

  gradient_refs<f_t> get_refs()
  {
    return  gradient_refs<f_t>(
                               d_target_d_site_frac_.ref(),
                               d_target_d_u_iso_.ref(),
                               d_target_d_u_star_.ref()
                              );
  }

protected:

  void pack(cctbx::uctbx::unit_cell const&       unit_cell,
            af::const_ref<ScattererType> const&  scatterers)
  {
    gradient_refs<f_t> gr_refs = get_refs();
    for(std::size_t i_sc=0; i_sc < scatterers.size(); i_sc++)
    {
      ScattererType const& scatterer = scatterers[i_sc];
      bool gf_site = scatterer.flags.grad_site();
      bool gf_u_iso = scatterer.flags.grad_u_iso() &&
                      scatterer.flags.use_u_iso();
      bool gf_u_aniso = scatterer.flags.grad_u_aniso() &&
                        scatterer.flags.use_u_aniso();
      if (gf_site)
      {
        scitbx::vec3<f_t> d_target_d_site_cart =
          unit_cell.v_times_fractionalization_matrix_transpose(
              gr_refs.site[i_sc]);
        for(std::size_t k=0;k<3;k++)
          packed_.push_back(d_target_d_site_cart[k]);
      }
      if (gf_u_iso)
        packed_.push_back(gr_refs.u_iso[i_sc]);
      if (gf_u_aniso)
      {
        scitbx::sym_mat3<f_t> d_target_d_u_cart =
          grad_u_star_as_u_cart(unit_cell, gr_refs.u_star[i_sc]);
        for(std::size_t k=0;k<6;k++)
          packed_.push_back(d_target_d_u_cart[k]);
      }
    }
  }

  af::shared<scitbx::vec3<f_t> >     d_target_d_site_frac_;
  af::shared<f_t>                    d_target_d_u_iso_;
  af::shared<scitbx::sym_mat3<f_t> > d_target_d_u_star_;
  af::shared<f_t>                    packed_;

};

// -----------------------------------------------------------------------------

template <typename ScattererType, typename MultipoleScattererType>
class gradients : public gradient_storage<ScattererType>
{
  public:

  typedef typename ScattererType::float_type f_t;
  typedef typename MultipoleScattererType::float_type mp_float_type;
  BOOST_MPL_ASSERT((boost::is_same<f_t,mp_float_type >));
  typedef std::complex<f_t> c_t;

  gradients() {}

  gradients(
      cctbx::uctbx::unit_cell const&                 unit_cell,
      cctbx::sgtbx::space_group const&               space_group,
      af::const_ref<ScattererType> const&            scatterers,
      af::const_ref<MultipoleScattererType> const&   mp_scatterers,
      mptbx::rf_calculator<4,f_t>                    rf_calculator,
      af::const_ref<cctbx::miller::index<> > const&  miller_indices,
      af::const_ref<c_t> const&                      d_target_d_f_calc)
  : gradient_storage<ScattererType>(scatterers)
  {
    typedef typename mptbx::rf_calculator<4,f_t>::rf_table_type rf_table_type;
    CCTBX_ASSERT(d_target_d_f_calc.size() == miller_indices.size());
    gradient_refs<f_t> gr_refs = gradient_storage<ScattererType>::get_refs();
    for(std::size_t i=0; i < miller_indices.size(); i++)
    {
      cctbx::miller::index<> const h = miller_indices[i];
      f_t stol = unit_cell.stol(h);
      f_t stol_sq = stol * stol;
      rf_table_type const & rfs_table = rf_calculator.calculate(stol);
      for(std::size_t i_sc=0; i_sc < scatterers.size(); i_sc++)
      {
        ScattererType const& scatterer = scatterers[i_sc];
        MultipoleScattererType const& mp_scatterer = mp_scatterers[i_sc];
        int kappa_idx = mp_scatterers[i_sc].kappa_idx;
        gradients_one_h_one_scatterer<ScattererType,MultipoleScattererType>
        g_o_h_o_s(space_group,
                  h,
                  stol_sq,
                  scatterer,
                  mp_scatterer,
                  rfs_table[kappa_idx],
                  d_target_d_f_calc[i]);
        bool gf_site = scatterer.flags.grad_site();
        bool gf_u_iso = scatterer.flags.grad_u_iso() &&
                        scatterer.flags.use_u_iso();
        bool gf_u_aniso = scatterer.flags.grad_u_aniso() &&
                          scatterer.flags.use_u_aniso();
        if (gf_site)
          gr_refs.site[i_sc] += g_o_h_o_s.d_target_d_site_frac;
        if (gf_u_iso)
          gr_refs.u_iso[i_sc] += g_o_h_o_s.d_target_d_u_iso;
        if (gf_u_aniso)
          gr_refs.u_star[i_sc] += g_o_h_o_s.d_target_d_u_star;
      }
    }
    gradient_storage<ScattererType>::pack(unit_cell,scatterers);
  }

};

}} // end namespace mptbx::sf4

#endif // GUARD
