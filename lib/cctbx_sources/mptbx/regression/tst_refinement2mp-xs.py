from __future__ import division
import iotbx.pdb
import libtbx.load_env
import os
import random
import mmtbx.f_model
from scitbx.array_family import flex
from libtbx import group_args
from cctbx import xray
import scitbx.lbfgs
from cctbx import adptbx
from libtbx import adopt_init_args
import cctbx

import libtbx.phil
import mptbx
from mptbx.io.xd2006 import xd2006_params, xd_setup
from cctbx import miller

# ------------------------------------------------------------------------------

class minimizer(object):
  def __init__(self,
        xray_structure,
        xds,
        f_obs,
        max_iterations=25,
        sites = False,
        u_iso = False):
    self.xray_structure = xray_structure
    self.xds = xds
    self.f_obs = f_obs
    self.sites = sites
    self.u_iso = u_iso
    self.f_calc = None
    if(self.sites):
      self.x = xray_structure.sites_cart().as_double()
    if(self.u_iso):
      self.x = self.xray_structure.extract_u_iso_or_u_equiv()
    self.call_count = 0
    self.minimizer = scitbx.lbfgs.run(
      target_evaluator=self,
      termination_params=scitbx.lbfgs.termination_parameters(
        max_iterations=max_iterations),
      exception_handling_params=scitbx.lbfgs.exception_handling_parameters(
        ignore_line_search_failed_rounding_errors=True,
        ignore_line_search_failed_step_at_lower_bound=True,
        ignore_line_search_failed_maxfev=True))

  def compute_functional_and_gradients(self):
    if(self.sites):
      self.xray_structure.set_sites_cart(sites_cart=flex.vec3_double(self.x))
    if(self.u_iso):
      self.xray_structure.set_u_iso(values=self.x)
    sf4 = mptbx.sf4(
      self.xray_structure.unit_cell(),
      self.xray_structure.space_group(),
      self.xray_structure.scatterers(),
      self.xds.xd_data.multipole_scatterers,
      self.xds.rf_calculator,
      self.f_obs.indices())
    self.f_calc = self.f_obs.customized_copy(data=sf4.f_mpl())
    target_obj = cctbx.xray.targets_least_squares(
                  compute_scale_using_all_data=False,
                  obs_type='F',
                  obs=self.f_obs.data(),
                  weights=flex.double(self.f_obs.data().size(), 1),
                  r_free_flags=None,
                  f_calc=self.f_calc.data(),
                  derivatives_depth=1,
                  scale_factor=1.0)
    self.t = target_obj.target_work()
    d_target_d_f_calc = target_obj.gradients_work()
    if(self.sites):
      for scatterer in self.xray_structure.scatterers():
        scatterer.flags.set_grad_site(state=True)
    if(self.u_iso):
      for scatterer in self.xray_structure.scatterers():
        scatterer.flags.set_grad_u_iso(state=True)
    global gradients
    gradients = mptbx.sf4_gradients(
                self.xray_structure.unit_cell(),
                self.xray_structure.space_group(),
                self.xray_structure.scatterers(),
                self.xds.xd_data.multipole_scatterers,
                self.xds.rf_calculator,
                self.f_obs.indices(),
                d_target_d_f_calc)
    if(self.sites):
      for scatterer in self.xray_structure.scatterers():
        scatterer.flags.set_grad_site(state=False)
    if(self.u_iso):
      for scatterer in self.xray_structure.scatterers():
        scatterer.flags.set_grad_u_iso(state=False)
    self.call_count += 1
    return self.t, gradients.packed()

# ------------------------------------------------------------------------------

def r_factor(f_obs, f_calc):
  assert f_obs.indices().all_eq(f_calc.indices())
  fo = f_obs.data()
  fc = abs(f_calc).data()
  scale = flex.sum(fo*fc)/flex.sum(fc*fc)
  return flex.sum(flex.abs(fo - scale * fc)) / flex.sum(fo)

def run(adp=True, xyz=True):

#  mas_file = libtbx.env.find_in_repositories(
#    relative_path="mptbx/regression/oxa_Uiso/xd.mas",
#    test=os.path.isfile)
#  inp_file = libtbx.env.find_in_repositories(
#    relative_path="mptbx/regression/oxa_Uiso/xd.inp",
#    test=os.path.isfile)

  mas_file = libtbx.env.find_in_repositories(
    relative_path="mptbx/regression/t.mas",
    test=os.path.isfile)
  inp_file = libtbx.env.find_in_repositories(
    relative_path="mptbx/regression/t.inp",
    test=os.path.isfile)

  # mptbx objects initialization from xd files:
  setup = 'xd {mas=%s ; inp=%s ; silent=True}' % (mas_file, inp_file)
  params = libtbx.phil.parse(xd2006_params)
  params = params.fetch(source=libtbx.phil.parse(setup))
  print 'Using:' ; params.show()
  xds = xd_setup(params.extract())

  # write initial structure
  xray_structure = xds.xd_data.structure
  with file("start.pdb", 'w') as f:
    f.write(xray_structure.as_pdb_file())

  # simulate f_obs
  miller_set = miller.build_set(xds.xd_data.structure, None, 1.0)
  sf4 = mptbx.sf4(
                  xds.xd_data.structure.unit_cell(),
                  xds.xd_data.structure.space_group(),
                  xds.xd_data.structure.scatterers(),
                  xds.xd_data.multipole_scatterers,
                  xds.rf_calculator,
                  miller_set.indices())
  f_obs = miller.array(miller_set, data=flex.abs(sf4.f_mpl()))

  # simulate poor starting model
  xrs_poor = xray_structure.deep_copy_scatterers()
  if(xyz):
    xrs_poor.shake_sites_in_place(mean_distance = 0.3)
  if(adp):
    for sc in xrs_poor.scatterers():
      sc.u_iso = adptbx.b_as_u(random.choice([6,7,8,9,11,12,13]))
  with file("poor.pdb", 'w') as f:
    f.write(xrs_poor.as_pdb_file())

  # refinement loop
  fmt="  mc %3d (adp)   target: %20.15f, call_count=%3d r_mult=%6.4f r_sph=%6.4f"
  total_call_count = 0
  with file("trj.pdb", 'w') as f:
    for macro_cycle in xrange(100):
      # refine coordinates
      if(xyz):
        minimized = minimizer(xray_structure=xrs_poor, xds=xds, f_obs=f_obs, sites = True)
        total_call_count += minimized.call_count
        f_calc_sph = f_obs.structure_factors_from_scatterers(
          xray_structure = xrs_poor, algorithm = "direct").f_calc()
        print fmt % \
          (macro_cycle, minimized.t, minimized.call_count,
          r_factor(f_obs = f_obs, f_calc = minimized.f_calc),
          r_factor(f_obs = f_obs, f_calc = f_calc_sph))
      # refine ADPs
      if(adp):
        minimized = minimizer(xray_structure=xrs_poor, xds=xds, f_obs=f_obs, u_iso = True)
        f_calc_sph = f_obs.structure_factors_from_scatterers(
          xray_structure = xrs_poor, algorithm = "direct").f_calc()
        total_call_count += minimized.call_count
        print fmt % \
          (macro_cycle, minimized.t, minimized.call_count,
          r_factor(f_obs = f_obs, f_calc = minimized.f_calc),
          r_factor(f_obs = f_obs, f_calc = f_calc_sph))

      # output
      f.write(xrs_poor.as_pdb_file())
  print 'total_call_count =', total_call_count

  with file("refined.pdb", 'w') as f:
    f.write(xrs_poor.as_pdb_file())


if (__name__ == "__main__"):
  run()
