from __future__ import division
import libtbx.load_env
import os
import random
import mmtbx.f_model
from scitbx.array_family import flex
from libtbx import group_args
from cctbx import xray
import scitbx.lbfgs
from cctbx import adptbx, miller
from libtbx import adopt_init_args
import cctbx

import libtbx.phil

import mptbx
from mptbx.io.xd2006 import xd2006_params, xd_setup
import mptbx.mp4 as mp4

from mptbx.minimization.simple_macro_loop import macro_loop, \
                                                 default_macro_loop_params

# ------------------------------------------------------------------------------

def run():
  
  mas_file = libtbx.env.find_in_repositories(
    relative_path="mptbx/regression/oxa2/xd.mas",
    test=os.path.isfile)
  inp_file = libtbx.env.find_in_repositories(
    relative_path="mptbx/regression/oxa2/xd.inp",
    test=os.path.isfile)

  # mptbx objects initialization from xd files:
  setup = 'xd {mas=%s ; inp=%s ; silent=True}' % (mas_file, inp_file)
  params = libtbx.phil.parse(xd2006_params)
  params = params.fetch(source=libtbx.phil.parse(setup))
  print 'Using:' ; params.show()
  xds = xd_setup(params.extract())

  mp4_model = mp4.multipolar_model_4(
                structure = xds.xd_data.structure,
                multipole_scatterers = xds.xd_data.multipole_scatterers,
                rf_calculator = xds.rf_calculator,
                lcs_manager = xds.xd_data.lcs_manager  )

  # write initial structure
  xray_structure = xds.xd_data.structure
  with file("start.pdb", 'w') as f:
    f.write(xray_structure.as_pdb_file())

  # simulate poor starting model
  xrs_poor = xray_structure.deep_copy_scatterers()
  xrs_poor.shake_sites_in_place(mean_distance = 0.1)
  xrs_poor.shake_adp(aniso_spread=1.5, random_u_cart_scale=10.0)
  with file("poor.pdb", 'w') as f:
    f.write(xrs_poor.as_pdb_file())
  mp4_model_poor = mp4.multipolar_model_4(
                    structure = xrs_poor,
                    multipole_scatterers = xds.xd_data.multipole_scatterers,
                    rf_calculator = xds.rf_calculator,
                    lcs_manager = xds.xd_data.lcs_manager  )

  # simulate f_obs
  miller_set = miller.build_set(xds.xd_data.structure, None, 1.0)
  mp4_model.compute_structure_factors(miller_set)
  f_obs = miller.array(miller_set, data=flex.abs(mp4_model.f_mpl().data()))
  
  # get fmodel
  params = mmtbx.f_model.sf_and_grads_accuracy_master_params.extract()
  params.algorithm = "direct"
  fmodel = mmtbx.f_model.manager(
    f_obs                        = f_obs,
    xray_structure               = xrs_poor,
    sf_and_grads_accuracy_params = params,
    target_name                  = "ls_wunit_kunit")
  mp4_model_poor.compute_structure_factors(miller_set)
  fmodel.update(f_calc=mp4_model_poor.f_mpl())
  
  params = default_macro_loop_params.fetch(
             libtbx.phil.parse("""
             macro_loop {
               file_trj = "trj.pdb"
               n_cycles = 7
               }
          """))
  total_call_count = macro_loop(
               mp4_model_poor,
               fmodel,
               params.extract(),
               minimizer_kw={'scale':1.0}
               )
  
  print 'total_call_count =', total_call_count
  with file("refined.pdb", 'w') as f:
    f.write(xrs_poor.as_pdb_file())


if (__name__ == "__main__"):
  run()
