from __future__ import division
import iotbx.pdb
from iotbx import reflection_file_reader
import libtbx.load_env
import os
import mmtbx.f_model
from scitbx.array_family import flex
from libtbx import adopt_init_args

import libtbx.phil
import mptbx
from mptbx.io.xd2006 import xd2006_params, xd_setup

class multipolar_f_calc(object):
  def __init__(self, file_mas, file_inp, miller_array, silent=True):
    adopt_init_args(self, locals())
    # setup some input parameters, use defaults for others
    setup = 'xd {mas=%s ; inp=%s ; silent=%s}' % \
    (file_mas, file_inp, silent)
    params = libtbx.phil.parse(xd2006_params)
    params = params.fetch(source=libtbx.phil.parse(setup))
    if not silent:
      print 'Using:' ; params.show()
    # mptbx object initialization from xd files:
    xds = xd_setup(params.extract())
    # calculation of structure factors
    miller_index = miller_array.indices()
    self.sf4 = mptbx.sf4(
                  xds.xd_data.structure.unit_cell(),
                  xds.xd_data.structure.space_group(),
                  xds.xd_data.structure.scatterers(),
                  xds.xd_data.multipole_scatterers,
                  xds.rf_calculator,
                  miller_index)
    self.xds = xds

  def f_calc_multipolar(self):
    return self.miller_array.customized_copy(data = self.sf4.f_mpl())

  def f_calc_spherical(self):
    return self.miller_array.customized_copy(data = self.sf4.f_sph())

def run():
  if __name__ == '__main__':
    global multipolar_f_calc_object
  # get xray_structure from PDB file
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="mptbx/regression/ygg/ygg.pdb",
    test=os.path.isfile)
  xray_structure = iotbx.pdb.input(file_name = pdb_file).xray_structure_simple()
  print "Input model:"
  xray_structure.show_summary(prefix="  ")
  # read in data from MTZ file:
  data_file = libtbx.env.find_in_repositories(
    relative_path="mptbx/regression/ygg/ygg.mtz",
    test=os.path.isfile)
  miller_arrays = reflection_file_reader.any_reflection_file(file_name =
    data_file).as_miller_arrays()
  for ma in miller_arrays:
    print ma.info().labels
    if(ma.info().labels == ['F-obs', 'SIGF-obs']):
      f_obs = ma
  print "Input data:"
  f_obs.show_comprehensive_summary(prefix="  ")
  # do scaling and compute R-factors
  fmodel = mmtbx.f_model.manager(
    f_obs          = f_obs,
    xray_structure = xray_structure)
  fmodel.update_all_scales()
  print "R-factor (iam): %6.4f" % fmodel.r_work()
  # multipolar F_calc
  mas_file = libtbx.env.find_in_repositories(
    relative_path="mptbx/regression/ygg/xd_lsdb.mas",
    test=os.path.isfile)
  inp_file = libtbx.env.find_in_repositories(
    relative_path="mptbx/regression/ygg/xd_lsdb.inp",
    test=os.path.isfile)
  multipolar_f_calc_object = multipolar_f_calc(
    file_mas     = mas_file,
    file_inp     = inp_file,
    miller_array = f_obs)
  f_s = multipolar_f_calc_object.f_calc_spherical()
  f_m = multipolar_f_calc_object.f_calc_multipolar()
  assert flex.mean(abs(f_s).data()) > 0, flex.mean(abs(f_s).data())
  assert flex.mean(abs(f_m).data()) > 0, flex.mean(abs(f_m).data())
  fmodel.update(f_calc = f_m)
  fmodel.update_all_scales()
  print "R-factor (multipolar): %6.4f" % fmodel.r_work()
  assert fmodel.r_work() <= 0.0428

if (__name__ == "__main__"):
  run()
