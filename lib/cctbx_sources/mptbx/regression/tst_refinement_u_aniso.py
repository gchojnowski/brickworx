from __future__ import division
import iotbx.pdb
import libtbx.load_env
import os
import random
import mmtbx.f_model
from scitbx.array_family import flex
from libtbx import group_args
from cctbx import xray
import scitbx.lbfgs
from cctbx import adptbx
from libtbx import adopt_init_args
import cctbx

class minimizer(object):
  def __init__(self,
        fmodel,
        max_iterations=25):
    self.fmodel = fmodel
    self.fmodel.xray_structure.scatterers().flags_set_grads(state=False)
    self.x_target_functor = self.fmodel.target_functor()
    self.fmodel.xray_structure.scatterers().flags_set_grad_u_aniso(
      iselection = self.fmodel.xray_structure.use_u_aniso().iselection())
    self.fmodel.xray_structure.scatterers().flags_set_grad_u_iso(
      iselection = self.fmodel.xray_structure.use_u_iso().iselection())
    self.x = flex.double(self.fmodel.xray_structure.n_parameters(), 0)
    self._scatterers_start = self.fmodel.xray_structure.scatterers()
    self.call_count = 0
    self.minimizer = scitbx.lbfgs.run(
      target_evaluator=self,
      termination_params=scitbx.lbfgs.termination_parameters(
        max_iterations=max_iterations),
      exception_handling_params=scitbx.lbfgs.exception_handling_parameters(
        ignore_line_search_failed_rounding_errors=True,
        ignore_line_search_failed_step_at_lower_bound=True,
        ignore_line_search_failed_maxfev=True))
    self.fmodel.xray_structure.tidy_us()
    self.apply_shifts()
    del self._scatterers_start
    self.fmodel.update_xray_structure(
      xray_structure = self.fmodel.xray_structure,
      update_f_calc  = True)

  def apply_shifts(self):
    apply_shifts_result = xray.ext.minimization_apply_shifts(
      unit_cell      = self.fmodel.xray_structure.unit_cell(),
      scatterers     = self._scatterers_start,
      shifts         = self.x)
    scatterers_shifted = apply_shifts_result.shifted_scatterers
    self.fmodel.xray_structure.replace_scatterers(
      scatterers = scatterers_shifted)
    self.fmodel.update_xray_structure(
      xray_structure = self.fmodel.xray_structure,
      update_f_calc  = True)

  def compute_functional_and_gradients(self):
    self.apply_shifts()
    tgx = func(fmodel=self.fmodel)
    f = tgx.target_work()
    g = tgx.grads_u_anisos()
    xray.minimization.add_gradients(
      scatterers        = self.fmodel.xray_structure.scatterers(),
      xray_gradients    = g)
    self.call_count += 1
    return f, g

class func(object):
  def __init__(self, fmodel):
    adopt_init_args(self, locals())
    weights = flex.double(self.fmodel.f_obs().data().size(), 1.0)
    self.core = xray.target_functors.least_squares(
      compute_scale_using_all_data = False,
      f_obs                        = self.fmodel.f_obs(),
      r_free_flags                 = self.fmodel.r_free_flags(),
      weights                      = weights,
      scale_factor                 = 1)
    self.r = self.core(f_calc = self.fmodel.f_model(), compute_gradients=True)
    self.d_target_d_f_calc = self.r.gradients_work() # XXX needs scales
    self.ge = cctbx.xray.structure_factors.gradients(
      miller_set = self.fmodel.f_obs())

  def target_work(self):
    return self.r.target_work()

  def grads_u_anisos(self):
    return self.ge(
      u_iso_refinable_params = None,
      d_target_d_f_calc      = self.d_target_d_f_calc,
      xray_structure         = self.fmodel.xray_structure,
      n_parameters           = self.fmodel.xray_structure.n_parameters(),
      miller_set             = self.fmodel.f_obs_work(),
      algorithm              = "direct").packed()

def get_inputs(pdb_file_name):
  pdb_inp = iotbx.pdb.input(file_name = pdb_file_name)
  return group_args(
    pdb_hierarchy  = pdb_inp.construct_hierarchy(),
    xray_structure = pdb_inp.xray_structure_simple())

def run():
  # get xray_structure from PDB file
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="mptbx/regression/phe_aniso.pdb",
    test=os.path.isfile)
  inp = get_inputs(pdb_file_name = pdb_file)
  if(1):
    inp.pdb_hierarchy.adopt_xray_structure(inp.xray_structure)
    inp.pdb_hierarchy.write_pdb_file(file_name="start.pdb")
  # simulate poor starting model
  xrs_poor = inp.xray_structure.deep_copy_scatterers()
  xrs_poor.shake_adp(aniso_spread=1.5, random_u_cart_scale=10.0)
  if(1):
    inp.pdb_hierarchy.adopt_xray_structure(xrs_poor)
    inp.pdb_hierarchy.write_pdb_file(file_name="poor.pdb")
  # simulate Fobs
  f_obs = abs(inp.xray_structure.structure_factors(
    d_min = 1.0,
    algorithm="direct").f_calc())
  r_free_flags = f_obs.generate_r_free_flags()
  # get fmodel
  params = mmtbx.f_model.sf_and_grads_accuracy_master_params.extract()
  params.algorithm = "direct"
  fmodel = mmtbx.f_model.manager(
    f_obs                        = f_obs,
    r_free_flags                 = r_free_flags,
    xray_structure               = xrs_poor,
    sf_and_grads_accuracy_params = params,
    target_name                  = "ls_wunit_kunit")
  # refinement loop
  print "start r_factor: %6.4f" % fmodel.r_work()
  total_call_count = 0
  for macro_cycle in xrange(10):
    minimized = minimizer(fmodel = fmodel)
    total_call_count += minimized.call_count
    print "  macro_cycle %3d (adp)   r_factor: %6.4f  call_count=%d" % \
    (macro_cycle, fmodel.r_work(), minimized.call_count)
  print 'total_call_count =', total_call_count
  if(1):
    inp.pdb_hierarchy.adopt_xray_structure(fmodel.xray_structure)
    inp.pdb_hierarchy.write_pdb_file(file_name="refined.pdb")

if (__name__ == "__main__"):
  run()
