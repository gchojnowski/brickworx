from __future__ import division
import iotbx.pdb
import libtbx.load_env
import os
import random
import mmtbx.f_model
from scitbx.array_family import flex
from libtbx import group_args
from cctbx import xray
import scitbx.lbfgs
from cctbx import adptbx
from libtbx import adopt_init_args
import cctbx

import libtbx.phil
import mptbx
from mptbx.io.xd2006 import xd2006_params, xd_setup
from cctbx import miller

# ------------------------------------------------------------------------------

class minimizer(object):
  def __init__(self,
        fmodel,
        xds,
        max_iterations=100,
        sites = False,
        u_iso = False):
    self.fmodel = fmodel
    self.xds = xds
    self.fmodel.xray_structure.scatterers().flags_set_grads(state=False)
    self.x_target_functor = self.fmodel.target_functor()
    self.sites = sites
    self.u_iso = u_iso
    
    if(self.sites):
      self.x = self.fmodel.xray_structure.sites_cart().as_double()
    if(self.u_iso):
      assert self.fmodel.xray_structure.scatterers().size() == \
        self.fmodel.xray_structure.use_u_iso().count(True)
      self.x = self.fmodel.xray_structure.extract_u_iso_or_u_equiv()
    
    if(self.sites):
      xray.set_scatterer_grad_flags(
        scatterers = self.fmodel.xray_structure.scatterers(),
        site       = True)
    if(self.u_iso):
      sel = flex.bool(
        self.fmodel.xray_structure.scatterers().size(), True).iselection()
      self.fmodel.xray_structure.scatterers().flags_set_grad_u_iso(
        iselection = sel)
    
    self.minimizer = scitbx.lbfgs.run(
      target_evaluator=self,
      termination_params=scitbx.lbfgs.termination_parameters(
        max_iterations=max_iterations),
      exception_handling_params=scitbx.lbfgs.exception_handling_parameters(
        ignore_line_search_failed_rounding_errors=True,
        ignore_line_search_failed_step_at_lower_bound=True,
        ignore_line_search_failed_maxfev=True))
    
    self.fmodel.xray_structure.tidy_us()
    self.fmodel.xray_structure.apply_symmetry_sites()
    self.fmodel.update_xray_structure(
      xray_structure = self.fmodel.xray_structure,
      update_f_calc  = False)
    sf4 = mptbx.sf4(
                  self.fmodel.xray_structure.unit_cell(),
                  self.fmodel.xray_structure.space_group(),
                  self.fmodel.xray_structure.scatterers(),
                  self.xds.xd_data.multipole_scatterers,
                  self.xds.rf_calculator,
                  self.fmodel.f_obs().indices())
    f_calc = self.fmodel.f_obs().customized_copy(data = sf4.f_mpl())
    self.fmodel.update(f_calc=f_calc)
    

  def compute_functional_and_gradients(self):
    if(self.sites):
      self.fmodel.xray_structure.set_sites_cart(
        sites_cart = flex.vec3_double(self.x))
    if(self.u_iso):
      self.fmodel.xray_structure.set_u_iso(values = self.x)

    self.fmodel.update_xray_structure(
      xray_structure = self.fmodel.xray_structure,
      update_f_calc  = False)
    sf4 = mptbx.sf4(
                  self.fmodel.xray_structure.unit_cell(),
                  self.fmodel.xray_structure.space_group(),
                  self.fmodel.xray_structure.scatterers(),
                  self.xds.xd_data.multipole_scatterers,
                  self.xds.rf_calculator,
                  self.fmodel.f_obs().indices())
    f_calc = self.fmodel.f_obs().customized_copy(data = sf4.f_mpl())
    self.fmodel.update(f_calc=f_calc)
    
    tgx = func(fmodel=self.fmodel, xds=self.xds, f_calc=f_calc)
    
    if(self.sites):
      f = tgx.target_work()
      g = tgx.grads_sites()
    if(self.u_iso):
      f = tgx.target_work()
      g = tgx.grads_u_isos()
      
#    print f, len(g), len(self.x)
      
    return f, g.as_double()

# ------------------------------------------------------------------------------

class func(object):
  def __init__(self, fmodel, xds, f_calc):
    adopt_init_args(self, locals())
    weights = flex.double(self.fmodel.f_obs().data().size(), 1.0)
    self.core = xray.target_functors.least_squares(
      compute_scale_using_all_data = False,
      f_obs                        = self.fmodel.f_obs(),
      r_free_flags                 = self.fmodel.r_free_flags(),
      weights                      = weights,
      scale_factor                 = 1)
    self.r = self.core(f_calc = f_calc, compute_gradients=True)
    self.d_target_d_f_calc = self.r.gradients_work() 
    self.gradients = mptbx.sf4_gradients(
                self.fmodel.xray_structure.unit_cell(), 
                self.fmodel.xray_structure.space_group(),
                self.fmodel.xray_structure.scatterers(), 
                self.xds.xd_data.multipole_scatterers,
                self.xds.rf_calculator,
                self.fmodel.f_obs().indices(),
                self.d_target_d_f_calc)

  def target_work(self):
    return self.r.target_work()

  def grads_u_isos(self):
    return self.gradients.packed()

  def grads_sites(self):
    return flex.vec3_double(self.gradients.packed())

# ------------------------------------------------------------------------------

def run():
  if (__name__ == "__main__"):
    global f_obs,fmodel

  mas_file = libtbx.env.find_in_repositories(
    relative_path="mptbx/regression/t.mas",
    test=os.path.isfile)
  inp_file = libtbx.env.find_in_repositories(
    relative_path="mptbx/regression/t.inp",
    test=os.path.isfile)

  # mptbx objects initialization from xd files:
  setup = 'xd {mas=%s ; inp=%s}' % (mas_file, inp_file)
  params = libtbx.phil.parse(xd2006_params)
  params = params.fetch(source=libtbx.phil.parse(setup))
  print 'Using:' ; params.show()
  xds = xd_setup(params.extract())
  
  xray_structure = xds.xd_data.structure
  with file("start.pdb", 'w') as f:
    f.write(xray_structure.as_pdb_file())
    
  # simulate poor starting model
  xrs_poor = xray_structure.deep_copy_scatterers()
  xrs_poor.shake_sites_in_place(mean_distance = 0.3)
  for sc in xrs_poor.scatterers():
    sc.u_iso = adptbx.b_as_u(random.choice([7,8,9,11,12,13]))
  with file("poor.pdb", 'w') as f:
    f.write(xrs_poor.as_pdb_file())

  # simulate f_obs
  miller_set = miller.build_set(xds.xd_data.structure, None, 1.0)
  sf4 = mptbx.sf4(
                  xds.xd_data.structure.unit_cell(),
                  xds.xd_data.structure.space_group(),
                  xds.xd_data.structure.scatterers(),
                  xds.xd_data.multipole_scatterers,
                  xds.rf_calculator,
                  miller_set.indices())
  f_obs = miller.array(miller_set, data=flex.abs(sf4.f_mpl())**2)
#  r_free_flags = f_obs.generate_r_free_flags()
  
  # get fmodel
  fmodel = mmtbx.f_model.manager(
    f_obs                        = f_obs,
#    r_free_flags                 = r_free_flags,
    xray_structure               = xrs_poor,
    target_name                  = "ls_wunit_kunit")
  
  # get initial f_calc
  sf4 = mptbx.sf4(
                  xds.xd_data.structure.unit_cell(),
                  xds.xd_data.structure.space_group(),
                  xds.xd_data.structure.scatterers(),
                  xds.xd_data.multipole_scatterers,
                  xds.rf_calculator,
                  miller_set.indices())
  f_calc = miller.array(miller_set, data=sf4.f_mpl())
  
  print len(list(f_obs))
  print len(list(f_calc))
  
  
  fmodel.update(f_calc=f_calc)
  #fmodel.update_all_scales()
  print "start r_factor: %6.4f" % fmodel.r_work()
    
  with file("trj.pdb", 'w') as f:
  # refinement loop
    for macro_cycle in xrange(20):
      # refine coordinates
      minimized = minimizer(fmodel = fmodel, xds=xds, sites = True)
      print "  macro_cycle %3d (sites) r_factor: %6.4f" % \
            (macro_cycle, fmodel.r_work())
      # refine ADPs
      minimized = minimizer(fmodel = fmodel, xds=xds, u_iso = True)
      print "  macro_cycle %3d (adp)   r_factor: %6.4f" % \
            (macro_cycle, fmodel.r_work())
      f.write(fmodel.xray_structure.as_pdb_file())

  with file("refined.pdb", 'w') as f:
    f.write(fmodel.xray_structure.as_pdb_file())


if (__name__ == "__main__"):
  run()
