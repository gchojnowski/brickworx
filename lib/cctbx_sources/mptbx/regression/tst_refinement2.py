from __future__ import division
import iotbx.pdb
import libtbx.load_env
import os
import random
import mmtbx.f_model
from scitbx.array_family import flex
from libtbx import group_args
from cctbx import xray
import scitbx.lbfgs
from cctbx import adptbx
from libtbx import adopt_init_args
import cctbx

class minimizer(object):
  def __init__(self,
        fmodel,
        max_iterations=100,
        sites = False,
        u_iso = False):
    self.fmodel = fmodel
    self.fmodel.xray_structure.scatterers().flags_set_grads(state=False)
    self.x_target_functor = self.fmodel.target_functor()
    self.sites = sites
    self.u_iso = u_iso
    if(self.sites):
      self.x = self.fmodel.xray_structure.sites_cart().as_double()
    if(self.u_iso):
      assert self.fmodel.xray_structure.scatterers().size() == \
        self.fmodel.xray_structure.use_u_iso().count(True)
      self.x = self.fmodel.xray_structure.extract_u_iso_or_u_equiv()
    if(self.sites):
      xray.set_scatterer_grad_flags(
        scatterers = self.fmodel.xray_structure.scatterers(),
        site       = True)
    if(self.u_iso):
      sel = flex.bool(
        self.fmodel.xray_structure.scatterers().size(), True).iselection()
      self.fmodel.xray_structure.scatterers().flags_set_grad_u_iso(
        iselection = sel)
    self.call_count = 0
    self.minimizer = scitbx.lbfgs.run(
      target_evaluator=self,
      termination_params=scitbx.lbfgs.termination_parameters(
        max_iterations=max_iterations),
      exception_handling_params=scitbx.lbfgs.exception_handling_parameters(
        ignore_line_search_failed_rounding_errors=True,
        ignore_line_search_failed_step_at_lower_bound=True,
        ignore_line_search_failed_maxfev=True))
    self.fmodel.xray_structure.tidy_us()
    self.fmodel.xray_structure.apply_symmetry_sites()
    self.fmodel.update_xray_structure(
      xray_structure = self.fmodel.xray_structure,
      update_f_calc  = True)

  def compute_functional_and_gradients(self):
    if(self.sites):
      self.fmodel.xray_structure.set_sites_cart(
        sites_cart = flex.vec3_double(self.x))
    if(self.u_iso):
      self.fmodel.xray_structure.set_u_iso(values = self.x)
    self.fmodel.update_xray_structure(
      xray_structure = self.fmodel.xray_structure,
      update_f_calc  = True)
    tgx = func(fmodel=self.fmodel)
    if(self.sites):
      f = tgx.target_work()
      g = tgx.grads_sites()
    if(self.u_iso):
      f = tgx.target_work()
      g = tgx.grads_u_isos()
    self.call_count += 1 
    return f, g.as_double()

class func(object):
  def __init__(self, fmodel):
    adopt_init_args(self, locals())
    weights = flex.double(self.fmodel.f_obs().data().size(), 1.0)
    self.core = xray.target_functors.least_squares(
      compute_scale_using_all_data = False,
      f_obs                        = self.fmodel.f_obs(),
      r_free_flags                 = self.fmodel.r_free_flags(),
      weights                      = weights,
      scale_factor                 = 1)
    self.r = self.core(f_calc = self.fmodel.f_model(), compute_gradients=True)
    self.d_target_d_f_calc = self.r.gradients_work() # XXX needs scales
    self.ge = cctbx.xray.structure_factors.gradients(
      miller_set = self.fmodel.f_obs())

  def target_work(self):
    return self.r.target_work()

  def grads_u_isos(self):
    return self.ge(
      u_iso_refinable_params = None,
      d_target_d_f_calc      = self.d_target_d_f_calc,
      xray_structure         = self.fmodel.xray_structure,
      n_parameters           = 0,
      miller_set             = self.fmodel.f_obs_work(),
      algorithm              = "direct").d_target_d_u_iso()

  def grads_sites(self):
    xray.set_scatterer_grad_flags(
      scatterers = self.fmodel.xray_structure.scatterers(),
      site=True)
    result = self.ge(
        u_iso_refinable_params = None,
        d_target_d_f_calc      = self.d_target_d_f_calc,
        xray_structure         = self.fmodel.xray_structure,
        n_parameters           = self.fmodel.xray_structure.n_parameters(),
        miller_set             = self.fmodel.f_obs_work(),
        algorithm              = "direct")
    return flex.vec3_double(result.packed())

def get_inputs(pdb_file_name):
  pdb_inp = iotbx.pdb.input(file_name = pdb_file_name)
  return group_args(
    pdb_hierarchy  = pdb_inp.construct_hierarchy(),
    xray_structure = pdb_inp.xray_structure_simple())

def run():
  # get xray_structure from PDB file
  pdb_file = libtbx.env.find_in_repositories(
    relative_path="mptbx/regression/t.pdb",
    test=os.path.isfile)
  inp = get_inputs(pdb_file_name = pdb_file)
  if(1):
    inp.pdb_hierarchy.adopt_xray_structure(inp.xray_structure)
    inp.pdb_hierarchy.write_pdb_file(file_name="start.pdb")
  # simulate poor starting model
  xrs_poor = inp.xray_structure.deep_copy_scatterers()
  xrs_poor.shake_sites_in_place(mean_distance = 0.3)
  for sc in xrs_poor.scatterers():
    sc.u_iso = adptbx.b_as_u(random.choice([7,8,9,11,12,13]))
  if(1):
    inp.pdb_hierarchy.adopt_xray_structure(xrs_poor)
    inp.pdb_hierarchy.write_pdb_file(file_name="poor.pdb")
  # simulate Fobs
  f_obs = abs(inp.xray_structure.structure_factors(
    d_min = 1.0,
    algorithm="direct").f_calc())
  r_free_flags = f_obs.generate_r_free_flags()
  # get fmodel
  params = mmtbx.f_model.sf_and_grads_accuracy_master_params.extract()
  params.algorithm = "direct"
  fmodel = mmtbx.f_model.manager(
    f_obs                        = f_obs,
    r_free_flags                 = r_free_flags,
    xray_structure               = xrs_poor,
    sf_and_grads_accuracy_params = params,
    target_name                  = "ls_wunit_kunit")
  # refinement loop
  print "start r_factor: %6.4f" % fmodel.r_work()
  total_call_count = 0
  for macro_cycle in xrange(100):
    # refine coordinates
    minimized = minimizer(fmodel = fmodel, sites = True)
    total_call_count += minimized.call_count
    print "  macro_cycle %3d (sites) r_factor: %6.4f  call_count=%d" % \
    (macro_cycle, fmodel.r_work(), minimized.call_count)
    # refine ADPs
    minimized = minimizer(fmodel = fmodel, u_iso = True)
    total_call_count += minimized.call_count
    print "  macro_cycle %3d (adp)   r_factor: %6.4f  call_count=%d" % \
    (macro_cycle, fmodel.r_work(), minimized.call_count)
  print 'total_call_count =', total_call_count
  if(1):
    inp.pdb_hierarchy.adopt_xray_structure(fmodel.xray_structure)
    inp.pdb_hierarchy.write_pdb_file(file_name="refined.pdb")

if (__name__ == "__main__"):
  run()
