from __future__ import division

import mptbx.io.xd2006

def collect_rf_tables(f_name_list, skip_bank=False):
  tables = {}
  for f_name in f_name_list:
    table = mptbx.io.xd2006.get_tables(f_name, skip_bank)
    tables.update(table)
  return tables

# ==============================================================================

if (__name__ == "__main__") :
  import optparse, pprint
  usage = \
  "usage: %prog --grd=<grid spacing> xd_lsm.out__test1 xd_lsm.out__test2..." +\
  "\n(try also --help)"
  parser = optparse.OptionParser(usage=usage)
  parser.add_option("-g", "--grd", type="float", dest="grd",
                  help="Grid spacing for tabulated radial function. " +
                       "XD2006v5.36's compile-time default is 0.05.")
  (options, args) = parser.parse_args()
  if len(args) < 1:
    parser.error("At least one name of output file produced by xdlsm run " +
                 "with test option enabled is a required argument.")
  if options.grd is None:
    parser.error("Grid spacing must be provided.")
  grd = options.grd

  tables = collect_rf_tables(args)

  print 'grd = %f' % grd
  print 'tables = \\'
  pp = pprint.PrettyPrinter(indent=2, width=80)
  pp.pprint(tables)

