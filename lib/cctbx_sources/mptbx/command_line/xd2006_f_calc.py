from __future__ import division

from cctbx import miller
from iotbx.reflection_file_reader import any_reflection_file
import libtbx.phil
from libtbx.utils import Sorry

import mptbx
from mptbx.io.xd2006 import xd2006_params, xd_setup

# ------------------------------------------------------------------------------

fobs_params = """

d_min = None
  .type = float
  .help = "d_min cutoff for auto generation of miller indices"

mtz_fobs = None
  .type = path

"""

local_params = """

output {
  mtz_mpl = xd_mpl.mtz
    .type = path
  mtz_sph = xd_sph.mtz
    .type = path
  mtz_def = xd_def.mtz
    .type = path
}

"""

# ------------------------------------------------------------------------------

def read_fobs_mtz(f_name, label=""):
  miller_arrays = any_reflection_file(file_name = f_name).as_miller_arrays()
  for ma in miller_arrays:
    if ma.info().labels == ['I-obs', 'SIGI-obs'] or \
       ma.info().labels == ['IOBS', 'SIGIOBS']:
      f_obs = ma.f_sq_as_f()
      break
    if ma.info().labels == ['F-obs', 'SIGF-obs']:
      f_obs = ma
      break
  else:
    msg = "No miller array with 'I-obs' or 'F-obs' labels found in %s." % f_name
    raise Sorry(msg)
  return f_obs

# ------------------------------------------------------------------------------

def xd_setup_with_f_obs(params, mtz_fobs=None, d_min=None):
  if sum(map(bool, (params.xd.hkl, mtz_fobs, d_min))) != 1:
    raise Sorry('Pleas specify one of: xd.hkl, mtz_fobs or d_min')
  xds = xd_setup(params)
  if params.xd.hkl:
    ma = xds.miller_array
  elif d_min:
    ma = miller.build_set(xds.xd_data.structure, None, params.d_min)
  else:
    ma = read_fobs_mtz(mtz_fobs)
    cs = xds.xd_data.structure.crystal_symmetry()
    if not cs.is_similar_symmetry(ma.crystal_symmetry()):
      raise Sorry, 'Symmetry mismatch between structure (xd) and f-obs (mtz).'
  return xds, ma

# ------------------------------------------------------------------------------

def setup(args):
  master_params = libtbx.phil.parse(xd2006_params+fobs_params+local_params)
  argument_interpreter = master_params.command_line_argument_interpreter()
  phil_objects = argument_interpreter.process_args(args)
  params = master_params.fetch(sources=phil_objects)
  params_e = params.extract()
  if not params_e.xd.silent:
    print 'Using:' ; params.show()
  params = params_e
  xds, miller_array = xd_setup_with_f_obs(params, params.mtz_fobs, params.d_min)
  miller_index = miller_array.indices()
  sf4 = mptbx.sf4(
                xds.xd_data.structure.unit_cell(),
                xds.xd_data.structure.space_group(),
                xds.xd_data.structure.scatterers(),
                xds.xd_data.multipole_scatterers,
                xds.rf_calculator,
                miller_index)
  return params, xds, miller_array, miller_index, sf4

# ------------------------------------------------------------------------------

_output = [
  ( 'mtz_mpl', lambda sf4: sf4.f_mpl() ),
  ( 'mtz_sph', lambda sf4: sf4.f_sph() ),
  ( 'mtz_def', lambda sf4: sf4.f_mpl() - sf4.f_sph() ),
]

def output(params, miller_array, sf4):
  for k, f_ in _output:
    f_name = getattr(params.output, k)
    if f_name:
      f = f_(sf4)
      miller_array = miller.array(miller_array, f)
      mtz_dataset = miller_array.as_mtz_dataset(column_root_label="f_calc")
      mtz_object = mtz_dataset.mtz_object()
      mtz_object.write(file_name = f_name)

# ------------------------------------------------------------------------------

def run(args):
  params, xds, miller_array, miller_index, sf4 = setup(args)
  output(params, miller_array, sf4)
  return params, xds, miller_array, miller_index, sf4

# ==============================================================================

if (__name__ == "__main__") :
  import sys
  params, xds, miller_array, miller_index, sf4 = run(sys.argv[1:])

