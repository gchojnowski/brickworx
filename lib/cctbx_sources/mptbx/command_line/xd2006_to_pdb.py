from __future__ import division

import libtbx.phil

from mptbx.io.xd2006 import xd2006_params, xd_setup

# ------------------------------------------------------------------------------

def run(args):
  master_params = libtbx.phil.parse(xd2006_params)
  argument_interpreter = master_params.command_line_argument_interpreter()
  params = master_params.fetch(source=libtbx.phil.parse('xd.silent=True'))
  phil_objects = argument_interpreter.process_args(args)
  params = params.fetch(sources=phil_objects).extract()
  xds = xd_setup(params)
  for i,s in enumerate(xds.xd_data.structure.scatterers()):
    s.label = s.label.replace('(','').replace(')','')
  print xds.xd_data.structure.as_pdb_file()

# ==============================================================================

if (__name__ == "__main__") :
  import sys
  params = run(sys.argv[1:])

