#ifndef MPTBX_XD2006_HPP
#define MPTBX_XD2006_HPP

#include <scitbx/array_family/shared.h>
#include <scitbx/array_family/boost_python/shared_wrapper.h>
#include <scitbx/mat3.h>

/*****************************************************************************/
/* This module contains functions adapted from XD2006 version 5.36.          */
/* The functions were modified so that all data is passed through arguments. */
/*****************************************************************************/

namespace mptbx {
namespace xd2006 {

namespace af = scitbx::af;

// -----------------------------------------------------------------------------

/* interpolation for radial functions */
template <typename FloatType>
void inter(FloatType const  sinthl,
           FloatType const *fz,
           FloatType const  kappa,
           int const        l,
           FloatType const  grd,
           int const       mgrd,
           FloatType       &f,
           FloatType       &df)
{
  FloatType fs;
  int is;
  FloatType xkl, zetd, stlz;
  --fz;
  stlz = sinthl / kappa / grd + 1;
  is = static_cast<int>(stlz);              // TO CHECK: rounding correctness
  if (is > mgrd - 2)
  {
    f = fz[mgrd];
    df = 0.0;
  }
  else
  {
    fs = stlz - is;
    zetd = -(sinthl) / kappa / kappa;
    if (fs < 1.0e-6)
    {
      f = fz[is];
      df = zetd * (fz[is + 1] * 2. - fz[is] * 1.5 - fz[is + 2] * .5) / grd;
    }
    else
    {
      f = fz[is] * (1. - fs) + fz[is + 1] * fs + fs * .5 * (fs - 1.) *
           (fz[is + 2] - fz[is + 1] * 2. + fz[is]);
      df = zetd * ((fs - 1.5) * fz[is] + (1. - fs) * 2. * fz[is + 1] +
           (fs - .5) * fz[is + 2]) / grd;
    }
  }
  if (l > 0)
  {
    xkl = 1. / pow(kappa, l);
    f *= xkl;
    df = df * xkl - l / kappa * f;
  }
}

// -----------------------------------------------------------------------------

template <typename FloatType>
void prod(FloatType *r, int const lmax)
{
  --r;
  if (lmax <= 1) return;
  r[4] = r[1] * r[1];
  r[5] = r[2] * r[2];
  r[6] = r[3] * r[3];
  r[7] = r[1] * r[2];
  r[8] = r[1] * r[3];
  r[9] = r[2] * r[3];
  if (lmax == 2) return;
  r[10] = r[4] * r[1];
  r[11] = r[5] * r[2];
  r[12] = r[6] * r[3];
  r[13] = r[4] * r[2];
  r[14] = r[5] * r[1];
  r[15] = r[4] * r[3];
  r[16] = r[6] * r[1];
  r[17] = r[5] * r[3];
  r[18] = r[6] * r[2];
  r[19] = r[7] * r[3];
  if (lmax == 3)  return;
  r[20] = r[10] * r[1];
  r[21] = r[11] * r[2];
  r[22] = r[12] * r[3];
  r[23] = r[10] * r[2];
  r[24] = r[11] * r[1];
  r[25] = r[10] * r[3];
  r[26] = r[12] * r[1];
  r[27] = r[11] * r[3];
  r[28] = r[12] * r[2];
  r[29] = r[13] * r[2];
  r[30] = r[15] * r[3];
  r[31] = r[17] * r[3];
  r[32] = r[19] * r[1];
  r[33] = r[19] * r[2];
  r[34] = r[19] * r[3];
}

// -----------------------------------------------------------------------------

template <typename FloatType>
void angular(FloatType const *h,
             FloatType const *lcs,
             int const        lmax,
             FloatType       *r,
             FloatType       *ang)
{
  static FloatType a[12] = { 4., -2.598076, -9.424778, -3.076923, -4.025424,
  -12.566371, -5.333333, 0.872326, 5.956491, 4.154333, 15.707963, 5.890486};

  int i;
  --ang; --r; --lcs; --h;

  r[1] = h[1] * lcs[1] + h[2] * lcs[2] + h[3] * lcs[3];
  r[2] = h[1] * lcs[4] + h[2] * lcs[5] + h[3] * lcs[6];
  r[3] = h[1] * lcs[7] + h[2] * lcs[8] + h[3] * lcs[9];
  prod(&r[1], lmax);

  // dipoles
  for (i = 1; i <= 3; ++i) ang[i] = r[i] * a[0];
  if (lmax == 1) return;
  // quadrupoles
  ang[4] = (r[6] + r[6] - r[4] - r[5]) * a[1];
  ang[5] = r[8] * a[2];
  ang[6] = r[9] * a[2];
  ang[7] = a[2] * (r[4] - r[5]) / 2.;
  ang[8] = r[7] * a[2];
  if (lmax == 2) return;
  // octapoles
  ang[9] = (r[12] * 2. - (r[15] + r[17]) * 3.) * a[3];
  ang[10] = (r[16] * 4. - r[10] - r[14]) * a[4];
  ang[11] = (r[18] * 4. - r[11] - r[13]) * a[4];
  ang[12] = (r[15] - r[17]) * a[5];
  ang[13] = (r[19] + r[19]) * a[5];
  ang[14] = (r[10] - r[14] * 3.) * a[6];
  ang[15] = (r[13] * 3. - r[11]) * a[6];
  if (lmax == 3) return;
  // hexadecapoles
  ang[16] = ((r[20] + r[21] + r[29] * 2. - (r[30] + r[31]) * 8.) *
            3. + r[22] * 8.) * a[7];
  ang[17] = (r[26] * 4. - (r[25] + r[33]) * 3.) * a[8];
  ang[18] = (r[28] * 4. - (r[27] + r[32]) * 3.) * a[8];
  ang[19] = ((r[30] - r[31]) * 6. + r[21] - r[20]) * a[9];
  ang[20] = (r[34] * 12. - r[23] * 2. - r[24] * 2.) * a[9];
  ang[21] = (r[25] - r[33] * 3.) * a[10];
  ang[22] = (r[32] * 3. - r[27]) * a[10];
  ang[23] = (r[20] + r[21] - r[29] * 6.) * a[11];
  ang[24] = (r[23] - r[24]) * 4. * a[11];
}

// -----------------------------------------------------------------------------

template <typename FloatType>
void mulpol(FloatType const *rfs,
            FloatType const *pop,
            FloatType const *ang,
            int const       lmax,
            FloatType       &avj,
            FloatType       &bvj,
            FloatType       &sph)
{
  int l, m, m1, m2;
  FloatType fia, popng, xapm, xappop;

  --ang; --pop; --rfs;

  avj = pop[1] * rfs[2] + pop[2] * rfs[3];
  bvj = 0.0;
  sph = avj;

  if (lmax == 0) return;

  xappop = 1.0;
  xapm = 0.0;
  for (l = 1; l <= lmax; ++l)
  {
    m1 = l * l;
    m2 = m1 + l + l;
    popng = 0.0;
    for (m = m1; m <= m2; ++m)
    {
      popng += pop[m + 2] * ang[m];
    }
    fia = popng * rfs[l + 3];
    avj += fia * xapm;
    bvj += fia * xappop;
    xapm = xappop;
    xappop = 1.0 - xappop;
  }
}

// =============================================================================

}} // end mptbx::xd2006 namespace

#endif // GUARD
