#ifndef MPTBX_SF4_HPP
#define MPTBX_SF4_HPP

#include <boost/mpl/assert.hpp>
#include <boost/type_traits.hpp>

#include <cctbx/sgtbx/miller_ops.h>
#include <cctbx/sgtbx/space_group.h>
#include <cctbx/xray/scatterer.h>

#include <mptbx/model.hpp>
#include <mptbx/xd2006.hpp>
#include <mptbx/radial_functions.hpp>

namespace mptbx {
namespace sf4 {

namespace af = scitbx::af;

using cctbx::adptbx::debye_waller_factor_u_iso;
using cctbx::adptbx::debye_waller_factor_u_star;

// -----------------------------------------------------------------------------

template <typename ScattererType, typename MultipoleScattererType>
struct one_h_one_scatterer
{
  typedef typename ScattererType::float_type f_t;
  typedef typename MultipoleScattererType::float_type mp_float_type;
  BOOST_MPL_ASSERT((boost::is_same<f_t,mp_float_type >));
  typedef std::complex<f_t> c_t;

  one_h_one_scatterer(
    cctbx::sgtbx::space_group const&     space_group,
    cctbx::miller::index<> const&        h,
    f_t const&                           stol_sq,
    ScattererType const&                 scatterer,
    MultipoleScattererType const&        mp_scatterer,
    rf_values_set<4,f_t> const&          rfs)
  :
    f_mpl(0,0), f_sph(0,0)
  {
    using scitbx::constants::two_pi;
    af::tiny<f_t,34> prod;
    af::tiny<f_t,24> ang;
    f_t a, b, sph;

    f_t f_core = rfs[0];
    c_t f_fpfdp(scatterer.fp, scatterer.fdp);

    f_t dw_iso(1.0);
    if (scatterer.flags.use_u_iso())
      dw_iso = debye_waller_factor_u_iso(stol_sq, scatterer.u_iso);

    for(std::size_t i_smx=0; i_smx < space_group.order_z(); i_smx++)
    {
      cctbx::sgtbx::rt_mx s = space_group(i_smx);
      cctbx::miller::index<> hr = h * s.r();
      f_t hrx = hr * scatterer.site;
      f_t ht = f_t(h * s.t()) / space_group.t_den();
      f_t phase = two_pi * (hrx + ht);
      c_t e_j_phase(std::cos(phase), std::sin(phase));

      f_t dw_u_star(1.0);
      if (scatterer.flags.use_u_aniso())
        dw_u_star = debye_waller_factor_u_star(hr, scatterer.u_star);
      f_t dw = dw_iso * dw_u_star;

      cctbx::miller::index<f_t> hr__f_t(hr);
      mptbx::xd2006::angular(hr__f_t.begin(),
                             mp_scatterer.lcs.begin(),
                             mp_scatterer.lmax,
                             prod.begin(),
                             ang.begin());
      mptbx::xd2006::mulpol(rfs.begin(),
                            mp_scatterer.pop.begin(),
                            ang.begin(),
                            mp_scatterer.lmax,
                            a, b, sph);
      c_t f_angular(a,b);
      c_t mpl_ = (f_core + f_fpfdp + f_angular) * mp_scatterer.amult;
      c_t sph_ = (f_core + f_fpfdp + sph) * mp_scatterer.amult;
      f_mpl += e_j_phase * dw * mpl_;
      f_sph += e_j_phase * dw * sph_;
    }
  }

  c_t f_mpl;
  c_t f_sph;
};

// -----------------------------------------------------------------------------

template <typename ScattererType, typename MultipoleScattererType>
struct one_h
{
  typedef typename ScattererType::float_type f_t;
  typedef typename MultipoleScattererType::float_type mp_float_type;
  BOOST_MPL_ASSERT((boost::is_same<f_t,mp_float_type >));
  typedef std::complex<f_t> c_t;

  one_h(
    cctbx::sgtbx::space_group const&                   space_group,
    cctbx::miller::index<> const&                      h,
    f_t const&                                         stol_sq,
    af::const_ref<ScattererType> const&                scatterers,
    af::const_ref<MultipoleScattererType> const&       mp_scatterers,
    af::const_ref<rf_values_set<4,f_t> > const&        rfs_table)
  :
    f_mpl(0,0), f_sph(0,0)
  {
    for(std::size_t i_sc=0; i_sc < scatterers.size(); i_sc++)
    {
      int kappa_idx = mp_scatterers[i_sc].kappa_idx;
      one_h_one_scatterer<ScattererType,MultipoleScattererType>
      o_h_o_s(space_group,
              h,
              stol_sq,
              scatterers[i_sc],
              mp_scatterers[i_sc],
              rfs_table[kappa_idx]);
      f_mpl += o_h_o_s.f_mpl;
      f_sph += o_h_o_s.f_sph;
    }
  }

  c_t f_mpl;
  c_t f_sph;
};

// -----------------------------------------------------------------------------

template <typename ScattererType, typename MultipoleScattererType>
struct structure_factors
{
  typedef typename ScattererType::float_type f_t;
  typedef typename MultipoleScattererType::float_type mp_float_type;
  BOOST_MPL_ASSERT((boost::is_same<f_t,mp_float_type >));
  typedef std::complex<f_t> c_t;

  structure_factors() {};

  structure_factors(
    cctbx::uctbx::unit_cell const&                     unit_cell,
    cctbx::sgtbx::space_group const&                   space_group,
    af::const_ref<ScattererType> const&                scatterers,
    af::const_ref<MultipoleScattererType> const&       mp_scatterers,
    mptbx::rf_calculator<4,f_t>                        rf_calculator,
    af::const_ref<cctbx::miller::index<> > const&      miller_indices)
  {
    typedef typename mptbx::rf_calculator<4,f_t>::rf_table_type rf_table_type;
    f_mpl_.reserve(miller_indices.size());
    f_sph_.reserve(miller_indices.size());
    for(std::size_t i=0; i < miller_indices.size(); i++)
    {
      cctbx::miller::index<> h = miller_indices[i];
      f_t stol = unit_cell.stol(h);
      f_t stol_sq = stol * stol;
      rf_table_type const & rfs_table = rf_calculator.calculate(stol);
      one_h<ScattererType,MultipoleScattererType>
      oh(
         space_group,
         h,
         stol_sq,
         scatterers,
         mp_scatterers,
         rfs_table.const_ref()
        );
      f_mpl_.push_back(oh.f_mpl);
      f_sph_.push_back(oh.f_sph);
    }
  };

  af::shared<c_t> f_mpl() const { return f_mpl_; }
  af::shared<c_t> f_sph() const { return f_sph_; }

  protected:

  af::shared<c_t> f_mpl_;
  af::shared<c_t> f_sph_;

};


// -----------------------------------------------------------------------------

}} // end mptbx::sf4 namespace

#endif // GUARD
