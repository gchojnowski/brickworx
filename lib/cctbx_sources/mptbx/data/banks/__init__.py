from __future__ import division

import os.path

import mptbx
import mptbx.io.xd2006readers as xd2006readers

_banks = {
  'CR': 'xd.bnk_RHF_CR',
  'SCM': 'xd.bnk_RDF_SCM',
  'BBB': 'xd.bnk_RHF_BBB',
  'VM': 'xd.bnk_PBE-QZ4P-ZORA'  # Volkov, Macchi
}

cache = {}

def load(bank, with_comment=False):
  if cache.has_key(bank):
    comment, d_bnk = cache[bank]
    if with_comment:
      return (comment, d_bnk)
    else:
      return d_bnk
  try:
    name = _banks[bank]
  except KeyError:
    raise ValueError, 'No bank: "%s". Available banks: %s' % \
                      (bank, _banks.keys())
  topdir = os.path.split(mptbx.__file__)[0]
  dname = os.path.join(topdir,'data','banks')
  f_name = os.path.join(dname, name)
  comment, d_bnk = xd2006readers.read_bnk.read_bnk(f_name)
  cache[bank] = (comment, d_bnk)
  if with_comment:
    return (comment, d_bnk)
  else:
    return d_bnk

# ==============================================================================

if (__name__ == "__main__"):
  for bank in _banks.keys():
    print bank, ':'
    d_bnk = load(bank)
    comment, d_bnk = load(bank, with_comment=True)
    print ''.join(comment[:12]) + ' [...........]'
    print d_bnk.keys()
    print
  #d_bnk = load('cr')
