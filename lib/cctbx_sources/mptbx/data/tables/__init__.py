from __future__ import division

def load(tables_code):
  if tables_code == 'grd_0_05':
    from grd_0_05 import grd, tables
    return grd, tables
  else:
    raise ValueError, 'No tables collection: "%s".' % tables_code
