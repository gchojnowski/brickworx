#include <boost/python/reference_existing_object.hpp>
#include <boost/python/return_value_policy.hpp>

#include <scitbx/array_family/boost_python/shared_wrapper.h>

#include <mptbx/wrap.hpp>
#include <mptbx/model.hpp>

namespace mptbx {
namespace boost_python {

// -----------------------------------------------------------------------------

template <int MAXL, typename FloatType>
typename kappa_set<MAXL,FloatType>::tiny_type
kappa_set_as_tiny(kappa_set<MAXL,FloatType> ks)
{
  typedef typename kappa_set<MAXL,FloatType>::tiny_type t_t;
  return static_cast<t_t>(ks);
};

template <int MAXL, typename FloatType>
struct kappa_set_wrappers
{
  typedef kappa_set<MAXL,FloatType> w_t;
  typedef typename w_t::tiny_type tiny_type;
  static void
  wrap()
  {
    using namespace boost::python;
    class_<w_t, boost::noncopyable>(
         "rf_values_set", no_init)
      .def(init<tiny_type> ())
      .def("as_tuple", kappa_set_as_tiny<MAXL,FloatType>)
      .def("get_valence_kappa", &w_t::get_valence_kappa)
      .def("set_valence_kappa", &w_t::set_valence_kappa)
      .def("get_deformation_kappas", &w_t::get_deformation_kappas)
      .def("set_deformation_kappas", &w_t::set_deformation_kappas)
      ;
  }
};

template <int MAXL, typename FloatType>
void wrap_shared_kappa_set()
{
  typedef kappa_set<MAXL,FloatType> w_t;
  namespace bp = boost::python;
  namespace afbp = scitbx::af::boost_python;
  typedef bp::return_internal_reference<> rir;
  afbp::shared_wrapper<w_t, rir>::wrap("shared_kappa_set");
}

// -----------------------------------------------------------------------------

template <int MAXL, typename FloatType>
typename rf_values_set<MAXL,FloatType>::tiny_type
rf_values_set_as_tiny(rf_values_set<MAXL,FloatType> rfv)
{
  typedef typename rf_values_set<MAXL,FloatType>::tiny_type t_t;
  return static_cast<t_t>(rfv);
};

template <int MAXL, typename FloatType>
struct rf_values_set_wrappers
{
  typedef rf_values_set<MAXL,FloatType> w_t;
  typedef typename w_t::tiny_type tiny_type;
  static void
  wrap()
  {
    using namespace boost::python;
    class_<w_t, boost::noncopyable>(
         "rf_values_set", no_init)
      .def(init<tiny_type> ())
      .def("as_tuple", rf_values_set_as_tiny<MAXL,FloatType>)
      ;
  }
};

template <int MAXL, typename FloatType>
void wrap_shared_rf_values_set()
{
  typedef rf_values_set<MAXL,FloatType> w_t;
  namespace bp = boost::python;
  namespace afbp = scitbx::af::boost_python;
  typedef bp::return_internal_reference<> rir;
  afbp::shared_wrapper<w_t, rir>::wrap("shared_rf_values_set");
}

// -----------------------------------------------------------------------------

template <int MAXL, typename FloatType>
void wrap_model_tts()
{
  conditional_tt_wrap< populations<MAXL,FloatType> >();
  conditional_tt_wrap< kappa_set<MAXL,FloatType> >();
  conditional_tt_wrap< rf_values_set<MAXL,FloatType> >();

  conditional_tt_wrap<
    typename kappa_set<MAXL,FloatType>::tiny_type >();
  conditional_tt_wrap<
    typename kappa_set<MAXL,FloatType>::tiny_type_deformation >();

  conditional_tt_wrap<
    typename rf_values_set<MAXL,FloatType>::tiny_type >();

  kappa_set_wrappers<MAXL,FloatType>::wrap();
  wrap_shared_kappa_set<MAXL,FloatType>();

  rf_values_set_wrappers<MAXL,FloatType>::wrap();
  wrap_shared_rf_values_set<MAXL,FloatType>();

}

// -----------------------------------------------------------------------------

template <int MAXL, typename FloatType>
struct multipole_scatterer_wrappers
{
  typedef multipole_scatterer<MAXL,FloatType> w_t;
  static void
  wrap()
  {
    using namespace boost::python;
    typedef return_value_policy<return_by_value> rbv;
    typedef default_call_policies dcp;
    class_<w_t, boost::noncopyable>("multipole_scatterer", no_init)
      .def(init<
                 FloatType,
                 int,
                 populations<MAXL,FloatType>,
                 int
                 >())
      .add_property("amult",
                    make_getter(&w_t::amult, rbv()),
                    make_setter(&w_t::amult, dcp()))
      .add_property("lmax",
                    make_getter(&w_t::lmax, rbv()),
                    make_setter(&w_t::lmax, dcp()))
      .add_property("pop",
                    make_getter(&w_t::pop, rbv()),
                    make_setter(&w_t::pop, dcp()))
      .add_property("lcs",
                    make_getter(&w_t::lcs, rbv()),
                    make_setter(&w_t::lcs, dcp()))
      .add_property("kappa_idx",
                    make_getter(&w_t::kappa_idx, rbv()),
                    make_setter(&w_t::kappa_idx, dcp()))
      ;
  }
};

// -----------------------------------------------------------------------------

template <int MAXL, typename FloatType>
void set_lmaxs(
  af::ref< multipole_scatterer<MAXL,FloatType> > const& mp_scatterers,
  af::const_ref<int> const& lmaxs)
{
  CCTBX_ASSERT(mp_scatterers.size() == lmaxs.size());
  for(std::size_t i=0; i < mp_scatterers.size(); i++) {
    mp_scatterers[i].lmax = lmaxs[i];
  }
}

template <int MAXL, typename FloatType>
void set_amults(
  af::ref< multipole_scatterer<MAXL,FloatType> > const& mp_scatterers,
  af::const_ref<FloatType> const& amults)
{
  CCTBX_ASSERT(mp_scatterers.size() == amults.size());
  for(std::size_t i=0; i < mp_scatterers.size(); i++) {
    mp_scatterers[i].amult = amults[i];
  }
}

template <int MAXL, typename FloatType>
void set_lcss_3vec3(
  af::ref< multipole_scatterer<MAXL,FloatType> > const& mp_scatterers,
  af::const_ref< scitbx::vec3<FloatType> > const& lcss_frac)
{
  CCTBX_ASSERT(3*mp_scatterers.size() == lcss_frac.size());
  for(std::size_t i=0; i < mp_scatterers.size(); i++)
    for(short s=0; s<3; s++)
      mp_scatterers[i].lcs.set_row(s, lcss_frac[3*i+s]);
}

template <int MAXL, typename FloatType>
void wrap_shared_multipole_scatterer()
{
  typedef multipole_scatterer<MAXL,FloatType> w_t;
  namespace bp = boost::python;
  namespace afbp = scitbx::af::boost_python;
  typedef bp::return_internal_reference<> rir;

  afbp::shared_wrapper<w_t, rir>::wrap(
    "shared_multipole_scatterer")
    .def("set_lmaxs", set_lmaxs<MAXL,FloatType>)
    .def("set_lcss_3vec3", set_lcss_3vec3<MAXL,FloatType>)
    .def("set_amults", set_lmaxs<MAXL,FloatType>)
  ;
}

// -----------------------------------------------------------------------------


void wrap_model()
{
  wrap_model_tts<MPTBX_MAXL,MPTBX_FLOAT_TYPE>();
  multipole_scatterer_wrappers<MPTBX_MAXL,MPTBX_FLOAT_TYPE>::wrap();
  wrap_shared_multipole_scatterer<MPTBX_MAXL,MPTBX_FLOAT_TYPE>();
}

// -----------------------------------------------------------------------------

}} // namespace mptbx::boost_python
