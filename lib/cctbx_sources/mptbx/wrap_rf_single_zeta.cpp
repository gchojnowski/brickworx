
#include <mptbx/wrap.hpp>
#include <mptbx/rf_single_zeta.hpp>

#include <boost/shared_ptr.hpp>

namespace mptbx {
namespace boost_python {

// -----------------------------------------------------------------------------

template <typename FloatType>
struct radial_function_single_zeta_wrappers
{
  typedef radial_function_single_zeta<FloatType> w_t;
  static void
  wrap()
  {
    using namespace boost::python;
    class_<w_t, bases<radial_function<FloatType> > > (
        "radial_function_single_zeta", no_init)
      .def("__call__", &w_t::operator())
      ;
  }
};

template <typename FloatType>
boost::python::object
make_single_zeta_rf_set(int maxl, int n_default, FloatType zeta)
{
  boost::shared_ptr< single_zeta_rf_set<FloatType> >
  szrfsp(new single_zeta_rf_set<FloatType>(maxl,n_default,zeta));
  boost::python::list res;
  for(int l=0; l <= maxl; l++) res.append(szrfsp->get_rf(l));
  return res;
};

/* templated function does not compile with ICC 9.1 & as far as I can 
   see is not necessary here.  Replace with MPTBX_FLOAT_TYPE=double. --NKS
 */
void wrap_make_single_zeta_rf_set()
{
  using namespace boost::python;
  def("make_single_zeta_rf_set", &make_single_zeta_rf_set<double>,
      (arg("maxl"), arg("n_default"), arg("zeta")),
      "Returns a list of SZ deformation radial functions for 0<=l<=maxl."
      "arguments: n_default, zeta");
};

// -----------------------------------------------------------------------------

void wrap_rf_single_zeta()
{
  radial_function_single_zeta_wrappers<MPTBX_FLOAT_TYPE>::wrap();
  wrap_make_single_zeta_rf_set();
}

// -----------------------------------------------------------------------------

}} // namespace mptbx::boost_python
