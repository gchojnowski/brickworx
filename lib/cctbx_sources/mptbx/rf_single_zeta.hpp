#ifndef MPTBX_RF_SINGLE_ZETA_HPP
#define MPTBX_RF_SINGLE_ZETA_HPP

#include <algorithm>
#include <math.h>
#include <boost/math/special_functions/factorials.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <mptbx/jn.hpp>
#include <mptbx/radial_functions.hpp>

namespace mptbx {

namespace af = scitbx::af;
using scitbx::constants::four_pi;
using boost::math::factorial;
using std::max;
using std::pow;

// -----------------------------------------------------------------------------

template <typename FloatType> struct radial_function_single_zeta;

// -----------------------------------------------------------------------------

template <typename FloatType>
struct single_zeta_rf_set
: public boost::enable_shared_from_this< single_zeta_rf_set<FloatType> >
{
  const int maxl;
  const int n_default;
  const FloatType zeta;

  single_zeta_rf_set(int maxl, int n_default, FloatType zeta)
  : maxl(maxl), n_default(n_default), zeta(zeta), stol_(-1.0), kappa_(-1.0)
  {};

  // at least one shared_ptr to single_zeta_rf_set must be in existence before
  // calling this method
  radial_function_single_zeta<FloatType> get_rf(int l)
  {
    TBXX_ASSERT(((l>=0)&&(l<=maxl)));
    radial_function_single_zeta<FloatType> rf(this->shared_from_this(),l);
    return rf;
  };

protected:

  boost::shared_ptr< jndjn_direct<FloatType> >
  set_stol_kappa(FloatType stol, FloatType kappa)
  {
    // parallelization note: whole method should be a critical section
    if((stol!=stol_)||(kappa!=kappa_))
    {
      FloatType s = four_pi * stol / kappa;
      int n = max(maxl,n_default) + 2;
      jndjn_ptr.reset(new jndjn_direct<FloatType>(n,s,zeta));
      stol_ = stol;
      kappa_ = kappa;
    }
    return jndjn_ptr;
  };

  FloatType stol_;
  FloatType kappa_;
  boost::shared_ptr< jndjn_direct<FloatType> > jndjn_ptr;

  friend class radial_function_single_zeta<FloatType>;

};

// -----------------------------------------------------------------------------

/*

Single zeta Slater radial functions divided by |h|^l.  (a_l <==> zeta)

real space:

  R(r) = rf_nrml * r^n_l * exp(-a_l r)

with normalization:

  rf_nrml = a^(n_l+3) / (n_l+2)!     ===>   \int R(r) d^3 r = 4 pi

returned value (h = 2 stol):

  f(stol,kappa) = rf_nrml * J_{n_l+2,l}(S=4*pi*stol/kappa,Z=a_l) / h^l

returned derivative:

  df(stol,kappa) = rf_nrml * (d J /d kappa) / h^l

notes:

1. h^l term cancels with spherical harmonics expressions
2. \int R(kappa*r) d^3 r = 4 pi / kappa^3

*/

template <typename FloatType>
struct radial_function_single_zeta : public radial_function<FloatType>
{

  radial_function_single_zeta(
      boost::shared_ptr< single_zeta_rf_set<FloatType> > szrfsp,
      int l)
  : szrfsp(szrfsp), l(l)
  {};

  virtual af::tiny<FloatType,2> operator() (FloatType stol, FloatType kappa)
  {
    jndjn_direct<FloatType> & jndjn = *((szrfsp->set_stol_kappa)(stol,kappa));
    int nl = max(l, szrfsp->n_default);
    int n = nl + 2;
    FloatType rf_nrml = pow(szrfsp->zeta,nl+3) / factorial<FloatType>(nl+2);
    af::tiny<FloatType,2> jdj = jndjn(n,l);
    FloatType h_len_l = pow(2.0*stol,l) ;
    FloatType f = rf_nrml * jdj[0] / h_len_l;
    FloatType df = rf_nrml * jdj[1] *
                  (-1.0 * four_pi * stol / (kappa * kappa))
                  / h_len_l;
    return af::tiny<FloatType,2>(f, df);
  };

private:

  int l;
  boost::shared_ptr< single_zeta_rf_set<FloatType> > szrfsp;

};

// -----------------------------------------------------------------------------


} // end mptbx namespace

#endif // GUARD
