#ifndef MPTBX_JN_HPP
#define MPTBX_JN_HPP

#include <tbxx/error_utils.hpp>
#include <scitbx/array_family/versa_matrix.h>

namespace mptbx {

namespace af = scitbx::af;

// -----------------------------------------------------------------------------

// Implements J_N,K integrals as defined in Acta Cryst. (1977). A33, 679-680
// "Generalized X-ray scattering factors. Simple closed-form expressions for the
// one-centre case with Slater-type orbitals" by J. Avery and K. J. Watson
// http://dx.doi.org/10.1107/S0567739477001697
//
// J_{N,k}(S,Z) = \int_0^\inf r^N exp(-Zr) j_k(Sr) dr
// where j_k are spherical Bessel functions
//

/*
 * Maxima code:
 *

JN(N):=(
  SZ : S^2 + Z^2,
  J_1_0 : 1 / SZ,
  JNk[1,0] : J_1_0,
  c1(nu) := (2*nu*S) / SZ,
  for nu: 1 step 1 thru (N-1) do
    JNk[nu+1,nu] : c1(nu) * JNk[nu,nu-1],
  c2(nu) := Z * (2*nu+2) / SZ,
  for nu: 0 step 1 thru (N-2) do
    JNk[nu+2,nu] : c2(nu) * JNk[nu+1,nu],
  c3b(mu,nu) := (mu+nu)*(mu-nu-1),
  c3c(mu) := 2*mu*Z,
  for nu: 0 step 1 thru (N-3) do
    for mu: nu+2 step 1 thru (N-1) do
      JNk[mu+1,nu] : (c3c(mu)*JNk[mu,nu] - c3b(mu,nu)*JNk[mu-1,nu]) / SZ,
  JNk
);


N : 8;
b : JN(N);
for mu: 1 step 1 thru N do
  for nu: 0 step 1 thru mu-1 do
    print([mu,nu,factor(b[mu,nu])]);

 */

// Using recursive expressions directly (no optimization for const Z).
// The J_Nk matrix in strictly lower triangular, but stored internally as
// plain lower triangular.

template<typename FloatType>
struct jn_direct
{
  typedef FloatType f_t;
  const int n;
  const f_t s, z;

  jn_direct(int n, f_t s, f_t z)
  : jnk_(n), n(n), s(s), z(z)
  {
    sz2 = s*s + z*z;
    jnk(1,0) = 1.0 / sz2;
    for(int nu=1; nu<n; nu++)
      jnk(nu+1,nu) = c1(nu) * jnk(nu,nu-1);
    for(int nu=0; nu<n-1; nu++)
      jnk(nu+2,nu) = c2(nu) * jnk(nu+1,nu);
    for(int nu=0; nu<n-2; nu++)
      for(int mu=nu+2; mu<n; mu++)
        jnk(mu+1,nu) = (c3c(mu)*jnk(mu,nu) - c3b(mu,nu)*jnk(mu-1,nu)) / sz2;
  };

  inline f_t operator()(int i, int j)
  {
    if((i<=0)||(j>=i))
      return 0.0;
    else
      return jnk_(i-1,j);
  };

  af::versa<f_t, af::packed_l_accessor> as_packed_l()
  {
    af::versa<f_t, af::packed_l_accessor> jnk__(n+1);
    for(int i=0; i<=n; i++)
      for(int j=0; j<i; j++)
        jnk__(i,j) = jnk(i,j);
    return jnk__;
  };

protected:

  af::versa<f_t, af::packed_l_accessor> jnk_;
  inline f_t & jnk(int i, int j) {return jnk_(i-1,j);};

  f_t sz2;

  inline f_t c1(int nu) { return (2*nu*s) / sz2; };
  inline f_t c2(int nu) { return  z * (2*nu+2) / sz2; };
  inline f_t c3b(int mu, int nu) { return  (mu+nu)*(mu-nu-1); };
  inline f_t c3c(int mu) { return  2*mu*z; };

};

// -----------------------------------------------------------------------------

// Calculates also:
//
//   d J_{N,k}(S,Z)
//   --------------
//        d S

/*
 * Maxima code:
 *

depends([JNk_nu_num1, JNk_nup1_nu, JNk_mum1_nu, JNk_mu_nu],[S]);
diff(1 / (S^2 + Z^2),S);
diff(c1(nu) * JNk_nu_num1,S);
diff(c2(nu) * JNk_nup1_nu, S);
diff((c3c(mu)*JNk_mu_nu - c3b(mu,nu)*JNk_mum1_nu) / SZ, S);

 */

template<typename FloatType>
struct jndjn_direct
: public jn_direct<FloatType>
{
  typedef FloatType f_t;

  jndjn_direct(int n, f_t s, f_t z)
  : jn_direct<FloatType>(n,s,z), djnk_(n)
  {
    const f_t & sz2(jn_direct<FloatType>::sz2);
    sz22 = sz2 * sz2;
    s2 = s*s;
    z2 = z*z;
    djnk(1,0) = -2.0 * s / sz22;
    for(int nu=1; nu<n; nu++)
      djnk(nu+1,nu) = 2.0 * djnk(nu,nu-1) * nu * s / sz2
                    + 2.0 * jnk(nu,nu-1) * nu / sz2
                    - 4.0 * jnk(nu,nu-1) * nu * s2 / sz22;
    for(int nu=0; nu<n-1; nu++)
      djnk(nu+2,nu) = djnk(nu+1,nu) * (2*nu+2) * z / sz2
                    - 2.0 * jnk(nu+1,nu) * (2*nu+2) * s * z / sz22;
    for(int nu=0; nu<n-2; nu++)
      for(int mu=nu+2; mu<n; mu++)
        djnk(mu+1,nu) = (2.0 * djnk(mu,nu) * mu * z
                         - djnk(mu-1,nu) * (-nu+mu-1)*(nu+mu))
                         / sz2
                      - 2.0 * s * (2.0*jnk(mu,nu) * mu * z
                                   - jnk(mu-1,nu) * (-nu+mu-1)*(nu+mu) )
                                   / sz22;
  };

  inline af::tiny<f_t,2> operator()(int i, int j)
  {
    if((i<=0)||(j>=i))
      return af::tiny<f_t,2>(0,0);
    else
      return af::tiny<f_t,2>(this->jnk_(i-1,j), djnk_(i-1,j));
  };

  af::versa<f_t, af::packed_l_accessor> djnk_as_packed_l()
  {
    af::versa<f_t, af::packed_l_accessor> djnk__((this->n)+1);
    for(int i=0; i<=(this->n); i++)
      for(int j=0; j<i; j++)
        djnk__(i,j) = djnk(i,j);
    return djnk__;
  };

protected:

  af::versa<f_t, af::packed_l_accessor> djnk_;

  inline f_t & jnk(int i, int j) {return jn_direct<FloatType>::jnk(i,j);};
  inline f_t & djnk(int i, int j) {return djnk_(i-1,j);};

  f_t s2, z2, sz22;

};

// -----------------------------------------------------------------------------

} // end mptbx namespace

#endif // GUARD
