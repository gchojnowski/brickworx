from __future__ import division
# -*- Mode: Python; c-basic-offset: 2; indent-tabs-mode: nil; tab-width: 8 -*-
#
# Known issues: Recentering on resize and when switching between
# different image types.  Ring centre on image switch.
#
# $Id: frame.py 323 2012-05-16 21:40:27Z hattne $

import os
import wx

from rstbx.viewer.frame import EVT_EXTERNAL_UPDATE
from rstbx.viewer.frame import XrayFrame as XFBaseClass
from rstbx.viewer import settings as rv_settings, image as rv_image
from wxtbx import bitmaps

class chooser_wrapper:
  def __init__(self, image_set, index):
    from dxtbx.imageset import ImageSet
    isinstance(image_set, ImageSet)
    self.image_set = image_set
    self.path = os.path.basename(image_set.paths()[index])
    self.index = index

  def __str__(self):
    return "%s [%d]"%(self.path,self.index)

  def get_detector_base(self):
    return self.image_set.get_detectorbase(self.index)

from rstbx.slip_viewer.slip_display import AppFrame
class XrayFrame (AppFrame,XFBaseClass) :
  def __init__ (self, *args, **kwds) :
    wx.Frame.__init__(self,*args,**kwds)
    self.settings = rv_settings()

    self.sizer = wx.BoxSizer(wx.VERTICAL)
    self.SetSizer(self.sizer)

    self.init_pyslip_presizer()

    self.sizer.Add(self.viewer, 1, wx.EXPAND)
    self.statusbar = self.CreateStatusBar()
    self.settings_frame = None
    self._calibration_frame = None
    self._ring_frame = None
    self.zoom_frame = None
    self.plot_frame = None

    self.metrology_matrices = None
    self.params = None

    # Currently displayed image.  XXX Can this be zapped?
    self._img = None

    self._distl = None
    self.toolbar = self.CreateToolBar(style=wx.TB_3DBUTTONS|wx.TB_TEXT)
    self.setup_toolbar()
    self.toolbar.Realize()
    self.mb = wx.MenuBar()
    self.setup_menus()
    self.SetMenuBar(self.mb)
    self.Fit()
    self.SetMinSize(self.GetSize())
    self.SetSize((720,720))
    self.OnShowSettings(None)
    self.Bind(EVT_EXTERNAL_UPDATE, self.OnExternalUpdate)

    self.Bind(wx.EVT_UPDATE_UI, self.OnUpdateUI, id=wx.ID_BACKWARD)
    self.Bind(wx.EVT_UPDATE_UI, self.OnUpdateUI, id=wx.ID_FORWARD)
    self.Bind(wx.EVT_UPDATE_UI, self.OnUpdateUI, id=self._id_calibration)
    self.Bind(wx.EVT_UPDATE_UI, self.OnUpdateUI, id=self._id_ring)

  def setup_toolbar(self) :
    XFBaseClass.setup_toolbar(self)

    btn = self.toolbar.AddLabelTool(id=wx.ID_SAVEAS,
    label="Save As...",
    bitmap=bitmaps.fetch_icon_bitmap("actions","save_all", 32),
    shortHelp="Save As...",
    kind=wx.ITEM_NORMAL)
    self.Bind(wx.EVT_MENU, self.OnSaveAs, btn)

  def init_pyslip_presizer(self):
    self.viewer  = wx.Panel(self, wx.ID_ANY, size=(1024,640))
    self.viewer.SetMinSize((640,640))
    self.viewer.SetBackgroundColour(wx.WHITE)
    self.viewer.ClearBackground()
    # create select event dispatch directory
    self.demo_select_dispatch = {}

    #self.tile_directory = None#"/Users/nksauter/rawdata/demo/insulin_1_001.img"

    # build the GUI
    self.make_gui(self.viewer)

    from rstbx.slip_viewer import pyslip
    # finally, bind events to handlers
    self.pyslip.Bind(pyslip.EVT_PYSLIP_SELECT, self.handle_select_event)
    self.pyslip.Bind(pyslip.EVT_PYSLIP_POSITION, self.handle_position_event)
    self.pyslip.Bind(pyslip.EVT_PYSLIP_LEVEL, self.handle_level_change)

  def init_pyslip_postsizer(self):
    self.pyslip.ZoomToLevel(-2)#tiles.zoom_level
    self.pyslip.GotoPosition(
      self.pyslip.tiles.get_initial_instrument_centering_within_picture_as_lon_lat()
    )

  def setup_menus (self) :
    file_menu = wx.Menu()
    self.mb.Append(file_menu, "File")
    item = file_menu.Append(-1, "Open integration results...")
    self.Bind(wx.EVT_MENU, self.OnLoadIntegration, item)
    item = file_menu.Append(-1, "Open image...")
    self.Bind(wx.EVT_MENU, self.OnLoadFile, item)
    actions_menu = wx.Menu()
    self.mb.Append(actions_menu, "Actions")
    item = actions_menu.Append(-1, "Change beam center...")
    self.Bind(wx.EVT_MENU, self.OnChangeBeamCenter, item)
    item = actions_menu.Append(-1, "Reset beam center to header value")
    self.Bind(wx.EVT_MENU, lambda evt: self.viewer.ResetBeamCenter(), item)
    item = actions_menu.Append(-1, "Save screenshot...")
    self.Bind(wx.EVT_MENU, self.OnScreenShot, item)

    # Known wxWidgets/wxPython issue
    # (http://trac.wxwidgets.org/ticket/12394): stock item ID is
    # expected for zero-length text.  Work around by making text
    # contain single space. XXX Placement
    self._id_calibration = wx.NewId()
    item = actions_menu.Append(self._id_calibration, " ")
    self.Bind(wx.EVT_MENU, self.OnCalibration, source=item)

    # XXX Placement
    self._id_ring = wx.NewId()
    item = actions_menu.Append(self._id_ring, " ")
    self.Bind(wx.EVT_MENU, self.OnRing, source=item)

  def add_file_name_or_data (self, file_name_or_data) :
      """The add_file_name_or_data() function appends @p
      file_name_or_data to the image chooser, unless it is already
      present.  For file-backed images, the base name is displayed in
      the chooser.  If necessary, the number of entries in the chooser
      is pruned.  The function returns the index of the recently added
      entry.  XXX This is probably the place for heuristics to determine
      if the viewer was given a pattern, or a plain list of files.  XXX
      Rename this function, because it only deals with the chooser?
      """

      key = self.get_key(file_name_or_data)
      for i in xrange(self.image_chooser.GetCount()) :
        if (key == str(self.image_chooser.GetClientData(i))) :
          return i
      if (self.image_chooser.GetCount() >= self.CHOOSER_SIZE) :
        self.image_chooser.Delete(0)
      i = self.image_chooser.GetCount()
      if (type(file_name_or_data) is dict) :
        self.image_chooser.Insert(key, i, None)
      elif (isinstance(file_name_or_data, chooser_wrapper)):
        self.image_chooser.Insert(key, i, file_name_or_data)
      else :
        self.image_chooser.Insert(os.path.basename(key), i, key)
      return i

  def load_image (self, file_name_or_data) :
    """The load_image() function displays the image from @p
    file_name_or_data.  The chooser is updated appropriately.
    """
    from iotbx.detectors.detectorbase import DetectorImageBase

    if (isinstance(file_name_or_data, dict)):
      img = rv_image(file_name_or_data)
      self.SetTitle(
        file_name_or_data.get("TIMESTAMP", "No timestamp available"))
    elif (isinstance(file_name_or_data, str)):
      img = rv_image(os.path.abspath(file_name_or_data))
      self.SetTitle(file_name_or_data)
    elif (isinstance(file_name_or_data, DetectorImageBase)):
      img = rv_image(file_name_or_data)
      self.SetTitle(file_name_or_data.filename)
    elif (isinstance(file_name_or_data, chooser_wrapper)):
      img = rv_image(file_name_or_data.get_detector_base())
      self.SetTitle(str(file_name_or_data))
    else:
      img = rv_image(os.path.abspath(file_name_or_data.encode("ascii")))
      self.SetTitle(file_name_or_data)

    # Update the selection in the chooser.
    i = self.add_file_name_or_data(file_name_or_data)
    self.image_chooser.SetSelection(i)

    self.pyslip.tiles.set_image(
      file_name_or_data=img, metrology_matrices=self.metrology_matrices)

    # Initialise position zoom level for first image.  XXX Why do we
    # have to coll ZoomToLevel to refresh subsequent images?
    if (self._img is None):
      self.init_pyslip_postsizer()
    else:
      self.pyslip.ZoomToLevel(self.pyslip.tiles.zoom_level)

    self._img = img # XXX

    self.settings_frame.set_image(self._img)
    self.update_statusbar() # XXX Not always working?
    self.Layout()

    beam_pixel_fast,beam_pixel_slow = self.pyslip.tiles.raw_image.get_beam_center_pixels_fast_slow()

    self.beam_center_cross_data = [
      ((self.pyslip.tiles.picture_fast_slow_to_map_relative(beam_pixel_fast+3.,beam_pixel_slow),
        self.pyslip.tiles.picture_fast_slow_to_map_relative(beam_pixel_fast-3.,beam_pixel_slow)),
        {'width': 2, 'color': '#0000FFA0', 'closed': False}),
      ((self.pyslip.tiles.picture_fast_slow_to_map_relative(beam_pixel_fast,beam_pixel_slow+3.),
        self.pyslip.tiles.picture_fast_slow_to_map_relative(beam_pixel_fast,beam_pixel_slow-3.)),
        {'width': 2, 'color': '#0000FFA0', 'closed': False})
                             ]
    # Unconditionally delete beam_layer and
    # spotfinder_layer--update_settings() will add them back if
    # appropriate.  This also creates the self.*_layer variables.
    if (hasattr(self, "beam_layer") and
        self.beam_layer is not None):
      self.pyslip.DeleteLayer(self.beam_layer)
    self.beam_layer = None

    if (hasattr(self, "spotfinder_layer") and
        self.spotfinder_layer is not None):
      self.pyslip.DeleteLayer(self.spotfinder_layer)
    self.spotfinder_layer = None

    if (hasattr(self, "tile_layer") and
        self.tile_layer is not None):
      self.pyslip.DeleteLayer(self.tile_layer)
    self.tile_layer = None

    if (hasattr(self, "tile_text_layer") and
        self.tile_text_layer is not None):
      self.pyslip.DeleteLayer(self.tile_text_layer)
    self.tile_text_layer = None

    self.update_settings()

    # Destroy the calibration frame if it present but unsupported for
    # this image.  XXX Need to do something about the ring tool too
    # when switching between different kinds of images.  XXX Centering
    # is broken when switching between different kinds of images.
    if (self._calibration_frame and
        not self.pyslip.tiles.raw_image.supports_quadrant_calibration()):
      self.OnCalibration(None)

  def get_key (self, file_name_or_data) :
      """This overridden get_key() function returns the key of @p file_name_or_data
      if it's an DetectorImageBase object.  Otherwise it returns the super class's
      key
      """
      from iotbx.detectors.detectorbase import DetectorImageBase
      if isinstance(file_name_or_data, DetectorImageBase):
        return file_name_or_data.filename
      elif isinstance(file_name_or_data, chooser_wrapper):
        return str(file_name_or_data)
      else: return super(XrayFrame, self).get_key(file_name_or_data)


  def update_settings (self, layout=True) :
    # XXX The zoom level from the settings panel are not taken into
    # account here.

    new_brightness = self.settings.brightness
    new_color_scheme = self.settings.color_scheme
    if new_brightness is not self.pyslip.tiles.current_brightness or \
       new_color_scheme is not self.pyslip.tiles.current_color_scheme:
      self.pyslip.tiles.update_brightness(new_brightness,new_color_scheme)
      self.pyslip.Update()#triggers redraw

    if (self.settings.show_beam_center):
      if (self.beam_layer is None):
        self.beam_layer = self.pyslip.AddPolygonLayer(
          self.beam_center_cross_data, name="<beam_layer>",
          show_levels=[-2, -1, 0, 1, 2, 3, 4, 5])
    elif (self.beam_layer is not None):
      self.pyslip.DeleteLayer(self.beam_layer)
      self.beam_layer = None

    if (self.settings.show_spotfinder_spots):
      if (self.spotfinder_layer is None):
        tdata = self.pyslip.tiles.get_spotfinder_data(self.params)
        self.spotfinder_layer = self.pyslip.AddPointLayer(
          tdata, color="green", name="<spotfinder_layer>",
          radius=2,
          renderer = frame.pyslip.LightweightDrawPointLayer,
          show_levels=[-2, -1, 0, 1, 2, 3, 4, 5])
    elif (self.spotfinder_layer is not None):
      self.pyslip.DeleteLayer(self.spotfinder_layer)
      self.spotfinder_layer = None

    if (self.settings.show_effective_tiling):
      if (self.tile_layer is None):
        tdata, ttdata = self.pyslip.tiles.get_effective_tiling_data(self.params)
        self.tile_layer = self.pyslip.AddPolygonLayer(
          tdata, name="<tiling_layer>",
          show_levels=[-2, -1, 0, 1, 2, 3, 4, 5])
      if (self.tile_text_layer is None):
        self.tile_text_layer = self.pyslip.AddTextLayer(
          ttdata, name="<tiling_text_layer>",
          show_levels=[-2, -1, 0, 1, 2, 3, 4, 5],
          colour='#0000FFA0',
          textcolour='#0000FFA0',
          fontsize=30,
          placement='cc',
          radius=0)
    elif (self.tile_layer is not None) and (self.tile_text_layer is not None):
        self.pyslip.DeleteLayer(self.tile_layer)
        self.tile_layer = None
        self.pyslip.DeleteLayer(self.tile_text_layer)
        self.tile_text_layer = None

    if hasattr(self,"user_callback"):
      self.user_callback(self)

  def OnCalibration(self, event):
    from rstbx.slip_viewer.calibration_frame import SBSettingsFrame

    if (not self._calibration_frame):
      self._calibration_frame = SBSettingsFrame(
        self, -1, "Quadrant calibration", style=wx.CAPTION | wx.CLOSE_BOX)
      self._calibration_frame.Show()
      self._calibration_frame.Raise()
    else:
      self._calibration_frame.Destroy()


  def OnRing(self, event):
    from rstbx.slip_viewer.ring_frame import RingSettingsFrame

    if (not self._ring_frame):
      self._ring_frame = RingSettingsFrame(
        self, -1, "Ring tool", style=wx.CAPTION | wx.CLOSE_BOX)
      self._ring_frame.Show()
      self._ring_frame.Raise()
    else:
      self._ring_frame.Destroy()


  def OnUpdateUI(self, event):
    # Toggle the text of the menu items depending on the state of the
    # calibration and ring frames.  If quadrant calibration is not
    # supported for this image, disable the corresponding menu item.
    # Enable/disable previous and next buttons based on the image's
    # position in the list.

    eid = event.GetId()
    if (eid == self._id_calibration):

      if (self._calibration_frame):
        event.SetText("Hide quadrant calibration")
      else:
        event.SetText("Show quadrant calibration")

      if self.pyslip.tiles.raw_image.supports_quadrant_calibration():
        event.Enable(True)
      else:
        event.Enable(False)
      return

    elif (eid == self._id_ring):
      if (self._ring_frame):
        event.SetText("Hide ring tool")
      else:
        event.SetText("Show ring tool")
      return

    elif (eid == wx.ID_BACKWARD):
      if (self.image_chooser.GetSelection() - 1 >= 0):
        event.Enable(True)
      else:
        event.Enable(False)
      return

    elif (eid == wx.ID_FORWARD):
      if (self.image_chooser.GetSelection() + 1 <
          self.image_chooser.GetCount()):
        event.Enable(True)
      else:
        event.Enable(False)
      return

  def OnSaveAs (self, event) :
      ### XXX TODO: Save overlays
      ### XXX TODO: Fix bug where multi-asic images are slightly cropped due to tranformation error'

      wildcard_str = "PNG file (*.png)|*.png|PDF file (*.pdf)|*.pdf"
      file_name = wx.SaveFileSelector("PNG or PDF",wildcard_str,"",self)

      if file_name == "":
        return

      ext = os.path.splitext(file_name)[1].lower()

      if ext == ".png":
        import Image
        from cStringIO import StringIO
        self.update_statusbar("Writing " + file_name + "...")

        flex_img = self.pyslip.tiles.raw_image.get_flex_image(brightness=1)
        if flex_img.supports_rotated_tiles_antialiasing_recommended:
            currentZoom = self.pyslip.level
            self.pyslip.tiles.UseLevel(0) #1:1 zoom level

            x, y, width, height = self._img._raw.bounding_box_mm()
            x1, y1 = self._img._raw.detector_coords_as_image_coords(x,y)
            x2, y2 = self._img._raw.detector_coords_as_image_coords(x+width,y+height)

            # Map > View - determine layout in X direction
            x_offset = x1
            import math
            start_x_tile = int(math.floor(x_offset / self.pyslip.tile_size_x))
            stop_x_tile = ((x2 + self.pyslip.tile_size_x - 1)/ self.pyslip.tile_size_x)
            stop_x_tile = int(stop_x_tile)
            col_list = range(start_x_tile, stop_x_tile)
            x_pix = start_x_tile * self.pyslip.tile_size_y - x_offset

            y_offset = y1
            start_y_tile = int(math.floor(y_offset / self.pyslip.tile_size_y))
            stop_y_tile = ((y2 + self.pyslip.tile_size_y - 1) / self.pyslip.tile_size_y)
            stop_y_tile = int(stop_y_tile)
            row_list = range(start_y_tile, stop_y_tile)
            y_pix_start = start_y_tile * self.pyslip.tile_size_y - y_offset

            bitmap = wx.EmptyBitmap(x2-x1, y2-y1)
            dc = wx.MemoryDC()
            dc.SelectObject(bitmap)

            # start pasting tiles
            for x in col_list:
                y_pix = y_pix_start
                for y in row_list:
                    dc.DrawBitmap(self.pyslip.tiles.GetTile(x, y), x_pix, y_pix, False)
                    y_pix += self.pyslip.tile_size_y
                x_pix += self.pyslip.tile_size_x

            dc.SelectObject(wx.NullBitmap)

            wximg = wx.ImageFromBitmap(bitmap)
            imageout = Image.new('RGB', (wximg.GetWidth(), wximg.GetHeight()))
            imageout.fromstring(wximg.GetData())

            self.pyslip.tiles.UseLevel(currentZoom)

        else: # write the image out at full resolution
            flex_img.setWindow(0.0, 0.0, 1)
            flex_img.spot_convention(0)
            flex_img.adjust(color_scheme=0)
            flex_img.prep_string()
            data_string = flex_img.export_string
            imageout = Image.fromstring("RGB",
                                     (flex_img.ex_size2(), flex_img.ex_size1()),
                                     data_string)

        out = StringIO()
        imageout.save(out, "PNG")
        open(file_name, "wb").write(out.getvalue())

        self.update_statusbar("Writing " + file_name + "..." + " Done.")
      elif ext == ".pdf":
        self.update_statusbar("Save as pdf not implemented.")
      else:
        self.update_statusbar("Unrecognized file extension: %s"%ext)

from rstbx.viewer.frame import SettingsFrame

def override_SF_set_image(self,image):
  self.Layout()
  self.Fit()
SettingsFrame.set_image = override_SF_set_image
