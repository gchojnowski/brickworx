/*
 * beam.cc
 *
 *  Copyright (C) 2013 Diamond Light Source
 *
 *  Author: James Parkhurst
 *
 *  This code is distributed under the BSD license, a copy of which is
 *  included in the root directory of this package.
 */
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <string>
#include <sstream>
#include <scitbx/constants.h>
#include <dxtbx/model/beam.h>

namespace dxtbx { namespace model { namespace boost_python {

  using namespace boost::python;
  using scitbx::deg_as_rad;
  using scitbx::rad_as_deg;

  std::string beam_to_string(const Beam &beam) {
    std::stringstream ss;
    ss << beam;
    return ss.str();
  }

  struct BeamPickleSuite : boost::python::pickle_suite {
    static
    boost::python::tuple getinitargs(const Beam &obj) {
      return boost::python::make_tuple(
        obj.get_direction(),
        obj.get_wavelength());
    }
  };

  static Beam* make_beam(vec3<double> sample_to_source, double wavelength,
                         double divergence, double sigma_divergence, bool deg) {
    Beam *beam = NULL;
    if (deg) {
      beam = new Beam(sample_to_source, wavelength, 
                      deg_as_rad(divergence), 
                      deg_as_rad(sigma_divergence));
    } else {
      beam = new Beam(sample_to_source, wavelength, 
                      divergence, sigma_divergence);
    }
    return beam;
  }

  static Beam* make_beam_w_s0(vec3<double> s0, double divergence, 
                              double sigma_divergence, bool deg) {
    Beam *beam = NULL;
    if (deg) {
      beam = new Beam(s0, deg_as_rad(divergence), 
                      deg_as_rad(sigma_divergence));
    } else {
      beam = new Beam(s0, divergence, sigma_divergence);
    }
    return beam;
  }
   
  static Beam* make_beam_w_all(vec3<double> sample_to_source, 
			       double wavelength,
			       double divergence, double sigma_divergence, 
			       vec3<double> polarization_normal, 
			       double polarization_fraction, bool deg) {
    Beam *beam = NULL;
    if (deg) {
      beam = new Beam(sample_to_source, wavelength, 
                      deg_as_rad(divergence), 
                      deg_as_rad(sigma_divergence),
                      polarization_normal,
                      polarization_fraction);
    } else {
      beam = new Beam(sample_to_source, wavelength, 
                      divergence, sigma_divergence,
                      polarization_normal,
                      polarization_fraction);
    }
    return beam;
  }
    
  static
  double get_divergence(const Beam &beam, bool deg) {
    double divergence = beam.get_divergence();
    return deg ? rad_as_deg(divergence) : divergence;
  }

  static
  double get_sigma_divergence(const Beam &beam, bool deg) {
    double sigma_divergence = beam.get_sigma_divergence();
    return deg ? rad_as_deg(sigma_divergence) : sigma_divergence;
  }

  static
  void set_divergence(Beam &beam, double divergence, bool deg) {
    beam.set_divergence(deg ? deg_as_rad(divergence) : divergence);
  }

  static
  void set_sigma_divergence(Beam &beam, double sigma_divergence, 
                            bool deg) {
    beam.set_sigma_divergence(
      deg ? deg_as_rad(sigma_divergence) : sigma_divergence);
  }

  void export_beam()
  {
    // Export BeamBase
    class_ <BeamBase> ("BeamBase");

    // Export Beam : BeamBase
    class_ <Beam, bases <BeamBase> > ("Beam")
      .def(init <vec3 <double>,
                 double> ((
          arg("direction"), 
          arg("wavelength"))))
      .def(init <vec3 <double> > ((
          arg("s0"))))
      .def("__init__",
          make_constructor(
          &make_beam, 
          default_call_policies(), (
            arg("direction"),
            arg("wavelength"),
            arg("divergence"),
            arg("sigma_divergence"),
            arg("deg") = true)))          
      .def("__init__",
          make_constructor(
          &make_beam_w_s0, 
          default_call_policies(), (
            arg("s0"),
            arg("divergence"),
            arg("sigma_divergence"),
            arg("deg") = true)))
      .def("__init__",
          make_constructor(
          &make_beam_w_all, 
          default_call_policies(), (
	          arg("direction"),
	          arg("wavelength"),
	          arg("divergence"),
	          arg("sigma_divergence"),
            arg("polarization_normal"),
            arg("polarization_fraction"),
            arg("deg") = true)))          
      .def("get_direction", 
        &Beam::get_direction)
      .def("set_direction",
        &Beam::set_direction)
      .def("get_wavelength", 
        &Beam::get_wavelength)
      .def("set_wavelength",
        &Beam::set_wavelength)
      .def("get_s0",
        &Beam::get_s0)
      .def("set_s0",
        &Beam::set_s0)
      .def("get_unit_s0",
        &Beam::get_unit_s0)
      .def("set_unit_s0",
        &Beam::set_unit_s0)
      .def("get_divergence",
        &get_divergence, (
          arg("deg") = true))
      .def("set_divergence",
        &set_divergence, (
          arg("divergence"),
          arg("deg") = true))
      .def("get_sigma_divergence",
        &get_sigma_divergence, (
          arg("deg") = true))
      .def("set_sigma_divergence",
        &set_sigma_divergence, (
          arg("sigma_divergence"),
          arg("deg") = true))
      .def("get_polarization_normal",
        &Beam::get_polarization_normal)
      .def("set_polarization_normal",
        &Beam::set_polarization_normal)
      .def("get_polarization_fraction",
        &Beam::get_polarization_fraction)
      .def("set_polarization_fraction",
        &Beam::set_polarization_fraction)          
      .def("__eq__", &Beam::operator==)
      .def("__ne__", &Beam::operator!=)
      .def("__str__", &beam_to_string)
      .def_pickle(BeamPickleSuite());
  }

}}} // namespace = dxtbx::model::boost_python
