#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================


from cctbx import miller
import os
from cctbx import maptbx, sgtbx
import scitbx
from libtbx.utils import n_dim_index_from_one_dim
from cctbx_maptbx_ext import grid_indices_around_sites, grid2cart
from scitbx.array_family import flex
import iotbx
import random
import sys
from optparse import OptionParser
import urllib2, urllib
from iotbx.pdb import input as pdb_input
from cctbx import crystal
import string
from iotbx import ccp4_map
import gzip
import json

try:
    from sklearn import svm
    from sklearn import preprocessing
    from sklearn.externals import joblib
except:
    svm = None
    preprocessing = None
    joblib = None

svm = None

import pickle
SCALE = True
MAX_EDSUM = 3.0

import numpy as np


truncated_icosahedron = [
(0.000,-0.710,-3.448),
(0.000,-0.710, 3.448),
(0.000, 0.710,-3.448),
(0.000, 0.710, 3.448),
(0.710,-3.448, 0.000),
(0.710, 3.448, 0.000),
(3.448, 0.000,-0.710),
(3.448, 0.000, 0.710),
(1.420,-3.009,-1.149),
(1.420,-3.009, 1.149),
(1.420, 3.009,-1.149),
(1.420, 3.009, 1.149),
(3.009,-1.149,-1.420),
(3.009,-1.149, 1.420),
(3.009, 1.149,-1.420),
(3.009, 1.149, 1.420),
(1.149,-1.420,-3.009),
(1.149,-1.420, 3.009),
(1.149, 1.420,-3.009),
(1.149, 1.420, 3.009),
(0.710,-2.570,-2.298),
(0.710,-2.570, 2.298),
(0.710, 2.570,-2.298),
(0.710, 2.570, 2.298),
(2.570,-2.298,-0.710),
(2.570,-2.298, 0.710),
(2.570, 2.298,-0.710),
(2.570, 2.298, 0.710),
(2.298,-0.710,-2.570),
(2.298,-0.710, 2.570),
(2.298, 0.710,-2.570),
(2.298, 0.710, 2.570)
]

# ------------------------------------------------------------------------------

class po4finder:

    def __init__(self, input_map, symm, mask=None):

        try:
            self.uc = input_map.unit_cell()
            self.fft_map = input_map
            self.symm = self.fft_map.crystal_symmetry()
            self.fft_map_data = self.fft_map.real_map_unpadded()
            self.fft_map_data_double = self.fft_map_data
            # XXX
            self.mask = mask

            map_stats = maptbx.statistics(self.fft_map_data)
            print " [SVM] ==> Successfully loaded FFT map data (mean/sigma = %.2f/%.2f)" % (map_stats.mean(), map_stats.sigma())


        except:

            print "Something went very wrong with the input map interpretation, exiting..."
            exit(1)


        self.grid_to_cart = grid2cart(self.fft_map_data.focus(), \
                                    self.uc.orthogonalization_matrix())
        self.peak_params = None
        self.peak_heights = None
        self.peak_sites_frac = None
        self.peak_eigenparams = None
        self.selected_peaks = None

    # -------------------------------------------------------------------------

    def ccp4_to_fft_map(self, inp_ccp4_map, resolution_factor=1/4.):
        a,b,c = inp_ccp4_map.unit_cell_parameters[:3]
        nx,ny,nz = inp_ccp4_map.unit_cell_grid
        d1,d2,d3=a/nx/resolution_factor,b/ny/resolution_factor,c/nz/resolution_factor
        d_min_guess_from_map = min(d1,d2,d3)


        complete_set = miller.build_set(
                                crystal_symmetry = self.symm,
                                anomalous_flag   = False,
                                d_min            = d_min_guess_from_map)
        miller_array =  complete_set.structure_factors_from_map(
                                            map            = inp_ccp4_map.data,
                                            use_scale      = True,
                                            anomalous_flag = False,
                                            use_sg         = True)


        return miller_array.fft_map(resolution_factor = resolution_factor, \
                                            symmetry_flags = maptbx.use_space_group_symmetry)



    # -------------------------------------------------------------------------

    def eigensystem(self, site_frac, radius=2.5):

        map_sites_orth = flex.vec3_double()
        map_values = flex.double()
        raw_map_values = flex.double()
        site_orth = xyz = self.uc.orthogonalize(site_frac)
        weight_scale = self.fft_map_data_double.eight_point_interpolation(site_frac)
        if (weight_scale <= 0) : weight_scale = flex.max(self.fft_map_data.as_1d())

        equiv_points = sgtbx.sym_equiv_sites(unit_cell=self.uc,
                                space_group=self.symm.space_group(),
                                original_site=site_frac)


        sel = grid_indices_around_sites(
                    unit_cell=self.uc,
                    fft_n_real=self.fft_map_data.focus(),
                    fft_m_real=self.fft_map_data.all(),
                    sites_cart=flex.vec3_double([site_orth]),
                    site_radii=flex.double([radius]))

        for i_seq in sel:
            uvw = n_dim_index_from_one_dim(i_seq, self.fft_map_data.all())
            map_site_frac = self.uc.fractionalize(self.grid_to_cart(uvw))
            # n_dim_index_from_one_dim returns grid coords from inside the UC only
            # these must be mapped onto the input site closest sym mate
            if self.uc.distance(site_frac, map_site_frac)>radius:
                dist_info = sgtbx.min_sym_equiv_distance_info(equiv_points, map_site_frac)
                map_site_frac = dist_info.sym_op() * map_site_frac

            assert self.uc.distance(site_frac, map_site_frac ) < (radius + 0.001), \
                                "Selected symmetry mates should be within %.2f\AA " % radius


            map_sites_orth.append(self.uc.orthogonalize(map_site_frac))
            raw_map_values.append(self.fft_map_data[i_seq])
            map_values.append(max(0.000001, self.fft_map_data[i_seq]) / weight_scale)


        return sum(raw_map_values)/float(len(raw_map_values)), \
                    scitbx.math.principal_axes_of_inertia(
                                    points=map_sites_orth,
                                    weights=map_values).eigensystem()

    # --no-----------------------------------------------------------------------

    def calc_knuspr_params(self, sites_frac=None, verbose=False):

        if not self.peak_sites_frac: self.peaksearch()

        peak_params = []
        self.peak_eigenparams = []
        i=0

        for site_frac in sites_frac if sites_frac else self.peak_sites_frac:
            ed_sum, eigen = self.eigensystem(site_frac)
            vals = eigen.values()
            self.peak_eigenparams.append((vals[0]-vals[2])/vals[1])
            params = [ed_sum, self.peak_eigenparams[-1], self.site_CC(site_frac)]
            i+=1
            peak_params.append(params)
        self.peak_params = peak_params

    # -------------------------------------------------------------------------

    def site_CC(self, site_frac, radius=2.5, npoints=50):

        npoints = len(truncated_icosahedron)

        mplu = []
        mmin = []

        site_orth = xyz = self.uc.orthogonalize(site_frac)
        site_orth = scitbx.matrix.col(site_orth)

        for ipoint in xrange(npoints):
            rnd_vec = radius*scitbx.matrix.col(truncated_icosahedron[ipoint])
            mmin.append(self.fft_map_data_double.eight_point_interpolation(self.uc.fractionalize(site_orth+rnd_vec)))
            mplu.append(self.fft_map_data_double.eight_point_interpolation(self.uc.fractionalize(site_orth-rnd_vec)))


        return flex.linear_correlation(x = flex.double(mmin), y = flex.double(mplu)).coefficient()






    # -------------------------------------------------------------------------

    def peaks_as_pdb(self, only_selected=False, filename=None):

        if self.peak_params == None: self.calc_knuspr_params()



        if only_selected:
            if not self.selected_peaks: self.SVM_select_po4()
            selected = self.selected_peaks
        else:
            selected = [True]*len(self.peak_params)


        tmp_structure = iotbx.pdb.hierarchy.root()
        tmp_structure.append_model(iotbx.pdb.hierarchy.model(id = "0"))
        chain = iotbx.pdb.hierarchy.chain(id="X")
        tmp_structure.models()[0].append_chain(chain)

        idx = 0
        for i,(h,site) in enumerate(zip(self.peak_heights, self.peak_sites_frac)):
            if not selected[i]: continue
            xyz = self.uc.orthogonalize(site)

            rg = iotbx.pdb.hierarchy.residue_group(resseq = "%i"%idx)
            ag = iotbx.pdb.hierarchy.atom_group(resname = "XXX", altloc = " ")
            atom = iotbx.pdb.hierarchy.atom()

            ag.append_atom(atom)
            rg.append_atom_group(atom_group = ag)
            chain.append_residue_group(rg)

            atom.name = "P"
            atom.set_element('P')
            atom.set_xyz(xyz)
            atom.set_occ(1.0)
            atom.set_b( min(h, 9999.99) )

            idx += 1

        if filename: tmp_structure.write_pdb_file(file_name=filename, \
                                crystal_symmetry=self.symm)

        return tmp_structure




    # -------------------------------------------------------------------------

    def peaksearch(self, max_number_of_peaks=1000):


        gridding = maptbx.crystal_gridding(
            unit_cell=self.uc,
            step=10,
            symmetry_flags = sgtbx.search_symmetry_flags(use_space_group_symmetry=True),
            space_group_info=self.symm.space_group_info(),
            assert_shannon_sampling=True)

        crystal_gridding_tags = maptbx.crystal_gridding_tags(gridding)

        focus = self.fft_map_data.focus()

        grid_tags = maptbx.grid_tags(focus)
        grid_tags.build(self.symm.space_group_info().type(),
                        sgtbx.search_symmetry_flags(use_space_group_symmetry=False))

        assert grid_tags.n_grid_misses() == 0

        crystal_gridding_tags = grid_tags



        peak_search_parameters = maptbx.peak_search_parameters(
                                    peak_search_level      = 1,
                                    max_peaks              = 0,
                                    peak_cutoff            = 1.0,
                                    interpolate            = True,
                                    min_distance_sym_equiv = 1.5,
                                    general_positions_only = True,
                                    min_cross_distance     = 1.5,
                                    min_cubicle_edge       = 5)



        parameters = peak_search_parameters

        data = self.fft_map_data.as_double()
        if self.mask: data *= self.mask
        peak_list = maptbx.peak_list(
                                data=data,
                                tags=crystal_gridding_tags.tag_array(),
                                peak_search_level=parameters.peak_search_level(),
                                max_peaks=parameters.max_peaks(),
                                peak_cutoff=parameters.peak_cutoff(),
                                interpolate=parameters.interpolate())

        #[x for (y,x) in sorted(zip(peak_list.sites(),X), key=lambda pair: pair[0])]
        assert peak_list.gridding() == self.fft_map_data.focus()

        if 0:
            cluster_analysis = maptbx.peak_cluster_analysis(
                    peak_list=peak_list,
                    special_position_settings=crystal.special_position_settings(
                                    crystal_symmetry=self.symm,
                                    min_distance_sym_equiv=parameters.min_distance_sym_equiv()),
                    general_positions_only=False,
                    effective_resolution=None,
                    min_cross_distance=4.0,
                    max_clusters=max_number_of_peaks).all()


            self.peak_heights = cluster_analysis.heights()
            self.peak_sites_frac = flex.vec3_double(cluster_analysis.sites())

        else:

            min_peak_distance = 4.0

            special_position_settings = crystal.special_position_settings(
                                                    crystal_symmetry=self.symm, \
                                                    min_distance_sym_equiv=min_peak_distance)
            self.peak_heights = []
            self.peak_sites_frac = []

            peaks = []
            for i,site in enumerate(peak_list.sites()):
                peaks.append( (special_position_settings.site_symmetry(site).exact_site(),
                               peak_list.heights()[i]) )

            # XXX ugly hack: osx and linux peak heights may slightly differ,
            # and thus the order of the peaksearch results. this will force
            # arbitrary order, and make the osx/linux results identical
            peaks_sorted = sorted( peaks, key=lambda x: (round(x[1],12), x[0][0]), reverse=True )

            #for i,site in enumerate(peak_list.sites()):
            for ipeak in peaks_sorted:
                peak_site = ipeak[0]#special_position_settings.site_symmetry(site).exact_site()
                peak_height = ipeak[1]#peak_list.heights()[i]
                site_symmetry = special_position_settings.site_symmetry(peak_site)
                equiv_sites = sgtbx.sym_equiv_sites(site_symmetry)
                keep = True
                for reduced_peak_site in self.peak_sites_frac:
                    dist = sgtbx.min_sym_equiv_distance_info(
                                    equiv_sites, reduced_peak_site).dist()
                    if (dist < min_peak_distance):
                        keep = False
                        break
                if (keep == True):
                    self.peak_sites_frac.append(peak_site)
                    self.peak_heights.append(peak_height)

                if len(self.peak_sites_frac) == max_number_of_peaks: break

        #for p,ph in zip(peak_list.sites(), peak_list.heights())[:10]: print p,ph
        #for p,ph in zip(self.peak_sites_frac, self.peak_heights)[:10]: print p,ph

        print " [SVM] ==> Detected %i putative phosphate groups in the Unit Cell" % len(self.peak_heights)

    # -------------------------------------------------------------------------



    def decision_function(self, x, SVs, sv_coef, nSV, rho, gamma):

        """
            A very simple, TWO-class version of the decision function mplemented in the sklearn
        """

        kvalue = np.zeros(len(SVs))
        for i in range(len(SVs)):
            kvalue[i] = np.exp( -gamma * np.dot((x-SVs[i]), (x-SVs[i])) )


        tmpsum = 0.0


        for k in range(nSV[0]):
            tmpsum += sv_coef[k] * kvalue[k]
        for k in range(nSV[1]):
            tmpsum += sv_coef[nSV[0]+k] * kvalue[nSV[0]+k]

        return rho-tmpsum

    # --------------------------------------------------------------------------


    def SVM_select_po4(self, top_hits_frac = 1.0, model_fname="./data/svm_model.dat"):

        path_to_script = os.path.dirname(os.path.realpath(__file__))

        print " [SVM] ==> Loading SVM model and running the classifier"


        # load the SVM model
        model_dict_fname = os.path.join(path_to_script, "..", "data/data.svm.gz")

        svm_data_dict = json.loads( gzip.open(model_dict_fname, "r").read() )

        support_vectors = np.array(svm_data_dict['support_vectors'])
        gamma = svm_data_dict['gamma']
        rho = svm_data_dict['rho']
        n_support = np.array(svm_data_dict['n_support'])
        sv_coef = np.array(svm_data_dict['sv_coef'])




        self.calc_knuspr_params()



        # generate ranks
        for i,item in enumerate(sorted(self.peak_params, key=lambda x: x[0], reverse=True)):
            item[0] = float(i+1)

        # Scale data onto a (0,1) range
        feature_range = (0, 1)

        X = self.peak_params

        data_min = np.min(X, axis=0)
        data_range = np.max(X, axis=0) - data_min


        data_range[data_range == 0.0] = 1.0
        scale_ = (feature_range[1] - feature_range[0]) / data_range
        min_ = feature_range[0] - data_min * scale_
        X *= scale_
        X += min_

        self.peak_params = X

        # run predictor
        scores = flex.double([self.decision_function(pp, support_vectors, sv_coef, n_support, rho, gamma) \
                                                for pp in self.peak_params]).as_numpy_array()



        # select peaks
        sel = scores[(scores>0)]
        sel.sort()
        cutoff = sel[-int(len(sel)*top_hits_frac)]

        self.selected_peaks = (scores>cutoff)
        return

    # -------------------------------------------------------------------------

    # this code doesn't work, but looks kind of interesting
    def check_peaks(structure, peak_sites, peak_scores, max_min_dist=1.0):

        p_scores = []
        nop_scores = []

        for peak_site, peak_score in zip(peak_sites, peak_scores):
            for scatterer in structure.scatterers():
                site_symmetry = structure.site_symmetry(scatterer.site)
                equiv_sites = sgtbx.sym_equiv_sites(site_symmetry)
                min_dist = None
                dist_info = sgtbx.min_sym_equiv_distance_info(equiv_sites, peak_site)
                if (min_dist is None):
                    min_dist = dist_info.dist()
                else:
                    min_dist = min(min_dist, dist_info.dist())
            print min_dist
            if min_dist<max_min_dist:
                p_scores.append(peak_score)
            else:
                nop_scores.append(peak_score)

        return p_scores, nop_scores


    # -------------------------------------------------------------------------


    def find_hits(self, target_ph, target_xrs, peaks_as_pdb, peak_scores):


        p_scores = []
        nop_scores = []

        test_hierarchy = iotbx.pdb.hierarchy.root()
        test_hierarchy.append_model(iotbx.pdb.hierarchy.model(id="0"))
        test_hierarchy.append_model(iotbx.pdb.hierarchy.model(id="1"))

        isel_cache = target_ph.atom_selection_cache()
        isel = isel_cache.iselection

        for chain in target_ph.select(isel(r"name P and (altloc ' ' or altloc 'a')")).chains():
            test_hierarchy.models()[0].append_chain(chain.detached_copy())

        for chain in peaks_as_pdb.chains():
            test_hierarchy.models()[1].append_chain(chain.detached_copy())

        test_hierarchy.atoms_reset_serial()

        sel_cache = test_hierarchy.atom_selection_cache()
        sel = sel_cache.selection

        uc = target_xrs.crystal_symmetry().unit_cell()
        test_xrs = test_hierarchy.extract_xray_structure(crystal_symmetry=target_xrs.crystal_symmetry())
        test_asu_mappings = test_xrs.asu_mappings(buffer_thickness=0.0)
        test_pairs = crystal.neighbors_simple_pair_generator(test_asu_mappings, distance_cutoff=1.0)

        clean_hits = [False]*len(peak_scores)

        for pair in test_pairs:

            resi_i = test_hierarchy.atoms()[pair.i_seq].parent().parent()
            resi_j = test_hierarchy.atoms()[pair.j_seq].parent().parent()

            if resi_i.parent().parent().id == resi_j.parent().parent().id: continue

            assert resi_i.only_atom().name.strip() == resi_j.only_atom().name.strip()
            assert resi_i.only_atom().name.strip() == "P"

            if resi_i.parent().parent().id > resi_j.parent().parent().id: resi_i, resi_j = resi_j, resi_i

            rt_mx_i = test_asu_mappings.get_rt_mx_i(pair)
            rt_mx_j = test_asu_mappings.get_rt_mx_j(pair)
            rt_mx_ji = rt_mx_i.inverse().multiply(rt_mx_j)


            assert uc.distance(uc.fractionalize(resi_i.only_atom().xyz), rt_mx_ji*uc.fractionalize(resi_j.only_atom().xyz))<1.0


            p_scores.append(peak_scores[string.atoi(resi_j.resseq)])

            clean_hits[string.atoi(resi_j.resseq)] = True

        for i,peak in enumerate(peak_scores):
            if not clean_hits[i]: nop_scores.append(peak_scores[i])

        return p_scores, nop_scores


    # -------------------------------------------------------------------------
