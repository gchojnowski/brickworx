#!/bin/sh

#export MMTBX_CCP4_MONOMER_LIB=~/xray/phenix/phenix-1.8-1069/chem_data/mon_lib/
#export CLIBD_MON=~/xray/phenix/phenix-1.8-1069/chem_data/geostd/

if [ ! -d ../virtualenv ] ; then
  ../_create_virtualenv
  ../_install_cctbx
fi

. ../virtualenv/bin/activate
. ../virtualenv/cctbx_build/setpaths.sh

libtbx.python ./looper.py $@
