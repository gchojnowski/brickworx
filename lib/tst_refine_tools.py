#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================


import mmtbx.refinement.rigid_body

from mmtbx.refinement.real_space import individual_sites
import mmtbx.command_line.real_space_refine as rs
import mmtbx.monomer_library.pdb_interpretation
from mmtbx import monomer_library
from scitbx.array_family import flex
import cctbx
from libtbx import group_args
import random

from refine_tools import refine_complete_structure, rigid_body_refinement, refinery


pdb_str_stem="""\
CRYST1   32.780   32.780  52.500  90.00  90.00  90.00 P 1
ATOM      1  P     A A   1       3.063   8.025  -4.135  1.00  0.00           P  
ATOM      2  O1P   A A   1       3.223   8.856  -5.350  1.00  0.00           O  
ATOM      3  O2P   A A   1       1.891   7.121  -4.118  1.00  0.00           O  
ATOM      4  O5'   A A   1       4.396   7.181  -3.881  1.00  0.00           O  
ATOM      5  C5'   A A   1       5.621   7.881  -3.587  1.00  0.00           C  
ATOM      6  C4'   A A   1       6.719   6.889  -3.258  1.00  0.00           C  
ATOM      7  O4'   A A   1       6.486   6.364  -1.919  1.00  0.00           O  
ATOM      8  C3'   A A   1       6.776   5.642  -4.140  1.00  0.00           C  
ATOM      9  O3'   A A   1       7.397   5.908  -5.390  1.00  0.00           O  
ATOM     10  C2'   A A   1       7.567   4.725  -3.208  1.00  0.00           C  
ATOM     11  O2'   A A   1       8.962   4.911  -3.068  1.00  0.00           O  
ATOM     12  C1'   A A   1       6.874   4.999  -1.877  1.00  0.00           C  
ATOM     13  N9    A A   1       5.666   4.158  -1.647  1.00  0.00           N  
ATOM     14  C8    A A   1       4.345   4.449  -1.900  1.00  0.00           C  
ATOM     15  N7    A A   1       3.524   3.496  -1.584  1.00  0.00           N  
ATOM     16  C5    A A   1       4.348   2.496  -1.086  1.00  0.00           C  
ATOM     17  C6    A A   1       4.082   1.215  -0.578  1.00  0.00           C  
ATOM     18  N6    A A   1       2.849   0.697  -0.485  1.00  0.00           N  
ATOM     19  N1    A A   1       5.134   0.481  -0.167  1.00  0.00           N  
ATOM     20  C2    A A   1       6.356   1.001  -0.262  1.00  0.00           C  
ATOM     21  N3    A A   1       6.725   2.179  -0.716  1.00  0.00           N  
ATOM     22  C4    A A   1       5.655   2.893  -1.121  1.00  0.00           C  
ATOM     23  P     U A   2       6.913   5.099  -6.683  1.00  0.00           P  
ATOM     24  O1P   U A   2       7.496   5.711  -7.898  1.00  0.00           O  
ATOM     25  O2P   U A   2       5.439   4.971  -6.666  1.00  0.00           O  
ATOM     26  O5'   U A   2       7.579   3.667  -6.429  1.00  0.00           O  
ATOM     27  C5'   U A   2       8.988   3.595  -6.135  1.00  0.00           C  
ATOM     28  C4'   U A   2       9.376   2.167  -5.806  1.00  0.00           C  
ATOM     29  O4'   U A   2       8.896   1.851  -4.467  1.00  0.00           O  
ATOM     30  C3'   U A   2       8.750   1.087  -6.688  1.00  0.00           C  
ATOM     31  O3'   U A   2       9.417   0.975  -7.938  1.00  0.00           O  
ATOM     32  C2'   U A   2       8.920  -0.112  -5.756  1.00  0.00           C  
ATOM     33  O2'   U A   2      10.194  -0.710  -5.616  1.00  0.00           O  
ATOM     34  C1'   U A   2       8.486   0.493  -4.425  1.00  0.00           C  
ATOM     35  N1    U A   2       7.014   0.438  -4.195  1.00  0.00           N  
ATOM     36  C2    U A   2       6.509  -0.720  -3.655  1.00  0.00           C  
ATOM     37  O2    U A   2       7.207  -1.677  -3.367  1.00  0.00           O  
ATOM     38  N3    U A   2       5.143  -0.735  -3.456  1.00  0.00           N  
ATOM     39  C4    U A   2       4.261   0.286  -3.745  1.00  0.00           C  
ATOM     40  O4    U A   2       3.056   0.157  -3.522  1.00  0.00           O  
ATOM     41  C5    U A   2       4.885   1.461  -4.308  1.00  0.00           C  
ATOM     42  C6    U A   2       6.213   1.503  -4.512  1.00  0.00           C  
ATOM     43  P     A A   3       8.572   0.556  -9.231  1.00  0.00           P  
ATOM     44  O1P   A A   3       9.394   0.756 -10.446  1.00  0.00           O  
ATOM     45  O2P   A A   3       7.262   1.245  -9.214  1.00  0.00           O  
ATOM     46  O5'   A A   3       8.359  -1.008  -8.977  1.00  0.00           O  
ATOM     47  C5'   A A   3       9.505  -1.830  -8.683  1.00  0.00           C  
ATOM     48  C4'   A A   3       9.061  -3.241  -8.354  1.00  0.00           C  
ATOM     49  O4'   A A   3       8.487  -3.248  -7.015  1.00  0.00           O  
ATOM     50  C3'   A A   3       7.950  -3.812  -9.236  1.00  0.00           C  
ATOM     51  O3'   A A   3       8.451  -4.266 -10.486  1.00  0.00           O  
ATOM     52  C2'   A A   3       7.446  -4.913  -8.304  1.00  0.00           C  
ATOM     53  O2'   A A   3       8.196  -6.104  -8.164  1.00  0.00           O  
ATOM     54  C1'   A A   3       7.407  -4.169  -6.973  1.00  0.00           C  
ATOM     55  N9    A A   3       6.139  -3.421  -6.743  1.00  0.00           N  
ATOM     56  C8    A A   3       5.854  -2.099  -6.996  1.00  0.00           C  
ATOM     57  N7    A A   3       4.646  -1.749  -6.680  1.00  0.00           N  
ATOM     58  C5    A A   3       4.079  -2.914  -6.182  1.00  0.00           C  
ATOM     59  C6    A A   3       2.804  -3.206  -5.674  1.00  0.00           C  
ATOM     60  N6    A A   3       1.820  -2.300  -5.581  1.00  0.00           N  
ATOM     61  N1    A A   3       2.574  -4.467  -5.263  1.00  0.00           N  
ATOM     62  C2    A A   3       3.556  -5.362  -5.358  1.00  0.00           C  
ATOM     63  N3    A A   3       4.781  -5.207  -5.812  1.00  0.00           N  
ATOM     64  C4    A A   3       4.984  -3.937  -6.217  1.00  0.00           C  
ATOM     65  P     A A   4       7.514  -4.163 -11.779  1.00  0.00           P  
ATOM     66  O1P   A A   4       8.313  -4.439 -12.994  1.00  0.00           O  
ATOM     67  O2P   A A   4       6.784  -2.876 -11.762  1.00  0.00           O  
ATOM     68  O5'   A A   4       6.490  -5.364 -11.525  1.00  0.00           O  
ATOM     69  C5'   A A   4       7.010  -6.675 -11.231  1.00  0.00           C  
ATOM     70  C4'   A A   4       5.874  -7.623 -10.902  1.00  0.00           C  
ATOM     71  O4'   A A   4       5.387  -7.318  -9.563  1.00  0.00           O  
ATOM     72  C3'   A A   4       4.631  -7.503 -11.784  1.00  0.00           C  
ATOM     73  O3'   A A   4       4.807  -8.156 -13.034  1.00  0.00           O  
ATOM     74  C2'   A A   4       3.612  -8.157 -10.852  1.00  0.00           C  
ATOM     75  O2'   A A   4       3.599  -9.564 -10.712  1.00  0.00           O  
ATOM     76  C1'   A A   4       3.981  -7.510  -9.521  1.00  0.00           C  
ATOM     77  N9    A A   4       3.318  -6.195  -9.291  1.00  0.00           N  
ATOM     78  C8    A A   4       3.793  -4.929  -9.544  1.00  0.00           C  
ATOM     79  N7    A A   4       2.965  -3.982  -9.228  1.00  0.00           N  
ATOM     80  C5    A A   4       1.858  -4.656  -8.730  1.00  0.00           C  
ATOM     81  C6    A A   4       0.628  -4.212  -8.222  1.00  0.00           C  
ATOM     82  N6    A A   4       0.289  -2.919  -8.129  1.00  0.00           N  
ATOM     83  N1    A A   4      -0.247  -5.150  -7.811  1.00  0.00           N  
ATOM     84  C2    A A   4       0.095  -6.433  -7.906  1.00  0.00           C  
ATOM     85  N3    A A   4       1.210  -6.965  -8.360  1.00  0.00           N  
ATOM     86  C4    A A   4       2.067  -6.006  -8.765  1.00  0.00           C  
ATOM     87  P     U B   5      -8.377  -1.902  -3.509  1.00  0.00           P  
ATOM     88  O1P   U B   5      -9.222  -1.943  -2.294  1.00  0.00           O  
ATOM     89  O2P   U B   5      -7.317  -0.869  -3.526  1.00  0.00           O  
ATOM     90  O5'   U B   5      -7.729  -3.341  -3.763  1.00  0.00           O  
ATOM     91  C5'   U B   5      -8.594  -4.454  -4.057  1.00  0.00           C  
ATOM     92  C4'   U B   5      -7.767  -5.681  -4.386  1.00  0.00           C  
ATOM     93  O4'   U B   5      -7.215  -5.525  -5.725  1.00  0.00           O  
ATOM     94  C3'   U B   5      -6.540  -5.913  -3.504  1.00  0.00           C  
ATOM     95  O3'   U B   5      -6.891  -6.491  -2.254  1.00  0.00           O  
ATOM     96  C2'   U B   5      -5.744  -6.826  -4.436  1.00  0.00           C  
ATOM     97  O2'   U B   5      -6.124  -8.181  -4.576  1.00  0.00           O  
ATOM     98  C1'   U B   5      -5.918  -6.101  -5.767  1.00  0.00           C  
ATOM     99  N1    U B   5      -4.915  -5.024  -5.997  1.00  0.00           N  
ATOM    100  C2    U B   5      -3.708  -5.398  -6.537  1.00  0.00           C  
ATOM    101  O2    U B   5      -3.440  -6.552  -6.825  1.00  0.00           O  
ATOM    102  N3    U B   5      -2.804  -4.373  -6.736  1.00  0.00           N  
ATOM    103  C4    U B   5      -3.001  -3.039  -6.447  1.00  0.00           C  
ATOM    104  O4    U B   5      -2.116  -2.211  -6.670  1.00  0.00           O  
ATOM    105  C5    U B   5      -4.298  -2.743  -5.884  1.00  0.00           C  
ATOM    106  C6    U B   5      -5.197  -3.721  -5.680  1.00  0.00           C  
ATOM    107  P     U B   6      -6.022  -6.126  -0.961  1.00  0.00           P  
ATOM    108  O1P   U B   6      -6.710  -6.617   0.254  1.00  0.00           O  
ATOM    109  O2P   U B   6      -5.688  -4.684  -0.978  1.00  0.00           O  
ATOM    110  O5'   U B   6      -4.699  -6.987  -1.215  1.00  0.00           O  
ATOM    111  C5'   U B   6      -4.826  -8.391  -1.509  1.00  0.00           C  
ATOM    112  C4'   U B   6      -3.467  -8.977  -1.838  1.00  0.00           C  
ATOM    113  O4'   U B   6      -3.086  -8.547  -3.177  1.00  0.00           O  
ATOM    114  C3'   U B   6      -2.309  -8.509  -0.956  1.00  0.00           C  
ATOM    115  O3'   U B   6      -2.293  -9.185   0.294  1.00  0.00           O  
ATOM    116  C2'   U B   6      -1.146  -8.847  -1.888  1.00  0.00           C  
ATOM    117  O2'   U B   6      -0.734 -10.193  -2.028  1.00  0.00           O  
ATOM    118  C1'   U B   6      -1.684  -8.332  -3.219  1.00  0.00           C  
ATOM    119  N1    U B   6      -1.422  -6.883  -3.449  1.00  0.00           N  
ATOM    120  C2    U B   6      -0.204  -6.546  -3.989  1.00  0.00           C  
ATOM    121  O2    U B   6       0.645  -7.372  -4.277  1.00  0.00           O  
ATOM    122  N3    U B   6       0.003  -5.195  -4.188  1.00  0.00           N  
ATOM    123  C4    U B   6      -0.884  -4.179  -3.899  1.00  0.00           C  
ATOM    124  O4    U B   6      -0.586  -3.003  -4.122  1.00  0.00           O  
ATOM    125  C5    U B   6      -2.135  -4.631  -3.336  1.00  0.00           C  
ATOM    126  C6    U B   6      -2.363  -5.939  -3.132  1.00  0.00           C  
ATOM    127  P     A B   7      -1.758  -8.408   1.587  1.00  0.00           P  
ATOM    128  O1P   A B   7      -2.072  -9.193   2.802  1.00  0.00           O  
ATOM    129  O2P   A B   7      -2.256  -7.014   1.570  1.00  0.00           O  
ATOM    130  O5'   A B   7      -0.180  -8.418   1.333  1.00  0.00           O  
ATOM    131  C5'   A B   7       0.473  -9.668   1.039  1.00  0.00           C  
ATOM    132  C4'   A B   7       1.932  -9.427   0.710  1.00  0.00           C  
ATOM    133  O4'   A B   7       2.020  -8.860  -0.629  1.00  0.00           O  
ATOM    134  C3'   A B   7       2.654  -8.408   1.592  1.00  0.00           C  
ATOM    135  O3'   A B   7       3.033  -8.968   2.842  1.00  0.00           O  
ATOM    136  C2'   A B   7       3.815  -8.064   0.660  1.00  0.00           C  
ATOM    137  O2'   A B   7       4.888  -8.974   0.520  1.00  0.00           O  
ATOM    138  C1'   A B   7       3.084  -7.921  -0.671  1.00  0.00           C  
ATOM    139  N9    A B   7       2.522  -6.560  -0.901  1.00  0.00           N  
ATOM    140  C8    A B   7       1.253  -6.091  -0.648  1.00  0.00           C  
ATOM    141  N7    A B   7       1.077  -4.846  -0.964  1.00  0.00           N  
ATOM    142  C5    A B   7       2.310  -4.449  -1.462  1.00  0.00           C  
ATOM    143  C6    A B   7       2.779  -3.228  -1.970  1.00  0.00           C  
ATOM    144  N6    A B   7       2.021  -2.126  -2.063  1.00  0.00           N  
ATOM    145  N1    A B   7       4.060  -3.178  -2.381  1.00  0.00           N  
ATOM    146  C2    A B   7       4.808  -4.276  -2.286  1.00  0.00           C  
ATOM    147  N3    A B   7       4.482  -5.467  -1.832  1.00  0.00           N  
ATOM    148  C4    A B   7       3.196  -5.489  -1.427  1.00  0.00           C  
ATOM    149  P     U B   8       3.063  -8.025   4.135  1.00  0.00           P  
ATOM    150  O1P   U B   8       3.223  -8.856   5.350  1.00  0.00           O  
ATOM    151  O2P   U B   8       1.891  -7.121   4.118  1.00  0.00           O  
ATOM    152  O5'   U B   8       4.397  -7.181   3.881  1.00  0.00           O  
ATOM    153  C5'   U B   8       5.621  -7.881   3.587  1.00  0.00           C  
ATOM    154  C4'   U B   8       6.719  -6.889   3.258  1.00  0.00           C  
ATOM    155  O4'   U B   8       6.486  -6.364   1.919  1.00  0.00           O  
ATOM    156  C3'   U B   8       6.776  -5.642   4.140  1.00  0.00           C  
ATOM    157  O3'   U B   8       7.397  -5.908   5.390  1.00  0.00           O  
ATOM    158  C2'   U B   8       7.567  -4.725   3.208  1.00  0.00           C  
ATOM    159  O2'   U B   8       8.962  -4.910   3.068  1.00  0.00           O  
ATOM    160  C1'   U B   8       6.874  -4.999   1.877  1.00  0.00           C  
ATOM    161  N1    U B   8       5.666  -4.158   1.647  1.00  0.00           N  
ATOM    162  C2    U B   8       5.867  -2.911   1.107  1.00  0.00           C  
ATOM    163  O2    U B   8       6.971  -2.482   0.819  1.00  0.00           O  
ATOM    164  N3    U B   8       4.725  -2.160   0.908  1.00  0.00           N  
ATOM    165  C4    U B   8       3.431  -2.543   1.197  1.00  0.00           C  
ATOM    166  O4    U B   8       2.487  -1.783   0.974  1.00  0.00           O  
ATOM    167  C5    U B   8       3.322  -3.869   1.760  1.00  0.00           C  
ATOM    168  C6    U B   8       4.416  -4.621   1.964  1.00  0.00           C  
END
"""

pdb_str = """\
CRYST1   32.780   32.780  52.500  90.00  90.00  90.00 P 1
ATOM   1647  P     U A  49      27.406  24.111   5.535  1.00 76.93           P
ATOM   1648  OP1   U A  49      26.997  25.010   6.589  1.00 76.93           O
ATOM   1649  OP2   U A  49      28.722  23.341   5.597  1.00 76.93           O
ATOM   1650  O5'   U A  49      26.301  22.971   5.410  1.00 76.93           O
ATOM   1651  C5'   U A  49      24.952  23.291   5.089  1.00 76.93           C
ATOM   1652  C4'   U A  49      24.125  22.037   4.965  1.00 76.93           C
ATOM   1653  O4'   U A  49      24.380  21.382   3.696  1.00 76.93           O
ATOM   1654  C3'   U A  49      24.358  20.949   6.005  1.00 76.93           C
ATOM   1655  O3'   U A  49      23.649  21.266   7.226  1.00 76.93           O
ATOM   1656  C2'   U A  49      23.806  19.741   5.252  1.00 76.93           C
ATOM   1657  O2'   U A  49      22.393  19.854   5.124  1.00 76.93           O
ATOM   1658  C1'   U A  49      24.336  19.992   3.856  1.00 76.93           C
ATOM   1659  N1    U A  49      25.679  19.422   3.665  1.00 76.93           N
ATOM   1660  C2    U A  49      25.744  18.046   3.478  1.00 76.93           C
ATOM   1661  O2    U A  49      24.753  17.328   3.587  1.00 76.93           O
ATOM   1662  N3    U A  49      27.001  17.548   3.176  1.00 76.93           N
ATOM   1663  C4    U A  49      28.165  18.289   3.087  1.00 76.93           C
ATOM   1664  O4    U A  49      29.220  17.736   2.680  1.00 76.93           O
ATOM   1665  C5    U A  49      28.014  19.686   3.390  1.00 76.93           C
ATOM   1666  C6    U A  49      26.815  20.193   3.666  1.00 76.93           C
ATOM   1667  H5'   U A  49      24.917  23.856   4.145  1.00 76.93           H
ATOM   1668 H5''   U A  49      24.528  23.942   5.869  1.00 76.93           H
ATOM   1669  H4'   U A  49      23.104  22.426   5.093  1.00 76.93           H
ATOM   1670  H3'   U A  49      25.393  20.804   6.346  1.00 76.93           H
ATOM   1671  H2'   U A  49      24.064  18.785   5.731  1.00 76.93           H
ATOM   1672 HO2'   U A  49      22.035  20.426   5.862  1.00 76.93           H
ATOM   1673  H1'   U A  49      23.684  19.510   3.113  1.00 76.93           H
ATOM   1674  H3    U A  49      27.074  16.566   3.007  1.00 76.93           H
ATOM   1675  H5    U A  49      28.897  20.342   3.393  1.00 76.93           H
ATOM   1676  H6    U A  49      26.731  21.265   3.902  1.00 76.93           H
ATOM   1677  P     G A  50      24.225  20.753   8.657  1.00 82.98           P
ATOM   1678  OP1   G A  50      23.332  21.317   9.712  1.00 82.98           O
ATOM   1679  OP2   G A  50      25.685  21.008   8.745  1.00 82.98           O
ATOM   1680  O5'   G A  50      24.034  19.201   8.547  1.00 82.98           O
ATOM   1681  C5'   G A  50      22.696  18.676   8.442  1.00 82.98           C
ATOM   1682  C4'   G A  50      22.732  17.193   8.270  1.00 82.98           C
ATOM   1683  O4'   G A  50      23.358  16.863   6.998  1.00 82.98           O
ATOM   1684  C3'   G A  50      23.534  16.415   9.298  1.00 82.98           C
ATOM   1685  O3'   G A  50      22.816  16.235  10.515  1.00 82.98           O
ATOM   1686  C2'   G A  50      23.827  15.134   8.534  1.00 82.98           C
ATOM   1687  O2'   G A  50      22.653  14.358   8.396  1.00 82.98           O
ATOM   1688  C1'   G A  50      24.099  15.661   7.119  1.00 82.98           C
ATOM   1689  N9    G A  50      25.511  15.935   6.825  1.00 82.98           N
ATOM   1690  C8    G A  50      26.158  17.145   6.907  1.00 82.98           C
ATOM   1691  N7    G A  50      27.424  17.067   6.547  1.00 82.98           N
ATOM   1692  C5    G A  50      27.627  15.721   6.241  1.00 82.98           C
ATOM   1693  C6    G A  50      28.820  15.008   5.846  1.00 82.98           C
ATOM   1694  O6    G A  50      29.946  15.450   5.636  1.00 82.98           O
ATOM   1695  N1    G A  50      28.587  13.640   5.703  1.00 82.98           N
ATOM   1696  C2    G A  50      27.361  13.033   5.887  1.00 82.98           C
ATOM   1697  N2    G A  50      27.324  11.702   5.725  1.00 82.98           N
ATOM   1698  N3    G A  50      26.258  13.683   6.212  1.00 82.98           N
ATOM   1699  C4    G A  50      26.462  15.009   6.393  1.00 82.98           C
ATOM   1700  H5'   G A  50      22.180  19.139   7.588  1.00 82.98           H
ATOM   1701 H5''   G A  50      22.121  18.934   9.343  1.00 82.98           H
ATOM   1702  H4'   G A  50      21.674  16.905   8.362  1.00 82.98           H
ATOM   1703  H3'   G A  50      24.449  16.912   9.652  1.00 82.98           H
ATOM   1704  H2'   G A  50      24.615  14.538   9.018  1.00 82.98           H
ATOM   1705 HO2'   G A  50      22.863  13.521   7.891  1.00 82.98           H
ATOM   1706  H1'   G A  50      23.804  14.881   6.402  1.00 82.98           H
ATOM   1707  H8    G A  50      25.673  18.075   7.238  1.00 82.98           H
ATOM   1708  H1    G A  50      29.361  13.060   5.449  1.00 82.98           H
ATOM   1709  H21   G A  50      26.464  11.208   5.846  1.00 82.98           H
ATOM   1710  H22   G A  50      28.157  11.205   5.482  1.00 82.98           H
ATOM   1711  P     U A  51      23.623  16.113  11.930  1.00 85.25           P
ATOM   1712  OP1   U A  51      22.670  16.092  13.089  1.00 85.25           O
ATOM   1713  OP2   U A  51      24.730  17.105  11.982  1.00 85.25           O
ATOM   1714  O5'   U A  51      24.263  14.685  11.748  1.00 85.25           O
ATOM   1715  C5'   U A  51      23.470  13.534  11.662  1.00 85.25           C
ATOM   1716  C4'   U A  51      24.311  12.340  11.340  1.00 85.25           C
ATOM   1717  O4'   U A  51      24.819  12.434   9.975  1.00 85.25           O
ATOM   1718  C3'   U A  51      25.579  12.126  12.166  1.00 85.25           C
ATOM   1719  O3'   U A  51      25.338  11.526  13.439  1.00 85.25           O
ATOM   1720  C2'   U A  51      26.342  11.168  11.253  1.00 85.25           C
ATOM   1721  O2'   U A  51      25.713   9.888  11.297  1.00 85.25           O
ATOM   1722  C1'   U A  51      26.075  11.780   9.876  1.00 85.25           C
ATOM   1723  N1    U A  51      27.123  12.793   9.594  1.00 85.25           N
ATOM   1724  C2    U A  51      28.337  12.327   9.133  1.00 85.25           C
ATOM   1725  O2    U A  51      28.554  11.135   8.986  1.00 85.25           O
ATOM   1726  N3    U A  51      29.292  13.288   8.893  1.00 85.25           N
ATOM   1727  C4    U A  51      29.167  14.622   9.106  1.00 85.25           C
ATOM   1728  O4    U A  51      30.128  15.375   8.847  1.00 85.25           O
ATOM   1729  C5    U A  51      27.876  15.030   9.617  1.00 85.25           C
ATOM   1730  C6    U A  51      26.908  14.101   9.814  1.00 85.25           C
ATOM   1731  H5'   U A  51      22.700  13.668  10.887  1.00 85.25           H
ATOM   1732 H5''   U A  51      22.943  13.373  12.614  1.00 85.25           H
ATOM   1733  H4'   U A  51      23.610  11.517  11.544  1.00 85.25           H
ATOM   1734  H3'   U A  51      26.094  13.064  12.420  1.00 85.25           H
ATOM   1735  H2'   U A  51      27.404  11.043  11.511  1.00 85.25           H
ATOM   1736 HO2'   U A  51      26.205   9.253  10.701  1.00 85.25           H
ATOM   1737  H1'   U A  51      26.080  11.020   9.081  1.00 85.25           H
ATOM   1738  H3    U A  51      30.166  12.971   8.524  1.00 85.25           H
ATOM   1739  H5    U A  51      27.681  16.089   9.846  1.00 85.25           H
ATOM   1740  H6    U A  51      25.918  14.428  10.163  1.00 85.25           H
ATOM   2032  P     A A  61      39.570  12.176   3.592  1.00 89.77           P
ATOM   2033  OP1   A A  61      40.329  11.518   2.509  1.00 89.77           O
ATOM   2034  OP2   A A  61      38.832  13.457   3.268  1.00 89.77           O
ATOM   2035  O5'   A A  61      38.526  11.146   4.238  1.00 89.77           O
ATOM   2036  C5'   A A  61      38.951   9.851   4.761  1.00 89.77           C
ATOM   2037  C4'   A A  61      37.769   9.086   5.318  1.00 89.77           C
ATOM   2038  O4'   A A  61      37.300   9.683   6.562  1.00 89.77           O
ATOM   2039  C3'   A A  61      36.531   9.048   4.438  1.00 89.77           C
ATOM   2040  O3'   A A  61      36.667   8.063   3.429  1.00 89.77           O
ATOM   2041  C2'   A A  61      35.424   8.759   5.453  1.00 89.77           C
ATOM   2042  O2'   A A  61      35.403   7.445   5.972  1.00 89.77           O
ATOM   2043  C1'   A A  61      35.880   9.589   6.645  1.00 89.77           C
ATOM   2044  N9    A A  61      35.296  10.938   6.675  1.00 89.77           N
ATOM   2045  C8    A A  61      35.899  12.100   6.290  1.00 89.77           C
ATOM   2046  N7    A A  61      35.191  13.170   6.519  1.00 89.77           N
ATOM   2047  C5    A A  61      34.009  12.683   7.064  1.00 89.77           C
ATOM   2048  C6    A A  61      32.860  13.329   7.536  1.00 89.77           C
ATOM   2049  N6    A A  61      32.711  14.688   7.568  1.00 89.77           N
ATOM   2050  N1    A A  61      31.859  12.561   7.993  1.00 89.77           N
ATOM   2051  C2    A A  61      32.021  11.233   8.006  1.00 89.77           C
ATOM   2052  N3    A A  61      33.065  10.505   7.618  1.00 89.77           N
ATOM   2053  C4    A A  61      34.047  11.305   7.148  1.00 89.77           C
ATOM   2054  H5'   A A  61      39.705   9.997   5.549  1.00 89.77           H
ATOM   2055 H5''   A A  61      39.429   9.266   3.962  1.00 89.77           H
ATOM   2056  H4'   A A  61      38.175   8.069   5.425  1.00 89.77           H
ATOM   2057  H3'   A A  61      36.332   9.968   3.869  1.00 89.77           H
ATOM   2058  H2'   A A  61      34.448   8.950   4.981  1.00 89.77           H
ATOM   2059 HO2'   A A  61      34.650   7.356   6.623  1.00 89.77           H
ATOM   2060  H1'   A A  61      35.542   9.095   7.567  1.00 89.77           H
ATOM   2061  H8    A A  61      36.896  12.131   5.826  1.00 89.77           H
ATOM   2062  H61   A A  61      31.864  15.089   7.917  1.00 89.77           H
ATOM   2063  H62   A A  61      33.448  15.280   7.242  1.00 89.77           H
ATOM   2064  H2    A A  61      31.167  10.659   8.395  1.00 89.77           H
ATOM   2065  P     C A  62      36.028   8.299   1.969  1.00 89.92           P
ATOM   2066  OP1   C A  62      36.477   7.125   1.192  1.00 89.92           O
ATOM   2067  OP2   C A  62      36.337   9.663   1.457  1.00 89.92           O
ATOM   2068  O5'   C A  62      34.471   8.094   2.211  1.00 89.92           O
ATOM   2069  C5'   C A  62      34.000   6.810   2.666  1.00 89.92           C
ATOM   2070  C4'   C A  62      32.568   6.874   3.135  1.00 89.92           C
ATOM   2071  O4'   C A  62      32.459   7.633   4.366  1.00 89.92           O
ATOM   2072  C3'   C A  62      31.572   7.510   2.187  1.00 89.92           C
ATOM   2073  O3'   C A  62      31.152   6.586   1.167  1.00 89.92           O
ATOM   2074  C2'   C A  62      30.447   7.894   3.133  1.00 89.92           C
ATOM   2075  O2'   C A  62      29.658   6.751   3.487  1.00 89.92           O
ATOM   2076  C1'   C A  62      31.221   8.337   4.375  1.00 89.92           C
ATOM   2077  N1    C A  62      31.494   9.809   4.401  1.00 89.92           N
ATOM   2078  C2    C A  62      30.542  10.640   4.977  1.00 89.92           C
ATOM   2079  O2    C A  62      29.538  10.107   5.445  1.00 89.92           O
ATOM   2080  N3    C A  62      30.738  12.001   4.993  1.00 89.92           N
ATOM   2081  C4    C A  62      31.863  12.517   4.430  1.00 89.92           C
ATOM   2082  N4    C A  62      32.033  13.862   4.422  1.00 89.92           N
ATOM   2083  C5    C A  62      32.857  11.686   3.851  1.00 89.92           C
ATOM   2084  C6    C A  62      32.640  10.342   3.865  1.00 89.92           C
ATOM   2085  H5'   C A  62      34.639   6.455   3.488  1.00 89.92           H
ATOM   2086 H5''   C A  62      34.086   6.077   1.851  1.00 89.92           H
ATOM   2087  H4'   C A  62      32.312   5.809   3.239  1.00 89.92           H
ATOM   2088  H3'   C A  62      31.967   8.365   1.619  1.00 89.92           H
ATOM   2089  H2'   C A  62      29.763   8.641   2.703  1.00 89.92           H
ATOM   2090 HO2'   C A  62      28.928   7.030   4.110  1.00 89.92           H
ATOM   2091  H1'   C A  62      30.614   8.116   5.265  1.00 89.92           H
ATOM   2092  H41   C A  62      32.854  14.261   4.013  1.00 89.92           H
ATOM   2093  H42   C A  62      31.337  14.457   4.826  1.00 89.92           H
ATOM   2094  H5    C A  62      33.768  12.113   3.407  1.00 89.92           H
ATOM   2095  H6    C A  62      33.399   9.670   3.438  1.00 89.92           H
ATOM   2096  P     A A  63      30.677   7.143  -0.262  1.00 76.34           P
ATOM   2097  OP1   A A  63      30.400   5.961  -1.126  1.00 76.34           O
ATOM   2098  OP2   A A  63      31.651   8.166  -0.742  1.00 76.34           O
ATOM   2099  O5'   A A  63      29.288   7.820   0.094  1.00 76.34           O
ATOM   2100  C5'   A A  63      28.194   6.996   0.512  1.00 76.34           C
ATOM   2101  C4'   A A  63      26.983   7.826   0.798  1.00 76.34           C
ATOM   2102  O4'   A A  63      27.198   8.520   2.056  1.00 76.34           O
ATOM   2103  C3'   A A  63      26.628   8.935  -0.197  1.00 76.34           C
ATOM   2104  O3'   A A  63      26.012   8.517  -1.467  1.00 76.34           O
ATOM   2105  C2'   A A  63      25.763   9.826   0.697  1.00 76.34           C
ATOM   2106  O2'   A A  63      24.495   9.267   0.953  1.00 76.34           O
ATOM   2107  C1'   A A  63      26.527   9.779   2.025  1.00 76.34           C
ATOM   2108  N9    A A  63      27.514  10.873   2.035  1.00 76.34           N
ATOM   2109  C8    A A  63      28.833  10.828   1.660  1.00 76.34           C
ATOM   2110  N7    A A  63      29.431  11.992   1.689  1.00 76.34           N
ATOM   2111  C5    A A  63      28.441  12.855   2.152  1.00 76.34           C
ATOM   2112  C6    A A  63      28.448  14.224   2.407  1.00 76.34           C
ATOM   2113  N6    A A  63      29.535  14.968   2.281  1.00 76.34           N
ATOM   2114  N1    A A  63      27.293  14.800   2.803  1.00 76.34           N
ATOM   2115  C2    A A  63      26.211  14.024   2.954  1.00 76.34           C
ATOM   2116  N3    A A  63      26.086  12.715   2.774  1.00 76.34           N
ATOM   2117  C4    A A  63      27.260  12.185   2.362  1.00 76.34           C
ATOM   2118  H5'   A A  63      28.478   6.431   1.412  1.00 76.34           H
ATOM   2119 H5''   A A  63      27.963   6.259  -0.271  1.00 76.34           H
ATOM   2120  H4'   A A  63      26.161   7.095   0.770  1.00 76.34           H
ATOM   2121  H3'   A A  63      27.509   9.432  -0.631  1.00 76.34           H
ATOM   2122  H2'   A A  63      25.603  10.816   0.242  1.00 76.34           H
ATOM   2123 HO2'   A A  63      23.971   9.885   1.541  1.00 76.34           H
ATOM   2124  H1'   A A  63      25.861   9.893   2.894  1.00 76.34           H
ATOM   2125  H8    A A  63      29.342   9.898   1.363  1.00 76.34           H
ATOM   2126  H61   A A  63      29.495  15.949   2.474  1.00 76.34           H
ATOM   2127  H62   A A  63      30.397  14.549   1.992  1.00 76.34           H
ATOM   2128  H2    A A  63      25.296  14.546   3.272  1.00 76.34           H
TER
"""

# -----------------------------------------------------------------------------

def get_pdb_inputs(pdb_str):
    raw_records = flex.std_string(pdb_str.splitlines())
    processed_pdb_file = rs.get_processed_pdb_object(raw_records=raw_records, rama_potential=None, log = None)
    xrs = processed_pdb_file.xray_structure(show_summary = False)
    #geometry_restraints_manager = rs.get_geometry_restraints_manager(
    #  processed_pdb_file = processed_pdb_file,
    #  xray_structure     = xrs)
    generic_restraints_manager = mmtbx.geometry_restraints.manager()
    geometry_restraints_manager = cctbx.geometry_restraints.manager.manager(
                                    generic_restraints_manager = generic_restraints_manager)
    pdb_hierarchy = processed_pdb_file.all_chain_proxies.pdb_hierarchy
    return group_args(ph  = pdb_hierarchy,
                        grm = geometry_restraints_manager,
                        xrs = xrs)


# -----------------------------------------------------------------------------


def test_rigid_body_refinement():
    pi = get_pdb_inputs(pdb_str=pdb_str)

    pi.ph.write_pdb_file(file_name="./tmp/original.pdb", \
                                        crystal_symmetry=pi.xrs.crystal_symmetry())
    '''
    sm = mmtbx.refinement.real_space.structure_monitor(
    pdb_hierarchy               = pi.ph,
    xray_structure              = pi.xrs)

    '''
    states_collector = mmtbx.utils.states(pdb_hierarchy  = pi.ph, \
                                            xray_structure = pi.xrs)

    for d_min in [4]:

        f_calc = pi.xrs.structure_factors(d_min = d_min).f_calc()
        fft_map = f_calc.fft_map(resolution_factor=0.25)
        fft_map.apply_sigma_scaling()
        target_map = fft_map.real_map_unpadded()



        rot_obj = mmtbx.refinement.rigid_body.rb_mat_zyz(phi = 20, psi = 30, the = 40)

        xrs_poor = pi.xrs.deep_copy_scatterers()
        xrs_poor.apply_rigid_body_shift( rot = rot_obj.rot_mat().as_mat3(), trans = [1,2,3] )

        pdb_hierarchy_poor = pi.ph.deep_copy()
        pdb_hierarchy_poor.adopt_xray_structure(xrs_poor)
        pdb_hierarchy_poor.write_pdb_file(file_name = "./tmp/poor.pdb")


        rigid_body_refinement(pdb_hierarchy_poor, \
                                inp_map=target_map, \
                                symm=pi.xrs.crystal_symmetry(), \
                                states_collector = states_collector)

        refine_complete_structure(pdb_hierarchy_poor, \
                                        inp_map=target_map, \
                                        symm=pi.xrs.crystal_symmetry(), \
                                        optimize_weights=False, \
                                        verbose=True)

        states_collector.add(pdb_hierarchy_poor.extract_xray_structure().sites_cart())

        fft_map.as_ccp4_map(file_name="./tmp/map.ccp4")
        pdb_hierarchy_poor.write_pdb_file(file_name="./tmp/refined_%f.pdb"%d_min, \
                                        crystal_symmetry=pi.xrs.crystal_symmetry())

        states_collector.write(file_name = "./tmp/all.pdb")


# -----------------------------------------------------------------------------


def test_individual_sites_refinement(wc_basepairs=None):

    # process input pdb string
    pi = get_pdb_inputs(pdb_str=pdb_str)

    pi.ph.write_pdb_file(file_name="./tmp/original.pdb", \
                                        crystal_symmetry=pi.xrs.crystal_symmetry())

    if wc_basepairs:
        hbond_params = refinery.create_hbond_proxies(pi.ph, wc_basepairs)
    else:
        hbond_params = None


    for d_min in [1, 2, 3]:
        # preare initial map

        print "d_min:", d_min
        f_calc = pi.xrs.structure_factors(d_min = d_min).f_calc()
        fft_map = f_calc.fft_map(resolution_factor=0.25)
        fft_map.apply_sigma_scaling()
        target_map = fft_map.real_map_unpadded()

        for shake_size in [1,2,3]:
            print "  shake_size:", shake_size

            xrs_poor = pi.xrs.deep_copy_scatterers()
            random.seed(0)
            flex.set_random_seed(0)
            xrs_poor.shake_sites_in_place(mean_distance = shake_size)
            pdb_hierarchy_poor = pi.ph.deep_copy()
            pdb_hierarchy_poor.adopt_xray_structure(xrs_poor)

            pdb_hierarchy_poor.write_pdb_file(file_name="./tmp/distorted_%f_%f.pdb"%(d_min, shake_size), \
                                        crystal_symmetry=pi.xrs.crystal_symmetry())

            refine_complete_structure(pdb_hierarchy_poor, \
                                        inp_map=target_map, \
                                        symm=pi.xrs.crystal_symmetry(), \
                                        optimize_weights=True, \
                                        verbose=True, hbond_params=hbond_params)

            pdb_hierarchy_poor.write_pdb_file(file_name="./tmp/refined_%f_%f.pdb"%(d_min, shake_size), \
                                        crystal_symmetry=pi.xrs.crystal_symmetry())

            dist = flex.mean(flex.sqrt((pi.xrs.sites_cart() - pdb_hierarchy_poor.extract_xray_structure().sites_cart()).dot()))
            print "Dictance to original structure = ", dist

def main():
    print " --> testing refinement of the individual sites"
    test_individual_sites_refinement()#wc_basepairs = [(51,61), (50,62), (49,63)])
    return
    print " --> testing rigid body refinement"
    test_rigid_body_refinement()

if __name__=="__main__":
    main()


