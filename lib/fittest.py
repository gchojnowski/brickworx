#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================


import os, sys, re

try:
    from brickworx_config import *
except:
    sys.path.append("..")
    from brickworx_config import *


sys.path.append(os.path.join(ROOT, "./lib/"))
sys.path.append(os.path.join(ROOT, "./lib/ClaRNA/"))
sys.path.append(os.path.join(ROOT, "./lib/smatch_cpp/"))
MOTIFS_DIR = os.path.join(ROOT, "./data/motifs/")

import json

import string
import urllib2, urllib
import numpy as np
from progressbar import ProgressBar, Percentage, Bar, ETA

from StringIO import StringIO
import gzip

from numpy import dot, sqrt, array, linalg, append

# ----------------- LOCAL DEPS ------------------------------------------------

import smatch
from peaksearch import po4finder

try:
    from clarna_utils import Clarna_utils
except:
    pass

Clarna_utils = None

from iso import isostericity

from compare_xmodels import CompareXModels
from maps import RealSpaceCorrelations, local_region_density_correlation

from grow import growth

# ------------------- CCTBX ---------------------------------------------------

import iotbx
from libtbx.test_utils import approx_equal
from iotbx import pdb
from cctbx import crystal, miller
from scitbx import matrix
from cctbx.array_family import flex
from iotbx.pdb import input as pdb_input

from cctbx import maptbx

from refine_tools import refinery
from mutate import mutants


# -----------------------------------------------------------------------------

def cc_match(arg):
    """
        this is a separate function for historical reasons. it can be used to
        run pattern matches on multiple cores. it can be merged with the main
        class now.
    """



    def calc_rscc_simple(frag, _target_map, _target_miller_set):


        uc = _target_map.crystal_symmetry().unit_cell()

        rs_f = maptbx.real_space_target_simple(
                                    unit_cell=uc,
                                    density_map=_target_map.real_map_unpadded(),
                                    sites_cart=frag.atoms().extract_xyz(),
                                    selection=flex.bool(frag.atoms().size(),True))
        return rs_f


    # -------------------------------------------------------------------------

    def calc_rscc(frag, _target_map, _target_miller_set, atom_radius=2.0):

        result = local_region_density_correlation(large_unit_cell = _target_miller_set.unit_cell(),
                                large_d_min=_target_miller_set.d_min(),
                                large_density_map=_target_map.real_map_unpadded(), \
                                frag = frag)

        return result

    # -------------------------------------------------------------------------


    def mutate2map(frag, target_map, target_miller_set, only_new=False):
        '''
            !!! residues must be numbered from 1 to N
            TODO: remove use mutate2map_loops instead
        '''


        mutate = mutants(debug=False)
        nres = frag.overall_counts().n_residues

        atoms = frag.atoms()
        atoms.set_b(new_b=flex.double(atoms.size(), 0.0))


        results = calc_rscc(frag, target_map, target_miller_set)

        ref_cc = np.array([r.cc for r in results]).mean()


        if only_new:
            new_bases = sorted([string.atoi(rg.resseq) for rg in frag.residue_groups() if rg.only_atom_group().altloc == "X"])
            ires_range = new_bases[:1]
        else:
            # XXX that requeres a special indexing of bases!
            assert [string.atoi(rg.resseq) for rg in frag.residue_groups()][-1] == nres
            ires_range = range(int(nres/2))

        wc_basepairs = [(i+1, nres-i, "WW_cis") for i in range(int(nres/2))]


        for ires in ires_range:
            new_frag = mutate.mutate_selected(frag, [ires+1,nres-ires])


            local_refinery_obj = refinery(pdb_hierarchy=new_frag, \
                                                  inp_map=target_map.real_map_unpadded(), \
                                                  symm = target_miller_set.crystal_symmetry(), \
                                                  d_min=3.0, \
                                                  max_iteration=200, \
                                                  optimize_weights=False, \
                                                  basepairs = wc_basepairs)


            new_frag.adopt_xray_structure(local_refinery_obj.process(new_frag.extract_xray_structure(), \
                                                                    verbose=False), \
                                           assert_identical_id_str=False )

            new_results = calc_rscc(new_frag, target_map, target_miller_set)
            new_cc = np.array([r.cc for r in new_results]).mean()
            if new_cc > ref_cc:
                frag = new_frag
                ref_cc = new_cc
                results = new_results

        for r in results:
            atoms = r.residue.rg.atoms()
            atoms.set_b(new_b=flex.double(atoms.size(), r.cc*100.0))

        return frag, results


    # -------------------------------------------------------------------------

    def cluster_results_by_frag(results, input_frag):

        unique_results = []
        unique_Psites = []
        uc = symm.unit_cell()
        orth_mx = uc.orthogonalization_matrix()
        frac_mx = uc.fractionalization_matrix()

        asu_mappings = crystal.direct_space_asu.asu_mappings(
            space_group = symm.space_group(),
            asu = symm.direct_space_asu().as_float_asu(),
            buffer_thickness = 0.0)

        for idx,res in enumerate(results):
            (match, rot, tran, rms) = res

            rt = matrix.rt( (rot, tran) )
            P_sites = flex.vec3_double([rt*atom.xyz for atom in input_frag.atoms() if atom.name.strip()=="P"])

            asu_mappings.process(uc.fractionalize(P_sites[0]))

            mapping = asu_mappings.mappings()[-1][0]
            P_sites_asu = orth_mx*(asu_mappings.get_rt_mx(mapping).as_rational().as_float() * (frac_mx * P_sites))

            unique = True
            for Ps in unique_Psites:
                if flex.mean((Ps-P_sites_asu).dot())<1.0:
                    unique=False
                    break


            if unique:
                unique_results.append(res)
                unique_Psites.append(P_sites_asu)


        return unique_results

    # -------------------------------------------------------------------------


    target = arg[0]
    fname = arg[1]
    target_miller_set = arg[2]
    target_map = arg[3]
    symm = arg[4]
    debug = arg[5]
    max_rmsd = arg[6]
    npivots = arg[7]
    ntests = arg[8]
    build_dna = arg[9]


    fname_base = os.path.basename(fname).split('.pdb')[0]
    cc_hits = []


    grower = growth(dna=build_dna)


    # ------------------------------

    raw_records = flex.std_string()
    raw_records.extend(flex.split_lines(open(fname).read()))
    try:
        input_frag = pdb_input(source_info=None, lines=raw_records).construct_hierarchy()
    except:
        return []

    refinery_obj = None


    # match P-patterns extracted from model stem
    sel_cache = input_frag.atom_selection_cache()
    assert sel_cache.n_seq == input_frag.atoms_size()
    isel = sel_cache.iselection

    pattern = input_frag.atoms().select(isel(r"name P")).extract_xyz()

    results = smatch.supermatch(target, \
                    symm.unit_cell().parameters(), \
                    symm.space_group_info().type().lookup_symbol()).find_matches(pattern, max_rmsd, False)




    if not results:
        " ==> No hits found!"
        return []

    bfname = os.path.basename(fname)[:-4]

    unique_results=[]
    for res_item in results:
        (match, rot, tran, rms) = res_item
        unique=True
        for unique_item in unique_results:
            if set(unique_item[0]) == set(match):
                unique=False
                break
        if unique:
            unique_results.append(res_item)

    results = cluster_results_by_frag(unique_results, input_frag)

    print " --> Processing top %i of %i hits (at max_rmsd = %.2f)" % (ntests,len(results),max_rmsd)

    simple_scores = []
    scored_results = []
    for res in results:
        (match, rot, tran, rms) = res

        rt = matrix.rt( (rot, tran) )

        frag = input_frag.deep_copy()
        frag.atoms().set_xyz(new_xyz=(rt * frag.atoms().extract_xyz()))
        score = calc_rscc_simple(frag, target_map, target_miller_set)
        scored_results.append( (res, score) )



    scored_results = sorted(scored_results, key = lambda x: x[1], reverse=True)



    for idx,res in enumerate(scored_results[:ntests]):
        res = res[0]

        (match, rot, tran, rms) = res

        rt = matrix.rt( (rot, tran) )



        frag = input_frag.deep_copy()
        frag.atoms().set_xyz(new_xyz=(rt * frag.atoms().extract_xyz()))

        assert approx_equal( rt.r.determinant(), 1.0, eps=1.e-6 )

        states_collector = None

        result = calc_rscc(frag, target_map, target_miller_set)
        init_cc = np.array([r.cc for r in result])

        print "    --> Testing match %i/%i (%i points/%.2fA rmsd/cc %.4f)" % (idx+1, ntests, \
                                                (len(match)-match.count(-3)), rms, init_cc.mean())

        if init_cc.mean()<0.30: continue

        # try to add basepairs at trhe ends of a fitted stem
        frag, result = mutate2map(frag, target_map, target_miller_set, only_new=False)

        append_to_leading_chain = True
        while True:
            tmp_frag = grower.grow_stem(frag, append_to_leading_chain)
            tmp_frag, result = mutate2map(tmp_frag, target_map, target_miller_set, only_new=True)
            print "        --> Testing new basepair: ",


            new_basepair_cc = np.array([r.cc for r in result if r.residue.rg.only_atom_group().altloc == "X"])
            if new_basepair_cc.min()<0.5:
                print "rejected"
                if append_to_leading_chain:
                    append_to_leading_chain = False
                else:
                    break
            else:
                frag = tmp_frag
                print "accepted"
                for ag in frag.atom_groups(): ag.altloc = ""


        # calculate RSCC for a complete structure
        result = calc_rscc(frag, target_map, target_miller_set)


        sum_cc = dict([(r.residue.resid.strip(), r.cc) for r in result])
        ave_cc = np.array([sum_cc.values()])
        if ave_cc.mean()<0.50: continue
        print " --> [%s] Accepted match with CC = %.2f" % (fname_base, \
                                                            ave_cc.mean())


        # set b_factor proportional to the CC
        for r in result:
            atoms = r.residue.rg.atoms()
            atoms.set_b(new_b=flex.double(atoms.size(), r.cc*100.0))



        if debug: frag.write_pdb_file(file_name="./tmp/hit_%s_%02i_%.6f_refined.pdb"%(bfname,idx, ave_cc.mean()), \
                                                                                    crystal_symmetry=symm)

        cc_hits.append(frag)
        return cc_hits




    return cc_hits



# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------


class Fittest:

    # fpair_sub.c: residue_ident

    # identify uncommon residues in a way similar to RNAVIEW
    # identifying a residue as follows:
    #  R-base  Y-base  amino-acid, others [default]
    # (puRine)
    #   +1        0        -1        -2   [default]
    #

    def _get_atom_by_name(self, resi, name):
        for atom in resi.atoms():
            if atom.name.strip()==name:
                return atom
        return None



    def _get_base_type(self, resi, dcrt=2.0, dcrt2=3.0):

        base_id = -2

        N9 = self._get_atom_by_name(resi, 'N9')
        N1 = self._get_atom_by_name(resi, 'N1')
        C2 = self._get_atom_by_name(resi, 'C2')
        C6 = self._get_atom_by_name(resi, 'C6')

        if N1 and C2 and C6:
            d1 = np.linalg.norm(np.array(N1.xyz, float) - np.array(C2.xyz, float))
            d2 = np.linalg.norm(np.array(N1.xyz, float) - np.array(C6.xyz, float))
            d3 = np.linalg.norm(np.array(C2.xyz, float) - np.array(C6.xyz, float))

            if d1<=dcrt and d2<=dcrt and d3<=dcrt2:
                base_id = 0
                if N9:
                    d3 = np.linalg.norm(np.array(N1.xyz, float) - np.array(N9.xyz, float))
                    if d3 >= 3.5 and d3 <= 4.5:
                        base_id = 1
            return base_id

        # try to detect an amino-acid
        CA = self._get_atom_by_name(resi, 'CA')
        C  = self._get_atom_by_name(resi, 'C')
        if not C:
            C  = self._get_atom_by_name(resi, 'N')
        if C and CA:
            d = np.linalg.norm(np.array(CA.xyz, float) - np.array(C.xyz, float))
            if d <= dcrt:
                base_id = -1
            return base_id
        return base_id

    # -------------------------------------------------------------------------
    # -------------------------------------------------------------------------

    def clean(self):
        try:
            os.remove("%s"%(self.tmp_dir + "%s.%.2f_map.mtz"%(self.target_pdbids[0], self.d_min)))
        except:
            pass

        try:
            os.remove("%s"%(self.tmp_dir + "%s.mtz"%(self.target_pdbids[0])))
        except:
            pass

        try:
            os.remove("%s"%(self.tmp_dir + "%s.%.2f_map.mtz.ccp4"%(self.target_pdbids[0], self.d_min)))
        except:
            pass

    # -------------------------------------------------------------------------


    def id_generator(self, size=6, chars=string.ascii_uppercase + string.digits):
            return ''.join(random.choice(chars) for x in range(size))


    # -------------------------------------------------------------------------

    def __init__(self, target_map, target_miller_set, symm, pdbid=None):

        self.target_map = target_map
        self.target_miller_set = target_miller_set
        self.symm = symm

        if Clarna_utils:
            self.cutils = Clarna_utils()
        else:
            self.cutils = Clarna_utils

        self.target_pdbids = [pdbid]

    # -------------------------------------------------------------------------

    def expand_with_buffer(self, hierarchy, symm, buffer_thickness=None, p1=False):
        """
            reduce phosphate atom pattern to ASU and add symmetry mates to a buffer
        """

        xs_p1 = hierarchy.extract_xray_structure(symm)

        if p1: xs_p1 = xs_p1.expand_to_p1(sites_mod_positive=True)

        asu_mapping = xs_p1.asu_mappings(buffer_thickness=buffer_thickness)

        assert asu_mapping.mappings().size() == xs_p1.scatterers().size()


        output_structure = iotbx.pdb.hierarchy.root()
        output_structure.append_model(iotbx.pdb.hierarchy.model(id="0"))
        chain = iotbx.pdb.hierarchy.chain(id="X")
        output_structure.models()[0].append_chain(chain)
        uc = self.symm.unit_cell()

        index=0
        # 0 - YYY will map asu
        # 1 - YYY will map buffer only
        switch = 0
        for mappings in asu_mapping.mappings():
            if len(mappings)>switch:
                for mapping in mappings[switch:]:
                    site = mapping.mapped_site()
                    rg = pdb.hierarchy.residue_group(resseq="%i"%index)
                    ag = pdb.hierarchy.atom_group(resname="YYY", altloc=" ")
                    atom = iotbx.pdb.hierarchy.atom()

                    ag.append_atom(atom)
                    rg.append_atom_group(atom_group=ag)
                    chain.append_residue_group(rg)

                    atom.name = "P"
                    atom.set_element('P')
                    atom.set_xyz(site)
                    atom.set_occ(1.0)
                    atom.set_b( 10.0 )

                    index+=1
        output_structure.atoms_reset_serial()
        return output_structure


    # -------------------------------------------------------------------------

    def get_phosphates_from_file(self, pdb_ifname, verbose=False):
        """
            Parse a user-provided PDB file and select phosphate atoms only.
            ALL phosphates are selected not only ones from NA backbone!
        """

        try:
            xyz_input_ph = self.read_fragment(pdb_ifname, remote=False)
        except:
            return 0

        sel_cache = xyz_input_ph.atom_selection_cache()
        assert sel_cache.n_seq == xyz_input_ph.atoms_size()
        isel = sel_cache.iselection
        self.peaks_as_pdb = xyz_input_ph.select(isel(r"name P"))

        print " ==> Found %i phosphorus atoms in the input file" % self.peaks_as_pdb.atoms().size()

        buffer_thickness = 0.0
        self.knuspr_structure = self.expand_with_buffer(self.peaks_as_pdb, \
                                                    self.symm, \
                                                    buffer_thickness = buffer_thickness, \
                                                    p1 = False)
        # buffer
        while self.knuspr_structure.atoms_size()<100 and buffer_thickness<20.0:
            buffer_thickness += 2.0
            self.knuspr_structure = self.expand_with_buffer(self.peaks_as_pdb, \
                                                                    self.symm, \
                                                                    buffer_thickness = buffer_thickness, \
                                                                    p1 = False)
        print " ==> Adjusted buffer thickness (%.2f\AA) that gives %i phosphates" % \
                                (buffer_thickness, self.knuspr_structure.atoms_size())

        self.original_knuspr_structure = self.knuspr_structure.deep_copy()

        return 1

    # -------------------------------------------------------------------------


    def merge_knuspr_results(self, list_of_pdbs):
        knuspr_structure = iotbx.pdb.hierarchy.root()
        knuspr_structure.append_model(iotbx.pdb.hierarchy.model(id = "0"))
        chain = iotbx.pdb.hierarchy.chain(id="X")
        knuspr_structure.models()[0].append_chain(chain)
        for target in list_of_pdbs:
            for rg in target.residue_groups():
                chain.append_residue_group(rg.detached_copy())


        self.original_knuspr_structure = knuspr_structure
        self.peaks_as_pdb = knuspr_structure

    # -------------------------------------------------------------------------

    def run_svm_knuspr(self, fudge_factor=0.0, \
                        mask=None, \
                        debug=False, \
                        max_number_of_peaks=1000, \
                        max_buffer_thickness = 20.0, \
                        verbose=False):

        """
            Run peak-search procedure nad SVM classifier to select
            a set of most plausible phosphate group positions
        """

        ps_obj = po4finder(self.target_map, self.symm, mask=mask)
        ps_obj.peaksearch(max_number_of_peaks = max_number_of_peaks)

        self.peaks_as_pdb = ps_obj.peaks_as_pdb(only_selected=True)

        buffer_thickness = 0.0
        self.knuspr_structure = self.expand_with_buffer(self.peaks_as_pdb, \
                                                    self.symm, \
                                                    buffer_thickness = buffer_thickness, \
                                                    p1 = False)
        # buffer
        while self.knuspr_structure.atoms_size()<100 and buffer_thickness<max_buffer_thickness:
            buffer_thickness += 2.0
            self.knuspr_structure = self.expand_with_buffer(self.peaks_as_pdb, \
                                                                    self.symm, \
                                                                    buffer_thickness = buffer_thickness, \
                                                                    p1 = False)
        print " ==> Adjusted buffer thickness (%.2f\AA) that gives %i phosphates" % \
                            (buffer_thickness, self.knuspr_structure.atoms_size())

        self.original_knuspr_structure = self.knuspr_structure.deep_copy()

    # -------------------------------------------------------------------------


    def __get_list_of_clusters(self, fragment_type = ['stem', 'loop', 'strand'][1], \
                                        max_distance = [1.0, 2.0, 5.0][1], \
                                        skip_target_related=False):
        '''
        output:
        cluster_id number_of_members cluster_name medoid_filename
        '''
        output_str = ""
        # get number of pages first
        data = urllib.urlencode({'max_distance'  : max_distance, \
                                'fragment_type' : fragment_type, \
                                'pdbid' : self.target_pdbids[0]})

        n_pages = string.atoi( urllib2.urlopen(url=CLUSTERS_URL, data=data).read() )
        for idx in range(n_pages):
            output_str += urllib2.urlopen(url=CLUSTERS_URL+"/page=%i"%idx, data=data).read()
        return output_str.split('\n')

    # -------------------------------------------------------------------------


    def read_fragment(self, fname, remote=False):
        """
            Simple function that parses a PDB data. Both local and remote.
        """
        raw_records = flex.std_string()

        if remote:
            try:
                url = urllib2.urlopen(url = FRAGS_URL+"/%s" % fname)
                url_f = StringIO(url.read())
                gzipfile = gzip.GzipFile(fileobj = url_f)
                raw_records.extend(flex.split_lines(gzipfile.read()))
            except:
                print fname

        else:

            if re.match(r".*\.gz$",fname):
                f = gzip.open(fname,"r")
            else:
                f = open(fname,"r")

            raw_records.extend(flex.split_lines(f.read()))

        return pdb_input(source_info=None, lines=raw_records).construct_hierarchy()

    # -------------------------------------------------------------------------

    def mutate2map_loops(self, frag, \
                                target_map, \
                                target_miller_set, \
                                min_clarna_score=0.5, \
                                loop_fname = None, \
                                basepairs_list = None):
        """
            Real-space refine and select most plausible sequence variant of a loop.
            for agiven base pair we test all variants that are structurally similar 
            (isosteric) and can to substitute for each other by mutation without
            disrupting the 3D structure. Here IDI=2.0 is used.
            Stombaugh et al. (2009) Frequency and isostericity of RNA base pairs.
            Nucleic Acids Res., 37, 2294-312.
        """

        btypes = {"A":"R", "G":"R", "C":"Y", "U":"Y"}

        iso = isostericity()

        atoms = frag.atoms()
        atoms.set_b(new_b=flex.double(atoms.size(), 0.0))
        atoms.set_occ(new_occ=flex.double(atoms.size(), 1.0))

        tmp_frag = frag.deep_copy()
        frag_fileobj = StringIO(tmp_frag.as_pdb_string())

        mutate = mutants(debug=False)

        results = local_region_density_correlation(large_unit_cell = target_miller_set.unit_cell(),
                                large_d_min=target_miller_set.d_min(),
                                large_density_map=target_map.real_map_unpadded(), \
                                frag = tmp_frag)

        ref_cc = np.array([r.cc for r in results]).mean()

        for basepair in basepairs_list:
            try:
                plausible_base_types = iso.get_base_types(pair=basepair[3], geom=basepair[2], IDI=2.0)
            except:
                print "ERROR: ", basepair[3], basepair[2]
                continue

            for base_types in plausible_base_types:
                bases2mutate = []
                if btypes[basepair[3][0].upper()] != base_types[0]: bases2mutate.append(basepair[0])
                if btypes[basepair[3][1].upper()] != base_types[1]: bases2mutate.append(basepair[1])

                if not bases2mutate: continue
                new_frag = mutate.mutate_selected(tmp_frag, bases2mutate)

                new_results = self.refine_fragment(new_frag, target_map, basepairs_list, target_miller_set, loop_fname)

                new_cc = np.array([r.cc for r in new_results]).mean()

                if new_cc > ref_cc:
                    tmp_frag = new_frag
                    ref_cc = new_cc
                    results = new_results

            for r in results:
                atoms = r.residue.rg.atoms()
                atoms.set_b(new_b=flex.double(atoms.size(), r.cc*100.0))

        return tmp_frag, results



    # -------------------------------------------------------------------------


    def refine_fragment(self, frag, target_map, basepairs, target_miller_set, name):

        atoms = frag.atoms()
        atoms.set_b(new_b=flex.double(atoms.size(), 20.0))
        atoms.set_occ(new_occ=flex.double(atoms.size(), 1.0))


        # real-space fitting
        tmp_refinery_obj = refinery(pdb_hierarchy=frag, \
                                                inp_map=target_map.real_map_unpadded(), \
                                                symm = target_map.crystal_symmetry(), \
                                                d_min=3.0, \
                                                max_iteration=200, \
                                                optimize_weights=False, \
                                                basepairs = basepairs)

        frag.adopt_xray_structure(tmp_refinery_obj.process(frag.extract_xray_structure(), \
                                                                    verbose=False), \
                                                                    assert_identical_id_str=False )

        result = local_region_density_correlation(large_unit_cell = target_miller_set.unit_cell(),
                                large_d_min=target_miller_set.d_min(),
                                large_density_map=target_map.real_map_unpadded(), \
                                frag = frag)


        return result


    # -------------------------------------------------------------------------

    def add_loops_simple(self, results_hierarchy, data_path=MOTIFS_DIR, debug=False, remote=False, server=False, top2proc=20):
        from looper import looper

        loop_data_complete = json.loads(gzip.open(os.path.join(data_path,"data.json.gz")).read())


        uc = self.target_map.crystal_symmetry().unit_cell()

        looper_obj = looper()
        target = self.original_knuspr_structure.atoms().extract_xyz()

        stemsnumber = len(results_hierarchy.models()) + 1
        if server: print " ^^^ (1/%i)%s" % (stemsnumber, 70*' ')

        smatch_obj = smatch.supermatch(target, \
                        uc.parameters(), \
                        self.target_map.crystal_symmetry().space_group_info().type().lookup_symbol())


        loops_no = len(loop_data_complete.keys())
        stems_no = len(results_hierarchy.models())
	if not server:
            widgets = [' --> Fitting loops: ', Percentage(), ' ', Bar(marker='=', left='[', right=']'), ' ', ETA()]
            pbar = ProgressBar(widgets=widgets, maxval=2*stems_no*(loops_no+10*top2proc), term_width=96).start()




        for idx,model in enumerate(results_hierarchy.models()):

            stem_ends = looper_obj.find_closing_basepairs(model)
            stem = iotbx.pdb.hierarchy.root()
            stem.append_model(model.detached_copy())


            for iend,end in enumerate(stem_ends):
                results = []
                stem_ref_points = looper_obj.get_basepair_ref(stem, end)

                for loop_idx, (fname, loop_data) in enumerate(loop_data_complete.items()):
                    if not server: pbar.update(2*idx*(loops_no+10*top2proc) + iend*(loops_no+10*top2proc) + loop_idx)


                    pattern = flex.vec3_double(tuple(x) for x in loop_data['pattern'])

                    for loop_end_ref in loop_data['closing_pairs_ref']:
                        loop_end_ref = flex.vec3_double(tuple(x) for x in loop_end_ref)
                        rtmx = looper_obj.match_fragments_simple(stem_ref_points, loop_end_ref)
                        (match, new_rot, new_tran, rms) = \
                                                    smatch_obj.finetune_match_wrapper(pattern, \
                                                                                      tuple(rtmx.r), \
                                                                                      tuple(rtmx.t), \
                                                                                      False)


                        if rms>1.0:
                            continue
                            new_rtmx = rtmx
                        else:
                            new_rtmx = matrix.rt( (new_rot, new_tran) )

                        move_frag_xyz = flex.vec3_double(tuple(x) for x in loop_data['xyz'])
                        move_frag_xyz = new_rtmx*move_frag_xyz


                        rs_f = maptbx.real_space_target_simple(
                                                            unit_cell=uc,
                                                            density_map=self.target_map.real_map_unpadded(),
                                                            sites_cart=move_frag_xyz,
                                                            selection=flex.bool(model.atoms().size(),True))
                        results.append( (fname, rs_f, len(pattern), new_rtmx) )


                top_hit = None

                for i,item in enumerate(sorted(results, key = lambda x: x[1]/x[2], reverse=True)[:top2proc]):
                    if not server: pbar.update(2*idx*(loops_no+10*top2proc) + iend*(loops_no+10*top2proc) + loops_no+10*i)


                    frag = self.read_fragment(os.path.join(data_path,item[0]))

                    frag.atoms().set_xyz(new_xyz=(item[-1] * frag.atoms().extract_xyz()))


                    rscc_tab = local_region_density_correlation(large_unit_cell = self.target_miller_set.unit_cell(),
                                large_d_min=self.target_miller_set.d_min(),
                                large_density_map=self.target_map.real_map_unpadded(), \
                                frag = frag)


                    if np.array([r.cc for r in rscc_tab]).mean()>0.4:
                        frag, rscc_tab = self.mutate2map_loops(frag, \
                                                                self.target_map, \
                                                                self.target_miller_set, \
                                                                loop_fname=item[0], \
                                                                basepairs_list=loop_data_complete[item[0]]['basepairs'])


                    rscc = np.array([r.cc for r in rscc_tab])
                    score = (rscc*(rscc>0.6)).sum()


                    for r in rscc_tab:
                        atoms = r.residue.rg.atoms()
                        atoms.set_b(new_b=flex.double(atoms.size(), r.cc*100.0))

                    if not top_hit:
                        top_hit = (frag, rscc.mean(), score, rscc.mean(), item[0])
                    elif score > top_hit[2]:
                        top_hit = (frag, rscc.mean(), score, rscc.mean(), item[0])


                if top_hit and top_hit[3]>0.5:
                    print " --> Found a loop! (%s, score = %.2f)%s" % (os.path.basename(top_hit[-1]), \
                                                                    top_hit[2], 40*' ')
                    xxxf = top_hit[0].only_model().detached_copy()
                    xxxf.id = str(len(results_hierarchy.models())+1)
                    results_hierarchy.append_model(xxxf)

            if server: print " ^^^ (%i/%i)%s" % (idx+2, stemsnumber, 70*' ')

        if not server: pbar.finish()



        return results_hierarchy


    # -------------------------------------------------------------------------

    @classmethod
    def merge_chains(self, iph, max_p_o3_dist=2.0):
        """
            nucleotides after the standard building and
            low CC filtering process are numbered more
            or less randomly. this functions finds contionuous
            chain segments and renumbers them accordingly
            well, not yet...
        """



        isel_cache = iph.atom_selection_cache()
        isel = isel_cache.iselection

        p_and_o3p = iph.select(isel(r"name P or name O3?"))


        resi_pairs = []


        for o3_atom in p_and_o3p.atoms():


            if o3_atom.name.strip() == "P":
                continue


            min_dist = None
            min_atom = None
            for p_atom in p_and_o3p.atoms():

                if o3_atom.parent().parent().resseq_as_int() == p_atom.parent().parent().resseq_as_int():
                    continue

                if not p_atom.name.strip() == "P":
                    continue


                dist = np.linalg.norm(np.array(p_atom.xyz, float) - np.array(o3_atom.xyz, float))

                if (min_dist is None) or min_dist>dist:
                    min_dist = dist
                    min_atom = p_atom


                if min_dist<max_p_o3_dist: break

            if min_dist<max_p_o3_dist:
                resi_pairs.append( (o3_atom.parent().parent().detached_copy(), min_atom.parent().parent().detached_copy()) )




        while True:
            merged=False
            for i in range(len(resi_pairs)):
                for j in range(i+1, len(resi_pairs)):

                    if resi_pairs[i][-1].resseq_as_int() == resi_pairs[j][0].resseq_as_int():
                        resi_pairs[i] += resi_pairs[j][1:]
                        merged=True
                        resi_pairs.pop(j)
                        break

                    if resi_pairs[i][0].resseq_as_int() == resi_pairs[j][-1].resseq_as_int():
                        resi_pairs[j] += resi_pairs[i][1:]
                        resi_pairs.pop(i)
                        merged=True
                        break

                if merged: break

            if not merged: break


        all_chain_ids = string.ascii_uppercase + string.ascii_lowercase + string.digits

        merged_hierarchy = iotbx.pdb.hierarchy.root()
        merged_hierarchy.append_model(iotbx.pdb.hierarchy.model(id="0"))
        for chidx, chain_segment in enumerate(sorted(resi_pairs, key=lambda x: len(x), reverse=True)):
            _failsafe_chain_id = all_chain_ids[chidx%62] + str('' if chidx<62 else (chidx/62)%10)
            _chain = iotbx.pdb.hierarchy.chain(id=_failsafe_chain_id)
            merged_hierarchy.models()[0].append_chain(_chain)
            for _rg_idx, _rg in enumerate(chain_segment):
                _rg.resseq = _rg_idx+1
                _chain.append_residue_group(_rg)


        return merged_hierarchy

    # -------------------------------------------------------------------------


    @classmethod
    def fix_modifications_and_nomenclature(self, iph):
        mutate = mutants(debug=False)

        for ag in iph.atom_groups():
            mutate.mutate(ag, ag.resname.strip().upper())
            for atm in ag.atoms():
                if atm.name.strip() == "O1P": atm.set_name(" OP1")
                elif atm.name.strip() == "O2P": atm.set_name(" OP2")


        for iatm, atm in enumerate(iph.atoms()):
            atm.set_serial(iatm+1)


    # -------------------------------------------------------------------------


    def postprocess_matches(self, results_hierarchy, stems_no=0):
        """ the results_hierarchy contains many models with
            separate matches. find models with overlapping
            bases and merge them selecting ones with highest CC
            with a strict preference of stem models (first stem_no)
            that are far more reliable
        """

        reps_hierarchy = iotbx.pdb.hierarchy.root()
        reps_hierarchy.append_model(iotbx.pdb.hierarchy.model(id="0"))
        reps_hierarchy.models()[0].append_chain(iotbx.pdb.hierarchy.chain(id="A"))

        xcmp = CompareXModels()

        uc = self.symm.unit_cell()
        orth_mx = uc.orthogonalization_matrix()
        frac_mx = uc.fractionalization_matrix()

        asu_mappings = crystal.direct_space_asu.asu_mappings(
            space_group = self.symm.space_group(),
            asu = self.symm.direct_space_asu().as_float_asu(),
            buffer_thickness = 0.0)

        unique_rgs = []

        for rg in results_hierarchy.residue_groups():
            base_type = xcmp._get_base_type(rg)
            if base_type < 0: continue

            rg_xyz = xcmp._sel_coords(rg)

            c1p_xyz = xcmp._get_atom_by_name(rg, "C1'").xyz

            # find asu mapping for the C1' atom (P may be missing)
            asu_mappings.process(uc.fractionalize(c1p_xyz))
            mapping = asu_mappings.mappings()[-1][0]
            rg_xyz = orth_mx*(asu_mappings.get_rt_mx(mapping).as_rational().as_float() * (frac_mx * rg_xyz))
            c1p_xyz = flex.vec3_double([mapping.mapped_site()])
            frac = asu_mappings.unit_cell().fractionalize
            assert asu_mappings.asu().is_inside(frac(mapping.mapped_site()))

            unique=True
            for rep_idx in range(len(unique_rgs)):
                rep_xyz, rep_rg = unique_rgs[rep_idx]

                for sym_op in self.symm.space_group():
                    if (sym_op.is_unit_mx()):
                        symm_c1p_xyz = c1p_xyz
                    else:
                        symm_c1p_xyz = orth_mx*(sym_op.as_rational().as_float() * (frac_mx * c1p_xyz))
                    if flex.mean((symm_c1p_xyz-rep_xyz).dot())<16.0:
                        mdl_ids = map(string.atoi,[rg.parent().parent().id, rep_rg.parent().parent().id])
                        if rg.atoms()[0].b<rep_rg.atoms()[0].b and min(mdl_ids)>stems_no:
                            unique_rgs[rep_idx] = (c1p_xyz, rg)
                        unique=False
                        break

                if not unique: break

            if unique:
                unique_rgs.append((c1p_xyz, rg))

            # remove repeated entries
            unique_rgs = list(set(unique_rgs))

        for idx in range(len(unique_rgs)):#,(c1p_xyz,rg) in unique_rgs:
            c1p_xyz,rg = unique_rgs[idx]
            if rg.atoms()[0].b<40: continue
            new_rg = rg.detached_copy()
            new_rg.resseq = idx
            reps_hierarchy.models()[0].chains()[0].append_residue_group(new_rg)


        reps_hierarchy.atoms().reset_i_seq()


        reps_hierarchy = self.merge_chains(reps_hierarchy)
        self.fix_modifications_and_nomenclature(reps_hierarchy)


        return reps_hierarchy

    # -------------------------------------------------------------------------

    def clean_neighbors(self, target_ph, hits_ph, symm):
        from cctbx import sgtbx

        uc = symm.unit_cell()
        xrs = target_ph.extract_xray_structure(crystal_symmetry=symm)


        isel_cache = hits_ph.atom_selection_cache()
        isel = isel_cache.iselection

        hit_phosphates = hits_ph.select(isel(r"name P and (altloc ' ' or altloc 'a')")).atoms()


        for atom in target_ph.atoms():
            assert atom.name == "P"
            site_symmetry = xrs.site_symmetry(uc.fractionalize(atom.xyz))
            equiv_sites = sgtbx.sym_equiv_sites(site_symmetry)

            min_dist = None
            for hit_atom in hit_phosphates:
                dist_info = sgtbx.min_sym_equiv_distance_info(equiv_sites, uc.fractionalize(hit_atom.xyz))
                if (min_dist is None):
                    min_dist = dist_info.dist()
                else:
                    min_dist = min(min_dist, dist_info.dist())

            if min_dist < 4.0: target_ph.only_chain().remove_residue_group(atom.parent().parent())

    # --------------------------------------------------------------------------

    def iterate_matches(self, input_knuspr_structure = None, \
                            data_path = "./tmp/motifs/", \
                            pattern_fname = None, \
                            max_rmsd = 0.5, \
                            calc_cc = True, \
                            verbose = False, \
                            np = 7, \
                            check_sequence = False, \
                            debug = False, \
                            ss_restrains = False, \
                            results_hierarchy = None, \
                            stats = False, \
                            build_dna = False):

        """
            iteratively match and real-space refine stem models
        """


        if not pattern_fname:
            pattern_fnames = [data_path+xxx for xxx in os.listdir(data_path) if re.match(".*\.pdb(.gz)?$", xxx)]
        else:
            pattern_fnames = pattern_fname.split(',')

        # XXX
        if input_knuspr_structure: self.knuspr_structure = input_knuspr_structure

        npivots = 60
        ntests = 10
        for idx in range(100):

            target = self.knuspr_structure.atoms().extract_xyz()
            print "\n ==> Number of target points: %i" % len(target)

            res = cc_match( (target, pattern_fnames[0], \
                                        self.target_miller_set, \
                                        self.target_map, \
                                        self.symm, \
                                        debug, \
                                        max_rmsd, \
                                        npivots, \
                                        ntests, \
                                        build_dna) )

            for frag in res:
                xxxf = frag.only_model().detached_copy()
                xxxf.id = str(len(results_hierarchy.models())+1)
                results_hierarchy.append_model(xxxf)

            if not res:
                max_rmsd += 0.5
                if max_rmsd>1.0: break
                continue

            self.clean_neighbors(self.knuspr_structure, res[0], self.symm)

        print " ==> Number of matched motifs/residues: %i/%i" % (len(results_hierarchy.models()), \
                                                            results_hierarchy.overall_counts().n_residues)

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------



def main():

    from tst_refine_tools import pdb_str, pdb_str_stem, get_pdb_inputs
    from looper import looper
    #from clarna_utils import Clarna_utils


    pi = get_pdb_inputs(pdb_str=pdb_str_stem)

    looper_obj = looper()

    #cutils = Clarna_utils()
    #print looper_obj.get_basepairs_list(cutils, pi.ph, min_clarna_score=0.5)


    f_calc = pi.xrs.structure_factors(d_min = 3.0).f_calc()
    fft_map = f_calc.fft_map(resolution_factor=0.25)
    fft_map.apply_sigma_scaling()

    fittest = Fittest(fft_map, f_calc, pi.xrs.crystal_symmetry())

    frag, rscc_tab = fittest.mutate2map_loops(pi.ph, \
                                              fittest.target_map, \
                                              fittest.target_miller_set, \
                                              basepairs_list = [])



if __name__=="__main__":
    main()
