#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================

from sklearn import svm
from sklearn import preprocessing
from sklearn.externals import joblib
import pickle, gzip, json

from cctbx import crystal
from cctbx import maptbx
from peaksearch import po4finder
from scitbx.array_family import flex
from iotbx.pdb import input as pdb_input
from libtbx import group_args
import iotbx
import random
import urllib2, urllib
import string
import math
import numpy as np
from iotbx import ccp4_map

from compare_xmodels import CompareXModels
from maps import RealSpaceCorrelations

import sys

from cctbx import sgtbx
from sklearn import cross_validation
from sklearn import preprocessing
MAX_EDSUM = 5.0



pdb_str = """\
CRYST1   32.780   32.780  52.500  90.00  90.00  90.00 P 1
ATOM   1647  P     U A  49      27.406  24.111   5.535  1.00 76.93           P
ATOM   1648  OP1   U A  49      26.997  25.010   6.589  1.00 76.93           O
ATOM   1649  OP2   U A  49      28.722  23.341   5.597  1.00 76.93           O
ATOM   1650  O5'   U A  49      26.301  22.971   5.410  1.00 76.93           O
ATOM   1651  C5'   U A  49      24.952  23.291   5.089  1.00 76.93           C
ATOM   1652  C4'   U A  49      24.125  22.037   4.965  1.00 76.93           C
ATOM   1653  O4'   U A  49      24.380  21.382   3.696  1.00 76.93           O
ATOM   1654  C3'   U A  49      24.358  20.949   6.005  1.00 76.93           C
ATOM   1655  O3'   U A  49      23.649  21.266   7.226  1.00 76.93           O
ATOM   1656  C2'   U A  49      23.806  19.741   5.252  1.00 76.93           C
ATOM   1657  O2'   U A  49      22.393  19.854   5.124  1.00 76.93           O
ATOM   1658  C1'   U A  49      24.336  19.992   3.856  1.00 76.93           C
ATOM   1659  N1    U A  49      25.679  19.422   3.665  1.00 76.93           N
ATOM   1660  C2    U A  49      25.744  18.046   3.478  1.00 76.93           C
ATOM   1661  O2    U A  49      24.753  17.328   3.587  1.00 76.93           O
ATOM   1662  N3    U A  49      27.001  17.548   3.176  1.00 76.93           N
ATOM   1663  C4    U A  49      28.165  18.289   3.087  1.00 76.93           C
ATOM   1664  O4    U A  49      29.220  17.736   2.680  1.00 76.93           O
ATOM   1665  C5    U A  49      28.014  19.686   3.390  1.00 76.93           C
ATOM   1666  C6    U A  49      26.815  20.193   3.666  1.00 76.93           C
ATOM   1667  H5'   U A  49      24.917  23.856   4.145  1.00 76.93           H
ATOM   1668 H5''   U A  49      24.528  23.942   5.869  1.00 76.93           H
ATOM   1669  H4'   U A  49      23.104  22.426   5.093  1.00 76.93           H
ATOM   1670  H3'   U A  49      25.393  20.804   6.346  1.00 76.93           H
ATOM   1671  H2'   U A  49      24.064  18.785   5.731  1.00 76.93           H
ATOM   1672 HO2'   U A  49      22.035  20.426   5.862  1.00 76.93           H
ATOM   1673  H1'   U A  49      23.684  19.510   3.113  1.00 76.93           H
ATOM   1674  H3    U A  49      27.074  16.566   3.007  1.00 76.93           H
ATOM   1675  H5    U A  49      28.897  20.342   3.393  1.00 76.93           H
ATOM   1676  H6    U A  49      26.731  21.265   3.902  1.00 76.93           H
ATOM   1677  P     G A  50      24.225  20.753   8.657  1.00 82.98           P
ATOM   1678  OP1   G A  50      23.332  21.317   9.712  1.00 82.98           O
ATOM   1679  OP2   G A  50      25.685  21.008   8.745  1.00 82.98           O
ATOM   1680  O5'   G A  50      24.034  19.201   8.547  1.00 82.98           O
ATOM   1681  C5'   G A  50      22.696  18.676   8.442  1.00 82.98           C
ATOM   1682  C4'   G A  50      22.732  17.193   8.270  1.00 82.98           C
ATOM   1683  O4'   G A  50      23.358  16.863   6.998  1.00 82.98           O
ATOM   1684  C3'   G A  50      23.534  16.415   9.298  1.00 82.98           C
ATOM   1685  O3'   G A  50      22.816  16.235  10.515  1.00 82.98           O
ATOM   1686  C2'   G A  50      23.827  15.134   8.534  1.00 82.98           C
ATOM   1687  O2'   G A  50      22.653  14.358   8.396  1.00 82.98           O
ATOM   1688  C1'   G A  50      24.099  15.661   7.119  1.00 82.98           C
ATOM   1689  N9    G A  50      25.511  15.935   6.825  1.00 82.98           N
ATOM   1690  C8    G A  50      26.158  17.145   6.907  1.00 82.98           C
ATOM   1691  N7    G A  50      27.424  17.067   6.547  1.00 82.98           N
ATOM   1692  C5    G A  50      27.627  15.721   6.241  1.00 82.98           C
ATOM   1693  C6    G A  50      28.820  15.008   5.846  1.00 82.98           C
ATOM   1694  O6    G A  50      29.946  15.450   5.636  1.00 82.98           O
ATOM   1695  N1    G A  50      28.587  13.640   5.703  1.00 82.98           N
ATOM   1696  C2    G A  50      27.361  13.033   5.887  1.00 82.98           C
ATOM   1697  N2    G A  50      27.324  11.702   5.725  1.00 82.98           N
ATOM   1698  N3    G A  50      26.258  13.683   6.212  1.00 82.98           N
ATOM   1699  C4    G A  50      26.462  15.009   6.393  1.00 82.98           C
ATOM   1700  H5'   G A  50      22.180  19.139   7.588  1.00 82.98           H
ATOM   1701 H5''   G A  50      22.121  18.934   9.343  1.00 82.98           H
ATOM   1702  H4'   G A  50      21.674  16.905   8.362  1.00 82.98           H
ATOM   1703  H3'   G A  50      24.449  16.912   9.652  1.00 82.98           H
ATOM   1704  H2'   G A  50      24.615  14.538   9.018  1.00 82.98           H
ATOM   1705 HO2'   G A  50      22.863  13.521   7.891  1.00 82.98           H
ATOM   1706  H1'   G A  50      23.804  14.881   6.402  1.00 82.98           H
ATOM   1707  H8    G A  50      25.673  18.075   7.238  1.00 82.98           H
ATOM   1708  H1    G A  50      29.361  13.060   5.449  1.00 82.98           H
ATOM   1709  H21   G A  50      26.464  11.208   5.846  1.00 82.98           H
ATOM   1710  H22   G A  50      28.157  11.205   5.482  1.00 82.98           H
ATOM   1711  P     U A  51      23.623  16.113  11.930  1.00 85.25           P
ATOM   1712  OP1   U A  51      22.670  16.092  13.089  1.00 85.25           O
ATOM   1713  OP2   U A  51      24.730  17.105  11.982  1.00 85.25           O
ATOM   1714  O5'   U A  51      24.263  14.685  11.748  1.00 85.25           O
ATOM   1715  C5'   U A  51      23.470  13.534  11.662  1.00 85.25           C
ATOM   1716  C4'   U A  51      24.311  12.340  11.340  1.00 85.25           C
ATOM   1717  O4'   U A  51      24.819  12.434   9.975  1.00 85.25           O
ATOM   1718  C3'   U A  51      25.579  12.126  12.166  1.00 85.25           C
ATOM   1719  O3'   U A  51      25.338  11.526  13.439  1.00 85.25           O
ATOM   1720  C2'   U A  51      26.342  11.168  11.253  1.00 85.25           C
ATOM   1721  O2'   U A  51      25.713   9.888  11.297  1.00 85.25           O
ATOM   1722  C1'   U A  51      26.075  11.780   9.876  1.00 85.25           C
ATOM   1723  N1    U A  51      27.123  12.793   9.594  1.00 85.25           N
ATOM   1724  C2    U A  51      28.337  12.327   9.133  1.00 85.25           C
ATOM   1725  O2    U A  51      28.554  11.135   8.986  1.00 85.25           O
ATOM   1726  N3    U A  51      29.292  13.288   8.893  1.00 85.25           N
ATOM   1727  C4    U A  51      29.167  14.622   9.106  1.00 85.25           C
ATOM   1728  O4    U A  51      30.128  15.375   8.847  1.00 85.25           O
ATOM   1729  C5    U A  51      27.876  15.030   9.617  1.00 85.25           C
ATOM   1730  C6    U A  51      26.908  14.101   9.814  1.00 85.25           C
ATOM   1731  H5'   U A  51      22.700  13.668  10.887  1.00 85.25           H
ATOM   1732 H5''   U A  51      22.943  13.373  12.614  1.00 85.25           H
ATOM   1733  H4'   U A  51      23.610  11.517  11.544  1.00 85.25           H
ATOM   1734  H3'   U A  51      26.094  13.064  12.420  1.00 85.25           H
ATOM   1735  H2'   U A  51      27.404  11.043  11.511  1.00 85.25           H
ATOM   1736 HO2'   U A  51      26.205   9.253  10.701  1.00 85.25           H
ATOM   1737  H1'   U A  51      26.080  11.020   9.081  1.00 85.25           H
ATOM   1738  H3    U A  51      30.166  12.971   8.524  1.00 85.25           H
ATOM   1739  H5    U A  51      27.681  16.089   9.846  1.00 85.25           H
ATOM   1740  H6    U A  51      25.918  14.428  10.163  1.00 85.25           H
ATOM   2032  P     A A  61      39.570  12.176   3.592  1.00 89.77           P
ATOM   2033  OP1   A A  61      40.329  11.518   2.509  1.00 89.77           O
ATOM   2034  OP2   A A  61      38.832  13.457   3.268  1.00 89.77           O
ATOM   2035  O5'   A A  61      38.526  11.146   4.238  1.00 89.77           O
ATOM   2036  C5'   A A  61      38.951   9.851   4.761  1.00 89.77           C
ATOM   2037  C4'   A A  61      37.769   9.086   5.318  1.00 89.77           C
ATOM   2038  O4'   A A  61      37.300   9.683   6.562  1.00 89.77           O
ATOM   2039  C3'   A A  61      36.531   9.048   4.438  1.00 89.77           C
ATOM   2040  O3'   A A  61      36.667   8.063   3.429  1.00 89.77           O
ATOM   2041  C2'   A A  61      35.424   8.759   5.453  1.00 89.77           C
ATOM   2042  O2'   A A  61      35.403   7.445   5.972  1.00 89.77           O
ATOM   2043  C1'   A A  61      35.880   9.589   6.645  1.00 89.77           C
ATOM   2044  N9    A A  61      35.296  10.938   6.675  1.00 89.77           N
ATOM   2045  C8    A A  61      35.899  12.100   6.290  1.00 89.77           C
ATOM   2046  N7    A A  61      35.191  13.170   6.519  1.00 89.77           N
ATOM   2047  C5    A A  61      34.009  12.683   7.064  1.00 89.77           C
ATOM   2048  C6    A A  61      32.860  13.329   7.536  1.00 89.77           C
ATOM   2049  N6    A A  61      32.711  14.688   7.568  1.00 89.77           N
ATOM   2050  N1    A A  61      31.859  12.561   7.993  1.00 89.77           N
ATOM   2051  C2    A A  61      32.021  11.233   8.006  1.00 89.77           C
ATOM   2052  N3    A A  61      33.065  10.505   7.618  1.00 89.77           N
ATOM   2053  C4    A A  61      34.047  11.305   7.148  1.00 89.77           C
ATOM   2054  H5'   A A  61      39.705   9.997   5.549  1.00 89.77           H
ATOM   2055 H5''   A A  61      39.429   9.266   3.962  1.00 89.77           H
ATOM   2056  H4'   A A  61      38.175   8.069   5.425  1.00 89.77           H
ATOM   2057  H3'   A A  61      36.332   9.968   3.869  1.00 89.77           H
ATOM   2058  H2'   A A  61      34.448   8.950   4.981  1.00 89.77           H
ATOM   2059 HO2'   A A  61      34.650   7.356   6.623  1.00 89.77           H
ATOM   2060  H1'   A A  61      35.542   9.095   7.567  1.00 89.77           H
ATOM   2061  H8    A A  61      36.896  12.131   5.826  1.00 89.77           H
ATOM   2062  H61   A A  61      31.864  15.089   7.917  1.00 89.77           H
ATOM   2063  H62   A A  61      33.448  15.280   7.242  1.00 89.77           H
ATOM   2064  H2    A A  61      31.167  10.659   8.395  1.00 89.77           H
ATOM   2065  P     C A  62      36.028   8.299   1.969  1.00 89.92           P
ATOM   2066  OP1   C A  62      36.477   7.125   1.192  1.00 89.92           O
ATOM   2067  OP2   C A  62      36.337   9.663   1.457  1.00 89.92           O
ATOM   2068  O5'   C A  62      34.471   8.094   2.211  1.00 89.92           O
ATOM   2069  C5'   C A  62      34.000   6.810   2.666  1.00 89.92           C
ATOM   2070  C4'   C A  62      32.568   6.874   3.135  1.00 89.92           C
ATOM   2071  O4'   C A  62      32.459   7.633   4.366  1.00 89.92           O
ATOM   2072  C3'   C A  62      31.572   7.510   2.187  1.00 89.92           C
ATOM   2073  O3'   C A  62      31.152   6.586   1.167  1.00 89.92           O
ATOM   2074  C2'   C A  62      30.447   7.894   3.133  1.00 89.92           C
ATOM   2075  O2'   C A  62      29.658   6.751   3.487  1.00 89.92           O
ATOM   2076  C1'   C A  62      31.221   8.337   4.375  1.00 89.92           C
ATOM   2077  N1    C A  62      31.494   9.809   4.401  1.00 89.92           N
ATOM   2078  C2    C A  62      30.542  10.640   4.977  1.00 89.92           C
ATOM   2079  O2    C A  62      29.538  10.107   5.445  1.00 89.92           O
ATOM   2080  N3    C A  62      30.738  12.001   4.993  1.00 89.92           N
ATOM   2081  C4    C A  62      31.863  12.517   4.430  1.00 89.92           C
ATOM   2082  N4    C A  62      32.033  13.862   4.422  1.00 89.92           N
ATOM   2083  C5    C A  62      32.857  11.686   3.851  1.00 89.92           C
ATOM   2084  C6    C A  62      32.640  10.342   3.865  1.00 89.92           C
ATOM   2085  H5'   C A  62      34.639   6.455   3.488  1.00 89.92           H
ATOM   2086 H5''   C A  62      34.086   6.077   1.851  1.00 89.92           H
ATOM   2087  H4'   C A  62      32.312   5.809   3.239  1.00 89.92           H
ATOM   2088  H3'   C A  62      31.967   8.365   1.619  1.00 89.92           H
ATOM   2089  H2'   C A  62      29.763   8.641   2.703  1.00 89.92           H
ATOM   2090 HO2'   C A  62      28.928   7.030   4.110  1.00 89.92           H
ATOM   2091  H1'   C A  62      30.614   8.116   5.265  1.00 89.92           H
ATOM   2092  H41   C A  62      32.854  14.261   4.013  1.00 89.92           H
ATOM   2093  H42   C A  62      31.337  14.457   4.826  1.00 89.92           H
ATOM   2094  H5    C A  62      33.768  12.113   3.407  1.00 89.92           H
ATOM   2095  H6    C A  62      33.399   9.670   3.438  1.00 89.92           H
ATOM   2096  P     A A  63      30.677   7.143  -0.262  1.00 76.34           P
ATOM   2097  OP1   A A  63      30.400   5.961  -1.126  1.00 76.34           O
ATOM   2098  OP2   A A  63      31.651   8.166  -0.742  1.00 76.34           O
ATOM   2099  O5'   A A  63      29.288   7.820   0.094  1.00 76.34           O
ATOM   2100  C5'   A A  63      28.194   6.996   0.512  1.00 76.34           C
ATOM   2101  C4'   A A  63      26.983   7.826   0.798  1.00 76.34           C
ATOM   2102  O4'   A A  63      27.198   8.520   2.056  1.00 76.34           O
ATOM   2103  C3'   A A  63      26.628   8.935  -0.197  1.00 76.34           C
ATOM   2104  O3'   A A  63      26.012   8.517  -1.467  1.00 76.34           O
ATOM   2105  C2'   A A  63      25.763   9.826   0.697  1.00 76.34           C
ATOM   2106  O2'   A A  63      24.495   9.267   0.953  1.00 76.34           O
ATOM   2107  C1'   A A  63      26.527   9.779   2.025  1.00 76.34           C
ATOM   2108  N9    A A  63      27.514  10.873   2.035  1.00 76.34           N
ATOM   2109  C8    A A  63      28.833  10.828   1.660  1.00 76.34           C
ATOM   2110  N7    A A  63      29.431  11.992   1.689  1.00 76.34           N
ATOM   2111  C5    A A  63      28.441  12.855   2.152  1.00 76.34           C
ATOM   2112  C6    A A  63      28.448  14.224   2.407  1.00 76.34           C
ATOM   2113  N6    A A  63      29.535  14.968   2.281  1.00 76.34           N
ATOM   2114  N1    A A  63      27.293  14.800   2.803  1.00 76.34           N
ATOM   2115  C2    A A  63      26.211  14.024   2.954  1.00 76.34           C
ATOM   2116  N3    A A  63      26.086  12.715   2.774  1.00 76.34           N
ATOM   2117  C4    A A  63      27.260  12.185   2.362  1.00 76.34           C
ATOM   2118  H5'   A A  63      28.478   6.431   1.412  1.00 76.34           H
ATOM   2119 H5''   A A  63      27.963   6.259  -0.271  1.00 76.34           H
ATOM   2120  H4'   A A  63      26.161   7.095   0.770  1.00 76.34           H
ATOM   2121  H3'   A A  63      27.509   9.432  -0.631  1.00 76.34           H
ATOM   2122  H2'   A A  63      25.603  10.816   0.242  1.00 76.34           H
ATOM   2123 HO2'   A A  63      23.971   9.885   1.541  1.00 76.34           H
ATOM   2124  H1'   A A  63      25.861   9.893   2.894  1.00 76.34           H
ATOM   2125  H8    A A  63      29.342   9.898   1.363  1.00 76.34           H
ATOM   2126  H61   A A  63      29.495  15.949   2.474  1.00 76.34           H
ATOM   2127  H62   A A  63      30.397  14.549   1.992  1.00 76.34           H
ATOM   2128  H2    A A  63      25.296  14.546   3.272  1.00 76.34           H
TER
"""

# -----------------------------------------------------------------------------

def get_pdb_inputs(pdb_str):
    raw_records = flex.std_string(pdb_str.splitlines())

    pdb_inp = pdb_input(source_info=None, lines=raw_records)
    pdb_hierarchy = pdb_inp.construct_hierarchy()
    symm = pdb_inp.crystal_symmetry()
    xray_structure = pdb_hierarchy.extract_xray_structure(symm)

    return group_args(ph  = pdb_hierarchy, xrs = xray_structure)


# -----------------------------------------------------------------------------


def basic_test(d_min = 3.0, resolution_factor = 0.2):

    pi = get_pdb_inputs(pdb_str=pdb_str)

    f_calc = pi.xrs.structure_factors(d_min = d_min).f_calc()
    fft_map = f_calc.fft_map(resolution_factor = resolution_factor, \
                                symmetry_flags = maptbx.use_space_group_symmetry)

    fft_map.apply_sigma_scaling()

    ps_obj = po4finder(fft_map)
    ps_obj.peaksearch(max_number_of_peaks = 10000)

    peaks_as_pdb = ps_obj.peaks_as_pdb(only_selected=False)
    data = ps_obj.peak_params

    p_data, nop_data = find_hits(pi, peaks_as_pdb, data, ps_obj)

    for fname, data in (["./tmp/pdata.dat", p_data], ["./tmp/nopdata.dat", nop_data]):
        ofile = open(fname, "w")
        for item in data:
            ofile.write("%s\n" % '\t'.join(map(str, item)))
        ofile.close()

    target_xcmp = CompareXModels(target=[pi.ph, pi.xrs.crystal_symmetry()])
    target_xcmp.compare_nuc_positions(pattern=[peaks_as_pdb, pi.xrs.crystal_symmetry()], \
                                    P_only=True, extended=False, verbose=False, P_dist=1.0)#verbose)


    print "TPR = ", float(len(target_xcmp.resi_pairs))/float(peaks_as_pdb.atoms().size())



def calc_scores(peaks_as_pdb, target_ph, symm):


    target_xcmp = CompareXModels(target=[target_ph, symm])
    target_xcmp.compare_nuc_positions(pattern=[peaks_as_pdb, symm], \
                                    P_only=True, extended=False, verbose=False, P_dist=1.0)#verbose)
    isel_cache = target_ph.atom_selection_cache()
    isel = isel_cache.iselection

    print "n_peaks = ", peaks_as_pdb.atoms().size()
    print "TPR = ", float(len(target_xcmp.resi_pairs))/float(peaks_as_pdb.atoms().size())
    print "COV = ", float(len(target_xcmp.resi_pairs))/float(target_ph.select(isel(r"name P and (altloc ' ' or altloc 'a')")).atoms().size())


# -----------------------------------------------------------------------------






# -----------------------------------------------------------------------------


def get_peaks(pdbid="1ehz"):

    from iotbx import reflection_file_utils
    from iotbx import reflection_file_reader


    structure = urllib2.urlopen("http://www.pdb.org/pdb/files/%s.pdb" % pdbid)
    pi = get_pdb_inputs(pdb_str=structure.read())

    if 1:
        rs_cc = RealSpaceCorrelations(tmp_dir="./tmp/")
        #rs_cc.init_from_rnadb([pdbid])
        #rs_cc.calc_map( d_min=d_min, verbose=False, fudge_factor=0.0 )
        #fft_map = rs_cc.fft_map_xray
        rs_cc.init_from_pdbredo([pdbid], malibu=False)
        mtz_fname = rs_cc.mtz_filename

        reflection_file = reflection_file_reader.any_reflection_file( file_name = mtz_fname)
        symm = reflection_file.file_content().crystals()[0].crystal_symmetry()

        xray_data_server =  reflection_file_utils.reflection_file_server(
                                                    crystal_symmetry = symm,
                                                    force_symmetry = True,
                                                    reflection_files=[reflection_file])

        map_coeffs = xray_data_server.get_xray_data(
                                        file_name = mtz_fname, \
                                        labels = ["FWT", "PHWT"], \
                                        ignore_all_zeros = True, \
                                        parameter_scope="xray_data") 



        fft_map = map_coeffs.fft_map(resolution_factor = 1/3.0)
        fft_map.apply_sigma_scaling()



    else:
        resolution_factor = 1./3.
        inp_ccp4_map = ccp4_map.map_reader(file_name="./tmp/%s.ccp4"%pdbid)
        fft_map = inp_ccp4_map



    # -------------------------------------------------------------------------


    ps_obj = po4finder(fft_map, pi.xrs.crystal_symmetry())
    ps_obj.peaksearch(max_number_of_peaks = 10000)

    peaks_as_pdb = ps_obj.peaks_as_pdb(only_selected=False)
    data = ps_obj.peak_params
    p_data, nop_data = ps_obj.find_hits(pi.ph, pi.xrs, peaks_as_pdb, ps_obj.peak_params)

    ofile = open("./tmp/scores/%s_data.dat"%pdbid, "w")
    for item in p_data:
        ofile.write("%f\t%f\t%f\t1\n" % tuple(item) )
    for item in nop_data:
        ofile.write("%f\t%f\t%f\t0\n" % tuple(item) )

    ofile.close()

    '''
    for fname, data in (["./tmp/%s_pdata.dat"%pdbid, p_data], ["./tmp/%s_nopdata.dat"%pdbid, nop_data]):
        ofile = open(fname, "w")
        for item in data:
            ofile.write("%s\n" % '\t'.join(map(str, item)))
        ofile.close()
    '''

    #peaks_as_pdb = ps_obj.peaks_as_pdb(only_selected=True)
    #calc_scores(peaks_as_pdb, pi.ph, pi.xrs.crystal_symmetry())

# -----------------------------------------------------------------------------


def test_sklearn():
    """
        most basic test of the test_sklearn engine
    """
    X = [[0, 0], [1, 1]]
    y = [1, 0]
    clf = svm.SVC()
    clf.fit(X, y)

    assert clf.predict([[2., 2.]])[0] == 0


# -----------------------------------------------------------------------------


def train_n_test(pdbid="1ehz"):

    rs_cc = RealSpaceCorrelations(tmp_dir="./tmp/")
    rs_cc.init_from_rnadb([pdbid])
    rs_cc.calc_map( d_min=0.0, verbose=False, fudge_factor=0.0 )
    fft_map = rs_cc.fft_map_xray


    ps_obj = po4finder(fft_map, rs_cc.symm)
    ps_obj.SVM_test_training(pdbid=pdbid)

    ps_obj.peaksearch(max_number_of_peaks = 10000)

    peaks_as_pdb = ps_obj.peaks_as_pdb(only_selected=True)
    calc_scores(peaks_as_pdb, rs_cc.complete_pdb_hierarchy, rs_cc.symm)


# -----------------------------------------------------------------------------

def get_scores(testfname = "../data/lowres_cplx_23062014_valid.txt", scores_dir=["../data/scores_redo", "./tmp/scores"][0]):

    p_data = []
    nop_data = []
    min_max_scaler = preprocessing.MinMaxScaler()

    for line in open(testfname, "r").readlines()[:100]:
        #pdbid = line.split("|")[4].strip()
        pdbid = line.strip()
        p_data_tmp = []
        nop_data_tmp = []

        try:
            for dataline in open("%s/%s_data.dat" % (scores_dir, pdbid), "r").readlines():
                if string.atoi(dataline.split()[-1])==1:
                    p_data_tmp.append( map(string.atof, dataline.split()[:3]) )
                else:
                    nop_data_tmp.append( map(string.atof, dataline.split()[:3]) )
        except:
            print "Datafile missing: ./data/scores/%s_data.dat" % pdbid
            continue

        data_tmp = p_data_tmp + nop_data_tmp
        # XXX SCALE HARD-LIMIT
        #for idata in data_tmp:
        #    if idata[0]>MAX_EDSUM: idata[0]=MAX_EDSUM

        for i,item in enumerate(sorted(data_tmp, key=lambda x: x[0], reverse=True)):
            item[0] = float(i+1)#+random.randint(1, 200)
            #item[0] = random.randint(1, 200)
            #item[1] = random.randint(1, 200)
            #item[2] = random.randint(1, 200)
            pass

        min_max_scaler.fit_transform(data_tmp)
        if p_data_tmp:
            p_data_tmp = min_max_scaler.transform(p_data_tmp)
        else:
            print "FOOBAR: ", pdbid
        if nop_data_tmp:
            nop_data_tmp = min_max_scaler.transform(nop_data_tmp)
        else:
            print "FOOBAR: ", pdbid

        p_data.extend(p_data_tmp)
        nop_data.extend(nop_data_tmp)


    return p_data, nop_data


def cross_fit(model_fname="../data/svm_model.dat"):
    from sklearn.grid_search import GridSearchCV
    from sklearn.metrics import accuracy_score, precision_score, recall_score, make_scorer

    from sklearn.cross_validation import train_test_split
    from sklearn.metrics import classification_report


    #scoring_func = make_scorer(accuracy_score, normalize=True)
    scoring_func = make_scorer(precision_score, pos_label=1, average='weighted')
    #scoring_func = make_scorer(recall_score, pos_label=1, average='weighted')


    p_data, nop_data = get_scores()

    # XXX
    nop_data = random.sample(nop_data, 2*len(p_data))


    class_data = [1]*len(p_data) + [0]*len(nop_data)
    data = p_data + nop_data

    data_train, data_test, class_data_train, class_data_test = train_test_split( data, class_data, test_size=0.5, random_state=0)
    weights = {1:1.0, 0:float(len(p_data))/float(len(nop_data))}

    print "0/1 count   : ", list(class_data_train).count(0), list(class_data_train).count(1)
    print "0/1 weights :  %.2f %.2f" % (weights[0], weights[1])
    #weights = [1.0]*len(p_data) + [len(p_data)/len(nop_data)]*len(nop_data)


    #clf = svm.SVC(gamma=1.0, C=200.)

    param_grid = [ {'C': [1, 2, 5, 10, 100, 1000, 5000], 'gamma': [1.0, 0.1, 0.01, 0.005, 0.001, 0.0001], 'kernel': ['rbf']} ]
    #param_grid = [ {'C': [1, 2, 3, 4], 'gamma': [0.1, 0.001, 0.005], 'kernel': ['rbf']} ]

    #print help(GridSearchCV)
    #clf = GridSearchCV(svm.SVC(C=1, class_weight=weights, probability=False), param_grid, cv=5, n_jobs=10, \
    #                                scoring = scoring_func)
    clf = GridSearchCV(svm.SVC(class_weight=weights, probability=False), param_grid, cv=5, n_jobs=20, \
                                    scoring = scoring_func)
    #                                scoring=['accuracy', 'precision', 'recall'][0])

    clf.fit(data_train, class_data_train)

    for params, mean_score, scores in clf.grid_scores_:
        print("%0.3f (+/-%0.03f) for %r" % (mean_score, scores.std() / 2, params))
    #scores = cross_validation.cross_val_score(clf, data_scaled, class_data, cv=5)
    print clf.best_estimator_
    print clf.best_estimator_.n_support_

    #print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

    class_true, class_pred = class_data_test, clf.predict(data_test)
    print(classification_report(class_true, class_pred))

    s = pickle.dumps(clf)
    open(model_fname, "w").write(s)


    # prepare a simpel dict with the SVM model params
    gamma = clf.best_estimator_.get_params()['gamma']
    support_vectors = clf.best_estimator_.support_vectors_
    sv_coef = clf.best_estimator_.dual_coef_[0]
    n_support = clf.best_estimator_.n_support_
    rho = clf.best_estimator_.intercept_[0]

    svm_data_dict = {}
    svm_data_dict['gamma'] = gamma
    svm_data_dict['support_vectors'] = support_vectors.tolist()
    svm_data_dict['rho'] = rho
    svm_data_dict['n_support'] = n_support.tolist()
    svm_data_dict['sv_coef'] = sv_coef.tolist()
    gzip.open("../data/data.svm.gz", "w").write(json.dumps(svm_data_dict))



















# -----------------------------------------------------------------------------


if __name__=="__main__":
    cross_fit()

    if 0:
        basic_test()
        test_sklearn()
        train_n_test(pdbid=sys.argv[1])

        get_peaks(pdbid=sys.argv[1])



