#!/bin/sh


ABSPATH=$(cd "$(dirname "$0")"; pwd)
. $ABSPATH/../virtualenv/cctbx_build/setpaths.sh

libtbx.python $ABSPATH/compare_xmodels.py $@
