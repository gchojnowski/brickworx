#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Pawel Piątkowski - ppiatkowski@genesilico.pl
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl
# =============================================================================


import os
from string import maketrans
from brickworx_config import ROOT
DATA_PATH = os.path.join(ROOT, "./data/iso.csv")


IDI_ISOSTERIC = 2.0
IDI_NEAR_ISOSTERIC = 3.3
IDI_NON_ISOSTERIC = 5.0

LETTERS = "ACGU"
TYPES = "RYRY"


class isostericity:

    def __init__(self, fname=DATA_PATH):
        self.table = {}
        with open(fname, "rb") as f:
            buf = []
            for line in f:
                if line=="\n":
                    self.table[buf[0][0]] = buf[1:]
                    buf = []
                else:
                    l = line.strip().split(",")
                    buf.append(l)


    def reverse_pair(self, pair):
        return pair[1] + pair[0]


    def reverse_geom(self, geom):
        return geom[0] + geom[2] + geom[1]


    def get_iso_pairs(self, pair="AU", geom="cWW", IDI=2.0):
        pair_geom = "%s %s" % (pair, geom)
        if not self.table.has_key(pair_geom):
            pair_geom = "%s %s" % \
                (self.reverse_pair(pair), self.reverse_geom(geom))
            reversed_geom = True
        else:
            reversed_geom = False
        pairs = []
        for i in range(4):
            for j in range(4):
                try:
                    v = float(self.table[pair_geom][i][j+1])
                    if v>0 and v<=IDI:
                        if reversed_geom:
                            pairs.append(LETTERS[j] + LETTERS[i])
                        else:
                            pairs.append(LETTERS[i] + LETTERS[j])
                except:
                    pass
        return pairs


    def get_base_types(self, pair="AU", geom="cWW", IDI=2.0):
        # ugly hack, this lib doeasn't accept standard geometry descrs (WW_cis)
        g = geom.split("_")
        if len(g)>1: geom = ("c" if g[1]=="cis" else "t")+g[0]

        self.pairs = self.get_iso_pairs(pair, geom, IDI)
        trans = maketrans(LETTERS, TYPES)
        base_types = []
        for pair in self.pairs:
            base_type = pair.translate(trans)
            if base_type not in base_types:
                base_types.append(base_type)
        return base_types


if __name__=="__main__":
    from sys import argv
    from os import path
    path_to_script = path.dirname(path.realpath(__file__))
    fname = path.join(path_to_script, "..", "./data/iso.csv")
    iso = isostericity(fname)

    if len(argv)<4:
        exit("USAGE: iso.py <pair> <type> <IDI>\n  ex.: iso.py AU cWW 2.0")
    pairs = iso.get_iso_pairs(pair=argv[1], geom=argv[2], IDI=float(argv[3]))
    types = iso.get_base_types(pair=argv[1], geom=argv[2], IDI=float(argv[3]))
    print pairs
    print types

