#!/bin/bash


if [ $(hostname) = 'peyote2' ] || [[ $(hostname -s) =~ ^c[0-9]{1,2} ]] ;  then
    export CLIBD_MON=/home/gchojnowski/xray/phenix/phenix-1.8-1069/chem_data/mon_lib/
    export MMTBX_CCP4_MONOMER_LIB=/home/gchojnowski/xray/phenix/phenix-1.8-1069/chem_data/mon_lib/
fi



if [ ! -d virtualenv ] ; then
  ./_create_virtualenv
  ./_install_cctbx
fi

. virtualenv/bin/activate
. virtualenv/cctbx_build/setpaths.sh


libtbx.python ./grow.py $@
